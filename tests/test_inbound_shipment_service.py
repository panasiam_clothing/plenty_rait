# pylint: disable=unused-variable
# pylint: disable=redefined-outer-name
# pylint: disable=missing-function-docstring
from freezegun import freeze_time
import pandas
from pandas.testing import assert_frame_equal
import pytest
from numpy import int8 as i8

from plenty_rait.inbound_shipment.service import (
    InboundPlanCreator, PackageListFiller, InsufficientPackagesError,
    DuplicateOrderItemsError
)


@pytest.fixture
def sample_redistribution():
    return {
            'amounts': [{'currency': 'EUR', 'grossTotal': 137.9,
                         'id': 45161, 'invoiceTotal': 137.9, 'isNet': True,
                         'isSystemCurrency': True, 'netTotal': 137.9,
                         'orderId': 12345
                         }],
            'dates': [{'date': '1999-01-01T01:00:00+02:00', 'orderId': 12345,
                       'typeId': 16},
                      {'date': '1999-01-01T01:00:00+02:00', 'orderId': 12345,
                       'typeId': 2},
                      {'date': '1999-01-01T01:00:00+02:00', 'orderId': 12345,
                       'typeId': 4}],
            'id': 12345, 'locationId': None, 'lockStatus': 'unlocked',
            'orderItems': [
                {'amounts': [{'currency': 'EUR', 'id': 88680,
                              'isPercentage': True, 'isSystemCurrency': True,
                              'orderItemId': 99998, 'priceGross': 19.7,
                              'priceNet': 19.7, 'priceOriginalGross': 19.7,
                              'priceOriginalNet': 19.7,
                              'updatedAt': '1999-01-01T01:00:00+02:00'}],
                 'attributeValues': 'Blue',
                 'id': 98765, 'itemVariationId': 1111, 'orderId': 12345,
                 'orderItemName': 'test_item_1', 'quantity': 4,
                 'transactions': [{'direction': 'in', 'id': 272,
                                   'orderItemId': 99998, 'quantity': 2,
                                   'receiptId': None,
                                    'userId': 1, 'warehouseLocationId': 234},
                                  {'direction': 'out', 'id': 269,
                                   'orderItemId': 99998, 'quantity': 2,
                                   'receiptId': 33334,
                                   'userId': 1, 'warehouseLocationId': 123}],
                 'typeId': 1},
                {'amounts': [{'currency': 'EUR', 'id': 88681,
                              'isPercentage': True, 'isSystemCurrency': True,
                              'orderItemId': 99999, 'priceGross': 20.7,
                              'priceNet': 20.7, 'priceOriginalGross': 20.7,
                              'priceOriginalNet': 20.7,
                              'updatedAt': '1999-01-01T01:00:00+02:00'}],
                 'attributeValues': 'Red',
                 'id': 98766, 'itemVariationId': 2222, 'orderId': 12345,
                 'orderItemName': 'test_item_2', 'quantity': 3,
                 'transactions': [{'direction': 'out', 'id': 269,
                                   'orderItemId': 99999, 'quantity': 3,
                                   'receiptId': None,
                                   'userId': 1, 'warehouseLocationId': 456}],
                 'typeId': 1}],
            'orderReferences': [], 'ownerId': '1', 'plentyId': 12345,
            'properties': [{'orderId': 12345, 'typeId': 6, 'value': 'de'},
                           {'orderId': 12345, 'typeId': 4, 'value': 'unpaid'}],
            'referrerId': 0,
            'relations': [{'orderId': 12345, 'referenceId': 101,
                           'referenceType': 'warehouse', 'relation': 'sender'},
                          {'orderId': 12345, 'referenceId': 102,
                           'referenceType': 'warehouse',
                           'relation': 'receiver'}
                          ],
            'shippingPackages': {'content': {
                1111: {'packages': {123: {11: {'packageNo': 1, 'quantity': 2}},
                                    234: {13: {'packageNo': 1, 'quantity': 2}}},
                       'totalQuantity': 4},
                2222: {'packages': {123: {12: {'packageNo': 2, 'quantity': 3}}},
                       'totalQuantity': 3},
            }, 'pallets': {123: [11, 12], 234: [13]}},
            'statusId': 18.1, 'statusName': 'test status', 'typeId': 15
        }


@pytest.fixture
def sku_mapping():
    return {
        1111: 'test_sku_1',
        2222: 'test_sku_2'
    }


@freeze_time('2021-10-29')
def test_build_header_section():
    config = {
        'name': 'test name',
        'street': 'test street nr. 12',
        'city': 'test city',
        'country_code': 'DE',
        'state': 'Bayern',
        'postal_code': '12345'
    }

    creator = InboundPlanCreator(sender_config=config, receiver_country='US')
    expectation = [
        ['PlanName', 'Shipment_2021-10-29'],
        ['ShipToCountry', 'US'],
        ['AddressName', 'test name'],
        ['AddressFieldOne', 'test street nr. 12'],
        ['AddressFieldTwo', ''],
        ['AddressCity', 'test city'],
        ['AddressCountryCode', 'DE'],
        ['AddressStateOrRegion', 'Bayern'],
        ['AddressPostalCode', '12345'],
        ['AddressDistrict', ''],
        ['', ''],
        ['MerchantSKU', 'Quantity'],
        ['', '']
    ]

    assert creator.header_section == expectation


def describe_create_inbound_plan():
    @freeze_time('2021-10-29')
    def with_valid_redistribution(sample_redistribution, sku_mapping):
        config = {
            'name': 'test name',
            'street': 'test street nr. 12',
            'city': 'test city',
            'country_code': 'DE',
            'state': 'Bayern',
            'postal_code': '12345'
        }
        creator = InboundPlanCreator(
            sender_config=config, receiver_country='US'
        )
        expectation = pandas.DataFrame(
            [
                ['PlanName', 'Shipment_2021-10-29'],
                ['ShipToCountry', 'US'],
                ['AddressName', 'test name'],
                ['AddressFieldOne', 'test street nr. 12'],
                ['AddressFieldTwo', ''],
                ['AddressCity', 'test city'],
                ['AddressCountryCode', 'DE'],
                ['AddressStateOrRegion', 'Bayern'],
                ['AddressPostalCode', '12345'],
                ['AddressDistrict', ''],
                ['', ''],
                ['MerchantSKU', 'Quantity'],
                ['', ''],
                ['test_sku_1', 4],
                ['test_sku_2', 3]
            ], columns=['MerchantSKU', 'Quantity']
        )
        result = creator.create_inbound_plan(
            order_response=sample_redistribution, sku_mapping=sku_mapping
        )

        assert_frame_equal(result, expectation)

    @freeze_time('2021-10-29')
    def with_zero_quantity_item(sample_redistribution, sku_mapping):
        sample_redistribution['orderItems'].append(
            {'amounts': [{'currency': 'EUR', 'id': 88682,
                          'isPercentage': True, 'isSystemCurrency': True,
                          'orderItemId': 99988, 'priceGross': 19.7,
                          'priceNet': 19.7, 'priceOriginalGross': 19.7,
                          'priceOriginalNet': 19.7,
                          'updatedAt': '1999-01-01T01:00:00+02:00'}],
             'attributeValues': 'Blue',
             'id': 98767, 'itemVariationId': 3333, 'orderId': 12345,
             'orderItemName': 'test_item_1', 'quantity': 0,
             'transactions': [],
             'typeId': 1}
        )
        config = {
            'name': 'test name',
            'street': 'test street nr. 12',
            'city': 'test city',
            'country_code': 'DE',
            'state': 'Bayern',
            'postal_code': '12345'
        }
        creator = InboundPlanCreator(
            sender_config=config, receiver_country='US'
        )
        expectation = pandas.DataFrame(
            [
                ['PlanName', 'Shipment_2021-10-29'],
                ['ShipToCountry', 'US'],
                ['AddressName', 'test name'],
                ['AddressFieldOne', 'test street nr. 12'],
                ['AddressFieldTwo', ''],
                ['AddressCity', 'test city'],
                ['AddressCountryCode', 'DE'],
                ['AddressStateOrRegion', 'Bayern'],
                ['AddressPostalCode', '12345'],
                ['AddressDistrict', ''],
                ['', ''],
                ['MerchantSKU', 'Quantity'],
                ['', ''],
                ['test_sku_1', 4],
                ['test_sku_2', 3]
            ], columns=['MerchantSKU', 'Quantity']
        )
        result = creator.create_inbound_plan(
            order_response=sample_redistribution, sku_mapping=sku_mapping
        )

        assert_frame_equal(result, expectation)

    @freeze_time('2021-10-29')
    def with_duplicate_items(sample_redistribution, sku_mapping):
        sample_redistribution['orderItems'].append(
            {'amounts': [{'currency': 'EUR', 'id': 88680,
                          'isPercentage': True, 'isSystemCurrency': True,
                          'orderItemId': 99998, 'priceGross': 19.7,
                          'priceNet': 19.7, 'priceOriginalGross': 19.7,
                          'priceOriginalNet': 19.7,
                          'updatedAt': '1999-01-01T01:00:00+02:00'}],
             'attributeValues': 'Blue',
             'id': 98765, 'itemVariationId': 1111, 'orderId': 12345,
             'orderItemName': 'test_item_1', 'quantity': 6,
             'transactions': [{'direction': 'in', 'id': 272,
                               'orderItemId': 99998, 'quantity': 2,
                               'receiptId': None,
                               'userId': 1, 'warehouseLocationId': 234},
                              {'direction': 'out', 'id': 269,
                               'orderItemId': 99998, 'quantity': 2,
                               'receiptId': 33334, 'userId': 1,
                               'warehouseLocationId': 123}],
             'typeId': 1}
        )
        config = {
            'name': 'test name',
            'street': 'test street nr. 12',
            'city': 'test city',
            'country_code': 'DE',
            'state': 'Bayern',
            'postal_code': '12345'
        }
        creator = InboundPlanCreator(
            sender_config=config, receiver_country='US'
        )
        with pytest.raises(DuplicateOrderItemsError) as exc_info:
            creator.create_inbound_plan(
                order_response=sample_redistribution, sku_mapping=sku_mapping
            )
        result_exception_df = exc_info.value.__dict__['error_df']
        expected_exception_df = pandas.DataFrame(
            [['test_sku_1', 4], ['test_sku_1', 6]],
            columns=['MerchantSKU', 'Quantity'], index=[13, 15],
            dtype=object
        )
        assert_frame_equal(result_exception_df, expected_exception_df)


def describe_get_package_assignment():
    def with_no_packages(sample_redistribution, sku_mapping):
        sample_redistribution['shippingPackages'] = {}
        filler = PackageListFiller()
        expectation = pandas.DataFrame()
        result = filler.get_package_assignment(
            order_response=sample_redistribution, sku_mapping=sku_mapping
        )
        assert_frame_equal(result, expectation)

    def with_one_pallet(sample_redistribution, sku_mapping):
        sample_redistribution['shippingPackages'] = {
            'content': {
                1111: {'packages': {
                    123: {11: {'packageNo': 1, 'quantity': 2},
                          13: {'packageNo': 3, 'quantity': 2}},
                }, 'totalQuantity': 4},
                2222: {'packages': {
                    123: {12: {'packageNo': 2, 'quantity': 3}}},
                    'totalQuantity': 3
                },
            },
            'pallets': {123: [11, 12, 13]}
        }
        filler = PackageListFiller()
        expectation = pandas.DataFrame(
            [
                ['test_sku_1', 2, 1],
                ['test_sku_2', 3, 2],
                ['test_sku_1', 2, 3]
            ], columns=['sku', 'quantity', 'packageNo']
        )
        result = filler.get_package_assignment(
            order_response=sample_redistribution, sku_mapping=sku_mapping
        )
        assert_frame_equal(result, expectation)

    def with_two_pallets(sample_redistribution, sku_mapping):
        filler = PackageListFiller()
        expectation = pandas.DataFrame(
            [
                ['test_sku_1', 2, 1],
                ['test_sku_2', 3, 2],
                ['test_sku_1', 2, 3]
            ], columns=['sku', 'quantity', 'packageNo']
        )
        result = filler.get_package_assignment(
            order_response=sample_redistribution, sku_mapping=sku_mapping
        )
        assert_frame_equal(result, expectation)


@pytest.fixture
def sample_package_list():
    return pandas.DataFrame(
        [
            ['1234', 'test_product_1', 'TESTASIN01', 'X001234567',
             '1234567891011', 'New', 'Seller', 'Foil packaging', '--', 10,
             '', 'Not Needed', '', 'Not Needed', '', 'Not Needed', '',
             'Not Needed'],
            ['2345', 'test_product_2', 'TESTASIN02', 'X001234568',
             '1234567891012', 'New', 'Seller', 'Foil packaging', '--', 20,
             '', 'Not Needed', '', 'Not Needed', '', 'Not Needed', '',
             'Not Needed'],
            ['3456', 'test_product_3', 'TESTASIN03', 'X001234569',
             '1234567891013', 'New', 'Seller', 'Foil packaging', '--', 30,
             '', 'Not Needed', '', 'Not Needed', '', 'Not Needed', '',
             'Not Needed'],
            ['4567', 'test_product_4', 'TESTASIN04', 'X001234570',
             '1234567891014', 'New', 'Seller', 'Foil packaging', '--', 40,
             '', 'Not Needed', '', 'Not Needed', '', 'Not Needed', '',
             'Not Needed']
        ],
        columns=['Merchant SKU', 'Title', 'ASIN', 'FNSKU', 'external-id',
                 'Condition', 'Who Will Prep?', 'Prep Type', 'Who Will Label?',
                 'Shipped', 'FBA12AB1QCDJU001 - Unit Quantity',
                 'FBA12AB1QCDJU001 Expiration Date (dd.mm.yy)',
                 'FBA12AB1QCDJU002 - Unit Quantity',
                 'FBA12AB1QCDJU002 Expiration Date (dd.mm.yy)',
                 'FBA12AB1QCDJU003 - Unit Quantity',
                 'FBA12AB1QCDJU003 Expiration Date (dd.mm.yy)',
                 'FBA12AB1QCDJU004 - Unit Quantity',
                 'FBA12AB1QCDJU004 Expiration Date (dd.mm.yy)']
    )


@pytest.fixture
def sample_package_list_filled():
    return pandas.DataFrame(
        [
            ['1234', 'test_product_1', 'TESTASIN01', 'X001234567',
             '1234567891011', 'New', 'Seller', 'Foil packaging', '--',
             i8(10), i8(5), 'Not Needed', i8(5), 'Not Needed', i8(0),
             'Not Needed', i8(0), 'Not Needed'],
            ['2345', 'test_product_2', 'TESTASIN02', 'X001234568',
             '1234567891012', 'New', 'Seller', 'Foil packaging', '--',
             i8(20), i8(5), 'Not Needed', i8(0), 'Not Needed', i8(10),
             'Not Needed', i8(5), 'Not Needed'],
            ['3456', 'test_product_3', 'TESTASIN03', 'X001234569',
             '1234567891013', 'New', 'Seller', 'Foil packaging', '--',
             i8(30), i8(0), 'Not Needed', i8(15), 'Not Needed', i8(0),
             'Not Needed', i8(15), 'Not Needed'],
            ['4567', 'test_product_4', 'TESTASIN04', 'X001234570',
             '1234567891014', 'New', 'Seller', 'Foil packaging', '--',
             i8(40), i8(10), 'Not Needed', i8(10), 'Not Needed', i8(10),
             'Not Needed', i8(10), 'Not Needed']
        ],
        columns=['Merchant SKU', 'Title', 'ASIN', 'FNSKU', 'external-id',
                 'Condition', 'Who Will Prep?', 'Prep Type', 'Who Will Label?',
                 'Shipped', 'FBA12AB1QCDJU001 - Unit Quantity',
                 'FBA12AB1QCDJU001 Expiration Date (dd.mm.yy)',
                 'FBA12AB1QCDJU002 - Unit Quantity',
                 'FBA12AB1QCDJU002 Expiration Date (dd.mm.yy)',
                 'FBA12AB1QCDJU003 - Unit Quantity',
                 'FBA12AB1QCDJU003 Expiration Date (dd.mm.yy)',
                 'FBA12AB1QCDJU004 - Unit Quantity',
                 'FBA12AB1QCDJU004 Expiration Date (dd.mm.yy)']
        )


def describe_fill_out_packagelist():
    def with_valid_data(sample_package_list, sample_package_list_filled):
        assignment = pandas.DataFrame(
            [
                ['1234', 5, 1], ['2345', 5, 1], ['4567', 10, 1],
                ['1234', 5, 2], ['3456', 15, 2], ['4567', 10, 2],
                ['2345', 10, 3], ['4567', 10, 3],
                ['2345', 5, 4], ['3456', 15, 4], ['4567', 10, 4]
            ], columns=['sku', 'quantity', 'packageNo']
        )

        filler = PackageListFiller()
        filler.package_list = sample_package_list
        filler.assignment = assignment
        assert filler.fill_out_packagelist() is True
        assert_frame_equal(filler.package_list, sample_package_list_filled)

    def with_insufficient_packages(sample_package_list):
        assignment = pandas.DataFrame(
            [
                ['1234', 5, 1], ['2345', 5, 1], ['4567', 10, 1],
                ['1234', 5, 2], ['4567', 10, 2],
                ['2345', 10, 3], ['4567', 10, 3],
                ['2345', 5, 4], ['3456', 15, 4]
            ], columns=['sku', 'quantity', 'packageNo']
        )
        filler = PackageListFiller()
        filler.package_list = sample_package_list
        filler.assignment = assignment
        with pytest.raises(InsufficientPackagesError):
            filler.fill_out_packagelist()


@pytest.fixture
def sample_package_list_header():
    return [
        ['Shipment ID', 'FBA12AB1QCDJ'], ['Name', 'FBA (30.11.21, 08:43) - 1'],
        ['Plan ID', 'PLN2DSYXB7'],
        ['Ship To', 'Amazon Fulfillment Poland'], ['Total SKUs', '11'],
        ['Total Units', '393'], ['Pack list', ' von '], [''],
        ['Merchant SKU', 'Title', 'ASIN', 'FNSKU', 'external-id', 'Condition',
         'Who Will Prep?', 'Prep Type', 'Who Will Label?', 'Shipped',
         'FBA12AB1QCDJU001 - Unit Quantity',
         'FBA12AB1QCDJU001 Expiration Date (dd.mm.yy)',
         'FBA12AB1QCDJU002 - Unit Quantity',
         'FBA12AB1QCDJU002 Expiration Date (dd.mm.yy)',
         'FBA12AB1QCDJU003 - Unit Quantity',
         'FBA12AB1QCDJU003 Expiration Date (dd.mm.yy)',
         'FBA12AB1QCDJU004 - Unit Quantity',
         'FBA12AB1QCDJU004 Expiration Date (dd.mm.yy)']
    ]

def test_join_package_list_parts(sample_package_list_filled,
                                 sample_package_list_header):
    expectation = pandas.DataFrame(
        [
            ['Shipment ID', 'FBA12AB1QCDJ', '', '', '', '', '', '', '', '',
             '', '', '', '', '', '', '', ''],
            ['Name', 'FBA (30.11.21, 08:43) - 1', '', '', '', '', '', '', '',
             '', '', '', '', '', '', '', '', ''],
            ['Plan ID', 'PLN2DSYXB7', '', '', '', '', '', '', '', '', '', '',
             '', '', '', '', '', ''],
            ['Ship To', 'Amazon Fulfillment Poland', '', '', '', '', '', '',
             '', '', '', '', '', '', '', '', '', ''],
            ['Total SKUs', '11', '', '', '', '', '', '', '', '', '', '', '',
             '', '', '', '', ''],
            ['Total Units', '393', '', '', '', '', '', '', '', '', '', '', '',
             '', '', '', '', ''],
            ['Pack list', ' von ', '', '', '', '', '', '', '', '', '', '', '',
             '', '', '', '', ''],
            ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
             '', ''],
            ['Merchant SKU', 'Title', 'ASIN', 'FNSKU', 'external-id',
             'Condition', 'Who Will Prep?', 'Prep Type', 'Who Will Label?',
             'Shipped', 'FBA12AB1QCDJU001 - Unit Quantity',
             'FBA12AB1QCDJU001 Expiration Date (dd.mm.yy)',
             'FBA12AB1QCDJU002 - Unit Quantity',
             'FBA12AB1QCDJU002 Expiration Date (dd.mm.yy)',
             'FBA12AB1QCDJU003 - Unit Quantity',
             'FBA12AB1QCDJU003 Expiration Date (dd.mm.yy)',
             'FBA12AB1QCDJU004 - Unit Quantity',
             'FBA12AB1QCDJU004 Expiration Date (dd.mm.yy)'],
            ['1234', 'test_product_1', 'TESTASIN01', 'X001234567',
             '1234567891011', 'New', 'Seller', 'Foil packaging', '--',
             i8(10), i8(5), 'Not Needed', i8(5), 'Not Needed', i8(0),
             'Not Needed', i8(0), 'Not Needed'],
            ['2345', 'test_product_2', 'TESTASIN02', 'X001234568',
             '1234567891012', 'New', 'Seller', 'Foil packaging', '--',
             i8(20), i8(5), 'Not Needed', i8(0), 'Not Needed', i8(10),
             'Not Needed', i8(5), 'Not Needed'],
            ['3456', 'test_product_3', 'TESTASIN03', 'X001234569',
             '1234567891013', 'New', 'Seller', 'Foil packaging', '--',
             i8(30), i8(0), 'Not Needed', i8(15), 'Not Needed', i8(0),
             'Not Needed', i8(15), 'Not Needed'],
            ['4567', 'test_product_4', 'TESTASIN04', 'X001234570',
             '1234567891014', 'New', 'Seller', 'Foil packaging', '--',
             i8(40), i8(10), 'Not Needed', i8(10), 'Not Needed', i8(10),
             'Not Needed', i8(10), 'Not Needed']
        ],
        columns=['Merchant SKU', 'Title', 'ASIN', 'FNSKU', 'external-id',
                 'Condition', 'Who Will Prep?', 'Prep Type', 'Who Will Label?',
                 'Shipped', 'FBA12AB1QCDJU001 - Unit Quantity',
                 'FBA12AB1QCDJU001 Expiration Date (dd.mm.yy)',
                 'FBA12AB1QCDJU002 - Unit Quantity',
                 'FBA12AB1QCDJU002 Expiration Date (dd.mm.yy)',
                 'FBA12AB1QCDJU003 - Unit Quantity',
                 'FBA12AB1QCDJU003 Expiration Date (dd.mm.yy)',
                 'FBA12AB1QCDJU004 - Unit Quantity',
                 'FBA12AB1QCDJU004 Expiration Date (dd.mm.yy)']
    )
    filler = PackageListFiller()
    filler.package_list = sample_package_list_filled
    filler.header_section = sample_package_list_header
    result = filler.join_package_list_parts()
    assert_frame_equal(result, expectation)


@pytest.fixture
def sample_header():
    return [
        ['PlanName', 'Shipment_2021-10-29'],
        ['ShipToCountry', 'US'],
        ['AddressName', 'test name'],
        ['AddressFieldOne', 'test street nr. 12'],
        ['AddressFieldTwo', ''],
        ['AddressCity', 'test city'],
        ['AddressCountryCode', 'DE'],
        ['AddressStateOrRegion', 'Bayern'],
        ['AddressPostalCode', '12345'],
        ['AddressDistrict', ''],
        ['', ''],
        ['MerchantSKU', 'Quantity'],
        ['', '']
    ]


def build_inbound_plan(sample_header, content) -> pandas.DataFrame:
    return pandas.DataFrame(
        sample_header + content,
        columns=['MerchantSKU', 'Quantity']
    )


@pytest.fixture
def sample_creator():
    config = {
        'name': 'test name',
        'street': 'test street nr. 12',
        'city': 'test city',
        'country_code': 'DE',
        'state': 'Bayern',
        'postal_code': '12345'
    }
    return InboundPlanCreator(
        sender_config=config, receiver_country='US'
    )


def describe_validate_with_package_list():
    def with_all_variations_packaged(sample_header, sample_creator):
        content = [
            ['1234', '10'],
            ['2345', '15'],
            ['3456', '20']
        ]
        inbound_plan = build_inbound_plan(sample_header, content)
        assignment = pandas.DataFrame(
            [
                ['1234', 10, 1],
                ['2345', 15, 1],
                ['3456', 20, 2]
            ],
            columns=['sku', 'quantity', 'packageNo']
        )
        expected = pandas.DataFrame([], columns=['sku', 'announced', 'packaged'])
        result = sample_creator.validate_with_package_list(
            inbound_plan=inbound_plan, assignment=assignment
        )
        assert_frame_equal(result, expected)

    def with_no_variations_packaged(sample_header, sample_creator):
        content = [
            ['1234', '10'],
            ['2345', '15'],
            ['3456', '20']
        ]
        inbound_plan = build_inbound_plan(sample_header, content)
        assignment = pandas.DataFrame(
            [],
            columns=['sku', 'quantity', 'packageNo']
        )
        expected = pandas.DataFrame([
            ['1234', 10, 0],
            ['2345', 15, 0],
            ['3456', 20, 0]
        ], columns=['sku', 'announced', 'packaged'])
        result = sample_creator.validate_with_package_list(
            inbound_plan=inbound_plan, assignment=assignment
        )
        assert_frame_equal(result, expected)

    def with_some_variations_packaged(sample_header, sample_creator):
        content = [
            ['1234', '10'],
            ['2345', '15'],
            ['3456', '20']
        ]
        inbound_plan = build_inbound_plan(sample_header, content)
        assignment = pandas.DataFrame(
            [
                ['2345', 15, 1],
                ['3456', 20, 1]
            ], columns=['sku', 'quantity', 'packageNo']
        )
        expected = pandas.DataFrame([
            ['1234', 10, 0],
        ], columns=['sku', 'announced', 'packaged'])
        result = sample_creator.validate_with_package_list(
            inbound_plan=inbound_plan, assignment=assignment
        )
        assert_frame_equal(result, expected)

    def with_partially_packed_variations(sample_header, sample_creator):
        content = [
            ['1234', '10'],
            ['2345', '15'],
            ['3456', '20']
        ]
        inbound_plan = build_inbound_plan(sample_header, content)
        assignment = pandas.DataFrame(
            [
                ['1234', 8, 1],
                ['2345', 10, 1],
                ['3456', 20, 1]
            ], columns=['sku', 'quantity', 'packageNo']
        )
        expected = pandas.DataFrame([
            ['1234', 10, 8],
            ['2345', 15, 10],
        ], columns=['sku', 'announced', 'packaged'])
        result = sample_creator.validate_with_package_list(
            inbound_plan=inbound_plan, assignment=assignment
        )
        assert_frame_equal(result, expected)


def describe_create_full_view_packagelist():
    def with_one_package_per_variation():
        sample_assignment = pandas.DataFrame(
                [
                    ['1234', 1, 1], ['2345', 2, 2],
                    ['3456', 3, 3], ['4567', 4, 4]
                ], columns=['sku', 'quantity', 'packageNo']
        )

        expected = pandas.DataFrame(
            [
                ['1234', 1, 0, 0, 0],
                ['2345', 0, 2, 0, 0],
                ['3456', 0, 0, 3, 0],
                ['4567', 0, 0, 0, 4]
            ], columns=['sku', 'package_1', 'package_2', 'package_3', 'package_4']
        )

        package_list_filler = PackageListFiller()
        package_list_filler.assignment = sample_assignment
        result = package_list_filler.create_full_view_packagelist()

        assert_frame_equal(result, expected)

    def with_one_or_more_packages_per_variation():
        sample_assignment = pandas.DataFrame(
                [
                    ['1234', 1, 1], ['1234', 5, 2],
                    ['2345', 2, 2],
                    ['3456', 3, 3], ['3456', 6, 1],
                    ['4567', 4, 4], ['4567', 7, 3]
                ], columns=['sku', 'quantity', 'packageNo']
        )

        expected = pandas.DataFrame(
            [
                ['1234', 1, 5, 0, 0],
                ['2345', 0, 2, 0, 0],
                ['3456', 6, 0, 3, 0],
                ['4567', 0, 0, 7, 4]
            ], columns=['sku', 'package_1', 'package_2', 'package_3', 'package_4']
        )
        package_list_filler = PackageListFiller()
        package_list_filler.assignment = sample_assignment
        result = package_list_filler.create_full_view_packagelist()

        assert_frame_equal(result, expected)
