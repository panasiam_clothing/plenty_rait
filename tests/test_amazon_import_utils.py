# pylint: disable=unused-variable, redefined-outer-name
# pylint: disable=missing-function-docstring
import datetime
import pytest
import dateutil.tz

from plenty_rait.amazon_import.utils import (
    transform_date, convert_to_euro, get_netto
)


@pytest.fixture
def mapping():
    mapping: dict = {
        'FBA': {
            'Amazon.de': 104.01,
            'Amazon.co.uk': 104.02,
            'Amazon.com': 104.03,
            'Amazon.fr': 104.04,
            'Amazon.it': 104.05,
            'Amazon.es': 104.06,
            'Amazon.nl': 104.09,
            'Amazon.se': 104.12
        },
        'FBM': {
            'Amazon.de': 4.01,
            'Amazon.co.uk': 4.02,
            'Amazon.com': 4.03,
            'Amazon.fr': 4.04,
            'Amazon.it': 4.05,
            'Amazon.es': 4.06,
            'Amazon.nl': 4.09,
            'Amazon.se': 4.12
        }
    }
    return mapping


@pytest.fixture
def match_table():
    sku_mapping: dict = {
        '1234x': 1234,
        '2345x': 2345,
        '3456x': 3456,
        '4567x': 4567,
        '5678x': 5678
    }

    asin_mapping: dict = {
        'B099999995': 1234,
        'B099999996': 2345,
        'B099999997': 3456,
        'B099999998': 4567,
        'B099999999': 5678
    }

    match_table: dict = {
        'sku': sku_mapping,
        'asin': asin_mapping
    }
    return match_table


@pytest.fixture
def currency_conversion():
    table: dict = {
        'EUR': 1,
        'GBP': 1.114943,
        'USD': 0.835976
    }
    return table


def describe_transform_date():
    def with_good_date():
        testset = [
            '2020-11-30T04:16:04+00:00', '2020-01-05T09:04:25+00:00',
            '2020-06-12T12:10:30+00:00', '2020-12-31T23:59:00+00:00'
        ]
        expect = [
            datetime.datetime(year=2020, month=11, day=30, hour=4, minute=16,
                              second=4, tzinfo=dateutil.tz.tzutc()),
            datetime.datetime(year=2020, month=1, day=5, hour=9, minute=4,
                              second=25, tzinfo=dateutil.tz.tzutc()),
            datetime.datetime(year=2020, month=6, day=12, hour=12, minute=10,
                              second=30, tzinfo=dateutil.tz.tzutc()),
            datetime.datetime(year=2020, month=12, day=31, hour=23, minute=59,
                              second=0, tzinfo=dateutil.tz.tzutc())
        ]
        result = []

        for test in testset:
            result.append(transform_date(amazon_date=test))

        assert expect == result

    def with_bad_date():
        testset = [
            '2020-11-3004:16:04+00:00', '2020-01-05T09:04:25',
            '2020-06-12', '31-12-2020T23:59:00+00:00'
        ]
        expect = [
            None, None, None, None
        ]
        result = []

        for test in testset:
            result.append(transform_date(amazon_date=test))

        assert expect == result

    def with_no_date():
        assert None is transform_date(amazon_date='')


def describe_convert_to_euro():
    def with_gbp(currency_conversion):
        testset = ['10.40', '20.00', '30.00']
        expect = ['11.6', '22.3', '33.45']
        result = []

        for test in testset:
            result.append(convert_to_euro(price=test, currency='GBP',
                                          con_table=currency_conversion))

        assert expect == result

    def with_dollar(currency_conversion):
        testset = ['10.40', '20.00', '30.00']
        expect = ['8.69', '16.72', '25.08']
        result = []

        for test in testset:
            result.append(convert_to_euro(price=test, currency='USD',
                                          con_table=currency_conversion))

        assert expect == result

    def with_euro(currency_conversion):
        testset = ['10.40', '20.00', '30.00']
        expect = ['10.4', '20.0', '30.0']
        result = []

        for test in testset:
            result.append(convert_to_euro(price=test, currency='EUR',
                                          con_table=currency_conversion))

        assert expect == result

    def with_unknown_currency(currency_conversion):
        assert  convert_to_euro(price='10.00', currency='XYZ',
                                con_table=currency_conversion) == ''

    def with_no_currency(currency_conversion):
        assert  convert_to_euro(price='10.00', currency='',
                                con_table=currency_conversion) == ''

    def with_no_price(currency_conversion):
        assert  convert_to_euro(price='', currency='GBP',
                                con_table=currency_conversion) == ''


def describe_get_netto():
    def with_total_and_tax():
        testset = [
            ('25.90', '4.32'), ('22.90', '3.82'), ('20.70', '3.45')
        ]
        expect = ['21.58', '19.08', '17.25']
        result = []

        for test in testset:
            result.append(get_netto(total=test[0], tax=test[1]))

        assert expect == result

    def with_total_without_tax():
        assert get_netto(total='20.00', tax='') == ''

    def without_total_with_tax():
        assert get_netto(total='', tax='4.32') == ''

    def without_total_without_tax():
        assert get_netto(total='', tax='') == ''

    def with_negative_total_with_positive_tax():
        assert get_netto(total='-10.00', tax='2.32') == ''

    def with_positive_total_with_negative_tax():
        assert get_netto(total='10.00', tax='-2.32') == ''

    def with_invalid_total():
        assert get_netto(total='abc', tax='cdf') == ''

    def with_invalid_tax():
        assert get_netto(total='10.00', tax='5.00') == ''
        assert get_netto(total='10.00', tax='10.00') == ''
        assert get_netto(total='10.00', tax='0.10') == ''
        assert get_netto(total='10.00', tax='0.00') == ''
