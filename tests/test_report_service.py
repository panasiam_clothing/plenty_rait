# pylint: disable=unused-variable, redefined-outer-name, protected-access
# pylint: disable=missing-function-docstring
import datetime
import pytest

from plenty_rait.report.service import Report, Interface
from plenty_rait.report.utils import ReportConfigHandler
from plenty_rait.report.config_schema import report_configuration_schema
from plenty_rait.helper.database_schema import (
    DailyStatisticModel, IdentificationModel, StockModel
)
from plenty_rait.helper.test_helper import setup_database
import plenty_rait.helper.config_helper


@pytest.fixture
def test_statistic() -> list:
    stats = [
        DailyStatisticModel(
            variation_id=1234, origin_id=4.03, day=13, month=7,
            year=2018, quantity=2, ean=1234567891011
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.03, day=15, month=1,
            year=2019, quantity=1, ean=1234567891011
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.01, day=5, month=3,
            year=2019, quantity=2, ean=1234567891011
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.01, day=14, month=5,
            year=2019, quantity=3, ean=1234567891011
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.01, day=24, month=8,
            year=2019, quantity=5, ean=1234567891011
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.03, day=17, month=3,
            year=2020, quantity=2, ean=1234567891011
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4.01, day=28, month=6,
            year=2020, quantity=3, ean=1234567891011
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=104.02, day=25, month=9,
            year=2020, quantity=6, ean=1234567891011
        )
    ]
    return stats


@pytest.fixture
def test_ident() -> list:
    ident = [
        IdentificationModel(variation_id=1234, ean='1234567891011',
                            sku='test_sku_1', asin_all='B012345678'),
        IdentificationModel(variation_id=1235, ean='1234567891012',
                            sku='test_sku_2', asin_all='B012345679'),
        IdentificationModel(variation_id=1236, ean='1234567891013',
                            sku='test_sku_3', asin_all='B012345688')
    ]
    return ident


@pytest.fixture
def incorrect_ident() -> list:
    ident = [
        IdentificationModel(variation_id=2345, ean='1234567891013',
                            sku='test_sku_2', asin_all='B012345679')
    ]
    return ident


@pytest.fixture
def test_stock() -> list:
    stock = [
        StockModel(variation_id=1234, ean='1234567891011', stock_warehouse_1=5,
                   stock_warehouse_2=3, stock_warehouse_3=7),
        StockModel(variation_id=1235, ean='1234567891012', stock_warehouse_1=4,
                   stock_warehouse_2=2, stock_warehouse_3=6),
        StockModel(variation_id=1236, ean='1234567891013', stock_warehouse_1=6,
                   stock_warehouse_2=4, stock_warehouse_3=8)
    ]
    return stock


@pytest.fixture
def sources(test_statistic, test_ident) -> dict:
    sources = {
        'stats': test_statistic,
        'ident': test_ident
    }
    return sources


@pytest.fixture
def err_sources(test_statistic, incorrect_ident) -> dict:
    sources = {
        'stats': test_statistic,
        'ident': incorrect_ident
    }
    return sources


@pytest.fixture
def stock_sources(test_stock, test_ident) -> dict:
    sources = {
        'stock': test_stock,
        'ident': test_ident
    }
    return sources


def describe_select_by_date_range() -> None:
    def with_no_sales_range(db_session) -> None:
        report = Report(session=db_session, ident='asin')
        with pytest.raises(RuntimeError):
            report.select_by_date_range(date_range=(None, None))

    def with_no_entry_in_range(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(session=db_session, ident='asin')
        date = datetime.date(2020, 12, 3)
        date_range = (date - datetime.timedelta(days=31), date)
        result = report.select_by_date_range(date_range=date_range)
        assert len(result.index) == 0

    def with_range_within_month(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(session=db_session, ident='asin')
        date = datetime.date(2020, 3, 20)
        date_range = (date - datetime.timedelta(days=10), date)
        result = report.select_by_date_range(date_range=date_range)
        assert result['quantity'].sum() == 2
        assert len(result.index) == 1

    def with_range_within_year(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(session=db_session, ident='asin')
        date = datetime.date(2019, 6, 20)
        date_range = (date - datetime.timedelta(days=71), date)
        result = report.select_by_date_range(date_range=date_range)
        assert result['quantity'].sum() == 3
        assert len(result.index) == 1

    def with_range_within_two_year_span(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(session=db_session, ident='asin')
        date = datetime.date(2020, 8, 13)
        date_range = (date - datetime.timedelta(days=531), date)
        result = report.select_by_date_range(date_range=date_range)
        assert result['quantity'].sum() == 15
        assert len(result.index) == 2

    def with_range_within_multi_year_span(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(session=db_session, ident='ean')
        date = datetime.date(2020, 3, 20)
        date_range = (date - datetime.timedelta(days=689), date)
        result = report.select_by_date_range(date_range=date_range)
        assert result['quantity'].sum() == 15
        assert len(result.index) == 2

    def with_no_entry_in_ident_table(db_session, err_sources: dict) -> None:
        setup_database(session=db_session, sources=err_sources)
        report = Report(session=db_session, ident='asin')
        date = datetime.date(2020, 3, 20)
        date_range = (date - datetime.timedelta(days=90), date)
        result = report.select_by_date_range(date_range=date_range)
        assert len(result.index) == 0

    def with_missing_ident_table(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources['stats'])
        report = Report(session=db_session, ident='asin')
        date = datetime.date(2020, 3, 20)
        date_range = (date - datetime.timedelta(days=4), date)
        result = report.select_by_date_range(date_range=date_range)
        assert len(result.index) == 0


def describe_select_by_year() -> None:
    def with_no_entry_in_year(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(session=db_session, ident='asin')
        result = report.select_by_year(year=2017)
        assert len(result.index) == 0

    def with_single_entry_in_year(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(session=db_session, ident='sku')
        result = report.select_by_year(year=2018)
        assert result['quantity'].sum() == 2
        assert len(result.index) == 1

    def with_multiple_entries_in_year(db_session, sources: dict) -> None:
        setup_database(session=db_session, sources=sources)
        report = Report(session=db_session, ident='variation_id')
        result = report.select_by_year(year=2019)
        assert result['quantity'].sum() == 11
        assert len(result.index) == 2


@pytest.fixture()
def dummy_warehouse_handler(monkeypatch):
    def init_top_level_config(name, config_type):
        del name, config_type
        return (
            {
                'warehouses': [
                    {
                        'plenty_warehouse_id': 104,
                        'database_column_name': 'stock_warehouse_1',
                        'sheet_column_name': 'ABC',
                        'gui_display_name': 'GUI-ABC',
                        'sync_with': 'plentymarkets'
                    }
                ]
            },
            '/abc/def'
        )
    monkeypatch.setattr(
        plenty_rait.helper.config_helper,
        'init_top_level_config',
        init_top_level_config
    )
    return plenty_rait.helper.config_helper.WarehouseConfigHandler()


def describe_get_stock() -> None:
    def with_valid_config(db_session, stock_sources: dict,
                          dummy_warehouse_handler) -> None:
        warehouses = dummy_warehouse_handler.config['warehouses']
        warehouses += [
                {
                    'plenty_warehouse_id': 105,
                    'database_column_name': 'stock_warehouse_2',
                    'sheet_column_name': 'BCD',
                    'gui_display_name': 'GUI-BCD',
                    'sync_with': 'plentymarkets'
                },
                {
                    'plenty_warehouse_id': 106,
                    'database_column_name': 'stock_warehouse_3',
                    'sheet_column_name': 'CDE',
                    'gui_display_name': 'GUI-CDE',
                    'sync_with': 'amazon'
                }
        ]
        expectation = {
            'asin': ['B012345678', 'B012345679', 'B012345688'],
            'ABC': [5, 4, 6],
            'BCD': [3, 2, 4],
            'CDE': [7, 6, 8]
        }

        setup_database(session=db_session, sources=stock_sources)
        report = Report(session=db_session, ident='asin')
        result = report.get_stock(warehouse_handler=dummy_warehouse_handler)

        for index, row in enumerate(result.itertuples()):
            assert row.ABC == expectation['ABC'][index]
            assert row.BCD == expectation['BCD'][index]
            assert row.CDE == expectation['CDE'][index]
            assert row.asin == expectation['asin'][index]

    def with_var_id_not_found_in_ident(db_session, stock_sources: dict,
                                       dummy_warehouse_handler) -> None:
        expectation = {
            'ean': ['1234567891011', '1234567891012', '1234567891013'],
            'ABC': [5, 4, 6]
        }

        setup_database(session=db_session, sources=stock_sources)
        db_session.add(
            StockModel(variation_id=1111, ean='1234567891000')
        )
        db_session.commit()
        report = Report(session=db_session, ident='ean')
        result = report.get_stock(warehouse_handler=dummy_warehouse_handler)

        for index, row in enumerate(result.itertuples()):
            assert row.ABC == expectation['ABC'][index]
            assert row.ean == expectation['ean'][index]


@pytest.fixture
def dummy_config_handler(monkeypatch, dummy_warehouse_handler):
    def init_config(name, config_type):
        del name, config_type
        return ''
    monkeypatch.setattr(plenty_rait.helper.config_helper,
                        'init_config', init_config)

    def read_configuration(path, config_type):
        del path, config_type
        return {
            'format': []
        }
    monkeypatch.setattr(plenty_rait.helper.config_helper,
                        'read_configuration',
                        read_configuration)

    def _validate_configuration(self):
        del self

    monkeypatch.setattr(ReportConfigHandler, '_validate_configuration',
                        _validate_configuration)
    return ReportConfigHandler(
        name='abc', config_type='json',
        config_schema=report_configuration_schema)


def fake_interface(config):
    def decorator(func):
        def wrapper(monkeypatch, dummy_config_handler):
            format_config = dummy_config_handler.config['format']
            for entry in config:
                format_config.append({'name': entry[0], 'content': entry[1]})

            def fake_init(self, connection_string, ident):
                del connection_string, ident
                self.config_handler = dummy_config_handler
            monkeypatch.setattr(Interface, '__init__',
                                fake_init)
            func()
        return wrapper
    return decorator


def describe_parse_origins():
    @fake_interface(config=[('fba_ger', 104.01)])
    def with_single_correct_entry():
        expectation = {104.01: 'fba_ger'}
        interface = Interface(connection_string='abc', ident='asin')
        result = interface._Interface__parse_origins()

        assert expectation == result

    @fake_interface(config=[('fba_ger', 104.01), ('fbm_ger', 4.01)])
    def with_multiple_correct_entries():
        expectation = {104.01: 'fba_ger', 4.01: 'fbm_ger'}
        interface = Interface(connection_string='abc', ident='asin')
        result = interface._Interface__parse_origins()

        assert expectation == result
