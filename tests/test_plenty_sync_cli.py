# pylint: disable=unused-variable, redefined-outer-name
# pylint: disable=missing-function-docstring
from collections import namedtuple
import datetime
import pytest
from freezegun import freeze_time

from plenty_rait.plenty_sync.cli import (
    get_time_range, InvalidDate
)


W3C_FORMAT = '%Y-%m-%dT%H:%M:%S'


def describe_get_time_range():
    def with_invalid_dates():
        args = namedtuple('Namespace', ('start', 'end'))
        args.plenty_sync_order_start = 'abc'
        args.plenty_sync_order_end = 'def'
        with pytest.raises(InvalidDate):
            get_time_range(parser=args)

    def with_valid_dates():
        args = namedtuple('Namespace', ('start', 'end'))
        args.plenty_sync_order_start = '2021-06-05'
        args.plenty_sync_order_end = '2021-06-06'

        expected_dates = (datetime.datetime(2021, 6, 5).strftime(W3C_FORMAT),
                          datetime.datetime(2021, 6, 6).strftime(W3C_FORMAT))

        assert expected_dates == get_time_range(parser=args)

    def with_valid_start_and_invalid_end_date():
        args = namedtuple('Namespace', ('start', 'end'))
        args.plenty_sync_order_start = '2021-06-05'
        args.plenty_sync_order_end = 'def'
        with pytest.raises(InvalidDate):
            get_time_range(parser=args)

    def with_invalid_start_and_valid_end_date():
        args = namedtuple('Namespace', ('start', 'end'))
        args.plenty_sync_order_start = 'abc'
        args.plenty_sync_order_end = '2021-06-06'
        with pytest.raises(InvalidDate):
            get_time_range(parser=args)

    @freeze_time('2021-07-05')
    def with_valid_start_and_no_end_date():
        args = namedtuple('Namespace', ('start', 'end'))
        args.plenty_sync_order_start = '2021-06-05'
        args.plenty_sync_order_end = None

        expected_dates = (datetime.datetime(2021, 6, 5).strftime(W3C_FORMAT),
                          datetime.datetime(2021, 7, 5).strftime(W3C_FORMAT))

        assert expected_dates == get_time_range(parser=args)

    def with_no_start_and_valid_end_date():
        args = namedtuple('Namespace', ('start', 'end'))
        args.plenty_sync_order_start = None
        args.plenty_sync_order_end = '2021-06-05'

        expected_dates = (datetime.datetime(2021, 6, 4).strftime(W3C_FORMAT),
                          datetime.datetime(2021, 6, 5).strftime(W3C_FORMAT))

        assert expected_dates == get_time_range(parser=args)

    @freeze_time('2021-07-05')
    def with_no_start_and_no_end_date():
        args = namedtuple('Namespace', ('start', 'end'))
        args.plenty_sync_order_start = None
        args.plenty_sync_order_end = None

        expected_dates = (
            datetime.datetime(2021, 7, 4, 12).strftime(W3C_FORMAT),
            datetime.datetime(2021, 7, 5).strftime(W3C_FORMAT)
        )

        assert expected_dates == get_time_range(parser=args)
