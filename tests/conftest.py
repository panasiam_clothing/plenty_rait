import pytest
from sqlalchemy.orm import scoped_session, sessionmaker
from plenty_rait.helper.database_schema import dal, Base


@pytest.fixture(scope='function')
def session(request):
    dal.connection = str('mysql+pymysql://testuser:testpw1234@localhost'
                         '/testdb')

    dal.connect()
    dal.s_session = scoped_session(sessionmaker(bind=dal.engine))
    dal.session = dal.s_session()
    dal.s_session.registry.clear()

    request.addfinalizer(Base.metadata.drop_all)
    return dal.session


@pytest.fixture(scope='function')
def db_session(request, session):
    request.addfinalizer(session.rollback)
    return session
