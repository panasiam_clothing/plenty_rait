# pylint: disable=unused-variable, redefined-outer-name, protected-access
# pylint: disable=missing-function-docstring
import datetime
from decimal import Decimal
from freezegun import freeze_time
import pandas
import pytest
import plenty_api
from sqlalchemy import null

from plenty_rait.helper.database_schema import (
    StockModel, IdentificationModel, OrdersModel, OrderItemsModel,
    ProducerModel, IncomingVariationModel, VariationInformationModel
)
from plenty_rait.plenty_sync.service import (
    get_stock, retrieve_value_by_id, get_amount, DatabaseService,
    get_priority_level, RedistributionService,
    find_matching_location_combinations, DataCollector, PlentyImport
)
from plenty_rait.helper.test_helper import setup_database
from plenty_rait.helper import config_helper
import plenty_rait.plenty_sync.service


IDENTIFICATION_TABLE_COLUMNS = ['variation_id', 'ean', 'sku', 'asin_all',
                                'name']
STOCK_TABLE_COLUMNS = ['variation_id', 'ean', 'stock_warehouse_1',
                       'stock_warehouse_2', 'stock_warehouse_3',
                       'stock_warehouse_4', 'stock_warehouse_5',
                       'stock_warehouse_6', 'stock_warehouse_7',
                       'stock_warehouse_8', 'stock_warehouse_9']
ORDER_TABLE_COLUMNS = ['order_id', 'order_item_id', 'variation_id', 'item_id',
                       'external_id', 'name', 'origin_id', 'net_price',
                       'gross_price', 'quantity', 'creation_date',
                       'payment_date', 'delivery_date', 'b2b']


@pytest.fixture
def test_existing_stock() -> list:
    stock = [
        # empty entry
        StockModel(variation_id=1234, item_id=123, stock_warehouse_1=0,
                   stock_warehouse_2=0),
        # stock in one warehouse
        StockModel(variation_id=2345, item_id=234, stock_warehouse_1=5,
                   stock_warehouse_2=0),
        # stock in two warehouses
        StockModel(variation_id=3456, item_id=345, stock_warehouse_1=5,
                   stock_warehouse_2=5),
        # stock in two non consecutive warehouses
        StockModel(variation_id=4567, item_id=456, stock_warehouse_1=5,
                   stock_warehouse_5=5),
    ]

    return stock


@pytest.fixture
def test_identification_table() -> list:
    table = []
    return table


@pytest.fixture
def test_existing_orders() -> list:
    orders = []
    return orders


@pytest.fixture
def test_existing_order_items() -> list:
    order_items = []
    return order_items


@pytest.fixture
def sources(test_identification_table: list, test_existing_orders: list,
            test_existing_order_items: list,
            test_existing_stock: list) -> dict:
    return {'orders': test_existing_orders,
            'order_items': test_existing_order_items,
            'stock': test_existing_stock,
            'ident': test_identification_table}


@pytest.fixture
def sample_api_variations():
    variations = [{
        'id': 1234, 'isMain': False, 'mainVariationId': 1230, 'itemId': 123,
        'categoryVariationId': 1234, 'marketVariationId': 1234,
        'clientVariationId': 1234, 'salesPriceVariationId': 1234,
        'supplierVariationId': 1234, 'warehouseVariationId': 1234,
        'position': 2, 'isActive': True, 'number': '123',
        'model': '', 'externalId': 'test_id_1', 'parentVariationId': None,
        'parentVariationQuantity': None, 'availability': 2,
        'estimatedAvailableAt': None, 'purchasePrice': 0, 'mainWarehouseId': 1,
        'name': 'test article 1', 'weightG': 1, 'weightNetG': 0,
        'widthMM': 2, 'lengthMM': 3, 'heightMM': 4,
        'propertyVariationId': '1234', 'tagVariationId': '1234',
        'stock': [{
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 1234, 'itemId': 123, 'warehouseId': 1,
            'physicalStock': 10, 'reservedStock': 2, 'netStock': 8,
            'reorderLevel': 0, 'deltaReorderLevel': -10
        }, {
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 1234, 'itemId': 123, 'warehouseId': 2,
            'physicalStock': 20, 'reservedStock': 0, 'netStock': 20,
            'reorderLevel': 0, 'deltaReorderLevel': -20
        }]
    }, {
        'id': 2345, 'isMain': False, 'mainVariationId': 2340, 'itemId': 234,
        'categoryVariationId': 2345, 'marketVariationId': 2345,
        'clientVariationId': 2345, 'salesPriceVariationId': 2345,
        'supplierVariationId': 2345, 'warehouseVariationId': 2345,
        'position': 3, 'isActive': True, 'number': '234', 'model': '',
        'externalId': 'test_id_2', 'parentVariationId': None,
        'parentVariationQuantity': None, 'availability': 2,
        'estimatedAvailableAt': None, 'purchasePrice': 0,
        'mainWarehouseId': 1, 'name': 'test article 2', 'weightG': 1,
        'weightNetG': 0, 'widthMM': 2, 'lengthMM': 3, 'heightMM': 4,
        'propertyVariationId': '2345', 'tagVariationId': '2345',
        'stock': [{
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 2345, 'itemId': 234, 'warehouseId': 1,
            'physicalStock': 20, 'reservedStock': 5, 'netStock': 15,
            'reorderLevel': 0, 'deltaReorderLevel': -20
        }, {
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 2345, 'itemId': 234, 'warehouseId': 2,
            'physicalStock': 30, 'reservedStock': 0, 'netStock': 30,
            'reorderLevel': 0, 'deltaReorderLevel': -30
        }]
    }, {
        'id': 3456, 'isMain': False, 'mainVariationId': 3450, 'itemId': 345,
        'categoryVariationId': 3456, 'marketVariationId': 3456,
        'clientVariationId': 3456, 'salesPriceVariationId': 3456,
        'supplierVariationId': 3456, 'warehouseVariationId': 3456,
        'position': 4, 'isActive': True, 'number': '345', 'model': '',
        'externalId': 'test_id_3', 'parentVariationId': None,
        'parentVariationQuantity': None, 'availability': 2,
        'estimatedAvailableAt': None, 'purchasePrice': 0,
        'mainWarehouseId': 1, 'name': 'test article 3', 'weightG': 1,
        'weightNetG': 0, 'widthMM': 2, 'lengthMM': 3, 'heightMM': 4,
        'propertyVariationId': '3456', 'tagVariationId': '3456',
        'stock': [{
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 3456, 'itemId': 345, 'warehouseId': 1,
            'physicalStock': 30, 'reservedStock': 10, 'netStock': 20,
            'reorderLevel': 0, 'deltaReorderLevel': -30
        }, {
            'purchasePrice': 0, 'reservedListing': 0, 'reservedBundles': 0,
            'variationId': 3456, 'itemId': 345, 'warehouseId': 3,
            'physicalStock': 40, 'reservedStock': 0, 'netStock': 40,
            'reorderLevel': 0, 'deltaReorderLevel': -40
        }]
    }]
    return variations


@pytest.fixture
def sample_order_items():
    """ Minimal representation of order items from an API response """
    variations = [
        {
            'amounts': [
                {
                    'currency': 'EUR',
                    'priceGross': 24.109,
                    'priceNet': 23.109
                },
                {
                    'currency': 'GBP',
                    'priceGross': 20.92,
                    'priceNet': 19.92
                }
            ],
            'id': 12345,
            'itemVariationId': 1234,
            'orderId': 5678,
            'quantity': 1,
            'referrerId': 4.02,
            'typeId': 1,
            'variation': {
                'itemId': 1234567,
                'name': 'test_name',
            }
        },
        {
            'amounts': [
                {
                    'currency': 'GBP',
                    'priceGross': 19.92,
                    'priceNet': 19.92
                }
            ],
            'id': 12345,
            'itemVariationId': 1234,
            'orderId': 5678,
            'quantity': 1,
            'referrerId': 4.02,
            'typeId': 1,
            'variation': {
                'itemId': 1234567,
                'name': 'test_name',
            }
        }
    ]
    return variations


def describe_get_stock():
    def with_no_config(sample_api_variations: list) -> None:
        for variation in sample_api_variations:
            result = get_stock(variation=variation, warehouse_ids=None)

            assert not result

    def with_no_variations() -> None:
        result = get_stock(variation={}, warehouse_ids=[1, 2, 3])

        assert not result

    def with_variations_and_config(sample_api_variations: list) -> None:
        expected = [
            [10, 20, 0],
            [20, 30, 0],
            [30, 0, 40]
        ]
        for i, variation in enumerate(sample_api_variations):
            result = get_stock(variation=variation,
                               warehouse_ids=[1, 2, 3])

            assert expected[i] == result


def describe_retrieve_value_by_id():
    def with_invalid_section() -> None:
        sample = {'section1': 1, 'section2': 'abc'}
        with pytest.raises(KeyError):
            retrieve_value_by_id(json=sample, id_value=1,
                                 section='section3', match_field='test',
                                 target_field='value')

    def with_non_numeric_id_field() -> None:
        sample = {
            'section1': [
                {'id_field': 'abc', 'value': 'test'}
            ]
        }

        assert retrieve_value_by_id(json=sample, id_value=1,
                                    section='section1',
                                    match_field='id_field',
                                    target_field='value') == ''

    def with_no_matching_id() -> None:
        sample = {
            'section1': [
                {'id_field': 1, 'value': 'test'},
                {'id_field': 2, 'value': 'test2'},
            ]
        }

        assert retrieve_value_by_id(json=sample, id_value=3,
                                    section='section1', match_field='id_field',
                                    target_field='value') == ''

    def with_valid_field_and_id() -> None:
        sample = {
            'section1': [
                {'id_field': 1, 'value': 'test'},
                {'id_field': 2, 'value': 'test2'},
            ]
        }

        assert retrieve_value_by_id(json=sample, id_value=1,
                                    section='section1', match_field='id_field',
                                    target_field='value') == 'test'

    def with_numeric_value() -> None:
        sample = {
            'section1': [
                {'id_field': 1, 'value': 12},
                {'id_field': 2, 'value': 'test2'},
            ]
        }

        assert retrieve_value_by_id(json=sample, id_value=1,
                                    section='section1', match_field='id_field',
                                    target_field='value') == '12'


def describe_get_amount():
    def with_invalid_type(sample_order_items: list) -> None:
        invalid_types = ['netto', 'abc', '']

        for price_type in invalid_types:
            with pytest.raises(KeyError):
                get_amount(json=sample_order_items[0], price_type=price_type)

    def with_valid_type_without_euro_price(sample_order_items: list) -> None:
        assert get_amount(json=sample_order_items[1],
                          price_type='net') == Decimal(0)

    def with_valid_type_and_euro_price(sample_order_items: list) -> None:
        assert str(get_amount(json=sample_order_items[0],
                              price_type='net')) == '23.11'
        assert str(get_amount(json=sample_order_items[0],
                              price_type='gross')) == '24.11'


@pytest.fixture
def sample_priority_config():
    config = {
        1: 4,
        2: 3,
        3: 2,
        4: 1
    }
    return config


def describe_get_priority_level():
    def with_missing_tags_section(sample_api_variations: list,
                                  sample_priority_config: dict) -> None:
        variation = sample_api_variations[0]
        assert get_priority_level(json=variation,
                                  priority_config=sample_priority_config) == -1

    def with_single_priority(sample_api_variations: list,
                             sample_priority_config: dict) -> None:
        variation = sample_api_variations[0]
        variation['tags'] = [{'tagId': '1'}]
        assert get_priority_level(json=variation,
                                  priority_config=sample_priority_config) == 4

    def with_multiple_priorites(sample_api_variations: list,
                                sample_priority_config: dict) -> None:
        variation = sample_api_variations[0]
        variation['tags'] = [{'tagId': '1'}, {'tagId': '3'}]
        assert get_priority_level(json=variation,
                                  priority_config=sample_priority_config) == 2

    def with_multiple_non_matching_tags(sample_api_variations: list,
                                        sample_priority_config: dict) -> None:
        variation = sample_api_variations[0]
        variation['tags'] = [{'tagId': '10'}, {'tagId': '13'}, {'tagId': '4'}]
        assert get_priority_level(json=variation,
                                  priority_config=sample_priority_config) == 1

    def with_no_matching_tag(sample_api_variations: list,
                             sample_priority_config: dict) -> None:
        variation = sample_api_variations[0]
        variation['tags'] = [{'tagId': '10'}, {'tagId': '13'}, {'tagId': '14'}]
        assert get_priority_level(json=variation,
                                  priority_config=sample_priority_config) == 4


def assert_identification_table(session, expectation) -> None:
    """ Helper function for sync_identification_data tests """
    for index, row in enumerate(session.query(IdentificationModel).all()):
        expected_values = expectation[index]
        assert expected_values.variation_id == row.variation_id
        assert expected_values.ean == row.ean
        assert expected_values.sku == row.sku
        assert expected_values.asin_all == row.asin_all
        assert expected_values.name == row.name


def describe_sync_identification_data():
    def with_same_entries(db_session) -> None:
        dataframe = pandas.DataFrame(
            [[1234, '1234567891010', 'test_sku_0', 'TESTASIN00', 'name0']],
            columns=IDENTIFICATION_TABLE_COLUMNS)

        db_entry = IdentificationModel(
            variation_id=1234, ean='1234567891010', sku='test_sku_0',
            asin_all='TESTASIN00', name='name0'
        )
        setup_database(session=db_session, sources=[db_entry])
        expected_table = [db_entry]

        db_service = DatabaseService(session=db_session)
        db_service.sync_identification_data(dataframe=dataframe)

        assert_identification_table(session=db_session,
                                    expectation=expected_table)

    def with_matching_var_id(db_session) -> None:
        dataframe = pandas.DataFrame(
            [[1235, '1234567891011', 'test_sku_1', 'TESTASIN01', 'name1+']],
            columns=IDENTIFICATION_TABLE_COLUMNS)

        setup_database(session=db_session, sources=[
            IdentificationModel(
                variation_id=1235, ean='1234567891011', sku='test_sku_1',
                asin_all='TESTASIN01', name='name1'
            )
        ])

        expected_table = [
            IdentificationModel(
                variation_id=1235, ean='1234567891011', sku='test_sku_1',
                asin_all='TESTASIN01', name='name1+'
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.sync_identification_data(dataframe=dataframe)

        assert_identification_table(session=db_session,
                                    expectation=expected_table)

    def with_matching_sku_and_ean(db_session) -> None:
        dataframe = pandas.DataFrame(
            [[2345, '1234567891012', 'test_sku_2', 'TESTASIN02', 'name2']],
            columns=IDENTIFICATION_TABLE_COLUMNS)

        setup_database(session=db_session, sources=[
            IdentificationModel(
                variation_id=1236, ean='1234567891012', sku='test_sku_2',
                asin_all='TESTASIN02', name='name2'
            )
        ])

        expected_table = [
            IdentificationModel(
                variation_id=2345, ean='1234567891012', sku='test_sku_2',
                asin_all='TESTASIN02', name='name2'
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.sync_identification_data(dataframe=dataframe)

        assert_identification_table(session=db_session,
                                    expectation=expected_table)

    def with_missing_export_entry(db_session) -> None:
        dataframe = pandas.DataFrame(
            [[1237, '1234567891013', 'test_sku_3', 'TESTASIN03', 'name3']],
            columns=IDENTIFICATION_TABLE_COLUMNS)

        setup_database(session=db_session, sources=[
            IdentificationModel(
                variation_id=1237, ean='1234567891013', sku='test_sku_3',
                asin_all='TESTASIN03', name='name3'
            ),
            IdentificationModel(
                variation_id=2346, ean='1234567891015', sku='test_sku_5',
                asin_all='TESTASIN05', name='name5'
            )
        ])

        expected_table = [
            IdentificationModel(
                variation_id=1237, ean='1234567891013', sku='test_sku_3',
                asin_all='TESTASIN03', name='name3'
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.sync_identification_data(dataframe=dataframe)

        assert_identification_table(session=db_session,
                                    expectation=expected_table)

    def with_missing_db_entry(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1238, '1234567891014', 'test_sku_4', 'TESTASIN04', 'name4'],
                [2346, '1234567891015', 'test_sku_5', 'TESTASIN05', 'name5']
            ],
            columns=IDENTIFICATION_TABLE_COLUMNS)

        setup_database(session=db_session, sources=[
            IdentificationModel(
                variation_id=1238, ean='1234567891014', sku='test_sku_4',
                asin_all='TESTASIN04', name='name4'
            )
        ])

        expected_table = [
            IdentificationModel(
                variation_id=1238, ean='1234567891014', sku='test_sku_4',
                asin_all='TESTASIN04', name='name4'
            ),
            IdentificationModel(
                variation_id=2346, ean='1234567891015', sku='test_sku_5',
                asin_all='TESTASIN05', name='name5'
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.sync_identification_data(dataframe=dataframe)

        assert_identification_table(session=db_session,
                                    expectation=expected_table)


def assert_stock_table(session, expectation) -> None:
    assert session.query(StockModel).count() == len(expectation)
    for index, row in enumerate(session.query(StockModel).all()):
        expected_values = expectation[index]
        assert expected_values.variation_id == row.variation_id
        assert expected_values.ean == row.ean
        for num in range(1, 10):
            expect = getattr(expected_values, f'stock_warehouse_{num}')
            actual = getattr(row, f'stock_warehouse_{num}')
            assert expect == actual


def describe_add_stock():
    def with_no_changes(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1234, '1234567891010', 0, 5, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        setup_database(session=db_session, sources=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_1=0,
                stock_warehouse_2=5, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)

    def with_adjusted_stock(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1234, '1234567891010', 0, 7, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        setup_database(session=db_session, sources=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_1=0,
                stock_warehouse_2=7, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)

    def with_new_location(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1234, '1234567891010', 5, 5, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        setup_database(session=db_session, sources=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_1=5,
                stock_warehouse_2=5, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)

    def with_removed_variation(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1234, '1234567891010', 0, 5, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        setup_database(session=db_session, sources=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            ),
            StockModel(
                variation_id=2345, ean='1234567891011', stock_warehouse_1=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_1=0,
                stock_warehouse_2=5, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)

    def with_added_variation(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [1234, '1234567891010', 0, 5, 0, 0, 0, 0, 0, 0, 0],
                [2345, '1234567891011', 5, 0, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        setup_database(session=db_session, sources=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_1=0,
                stock_warehouse_2=5, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            ),
            StockModel(
                variation_id=2345, ean='1234567891011', stock_warehouse_1=5,
                stock_warehouse_2=0, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)

    def with_changed_variation_id(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [2345, '1234567891010', 0, 10, 0, 0, 0, 0, 0, 0, 0]
            ],
            columns=STOCK_TABLE_COLUMNS)

        setup_database(session=db_session, sources=[
            StockModel(
                variation_id=1234, ean='1234567891010', stock_warehouse_2=5
            )
        ])

        expected_table = [
            StockModel(
                variation_id=2345, ean='1234567891010', stock_warehouse_1=0,
                stock_warehouse_2=10, stock_warehouse_3=0, stock_warehouse_4=0,
                stock_warehouse_5=0, stock_warehouse_6=0, stock_warehouse_7=0,
                stock_warehouse_8=0, stock_warehouse_9=0
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.add_stock(dataframe=dataframe)

        assert_stock_table(session=db_session, expectation=expected_table)


def assert_order_table(session, expectation) -> None:
    assert len(expectation) == session.query(OrdersModel).count()
    order_attributes = ['origin_id', 'netto_total', 'gross_total',
                        'creation_date', 'payment_date', 'delivery_date',
                        'b2b', 'external_id']
    order_item_attributes = ['external_id', 'origin_id', 'variation_id', 'ean',
                             'item_id', 'name', 'quantity', 'netto', 'gross',
                             'statistic_entry', 'daily_statistic_entry']
    assert session.query(OrdersModel).count() == len(expectation)
    for index, row in enumerate(session.query(OrdersModel).all()):
        expected_values = expectation[index]
        for o_attr in order_attributes:
            expect = getattr(expected_values, o_attr)
            actual = getattr(row, o_attr)
            assert expect == actual
        for expect_item, actual_item in zip(expected_values.order_items,
                                            row.order_items):
            for oi_attr in order_item_attributes:
                expect = getattr(expect_item, oi_attr)
                actual = getattr(actual_item, oi_attr)
                assert expect == actual


def setup_id_table(func):
    def wrapper(db_session):
        setup_database(session=db_session, sources=[
            IdentificationModel(
                variation_id=1234, ean='1234567891011', sku='test_sku_1',
                asin_all='TESTASIN01', name='test_name1'
            ),
            IdentificationModel(
                variation_id=2345, ean='1234567891012', sku='test_sku_2',
                asin_all='TESTASIN02', name='test_name2'
            )
        ])
        func(db_session=db_session)
    return wrapper


def describe_handle_order():
    @setup_id_table
    def with_new_order_and_one_item(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [12345, 98765, 1234, 101, 'externid', 'test_name_1',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        expected_table = [
            OrdersModel(
                id=12345, origin_id=Decimal('1.0'),
                netto_total=Decimal('15.0'), gross_total=Decimal('18.0'),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=False,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal('1.0'),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal('15.0'), gross=Decimal('18.0'),
                        statistic_entry=-1, daily_statistic_entry=-1
                    )
                ]
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)

    @setup_id_table
    def with_new_order_and_two_item(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [12345, 98765, 1234, 101, 'externid', 'test_name_1',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False],
                [12345, 98766, 2345, 101, 'externid', 'test_name_2',
                 1.0, 15.0, 18.0, 2, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        expected_table = [
            OrdersModel(
                id=12345, origin_id=Decimal('1.0'),
                netto_total=Decimal('45.0'), gross_total=Decimal('54.0'),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=False,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal('1.0'),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal('15.0'), gross=Decimal('18.0'),
                        statistic_entry=-1, daily_statistic_entry=-1
                    ),
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal('1.0'),
                        variation_id=2345, ean='1234567891012', item_id=101,
                        name='test_name_2', quantity=2, netto=Decimal('15.0'),
                        gross=Decimal('18.0'), statistic_entry=-1,
                        daily_statistic_entry=-1
                    )
                ]
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)

    @setup_id_table
    def with_two_new_orders(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [12345, 98765, 1234, 101, 'externid', 'test_name_1',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False],
                [23456, 98766, 2345, 101, 'externid2', 'test_name_2',
                 4.01, 15.0, 18.0, 2, '2021-05-03T19:00:00+02:00',
                 '2021-05-06T06:00:00+02:00', '2021-05-06T09:00:00+02:00',
                 False]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        expected_table = [
            OrdersModel(
                id=12345, origin_id=Decimal('1.0'),
                netto_total=Decimal('15.0'), gross_total=Decimal('18.0'),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=False,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal('1.0'),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal('15.0'), gross=Decimal('18.0'),
                        statistic_entry=-1, daily_statistic_entry=-1
                    )
                ]
            ),
            OrdersModel(
                id=23456, origin_id=Decimal('4.01'),
                netto_total=Decimal('30.0'), gross_total=Decimal('36.0'),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 6),
                delivery_date=datetime.date(2021, 5, 6), b2b=False,
                external_id='externid2', order_items=[
                    OrderItemsModel(
                        external_id='externid2', origin_id=Decimal('4.01'),
                        variation_id=2345, ean='1234567891012', item_id=101,
                        name='test_name_2', quantity=2, netto=Decimal('15.0'),
                        gross=Decimal('18.0'), statistic_entry=-1,
                        daily_statistic_entry=-1
                    )
                ]
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)

    @setup_id_table
    def with_new_b2b_order(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [12345, 98765, 1234, 101, 'externid', 'test_name_1',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 True]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        expected_table = [
            OrdersModel(
                id=12345, origin_id=Decimal('1.0'),
                netto_total=Decimal('15.0'), gross_total=Decimal('18.0'),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=True,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal('1.0'),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal('15.0'), gross=Decimal('18.0'),
                        statistic_entry=-1, daily_statistic_entry=-1
                    )
                ]
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)

    @setup_id_table
    def with_existing_order(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [12345, 98765, 1234, 101, 'externid', 'test_name_1',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        setup_database(session=db_session, sources=[
            OrdersModel(
                id=12345, origin_id=Decimal('1.0'),
                netto_total=Decimal('15.0'), gross_total=Decimal('18.0'),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=False,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal('1.0'),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal('15.0'), gross=Decimal('18.0'),
                        statistic_entry=-1, daily_statistic_entry=-1
                    )
                ]
            )
        ])
        expected_table = [
            OrdersModel(
                id=12345, origin_id=Decimal('1.0'),
                netto_total=Decimal('15.0'), gross_total=Decimal('18.0'),
                creation_date=datetime.date(2021, 5, 3),
                payment_date=datetime.date(2021, 5, 4),
                delivery_date=datetime.date(2021, 5, 4), b2b=False,
                external_id='externid', order_items=[
                    OrderItemsModel(
                        external_id='externid', origin_id=Decimal('1.0'),
                        variation_id=1234, ean='1234567891011',
                        item_id=101, name='test_name_1', quantity=1,
                        netto=Decimal('15.0'), gross=Decimal('18.0'),
                        statistic_entry=-1, daily_statistic_entry=-1
                    )
                ]
            )
        ]

        db_service = DatabaseService(session=db_session)
        db_service.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)

    @setup_id_table
    def with_missing_variation_id(db_session) -> None:
        dataframe = pandas.DataFrame(
            [
                [34567, 98765, 3456, 101, 'externid', 'test_name_3',
                 1.0, 15.0, 18.0, 1, '2021-05-03T18:00:00+02:00',
                 '2021-05-04T06:00:00+02:00', '2021-05-04T09:00:00+02:00',
                 False]
            ],
            columns=ORDER_TABLE_COLUMNS
        )
        expected_table = []

        db_service = DatabaseService(session=db_session)
        db_service.handle_order(dataframe=dataframe)

        assert_order_table(session=db_session, expectation=expected_table)


def producer_equal(producer, expect):
    return producer.plenty_id == expect[0] and\
        producer.producer_name == expect[1] and\
        producer.delivery_time == expect[2]


def describe_sync_producers():
    def with_new_producer(db_session):
        dataframe = pandas.DataFrame([
            [123, 'producer_A', 180],
            [124, 'producer_B', 160]
        ], columns=['plenty_id', 'producer_name', 'delivery_time'])
        setup_database(session=db_session, sources=[
            ProducerModel(
                plenty_id=123, producer_name='producer_A', delivery_time=180
            )
        ])
        amount_before = db_session.query(ProducerModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_producers(dataframe=dataframe)
        assert amount_before + 1 == db_session.query(ProducerModel).count()
        assert all(
            producer_equal(producer=x, expect=y)
            for x, y in zip(
                db_session.query(ProducerModel).all(),
                [[123, 'producer_A', 180], [124, 'producer_B', 160]]
            )
        )

    def with_updated_name(db_session):
        dataframe = pandas.DataFrame([
            [123, 'producer_A_new', 180]
        ], columns=['plenty_id', 'producer_name', 'delivery_time'])
        setup_database(session=db_session, sources=[
            ProducerModel(
                plenty_id=123, producer_name='producer_A', delivery_time=180
            )
        ])
        amount_before = db_session.query(ProducerModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_producers(dataframe=dataframe)
        assert amount_before == db_session.query(ProducerModel).count()
        assert producer_equal(producer=db_session.query(ProducerModel).first(),
                              expect=[123, 'producer_A_new', 180])

    def with_updated_delivery_time(db_session):
        dataframe = pandas.DataFrame([
            [123, 'producer_A', 170],
            [124, 'producer_B', 180]
        ], columns=['plenty_id', 'producer_name', 'delivery_time'])
        setup_database(session=db_session, sources=[
            ProducerModel(
                plenty_id=123, producer_name='producer_A', delivery_time=180
            ),
            ProducerModel(
                plenty_id=124, producer_name='producer_B', delivery_time=180
            )
        ])
        amount_before = db_session.query(ProducerModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_producers(dataframe=dataframe)
        assert amount_before == db_session.query(ProducerModel).count()
        assert producer_equal(
            producer=db_session.query(ProducerModel).first(),
            expect=[123, 'producer_A', 170])
        assert producer_equal(
            producer=db_session.query(ProducerModel).filter(
                ProducerModel.plenty_id == 124).one(),
            expect=[124, 'producer_B', 180]
        )

    def with_deleted_producer(db_session):
        dataframe = pandas.DataFrame([
            [124, 'producer_B', 180]
        ], columns=['plenty_id', 'producer_name', 'delivery_time'])
        setup_database(session=db_session, sources=[
            ProducerModel(
                plenty_id=123, producer_name='producer_A', delivery_time=180
            ),
            ProducerModel(
                plenty_id=124, producer_name='producer_B', delivery_time=180
            )
        ])
        amount_before = db_session.query(ProducerModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_producers(dataframe=dataframe)
        assert amount_before - 1 == db_session.query(ProducerModel).count()
        assert producer_equal(producer=db_session.query(ProducerModel).first(),
                              expect=[124, 'producer_B', 180])


def setup_producers(func):
    def wrapper(db_session):
        setup_database(session=db_session, sources=[
            ProducerModel(plenty_id=1, producer_name='producer_A',
                          delivery_time=12),
            ProducerModel(plenty_id=2, producer_name='producer_B',
                          delivery_time=22)
        ])
        func(db_session=db_session)
    return wrapper


def information_equal(entry: VariationInformationModel, expect: list) -> bool:
    return entry.variation_id == expect[0] and \
        entry.ean == expect[1] and \
        entry.priority == expect[2] and \
        entry.producer_id == expect[3] and \
        entry.creation_date == expect[4]


def describe_sync_information_map():
    @setup_producers
    def with_new_entry(db_session):
        dataframe = pandas.DataFrame([
            [1234, '1234567891010', 1, 1, '2021-10-27T15:58:01+02:00']
        ], columns=['variation_id', 'ean', 'priority', 'producer_id',
                    'creation_date'])
        amount_before = db_session.query(VariationInformationModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_variation_information_map(dataframe=dataframe)
        assert amount_before + 1 == db_session.query(
            VariationInformationModel).count()
        entry = db_session.query(VariationInformationModel).first()
        assert information_equal(
            entry=entry,
            expect=[1234, '1234567891010', 1, 1, datetime.date(2021, 10, 27)]
        )
        assert producer_equal(producer=entry.producer,
                              expect=[1, 'producer_A', 12])

    def without_producers(db_session):
        dataframe = pandas.DataFrame([
            [1234, '1234567891010', 2, null(), '2021-10-27T15:58:01+02:00']
        ], columns=['variation_id', 'ean', 'priority', 'producer_id',
                    'creation_date'])
        amount_before = db_session.query(VariationInformationModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_variation_information_map(dataframe=dataframe)
        assert amount_before + 1 == db_session.query(
            VariationInformationModel).count()
        entry = db_session.query(VariationInformationModel).first()
        assert information_equal(
            entry=entry,
            expect=[1234, '1234567891010', 2, None,
                    datetime.date(2021, 10, 27)]
        )
        assert entry.producer is None

    @setup_producers
    def with_update(db_session):
        dataframe = pandas.DataFrame([
            [1234, '1234567891010', 2, 1, '2021-10-27T15:58:01+02:00']
        ], columns=['variation_id', 'ean', 'priority', 'producer_id',
                    'creation_date'])
        setup_database(session=db_session, sources=[
            VariationInformationModel(
                variation_id=1234, ean='1234567891010', priority=1,
                producer_id=1, creation_date=datetime.date(2021, 10, 27)
            )
        ])
        amount_before = db_session.query(VariationInformationModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_variation_information_map(dataframe=dataframe)
        assert amount_before == db_session.query(
            VariationInformationModel).count()
        entry = db_session.query(VariationInformationModel).first()
        assert information_equal(
            entry=entry,
            expect=[1234, '1234567891010', 2, 1,
                    datetime.date(2021, 10, 27)]
        )


def incoming_equal(entry: IncomingVariationModel, expect: list) -> bool:
    return entry.variation_id == expect[0] and \
        entry.ean == expect[1] and \
        entry.order_id == expect[2] and \
        entry.quantity == expect[3] and \
        entry.order_date == expect[4] and \
        entry.receive_date == expect[5] and \
        entry.producer_id == expect[6]


def describe_sync_incoming_variations():
    @setup_producers
    @setup_id_table
    def with_empty_database(db_session):
        dataframe = pandas.DataFrame([
            [1234, 123, 5, '2021-10-27T15:58:01+02:00', 1]
        ], columns=['variation_id', 'order_id', 'quantity', 'order_date',
                    'producer_id'])
        amount_before = db_session.query(IncomingVariationModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_incoming_variations(dataframe=dataframe)
        assert amount_before + 1 == \
            db_session.query(IncomingVariationModel).count()
        assert incoming_equal(
            entry=db_session.query(IncomingVariationModel).first(),
            expect=[1234, '1234567891011', 123, 5, datetime.date(2021, 10, 27),
                    None, 1]
        )

    @setup_producers
    @setup_id_table
    @freeze_time('2021-10-29')
    def with_empty_dataframe(db_session):
        dataframe = pandas.DataFrame(
            columns=['variation_id', 'order_id', 'quantity', 'order_date',
                     'producer_id'])
        setup_database(session=db_session, sources=[
            IncomingVariationModel(
                variation_id=1234, ean='1234567891011', order_id=123,
                quantity=5, order_date=datetime.date(2021, 10, 27),
                receive_date=null(), producer_id=1
            ),
            IncomingVariationModel(
                variation_id=2345, ean='1234567891012', order_id=124,
                quantity=6, order_date=datetime.date(2021, 10, 28),
                receive_date=null(), producer_id=2
            ),
            IncomingVariationModel(
                variation_id=2345, ean='1234567891012', order_id=122,
                quantity=5, order_date=datetime.date(2021, 10, 25),
                receive_date=datetime.date(2021, 10, 27), producer_id=2
            )
        ])
        amount_before = db_session.query(IncomingVariationModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_incoming_variations(dataframe=dataframe)
        expectation = [
            [1234, '1234567891011', 123, 5, datetime.date(2021, 10, 27),
             datetime.date(2021, 10, 29), 1],
            [2345, '1234567891012', 122, 5, datetime.date(2021, 10, 25),
             datetime.date(2021, 10, 27), 2],
            [2345, '1234567891012', 124, 6, datetime.date(2021, 10, 28),
             datetime.date(2021, 10, 29), 2]
        ]
        assert amount_before == \
            db_session.query(IncomingVariationModel).count()
        assert all(
            incoming_equal(entry=entry, expect=expect) for entry, expect in
            zip(db_session.query(IncomingVariationModel).all(), expectation)
        )

    @setup_producers
    @setup_id_table
    def with_new_variation(db_session):
        dataframe = pandas.DataFrame([
            [1234, 123, 5, '2021-10-27T15:58:01+02:00', 1],
            [2345, 123, 3, '2021-10-28T15:58:01+02:00', 2]
        ], columns=['variation_id', 'order_id', 'quantity', 'order_date',
                    'producer_id'])
        setup_database(session=db_session, sources=[
            IncomingVariationModel(
                variation_id=1234, ean='1234567891011', order_id=123,
                quantity=5, order_date=datetime.date(2021, 10, 27),
                receive_date=null(), producer_id=1
            )
        ])
        amount_before = db_session.query(IncomingVariationModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_incoming_variations(dataframe=dataframe)
        expectation = [
            [1234, '1234567891011', 123, 5, datetime.date(2021, 10, 27),
             None, 1],
            [2345, '1234567891012', 123, 3, datetime.date(2021, 10, 28),
             None, 2]
        ]
        assert amount_before + 1 == \
            db_session.query(IncomingVariationModel).count()
        assert all(
            incoming_equal(entry=entry, expect=expect) for entry, expect in
            zip(db_session.query(IncomingVariationModel).all(), expectation)
        )

    @setup_producers
    @setup_id_table
    def with_existing_entries_without_updates(db_session):
        dataframe = pandas.DataFrame([
            [1234, 123, 5, '2021-10-27T15:58:01+02:00', 1]
        ], columns=['variation_id', 'order_id', 'quantity', 'order_date',
                    'producer_id'])
        setup_database(session=db_session, sources=[
            IncomingVariationModel(
                variation_id=1234, ean='1234567891011', order_id=123,
                quantity=5, order_date=datetime.date(2021, 10, 27),
                receive_date=null(), producer_id=1
            )
        ])
        amount_before = db_session.query(IncomingVariationModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_incoming_variations(dataframe=dataframe)
        assert amount_before == \
            db_session.query(IncomingVariationModel).count()
        assert incoming_equal(
            entry=db_session.query(IncomingVariationModel).first(),
            expect=[1234, '1234567891011', 123, 5, datetime.date(2021, 10, 27),
                    None, 1]
        )

    @setup_producers
    @setup_id_table
    def with_existing_entries_with_updates(db_session):
        dataframe = pandas.DataFrame([
            [1234, 123, 3, '2021-10-27T15:58:01+02:00', 1],
            [2345, 124, 5, '2021-10-27T15:58:01+02:00', 2]
        ], columns=['variation_id', 'order_id', 'quantity', 'order_date',
                    'producer_id'])
        setup_database(session=db_session, sources=[
            IncomingVariationModel(
                variation_id=1234, ean='1234567891011', order_id=123,
                quantity=5, order_date=datetime.date(2021, 10, 27),
                receive_date=null(), producer_id=1
            ),
            IncomingVariationModel(
                variation_id=2345, ean='1234567891012', order_id=124,
                quantity=5, order_date=datetime.date(2021, 10, 27),
                receive_date=null(), producer_id=2
            )
        ])
        amount_before = db_session.query(IncomingVariationModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_incoming_variations(dataframe=dataframe)
        expectation = [
            [1234, '1234567891011', 123, 3, datetime.date(2021, 10, 27),
             None, 1],
            [2345, '1234567891012', 124, 5, datetime.date(2021, 10, 27),
             None, 2]
        ]
        assert amount_before == \
            db_session.query(IncomingVariationModel).count()
        assert all(
            incoming_equal(entry=entry, expect=expect) for entry, expect in
            zip(db_session.query(IncomingVariationModel).all(), expectation)
        )

    @setup_producers
    @setup_id_table
    @freeze_time('2021-10-28')
    def with_existing_entries_with_finish(db_session):
        dataframe = pandas.DataFrame([
            [2345, 124, 5, '2021-10-28T15:58:01+02:00', 2]
        ], columns=['variation_id', 'order_id', 'quantity', 'order_date',
                    'producer_id'])
        setup_database(session=db_session, sources=[
            IncomingVariationModel(
                variation_id=1234, ean='1234567891011', order_id=123,
                quantity=5, order_date=datetime.date(2021, 10, 27),
                receive_date=null(), producer_id=1
            )
        ])
        amount_before = db_session.query(IncomingVariationModel).count()
        db_service = DatabaseService(session=db_session)
        db_service.sync_incoming_variations(dataframe=dataframe)
        expectation = [
            [1234, '1234567891011', 123, 5, datetime.date(2021, 10, 27),
             datetime.date(2021, 10, 28), 1],
            [2345, '1234567891012', 124, 5, datetime.date(2021, 10, 28),
             None, 2]
        ]
        assert amount_before + 1 == \
            db_session.query(IncomingVariationModel).count()
        assert all(
            incoming_equal(entry=entry, expect=expect) for entry, expect in
            zip(db_session.query(IncomingVariationModel).all(), expectation)
        )


@pytest.fixture
def sample_id_table():
    id_table = [
        IdentificationModel(
            variation_id=1234, ean='1234567891010', sku='test_sku_0',
            asin_all='TESTASIN00', name='name0'
        ),
        IdentificationModel(
            variation_id=2345, ean='1234567891011', sku='test_sku_1',
            asin_all='TESTASIN01', name='name1'
        ),
        IdentificationModel(
            variation_id=3456, ean='1234567891012', sku='test_sku_2',
            asin_all='TESTASIN02', name='name2'
        )
    ]
    return id_table


def describe_get_id_map():
    def with_variation_id(db_session, sample_id_table: list):
        setup_database(session=db_session, sources=sample_id_table)
        id_type = 'variation_id'
        expect = {'1234': {'name': 'name0'}, '2345': {'name': 'name1'},
                  '3456': {'name': 'name2'}}
        db_service = DatabaseService(session=db_session)
        assert expect == db_service.get_id_map(id_type=id_type)

    def with_ean(db_session, sample_id_table: list):
        setup_database(session=db_session, sources=sample_id_table)
        id_type = 'ean'
        expect = {'1234567891010': {'name': 'name0', 'variation_id': '1234'},
                  '1234567891011': {'name': 'name1', 'variation_id': '2345'},
                  '1234567891012': {'name': 'name2', 'variation_id': '3456'}}
        db_service = DatabaseService(session=db_session)
        assert expect == db_service.get_id_map(id_type=id_type)

    def with_sku_and_additional(db_session, sample_id_table: list):
        setup_database(session=db_session, sources=sample_id_table)
        id_type = 'sku'
        expect = {'test_sku_0': {'ean': '1234567891010',
                                 'variation_id': '1234'},
                  'test_sku_1': {'ean': '1234567891011',
                                 'variation_id': '2345'},
                  'test_sku_2': {'ean': '1234567891012',
                                 'variation_id': '3456'}}
        db_service = DatabaseService(session=db_session)
        assert expect == db_service.get_id_map(id_type=id_type,
                                               additional_map=['ean'])

    def with_asin_without_additional(db_session, sample_id_table: list):
        setup_database(session=db_session, sources=sample_id_table)
        id_type = 'asin'
        expect = {'TESTASIN00': {'variation_id': '1234'},
                  'TESTASIN01': {'variation_id': '2345'},
                  'TESTASIN02': {'variation_id': '3456'}}
        db_service = DatabaseService(session=db_session)
        assert expect == db_service.get_id_map(id_type=id_type,
                                               additional_map=[])


@pytest.fixture()
def fake_plenty(monkeypatch):
    # pylint: disable=invalid-name
    def fake_login(self, login_method: str, login_data: dict):
        del self, login_method, login_data
        return True
    monkeypatch.setattr(plenty_api.PlentyApi, '_PlentyApi__authenticate',
                        fake_login)


@pytest.fixture()
def fake_warehouse_handler(monkeypatch):
    def init_top_level_config(name, config_type):
        return (
            {
                'warehouses': [
                    {
                        'plenty_warehouse_id': 104,
                        'database_column_name': 'stock_warehouse_1',
                        'sheet_column_name': 'ABC',
                        'gui_display_name': 'GUI-ABC',
                        'sync_with': 'plentymarkets'
                    }
                ]
            },
            '/abc/def'
        )
    monkeypatch.setattr(
        config_helper,
        'init_top_level_config',
        init_top_level_config
    )
    return config_helper.WarehouseConfigHandler()


@pytest.fixture()
def fake_collector(monkeypatch, fake_warehouse_handler):
    def fake_get_plenty_api_base_url():
        return 'www.abc.def'
    monkeypatch.setattr(
        plenty_rait.plenty_sync.service, 'get_plenty_api_base_url',
        fake_get_plenty_api_base_url
    )
    return DataCollector(
        collect_options={}, config={'base_url': 'www.abc.def'},
        warehouse_handler=fake_warehouse_handler
    )


def describe_pull_storage_locations():
    # pylint: disable=unused-argument
    def with_empty_response(fake_plenty, fake_collector, monkeypatch):
        def empty_response(self, warehouse_id):
            return []
        monkeypatch.setattr(plenty_api.PlentyApi,
                            'plenty_api_get_storagelocations', empty_response)
        result = fake_collector.pull_storage_locations(warehouse_id=104)
        expect = {}
        assert expect == result

    # pylint: disable=unused-argument
    def with_empty_locations(fake_plenty, fake_collector, monkeypatch):
        def response(self, warehouse_id):
            return [{'variationId': 1234, 'storageLocationId': 1,
                     'quantity': 1, 'warehouseId': 104},
                    {'variationId': 2345, 'storageLocationId': 2,
                     'quantity': 0, 'warehouseId': 104}]

        monkeypatch.setattr(plenty_api.PlentyApi,
                            'plenty_api_get_storagelocations', response)

        result = fake_collector.pull_storage_locations(warehouse_id=104)
        expect = {'1234': {'1': 1}}
        assert expect == result

    # pylint: disable=unused-argument
    def with_negative_locations(fake_plenty, fake_collector, monkeypatch):
        def response(self, warehouse_id):
            return [{'variationId': 1234, 'storageLocationId': 1,
                     'quantity': 1, 'warehouseId': 104},
                    {'variationId': 2345, 'storageLocationId': 2,
                     'quantity': -3, 'warehouseId': 104}]

        monkeypatch.setattr(plenty_api.PlentyApi,
                            'plenty_api_get_storagelocations', response)

        result = fake_collector.pull_storage_locations(warehouse_id=104)
        expect = {'1234': {'1': 1}}
        assert expect == result

    # pylint: disable=unused-argument
    def with_single_location(fake_plenty, fake_collector, monkeypatch):
        def response(self, warehouse_id):
            return [{'variationId': 1234, 'storageLocationId': 1,
                     'quantity': 1, 'warehouseId': 104}]

        monkeypatch.setattr(plenty_api.PlentyApi,
                            'plenty_api_get_storagelocations', response)

        result = fake_collector.pull_storage_locations(warehouse_id=104)
        expect = {'1234': {'1': 1}}
        assert expect == result

    # pylint: disable=unused-argument
    def with_multiple_locations(fake_plenty, fake_collector, monkeypatch):
        def response(self, warehouse_id):
            return [{'variationId': 1234, 'storageLocationId': 1,
                     'quantity': 1, 'warehouseId': 104},
                    {'variationId': 1234, 'storageLocationId': 2,
                     'quantity': 2, 'warehouseId': 104}]

        monkeypatch.setattr(plenty_api.PlentyApi,
                            'plenty_api_get_storagelocations', response)

        result = fake_collector.pull_storage_locations(warehouse_id=104)
        expect = {'1234': {'1': 1, '2': 2}}
        assert expect == result

    # pylint: disable=unused-argument
    def with_multiple_variations(fake_plenty, fake_collector, monkeypatch):
        def response(self, warehouse_id):
            return [{'variationId': 1234, 'storageLocationId': 1,
                     'quantity': 1, 'warehouseId': 104},
                    {'variationId': 2345, 'storageLocationId': 2,
                     'quantity': 2, 'warehouseId': 104}]

        monkeypatch.setattr(plenty_api.PlentyApi,
                            'plenty_api_get_storagelocations', response)

        result = fake_collector.pull_storage_locations(warehouse_id=104)
        expect = {'1234': {'1': 1}, '2345': {'2': 2}}
        assert expect == result

    # pylint: disable=unused-argument
    def with_restricted_variations(fake_plenty, fake_collector, monkeypatch):
        def response(self, warehouse_id):
            return [{'variationId': 1234, 'storageLocationId': 1,
                     'quantity': 1, 'warehouseId': 104},
                    {'variationId': 2345, 'storageLocationId': 2,
                     'quantity': 3, 'warehouseId': 104}]

        monkeypatch.setattr(plenty_api.PlentyApi,
                            'plenty_api_get_storagelocations', response)

        result = fake_collector.pull_storage_locations(warehouse_id=104,
                                                       variations=['2345'])
        expect = {'2345': {'2': 3}}
        assert expect == result


def find_best_fit_setup(variations: dict, request: dict,
                        priority_buffer: dict = None,
                        empty_threshold: int = 0,
                        exclusion: list = None) -> tuple:
    out_locations = {var: {} for var in variations.keys()
                     if len(variations[var]) > 0}
    i = 0
    for variation in variations:
        for location in variations[variation]:
            out_locations[variation].update({str(i): location})
            i += 1

    config = {
        'plenty_warehouse_id': 104,
        'database_column_name': 'stock_warehouse_1'
    }

    if priority_buffer or empty_threshold or exclusion:
        config['attributes'] = {}
    if priority_buffer:
        config['attributes']['variation_buffer_map'] = priority_buffer

    if empty_threshold:
        config['attributes']['empty_threshold'] = empty_threshold

    if exclusion:
        config['attributes']['empty_exclusions'] = exclusion

    return (out_locations, request, config)


def describe_find_best_fit_locations():
    # The Tests below are all without extra options like empty threshold or
    # priority buffer
    #######################################################################
    def with_no_locations():
        (loc, request, config) = find_best_fit_setup(
            {'1234': []}, request={'1234': {'quantity': 5}})
        expect = {}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_exact_match_of_one_location():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [5, 10]}, request={'1234': {'quantity': 10}})
        expect = {'1234': {'total_qty': 10, 'locations': {'1': 10}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_exact_match_of_two_locations():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [5, 20, 10, 15]}, request={'1234': {'quantity': 30}})
        expect = {'1234': {'total_qty': 30, 'locations': {'1': 20, '2': 10}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_sufficient_stock_in_one_location():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [20]}, {'1234': {'quantity': 10}})
        expect = {'1234': {'total_qty': 10, 'locations': {'0': 10}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_sufficient_stock_in_two_locations():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [20, 25]}, request={'1234': {'quantity': 35}})
        expect = {'1234': {'total_qty': 35, 'locations': {'0': 20, '1': 15}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_insufficient_stock_in_one_location():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [15]}, request={'1234': {'quantity': 20}})
        expect = {'1234': {'total_qty': 15, 'locations': {'0': 15}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_insufficient_stock_in_two_locations():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [15, 10]}, request={'1234': {'quantity': 30}})
        expect = {'1234': {'total_qty': 25, 'locations': {'0': 15, '1': 10}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_sufficient_stock_in_two_equal_locations():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [20, 20]}, request={'1234': {'quantity': 30}})
        expect = {'1234': {'total_qty': 30, 'locations': {'0': 20, '1': 10}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_variation_without_locations():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [20, 20], '2345': []},
            request={'1234': {'quantity': 30}, '2345': {'quantity': 10}})
        expect = {'1234': {'total_qty': 30, 'locations': {'0': 20, '1': 10}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    # The tests below are with an empty threshold
    #######################################################################
    def with_empty_threshold_variations_above_threshold():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [30, 20], '2345': [10, 5]},
            request={'1234': {'quantity': 40}, '2345': {'quantity': 5}},
            empty_threshold=5
        )
        expect = {'1234': {'total_qty': 40, 'locations': {'0': 20, '1': 20}},
                  '2345': {'total_qty': 5, 'locations': {'3': 5}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_empty_threshold_without_exclusions():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [30, 20], '2345': [10, 5]},
            request={'1234': {'quantity': 40}, '2345': {'quantity': 5}},
            empty_threshold=20
        )
        expect = {'1234': {'total_qty': 50, 'locations': {'0': 30, '1': 20}},
                  '2345': {'total_qty': 15, 'locations': {'2': 10, '3': 5}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_empty_threshold_with_exclusions():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [30, 20], '2345': [10, 5]},
            request={'1234': {'quantity': 40}, '2345': {'quantity': 5}},
            empty_threshold=20, exclusion=[1234]
        )
        expect = {'1234': {'total_qty': 40, 'locations': {'0': 20, '1': 20}},
                  '2345': {'total_qty': 15, 'locations': {'2': 10, '3': 5}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    # The tests below are with a priority buffer and/or general buffer
    #######################################################################
    def with_general_buffer():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [30, 20], '2345': [10, 5]},
            request={'1234': {'quantity': 40}, '2345': {'quantity': 15}}
        )
        expect = {'1234': {'total_qty': 40, 'locations': {'0': 20, '1': 20}},
                  '2345': {'total_qty': 10, 'locations': {'2': 10}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104, general_buffer=5)
        assert expect == result

    def with_no_available_stock_for_variation_after_buffer():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [10], '2345': [20]},
            request={'1234': {'quantity': 5}, '2345': {'quantity': 10}})
        expect = {'2345': {'total_qty': 10, 'locations': {'1': 10}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104, general_buffer=10)
        assert expect == result

    def with_priority_buffer():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [30, 20], '2345': [10, 5]},
            request={'1234': {'quantity': 40}, '2345': {'quantity': 15}},
            priority_buffer={'1234': 15, '2345': 10}
        )
        expect = {'1234': {'total_qty': 35, 'locations': {'0': 15, '1': 20}},
                  '2345': {'total_qty': 5, 'locations': {'3': 5}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104)
        assert expect == result

    def with_priority_and_general_buffer():
        (loc, request, config) = find_best_fit_setup(
            {'1234': [30, 20], '2345': [10, 5]},
            request={'1234': {'quantity': 40}, '2345': {'quantity': 15}},
            priority_buffer={'1234': 15, '2345': 5}
        )
        expect = {'1234': {'total_qty': 35, 'locations': {'0': 15, '1': 20}},
                  '2345': {'total_qty': 5, 'locations': {'3': 5}}}

        service = RedistributionService(source_warehouse_config=config)
        result = service.find_best_fit_locations(
            locations=loc, requests=request, source_id=104, general_buffer=10)
        assert expect == result


def describe_find_matching_location_combinations():
    def with_no_match():
        locations = {
            '1': 12,
            '2': 3
        }
        request = 18

        assert not find_matching_location_combinations(
            locations=locations, request=request)

    def with_one_location_match():
        locations = {
            '1': 12
        }
        request = 12

        assert {'1': 12} == find_matching_location_combinations(
            locations=locations, request=request)

    def with_two_location_match():
        locations = {
            '1': 12,
            '2': 3
        }
        request = 15

        assert {'1': 12, '2': 3} == find_matching_location_combinations(
            locations=locations, request=request)

    def with_three_location_match():
        locations = {
            '1': 12,
            '2': 3,
            '3': 3
        }
        request = 18

        assert {'1': 12, '2': 3, '3': 3} == find_matching_location_combinations(
            locations=locations, request=request)

    def with_multiple_matches():
        locations = {
            '1': 12,
            '2': 3,
            '3': 3,
            '4': 15,
            '5': 9
        }
        request = 15

        assert {'4': 15} == find_matching_location_combinations(
            locations=locations, request=request)

    def with_no_input_locations():
        locations = {}
        request = 18

        assert not find_matching_location_combinations(
            locations=locations, request=request)


def describe_cleanup_variations_ids():
    def with_missing_ean(db_session, sample_id_table):
        """
        In this case the variation is not changed as no match is found
        But a warning is emitted
        """
        existing = [
            OrdersModel(
                origin_id=1, netto_total=1, gross_total=1,
                creation_date=datetime.date(2021, 1, 1), b2b=False,
                external_id='1234',
                order_items=[
                    OrderItemsModel(external_id='1234', variation_id=4567,
                                    origin_id=1, ean='1234567891015',
                                    item_id=1234, quantity=2, netto=1, gross=1)
                ])
        ]
        setup_database(session=db_session, sources=existing + sample_id_table)

        before = db_session.query(OrderItemsModel).filter(
            OrderItemsModel.variation_id == 4567
        ).count()
        database = DatabaseService(session=db_session)
        database.cleanup_variation_ids()
        after = db_session.query(OrderItemsModel).filter(
            OrderItemsModel.variation_id == 4567
        ).count()

        assert before == after

    def with_match(db_session, sample_id_table):
        """
        In this case the variation is changed as we found a match in the
        identification table.
        """
        existing = [
            OrdersModel(
                origin_id=1, netto_total=1, gross_total=1,
                creation_date=datetime.date(2021, 1, 1), b2b=False,
                external_id='1234',
                order_items=[
                    OrderItemsModel(external_id='1234', variation_id=4567,
                                    origin_id=1, ean='1234567891011',
                                    item_id=1234, quantity=2, netto=1, gross=1)
                ])
        ]
        setup_database(session=db_session, sources=existing + sample_id_table)

        before = db_session.query(OrderItemsModel).filter(
            OrderItemsModel.variation_id == 4567
        ).count()
        database = DatabaseService(session=db_session)
        database.cleanup_variation_ids()
        after = db_session.query(OrderItemsModel).filter(
            OrderItemsModel.variation_id == 4567
        ).count()

        assert (before - 1) == after
        assert db_session.query(OrderItemsModel).filter(
            OrderItemsModel.variation_id == 2345
        ).count() == 1

    def with_wrong_variation_in_multiple_tables(db_session, sample_id_table):
        existing = [
            OrdersModel(
                origin_id=1, netto_total=1, gross_total=1,
                creation_date=datetime.date(2021, 1, 1), b2b=False,
                external_id='1234',
                order_items=[
                    OrderItemsModel(external_id='1234', variation_id=4567,
                                    origin_id=1, ean='1234567891011',
                                    item_id=1234, quantity=2, netto=1, gross=1)
                ]),
            ProducerModel(plenty_id=1, producer_name='test', delivery_time=10),
            IncomingVariationModel(variation_id=4567, ean='1234567891011',
                                   order_id=12345, quantity=3,
                                   order_date='2021-08-10', producer_id=1),
            VariationInformationModel(variation_id=4567, ean='1234567891011',
                                      priority=1, producer_id=1)
        ]
        setup_database(session=db_session, sources=existing + sample_id_table)

        before_old = []
        before_new = []
        for model in [OrderItemsModel, IncomingVariationModel,
                      VariationInformationModel]:
            before_old.append(db_session.query(model).filter(
                model.variation_id == 4567
            ).count())
            before_new.append(db_session.query(model).filter(
                model.variation_id == 2345
            ).count())
        database = DatabaseService(session=db_session)
        database.cleanup_variation_ids()
        after_old = []
        after_new = []
        for model in [OrderItemsModel, IncomingVariationModel,
                      VariationInformationModel]:
            after_old.append(db_session.query(model).filter(
                model.variation_id == 4567
            ).count())
            after_new.append(db_session.query(model).filter(
                model.variation_id == 2345
            ).count())

        assert all((x - 1) == y for x, y in zip(before_old, after_old))
        assert all((x + 1) == y for x, y in zip(before_new, after_new))


# pylint: disable=unused-argument
@pytest.fixture
def plenty_import(fake_plenty, fake_collector, db_session):
    database = DatabaseService(session=db_session)
    return PlentyImport(collector=fake_collector, database=database)


def describe_get_file_columns():
    def with_no_config_or_cli_values(plenty_import):
        """ Take the default values """
        config = {}
        expected_columns = {'ident_column': 'ean',
                            'ident_type': 'ean',
                            'qty_column': 'qty'}
        plenty_import.config = config
        plenty_import.file_type = 'redistribution'
        result_columns = plenty_import._PlentyImport__get_file_columns()

        assert expected_columns == result_columns

    def with_config_without_cli_values(plenty_import):
        """ Prefer the config values over the default values """
        config = {
            'redistribution_id_column': 'variation_id',
            'redistribution_id_type': 'variation_id',
            'redistribution_qty_column': 'qty_config'
        }
        expected_columns = {'ident_column': 'variation_id',
                            'ident_type': 'variation_id',
                            'qty_column': 'qty_config'}
        plenty_import.config = config
        plenty_import.file_type = 'redistribution'
        result_columns = plenty_import._PlentyImport__get_file_columns()

        assert expected_columns == result_columns

    def without_config_with_cli_values(plenty_import):
        """ Prefer the cli values over the default values """
        config = {}
        expected_columns = {'ident_column': 'ident_cli',
                            'ident_type': 'sku',
                            'qty_column': 'qty_cli'}
        plenty_import.config = config
        plenty_import.ident_column = {'name': 'ident_cli', 'type': 'sku'}
        plenty_import.qty_column = 'qty_cli'
        plenty_import.file_type = 'redistribution'
        result_columns = plenty_import._PlentyImport__get_file_columns()

        assert expected_columns == result_columns

    def with_config_with_cli_values(plenty_import):
        """ Prefer the cli values over the config and default values """
        config = {}
        config = {
            'redistribution_id_column': 'variation_id',
            'redistribution_id_type': 'variation_id',
            'redistribution_plenty_sync_qty_column': 'qty_config'
        }
        expected_columns = {'ident_column': 'asin_cli',
                            'ident_type': 'asin',
                            'qty_column': 'qty_cli'}
        plenty_import.config = config
        plenty_import.ident_column = {'name': 'asin_cli', 'type': 'asin'}
        plenty_import.qty_column = 'qty_cli'
        plenty_import.file_type = 'redistribution'
        result_columns = plenty_import._PlentyImport__get_file_columns()

        assert expected_columns == result_columns


@pytest.fixture
def id_map():
    return {
        'ean': {
            '1234567891011': {'name': 'name_1', 'variation_id': '1234'},
            '1234567891012': {'name': 'name_2', 'variation_id': '2345'},
            '1234567891013': {'name': 'name_3', 'variation_id': '3456'}
        },
        'variation_id': {
            '1234': {'name': 'name_1'},
            '2345': {'name': 'name_2'},
            '3456': {'name': 'name_3'}
        },
        'sku': {
            'testsku_1': {'name': 'name_1', 'variation_id': '1234'},
            'testsku_2': {'name': 'name_2', 'variation_id': '2345'},
            'testsku_3': {'name': 'name_3', 'variation_id': '3456'}
        }
    }


def fake_file_input(frame):
    def decorator(func):
        def wrapper(monkeypatch, plenty_import, id_map):
            def fake_file(self, dtype, sheet_name):
                del self, dtype, sheet_name
                return frame
            monkeypatch.setattr(pandas, 'read_excel', fake_file)

            def fake_encoding(path, logger):
                del path, logger
                return 'UTF-8'
            monkeypatch.setattr(plenty_rait.plenty_sync.service,
                                'get_encoding', fake_encoding)
            func(plenty_import, id_map)
        return wrapper
    return decorator


def describe_read_import_request():
    @fake_file_input(frame=pandas.DataFrame(
        [], columns=['ean', 'qty']))
    def with_empty_file(plenty_import, id_map):
        expect = {}

        assert expect == plenty_import._PlentyImport__read_import_request(
            path='abc.csv',
            columns={'ident_column': 'ean',
                     'ident_type': 'ean',
                     'qty_column': 'qty'},
            id_map=id_map['ean'], sheet='abc_sheet')

    @fake_file_input(frame=pandas.DataFrame(
        [['1234567891011', 10]], columns=['wrong', 'qty']))
    def with_wrong_header(plenty_import, id_map):
        expect = {}

        assert expect == plenty_import._PlentyImport__read_import_request(
            path='abc.csv',
            columns={'ident_column': 'ean', 'ident_type': 'ean',
                     'qty_column': 'qty'},
            id_map=id_map['ean'], sheet='abc_sheet')

    @fake_file_input(frame=pandas.DataFrame(
        [['1234567891011', 'abc']], columns=['ean', 'qty']))
    def with_non_numeric_values(plenty_import, id_map):
        expect = {}

        assert expect == plenty_import._PlentyImport__read_import_request(
            path='abc.csv',
            columns={'ident_column': 'ean',
                     'ident_type': 'ean',
                     'qty_column': 'qty'},
            id_map=id_map['ean'], sheet='abc_sheet')

    @fake_file_input(frame=pandas.DataFrame(
        [['1234567891011', 10], ['1234567891012', 15]],
        columns=['ean', 'qty']))
    def with_a_valid_file_with_ean(plenty_import, id_map):
        expect = {'1234': {'quantity': 10}, '2345': {'quantity': 15}}

        assert expect == plenty_import._PlentyImport__read_import_request(
            path='abc.csv',
            columns={'ident_column': 'ean', 'ident_type': 'ean',
                     'qty_column': 'qty'},
            id_map=id_map['ean'], sheet='abc_sheet')

    @fake_file_input(frame=pandas.DataFrame(
        [['1234', 10], ['2345', 15]],
        columns=['varid', 'qty']))
    def with_a_valid_file_with_variation_id(plenty_import, id_map):
        expect = {'1234': {'quantity': 10}, '2345': {'quantity': 15}}

        assert expect == plenty_import._PlentyImport__read_import_request(
            path='abc.csv',
            columns={'ident_column': 'varid',
                     'ident_type': 'variation_id',
                     'qty_column': 'qty'},
            id_map=id_map['variation_id'], sheet='abc_sheet')

    @fake_file_input(frame=pandas.DataFrame(
        [['testsku_1', 10], ['testsku_3', 15]],
        columns=['skucolumn', 'qty']))
    def with_a_valid_file_with_sku(plenty_import, id_map):
        expect = {'1234': {'quantity': 10}, '3456': {'quantity': 15}}

        assert expect == plenty_import._PlentyImport__read_import_request(
            path='abc.csv',
            columns={'ident_column': 'skucolumn',
                     'ident_type': 'sku',
                     'qty_column': 'qty'},
            id_map=id_map['sku'], sheet='abc_sheet')

    @fake_file_input(frame=pandas.DataFrame(
        [['testsku_1', 10, 'extra'], ['testsku_3', 15, 'extra_1']],
        columns=['skucolumn', 'qty', 'extracolumn']))
    def with_extra_column(plenty_import, id_map):
        expect = {'1234': {'extracolumn': 'extra', 'quantity': 10},
                  '3456': {'extracolumn': 'extra_1', 'quantity': 15}}

        assert expect == plenty_import._PlentyImport__read_import_request(
            path='abc.csv',
            columns={'ident_column': 'skucolumn',
                     'ident_type': 'sku',
                     'qty_column': 'qty'},
            id_map=id_map['sku'], sheet='abc_sheet',
            extra_columns=['extracolumn'])
