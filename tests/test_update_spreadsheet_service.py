# pylint: disable=unused-variable, redefined-outer-name
# pylint: disable=missing-function-docstring
import copy
from openpyxl import Workbook
from openpyxl.utils.dataframe import dataframe_to_rows
import pytest
import pandas
from pandas.testing import assert_frame_equal

from plenty_rait.update_spreadsheet.service import (
    get_formulas, remove_deprecated_rows, update_old_identification_values,
    remove_duplicate_rows, insert_new_rows, apply_formulas
)


EXCEL_COLUMNS = [
    'variation_id', 'ean', 'asin', 'name', 'sku', 'stock',
    'estimated stock at arrival', 'target stock', 'order suggestion',
    'order suggestion (rounded)', 'adjustment (constant)',
    'adjustment (percentage)', 'adjustment (total)', 'months to produce',
    'months to ship', 'buffer'
]
FORMULA_COLUMNS = EXCEL_COLUMNS[5:10]

DATABASE_COLUMNS = [
    'variation_id', 'ean', 'sku', 'asin', 'name'
]


@pytest.fixture
def empty_formulas() -> dict:
    formulas = {
        'stock': '',
        'estimated stock at arrival': '',
        'target stock': '',
        'order suggestion': '',
        'order suggestion (rounded)': ''
    }
    return formulas


@pytest.fixture
def sample_sheet_config() -> dict:
    sheet_config = {
        'file': '',
        'sheets': {},
        'stock_columns': {},
        'predicted_months': 13
    }
    return sheet_config


@pytest.fixture
def valid_formulas(empty_formulas) -> dict:
    formulas = empty_formulas
    for key in formulas:
        formulas[key] = '=SUM($A1, $B1)'
    return formulas


@pytest.fixture
def sample_source_data() -> pandas.DataFrame:
    llist = [
        ['1234', '1234567891011', 'test_sku', 'B012345678', 'test_name'],
        ['2345', '1234567891012', 'test_sku_1', 'B012345679', 'test_name_1'],
        ['4567', '1234567891014', 'test_sku_3', 'B012345681', 'test_name_3'],
        ['5678', '1234567891015', 'test_sku_4', 'B012345682', 'test_name_4']
    ]
    return pandas.DataFrame(llist, columns=DATABASE_COLUMNS)


@pytest.fixture
def sample_sheet_data() -> pandas.DataFrame:
    llist = [
        ['1234', '1234567891011',  'B012345678', 'test_name', 'test_sku',
         '=SUM($A1, $B1)', '=SUM($A1, $B1)', '=SUM($A1, $B1)',
         '=SUM($A1, $B1)', '=SUM($A1, $B1)', '', '', '', '2', '3', '20'],
        ['2345', '1234567891012',  'B012345679', 'test_name_1', 'test_sku_1',
         '=SUM($A2, $B2)', '=SUM($A2, $B2)', '=SUM($A2, $B2)',
         '=SUM($A2, $B2)', '=SUM($A2, $B2)', '', '', '', '2', '3', '20']
    ]
    return pandas.DataFrame(llist, columns=EXCEL_COLUMNS)


@pytest.fixture
def sample_sheet_data_with_gap() -> pandas.DataFrame:
    llist = [
        ['1234', '1234567891011',  'B012345678', 'test_name', 'test_sku',
         '=SUM($A1, $B1)', '=SUM($A1, $B1)', '=SUM($A1, $B1)',
         '=SUM($A1, $B1)', '=SUM($A1, $B1)', '', '', '', '2', '3', '20'],
        ['6789', '1234567891016',  'B012345683', 'test_name_5', 'test_sku_5',
         '=SUM($A2, $B2)', '=SUM($A2, $B2)', '=SUM($A2, $B2)',
         '=SUM($A2, $B2)', '=SUM($A2, $B2)', '', '', '', '2', '3', '20']
    ]
    return pandas.DataFrame(llist, columns=EXCEL_COLUMNS)


@pytest.fixture
def sample_sheet_data_with_deprecated_variations(
        sample_sheet_data) -> pandas.DataFrame:
    sample_sheet_data = sample_sheet_data.append({
        'variation_id': '3456', 'ean': '1234567891013', 'asin': 'B012345680',
        'name': 'test_name_2', 'sku': 'test_sku_2',
        'stock': '=SUM($A3, $B3)',
        'estimated stock at arrival': '=SUM($A3, $B3)',
        'target stock': '=SUM($A3, $B3)',
        'order suggestion': '=SUM($A3, $B3)',
        'order suggestion (rounded)': '=SUM($A3, $B3)',
        'adjustment (constant)': '',
        'adjustment (percentage)': '',
        'adjustment (total)': '',
        'months to produce': '2',
        'months to ship': '3',
        'buffer': '20'
    }, ignore_index=True)
    sample_sheet_data.reset_index(drop=True, inplace=True)
    return sample_sheet_data


@pytest.fixture
def sample_sheet_data_with_incomplete_variations(
        sample_sheet_data) -> pandas.DataFrame:
    sample_sheet_data = sample_sheet_data.append({
        'variation_id': '3456', 'ean': '1234567891013', 'asin': 'B012345680',
        'name': 'test_name_2', 'sku': 'test_sku_2',
        'stock': '',
        'estimated stock at arrival': '',
        'target stock': '',
        'order suggestion': '',
        'order suggestion (rounded)': '',
        'adjustment (constant)': '',
        'adjustment (percentage)': '',
        'adjustment (total)': '',
        'months to produce': '',
        'months to ship': '',
        'buffer': ''
    }, ignore_index=True)
    sample_sheet_data = sample_sheet_data.append({
        'variation_id': '4567', 'ean': '1234567891014', 'asin': 'B012345681',
        'name': 'test_name_3', 'sku': 'test_sku_3',
        'stock': '',
        'estimated stock at arrival': '',
        'target stock': '',
        'order suggestion': '',
        'order suggestion (rounded)': '',
        'adjustment (constant)': '',
        'adjustment (percentage)': '',
        'adjustment (total)': '',
        'months to produce': '',
        'months to ship': '',
        'buffer': ''
    }, ignore_index=True)
    return sample_sheet_data


@pytest.fixture
def sample_sheet_data_with_deprecated_data(
        sample_sheet_data) -> pandas.DataFrame:
    sample_sheet_data.loc[0, 'ean'] = '1234567891009'
    sample_sheet_data.loc[1, 'name'] = 'old_name_2'
    return sample_sheet_data


@pytest.fixture
def expected_sheet_data_after_insertion() -> pandas.DataFrame:
    llist = [
        ['1234', '1234567891011',  'B012345678', 'test_name', 'test_sku',
         '=SUM($A1, $B1)', '=SUM($A1, $B1)', '=SUM($A1, $B1)',
         '=SUM($A1, $B1)', '=SUM($A1, $B1)', '', '', '', '2', '3', '20'],
        ['2345', '1234567891012',  'B012345679', 'test_name_1', 'test_sku_1',
         '=SUM($A2, $B2)', '=SUM($A2, $B2)', '=SUM($A2, $B2)',
         '=SUM($A2, $B2)', '=SUM($A2, $B2)', '', '', '', '2', '3', '20'],
        ['4567', '1234567891014', 'B012345681', 'test_name_3', 'test_sku_3',
         '', '', '', '', '', '', '', '', '', '', ''],
        ['5678', '1234567891015', 'B012345682', 'test_name_4', 'test_sku_4',
         '', '', '', '', '', '', '', '', '', '', '']
    ]
    return pandas.DataFrame(llist, columns=EXCEL_COLUMNS)


@pytest.fixture
def expected_sheet_data_with_gap_after_insertion() -> pandas.DataFrame:
    llist = [
        ['1234', '1234567891011',  'B012345678', 'test_name', 'test_sku',
         '=SUM($A1, $B1)', '=SUM($A1, $B1)', '=SUM($A1, $B1)',
         '=SUM($A1, $B1)', '=SUM($A1, $B1)', '', '', '', '2', '3', '20'],
        ['2345', '1234567891012',  'B012345679', 'test_name_1', 'test_sku_1',
         '', '', '', '', '', '', '', '', '', '', ''],
        ['4567', '1234567891014', 'B012345681', 'test_name_3', 'test_sku_3',
         '', '', '', '', '', '', '', '', '', '', ''],
        ['5678', '1234567891015', 'B012345682', 'test_name_4', 'test_sku_4',
         '', '', '', '', '', '', '', '', '', '', ''],
        ['6789', '1234567891016',  'B012345683', 'test_name_5', 'test_sku_5',
         '=SUM($A2, $B2)', '=SUM($A2, $B2)', '=SUM($A2, $B2)',
         '=SUM($A2, $B2)', '=SUM($A2, $B2)', '', '', '', '2', '3', '20']
    ]
    return pandas.DataFrame(llist, columns=EXCEL_COLUMNS)


def describe_remove_deprecated_rows() -> None:
    def with_no_deprecated_row(sample_sheet_data, sample_source_data):
        expected_df = copy.deepcopy(sample_sheet_data)
        result_df = remove_deprecated_rows(
            source_df=sample_source_data, sheet_df=sample_sheet_data)

        assert_frame_equal(expected_df, result_df)

    def with_one_deprecated_row(sample_sheet_data_with_deprecated_variations,
                                sample_source_data):
        sheet_data = sample_sheet_data_with_deprecated_variations
        expected_df = copy.deepcopy(sheet_data)
        expected_df = expected_df.drop(2)
        expected_df = expected_df.reset_index(drop=True)

        result_df = remove_deprecated_rows(
            source_df=sample_source_data, sheet_df=sheet_data)

        assert_frame_equal(expected_df, result_df)


def describe_update_old_identification_values() -> None:
    def with_no_old_values(sample_sheet_data, sample_source_data):
        expected_df = copy.deepcopy(sample_sheet_data)
        result_df = update_old_identification_values(
            source_df=sample_source_data, sheet_df=sample_sheet_data)

        assert_frame_equal(expected_df, result_df)

    def with_old_values(sample_sheet_data_with_deprecated_data,
                        sample_source_data):
        sheet_data = sample_sheet_data_with_deprecated_data
        expected_df = copy.deepcopy(sheet_data)
        expected_df.loc[0, 'ean'] = '1234567891011'
        expected_df.loc[1, 'name'] = 'test_name_1'

        result_df = update_old_identification_values(
                source_df=sample_source_data, sheet_df=sheet_data)

        assert_frame_equal(expected_df, result_df)


def describe_remove_duplicate_rows() -> None:
    def with_no_duplicates(sample_sheet_data, sample_source_data):
        expected_df = copy.deepcopy(sample_source_data)
        sample_sheet_data = sample_sheet_data[0:0]

        result_df = remove_duplicate_rows(source_df=sample_source_data,
                                          sheet_df=sample_sheet_data)

        assert_frame_equal(expected_df, result_df)

    def with_duplicates(sample_sheet_data, sample_source_data):
        expected_df = copy.deepcopy(sample_source_data)
        expected_df = expected_df[2:]
        expected_df = expected_df.reset_index(drop=True)

        result_df = remove_duplicate_rows(source_df=sample_source_data,
                                          sheet_df=sample_sheet_data)

        assert_frame_equal(expected_df, result_df)


def describe_insert_new_rows() -> None:
    def with_no_new_rows(sample_sheet_data, sample_source_data):
        expected_df = copy.deepcopy(sample_sheet_data)
        source_df = sample_source_data[0:0]

        result_df = insert_new_rows(source_df=source_df,
                                    sheet_df=sample_sheet_data)

        assert_frame_equal(expected_df, result_df)

    def with_new_rows(sample_sheet_data, sample_source_data,
                      expected_sheet_data_after_insertion):
        source_df = sample_source_data
        source_df = source_df.drop([0, 1]).reset_index(drop=True)
        expected_df = expected_sheet_data_after_insertion

        result_df = insert_new_rows(source_df=source_df,
                                    sheet_df=sample_sheet_data)

        assert_frame_equal(expected_df, result_df)

    def with_new_rows_in_the_middle(
            sample_sheet_data_with_gap, sample_source_data,
            expected_sheet_data_with_gap_after_insertion):
        source_df = sample_source_data
        source_df = source_df.drop([0]).reset_index(drop=True)
        expected_df = expected_sheet_data_with_gap_after_insertion

        result_df = insert_new_rows(source_df=source_df,
                                    sheet_df=sample_sheet_data_with_gap)

        assert_frame_equal(expected_df, result_df)


def describe_apply_formulas() -> None:
    def with_empty_formulas(sample_sheet_data_with_incomplete_variations,
                            empty_formulas):
        sheet_data = sample_sheet_data_with_incomplete_variations
        expected_df = copy.deepcopy(sheet_data)
        expected_df[FORMULA_COLUMNS] = ''

        result_df = apply_formulas(
            sheet_df=sample_sheet_data_with_incomplete_variations,
            formulas=empty_formulas)

        assert_frame_equal(expected_df, result_df)

    def with_valid_formulas(sample_sheet_data_with_incomplete_variations,
                            valid_formulas):
        sheet_data = sample_sheet_data_with_incomplete_variations
        expected_df = copy.deepcopy(sheet_data)
        expected_formulas = []
        for i in range(1, 5):
            sub_set = len(FORMULA_COLUMNS) * [str(f'=SUM($A{i}, $B{i})')]
            expected_formulas.append(sub_set)
        expected_df[FORMULA_COLUMNS] = expected_formulas

        result_df = apply_formulas(
            sheet_df=sample_sheet_data_with_incomplete_variations,
            formulas=valid_formulas)
        assert_frame_equal(expected_df, result_df)


@pytest.fixture()
def dummy_sheet():
    workbook = Workbook()
    return workbook.active

def fake_sheet_content(data_dict):
    def decorator(func):
        def wrapper(dummy_sheet):
            dataframe = pandas.DataFrame.from_dict(data_dict)
            for row in dataframe_to_rows(dataframe, index=False, header=True):
                dummy_sheet.append(row)
            func(dummy_sheet)
        return wrapper
    return decorator

def describe_get_formulas() -> None:
    @fake_sheet_content(data_dict={
        'a': {0: 12, 1: 14},
        'b': {0: 'test', 1: 'tester'}
    })
    def with_no_formulas(dummy_sheet) -> None:
        assert not get_formulas(sheet=dummy_sheet)

    @fake_sheet_content(data_dict={
        'a': {0: 12, 1: 14},
        'b': {0: '=VLOOKUP($A2, named_range, 2, 0)',
              1: '=VLOOKUP($A3, named_range, 2, 0)'},
        'c': {0: '=SUM($A2:$B2)', 1: '=SUM($A3:$B3)'}
    })
    def with_valid_formulas(dummy_sheet) -> None:
        assert get_formulas(sheet=dummy_sheet) == {
            'b': '=VLOOKUP($A2, named_range, 2, 0)',
            'c': '=SUM($A2:$B2)'
        }

    @fake_sheet_content(data_dict={
        'a': {0: 12, 1: 14},
        'b': {0: '=VLOOKUP($A100, named_range, 2, 0)',
              1: '=VLOOKUP($A101, named_range, 2, 0)'},
        'c': {0: '=SUM($A100:$B100)', 1: '=SUM($A101:$B101)'}
    })
    def with_incorrect_row_indeces_in_formulas(dummy_sheet) -> None:
        assert get_formulas(sheet=dummy_sheet) == {
            'b': '=VLOOKUP($A2, named_range, 2, 0)',
            'c': '=SUM($A2:$B2)'
        }

    @fake_sheet_content(data_dict={
        'a': {0: 12, 1: 14},
        'b': {0: '=VLOOKUP(A100, named_range, 2, 0)',
              1: '=VLOOKUP(A101, named_range, 2, 0)'},
        'c': {0: '=SUM(A100:B100)', 1: '=SUM(A101:B101)'}
    })
    def with_formulas_without_dollar_signs(dummy_sheet) -> None:
        assert get_formulas(sheet=dummy_sheet) == {
            'b': '=VLOOKUP(A2, named_range, 2, 0)',
            'c': '=SUM(A2:B2)'
        }
