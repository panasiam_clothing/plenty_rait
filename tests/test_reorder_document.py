# pylint: disable=redefined-outer-name,missing-function-docstring,unused-variable
import pytest
import pandas
from pandas.testing import assert_frame_equal

from plenty_rait.reorder_document.service import DataFrameBuilder


@pytest.fixture
def sample_reorder() -> dict:
    return {
            'amounts': [{'currency': 'EUR', 'grossTotal': 137.9,
                         'id': 45161, 'invoiceTotal': 137.9, 'isNet': True,
                         'isSystemCurrency': True, 'netTotal': 137.9,
                         'orderId': 12345
                         }],
            'dates': [{'date': '1999-01-01T01:00:00+02:00', 'orderId': 12345,
                       'typeId': 16},
                      {'date': '1999-01-01T01:00:00+02:00', 'orderId': 12345,
                       'typeId': 2},
                      {'date': '1999-01-01T01:00:00+02:00', 'orderId': 12345,
                       'typeId': 4}],
            'id': 12345, 'locationId': None, 'lockStatus': 'unlocked',
            'orderItems': [
                {'amounts': [{'currency': 'EUR', 'id': 88680,
                              'isPercentage': True, 'isSystemCurrency': True,
                              'orderItemId': 99998, 'priceGross': 19.7,
                              'priceNet': 19.7, 'priceOriginalGross': 19.7,
                              'priceOriginalNet': 19.7,
                              'updatedAt': '1999-01-01T01:00:00+02:00'}],
                 'attributeValues': 'Blue',
                 'id': 98765, 'itemVariationId': 1111, 'orderId': 12345,
                 'orderItemName': 'test_item_1', 'quantity': 4,
                 'typeId': 1},
                {'amounts': [{'currency': 'EUR', 'id': 88681,
                              'isPercentage': True, 'isSystemCurrency': True,
                              'orderItemId': 99999, 'priceGross': 20.7,
                              'priceNet': 20.7, 'priceOriginalGross': 20.7,
                              'priceOriginalNet': 20.7,
                              'updatedAt': '1999-01-01T01:00:00+02:00'}],
                 'attributeValues': 'Red',
                 'id': 98766, 'itemVariationId': 2222, 'orderId': 12345,
                 'orderItemName': 'test_item_2', 'quantity': 3,
                 'typeId': 1}],
            'orderReferences': [], 'ownerId': '1', 'plentyId': 12345,
            'properties': [{'orderId': 12345, 'typeId': 6, 'value': 'de'},
                           {'orderId': 12345, 'typeId': 4, 'value': 'unpaid'}],
            'referrerId': 0,
            'relations': [{'orderId': 12345, 'referenceId': 101,
                           'referenceType': 'warehouse', 'relation': 'sender'},
                          {'orderId': 12345, 'referenceId': 102,
                           'referenceType': 'contact',
                           'relation': 'receiver'}
                          ],
            'statusId': 18.1, 'statusName': 'test status', 'typeId': 15
        }


@pytest.fixture
def sample_variation_data() -> list:
    return [
        {
            'id': 1111,
            'isActive': True,
            'isMain': False,
            'itemId': 12345,
            'mainVariationId': 1000,
            'name': 'Test product number 1',
            'number': 'test_sku_1',
            'position': 1,
        },
        {
            'id': 2222,
            'isActive': True,
            'isMain': False,
            'itemId': 23456,
            'mainVariationId': 2000,
            'name': 'Test product number 2',
            'number': 'test_sku_2',
            'position': 2,
        }
    ]


@pytest.fixture
def sample_selections() -> pandas.DataFrame:
    return pandas.DataFrame(
        [
            [2, 1, 'test_selection_1'],
            [2, 2, 'test_selection_1_2'],
            [3, 3, 'test_selection_2']
        ], columns=['property_id', 'selection_id', 'value']
    )


@pytest.fixture
def sample_database_mapping() -> dict:
    return {
        1111: {'ean': '1234567891011', 'sku': 'test_sku_1'},
        2222: {'ean': '1234567891012', 'sku': 'test_sku_2'}
    }


@pytest.fixture
def sample_features() -> list:
    return [
        [
            {
                'propertyId': 1,
                'propertyRelation': {
                    'cast': 'shortText'
                },
                'relationValues': [
                    {
                        'lang': 'DE',
                        'value': 'test_feature_1'
                    },
                    {
                        'lang': 'EN',
                        'value': 'test_feature_1_en'
                    }
                ],
            },
            {
                'propertyId': 2,
                'propertyRelation': {
                    'cast': 'selection'
                },
                'relationValues': [
                    {
                        'lang': '0', 'value': '1'
                    }
                ]
            }
        ],
        [
            {
                'propertyId': 1,
                'propertyRelation': {
                    'cast': 'shortText'
                },
                'relationValues': [
                    {
                        'lang': 'DE',
                        'value': 'test_feature_2'
                    },
                    {
                        'lang': 'EN',
                        'value': 'test_feature_2_en'
                    }
                ],
            },
            {
                'propertyId': 2,
                'propertyRelation': {
                    'cast': 'selection'
                },
                'relationValues': [
                    {
                        'lang': '0', 'value': '1'
                    }
                ]
            }
        ]
    ]


def describe_build_dataframe() -> None:
    def with_empty_columns(
        sample_reorder: dict, sample_variation_data: list
    ) -> None:
        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'test1',
                    'type': 'empty'
                },
                {
                    'name': 'test2',
                    'type': 'empty'
                }
            ]
        }
        expectation = pandas.DataFrame(
            [
                ['', ''],
                ['', '']
            ], columns=['test1', 'test2']
        )
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data
        )
        result = builder.build_dataframe()
        assert_frame_equal(result, expectation)

    def with_features(
        sample_reorder: dict, sample_variation_data: list,
        sample_selections: pandas.DataFrame, sample_features: list
    ) -> None:
        variations = sample_variation_data
        for variation, feature in zip(variations, sample_features):
            variation['properties'] = feature
        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'feature1',
                    'type': 'feature',
                    'feature_id': 1
                },
                {
                    'name': 'feature2',
                    'type': 'feature',
                    'feature_id': 2
                },
                {
                    'name': 'test3',
                    'type': 'empty'
                }
            ]
        }
        expectation = pandas.DataFrame(
            [
                ['test_feature_1', 'test_selection_1', ''],
                ['test_feature_2', 'test_selection_1', '']
            ], columns=['feature1', 'feature2', 'test3']
        )
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data,
            selection_map=sample_selections
        )
        result = builder.build_dataframe()
        assert_frame_equal(result, expectation)

    def with_pictures(
        sample_reorder: dict, sample_variation_data: list
    ) -> None:
        variations = sample_variation_data
        variations[0]['images'] = [
            {
                'url': 'https://test_image_1.jpg',
            },
            {
                'url': 'https://test_image_1_1.jpg',
            }
        ]
        variations[1]['images'] = [
            {
                'url': 'https://test_image_2.jpg',
            },
            {
                'url': 'https://test_image_2_1.jpg',
            }
        ]
        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'image1',
                    'type': 'picture',
                    'picture_index': 0
                },
                {
                    'name': 'image2',
                    'type': 'picture',
                    'picture_index': 1
                },
                {
                    'name': 'test3',
                    'type': 'empty'
                }
            ]
        }
        expectation = pandas.DataFrame(
            [
                ['https://test_image_1.jpg', 'https://test_image_1_1.jpg', ''],
                ['https://test_image_2.jpg', 'https://test_image_2_1.jpg', '']
            ], columns=['image1', 'image2', 'test3']
        )
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data
        )
        result = builder.build_dataframe()
        assert_frame_equal(result, expectation)

    def with_identification(
        sample_reorder: dict, sample_variation_data: list,
        sample_database_mapping: dict
    ) -> None:
        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'ident1',
                    'type': 'identification',
                    'identification_type': 'ean'
                },
                {
                    'name': 'ident2',
                    'type': 'identification',
                    'identification_type': 'sku'
                },
                {
                    'name': 'test3',
                    'type': 'empty'
                }
            ]
        }
        expectation = pandas.DataFrame(
            [
                ['1234567891011', 'test_sku_1', ''],
                ['1234567891012', 'test_sku_2', '']
            ], columns=['ident1', 'ident2', 'test3']
        )
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data,
            ident_mapping=sample_database_mapping
        )
        result = builder.build_dataframe()
        assert_frame_equal(result, expectation)

    def with_reorder_data(
        sample_reorder: dict, sample_variation_data: list
    ) -> None:
        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'quantity',
                    'type': 'reorder_data',
                    'field_name': 'quantity'
                },
                {
                    'name': 'test2',
                    'type': 'empty'
                }
            ]
        }
        expectation = pandas.DataFrame(
            [
                [4, ''],
                [3, '']
            ], columns=['quantity', 'test2']
        )
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data,
        )
        result = builder.build_dataframe()
        assert_frame_equal(result, expectation)

    def with_missing_feature(
        sample_reorder: dict, sample_variation_data: list,
        sample_selections: pandas.DataFrame, sample_features: list
    ) -> None:
        variations = sample_variation_data
        sample_features[0][1]['propertyId'] = 3
        sample_features[0][1]['relationValues'][0]['value'] = '2'
        sample_features[1][1]['propertyId'] = 3
        sample_features[1][1]['relationValues'][0]['value'] = '2'
        for variation, feature in zip(variations, sample_features):
            variation['properties'] = feature
        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'feature1',
                    'type': 'feature',
                    'feature_id': 1
                },
                {
                    'name': 'feature2',
                    'type': 'feature',
                    'feature_id': 2
                },
                {
                    'name': 'test3',
                    'type': 'empty'
                }
            ]
        }
        expectation = pandas.DataFrame(
            [
                ['test_feature_1', '', ''],
                ['test_feature_2', '', '']
            ], columns=['feature1', 'feature2', 'test3']
        )
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data,
            selection_map=sample_selections
        )
        result = builder.build_dataframe()
        assert_frame_equal(result, expectation)

    def with_missing_datbase_identification(
        sample_reorder: dict, sample_variation_data: list,
        sample_database_mapping: dict
    ) -> None:
        sample_database_mapping[1111]['asin_all'] = ''
        sample_database_mapping.pop(2222)
        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'ident1',
                    'type': 'identification',
                    'identification_type': 'ean'
                },
                {
                    'name': 'ident2',
                    'type': 'identification',
                    'identification_type': 'asin_all'
                },
                {
                    'name': 'test3',
                    'type': 'empty'
                }
            ]
        }
        expectation = pandas.DataFrame(
            [
                ['1234567891011', '', ''],
                ['', '', '']
            ], columns=['ident1', 'ident2', 'test3']
        )
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data,
            ident_mapping=sample_database_mapping
        )
        result = builder.build_dataframe()
        assert_frame_equal(result, expectation)

    def with_no_pictures(
        sample_reorder: dict, sample_variation_data: list,
    ) -> None:
        variations = sample_variation_data
        variations[0]['images'] = [
            {
                'url': 'https://test_image_1.jpg',
            },
            {
                'url': 'https://test_image_1_1.jpg',
            }
        ]
        variations[1]['images'] = []
        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'image1',
                    'type': 'picture',
                    'picture_index': 0
                },
                {
                    'name': 'image2',
                    'type': 'picture',
                    'picture_index': 1
                },
                {
                    'name': 'test3',
                    'type': 'empty'
                }
            ]
        }
        expectation = pandas.DataFrame(
            [
                ['https://test_image_1.jpg', 'https://test_image_1_1.jpg', ''],
                ['', '', '']
            ], columns=['image1', 'image2', 'test3']
        )
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data
        )
        result = builder.build_dataframe()
        assert_frame_equal(result, expectation)

    def with_fallback_value(
        sample_reorder: dict, sample_variation_data: list,
        sample_features: list, sample_database_mapping: dict,
        sample_selections: pandas.DataFrame
    ) -> None:
        sample_database_mapping[1111]['name'] = 'test_name_1'
        sample_database_mapping[2222]['name'] = 'test_name_2'
        variations = sample_variation_data
        sample_features[0][1]['propertyId'] = 3
        sample_features[0][1]['relationValues'][0]['value'] = '2'
        sample_features[1][1]['propertyId'] = 3
        sample_features[1][1]['relationValues'][0]['value'] = '2'
        for variation, feature in zip(variations, sample_features):
            variation['properties'] = feature

        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'feature1',
                    'type': 'feature',
                    'feature_id': 1
                },
                {
                    'name': 'feature2',
                    'type': 'feature',
                    'feature_id': 2,
                    'fallback': {
                        'type': 'identification',
                        'identification_type': 'name'
                    }
                },
                {
                    'name': 'test3',
                    'type': 'empty'
                }
            ]
        }
        expectation = pandas.DataFrame(
            [
                ['test_feature_1', 'test_name_1', ''],
                ['test_feature_2', 'test_name_2', '']
            ], columns=['feature1', 'feature2', 'test3']
        )
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data,
            selection_map=sample_selections,
            ident_mapping=sample_database_mapping
        )
        result = builder.build_dataframe()
        assert_frame_equal(result, expectation)

    def with_missing_value_and_missing_fallback(
        sample_reorder: dict, sample_variation_data: list,
        sample_features: list, sample_database_mapping: dict,
        sample_selections: pandas.DataFrame
    ) -> None:
        variations = sample_variation_data
        sample_features[0][1]['propertyId'] = 3
        sample_features[0][1]['relationValues'][0]['value'] = '2'
        sample_features[1][1]['propertyId'] = 3
        sample_features[1][1]['relationValues'][0]['value'] = '2'
        for variation, feature in zip(variations, sample_features):
            variation['properties'] = feature

        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'feature1',
                    'type': 'feature',
                    'feature_id': 1
                },
                {
                    'name': 'feature2',
                    'type': 'feature',
                    'feature_id': 2,
                    'fallback': {
                        'type': 'identification',
                        'identification_type': 'name'
                    }
                },
                {
                    'name': 'test3',
                    'type': 'empty'
                }
            ]
        }
        expectation = pandas.DataFrame(
            [
                ['test_feature_1', '', ''],
                ['test_feature_2', '', '']
            ], columns=['feature1', 'feature2', 'test3']
        )
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data,
            selection_map=sample_selections,
            ident_mapping=sample_database_mapping
        )
        result = builder.build_dataframe()
        assert_frame_equal(result, expectation)

    def with_invalid_field_name(
        sample_reorder: dict, sample_variation_data: list,
    ) -> None:
        config = {
            'export_language': 'de',
            'columns': [
                {
                    'name': 'this_can_not_work',
                    'type': 'reorder_data',
                    'field_name': 'just_wrong'
                },
                {
                    'name': 'test2',
                    'type': 'empty'
                }
            ]
        }
        builder = DataFrameBuilder(
            config=config, reorder_data=sample_reorder,
            variation_data=sample_variation_data,
        )
        with pytest.raises(RuntimeError):
            builder.build_dataframe()
