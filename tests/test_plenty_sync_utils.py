# pylint: disable=unused-variable, redefined-outer-name, protected-access
# pylint: disable=missing-function-docstring
import pytest
from plenty_rait.plenty_sync.utils import (
    PlentySyncConfigHandler, MissingPriorityBufferMapping,
)
import plenty_rait.helper.config_helper


@pytest.fixture()
def dummy_warehouse_handler(monkeypatch):
    def init_top_level_config(name, config_type):
        del name, config_type
        return (
            {
                'warehouses': [
                    {
                        'plenty_warehouse_id': 104,
                        'database_column_name': 'stock_warehouse_1',
                        'sheet_column_name': 'ABC',
                        'gui_display_name': 'GUI-ABC',
                        'sync_with': 'plentymarkets'
                    }
                ]
            },
            '/abc/def'
        )
    monkeypatch.setattr(
        plenty_rait.helper.config_helper,
        'init_top_level_config',
        init_top_level_config
    )


@pytest.fixture
def dummy_config_handler(monkeypatch, dummy_warehouse_handler):
    def init_config(name, config_type):
        del name, config_type
        return ''
    monkeypatch.setattr(plenty_rait.helper.config_helper,
                        'init_config', init_config)

    def read_configuration(path, config_type):
        del path, config_type
        return {
            'base_url': 'abc.com',
            'plenty_id': 12345,
            'ean_barcode_id': 1,
            'asin_id': 0,
            'b2b_customer_id': 3
        }
    monkeypatch.setattr(plenty_rait.helper.config_helper,
                        'read_configuration',
                        read_configuration)

    def _validate_configuration(self):
        del self

    monkeypatch.setattr(PlentySyncConfigHandler, '_validate_configuration',
                        _validate_configuration)
    return PlentySyncConfigHandler(
        name='abc', config_type='json',
        config_schema=None)


def describe___build_buffer_map():
    def with_missing_priority_in_buffer_priority_map(dummy_config_handler):
        priority_map = {'1234': 1, '2345': 2, '3456': 3, '4567': 4, '5678': 1}
        warehouse = dummy_config_handler.warehouse_handler.config['warehouses']
        warehouse[0].update({
            'plenty_warehouse_id': 101,
            'attributes': {
                'priority_buffer_mapping': [
                    {'level': 1, 'buffer': 15},
                    {'level': 2, 'buffer': 10},
                    {'level': 3, 'buffer': 5}
                ]
            }
        })
        with pytest.raises(MissingPriorityBufferMapping):
            dummy_config_handler._PlentySyncConfigHandler__build_buffer_map(
                priority_map=priority_map, config=warehouse[0])

    def with_wildcard_priority(dummy_config_handler):
        priority_map = {'1234': 1, '2345': 2, '3456': 3, '4567': 4, '5678': 1}
        warehouse = dummy_config_handler.warehouse_handler.config['warehouses']
        warehouse[0].update({
            'plenty_warehouse_id': 101,
            'attributes': {
                'priority_buffer_mapping': [
                    {'level': 1, 'buffer': 15},
                    {'level': 2, 'buffer': 10},
                    {'level': -1, 'buffer': 5}
                ]
            }
        })
        assert {'1234': 15, '2345': 10, '3456': 5, '4567': 5, '5678': 15} == \
            dummy_config_handler._PlentySyncConfigHandler__build_buffer_map(
                priority_map=priority_map, config=warehouse[0])

    def with_only_wildcard_priority(dummy_config_handler):
        priority_map = {'1234': 1, '2345': 2, '3456': 3, '4567': 4, '5678': 1}
        warehouse = dummy_config_handler.warehouse_handler.config['warehouses']
        warehouse[0].update({
            'plenty_warehouse_id': 101,
            'attributes': {
                'priority_buffer_mapping': [
                    {'level': -1, 'buffer': 7}
                ]
            }
        })
        assert {'1234': 7, '2345': 7, '3456': 7, '4567': 7, '5678': 7} == \
            dummy_config_handler._PlentySyncConfigHandler__build_buffer_map(
                priority_map=priority_map, config=warehouse[0])

    def with_valid_priorities(dummy_config_handler):
        priority_map = {'1234': 1, '2345': 2, '3456': 3, '4567': 4, '5678': 1}
        warehouse = dummy_config_handler.warehouse_handler.config['warehouses']
        warehouse[0].update({
            'plenty_warehouse_id': 101,
            'attributes': {
                'priority_buffer_mapping': [
                    {'level': 1, 'buffer': 15},
                    {'level': 2, 'buffer': 10},
                    {'level': 3, 'buffer': 5},
                    {'level': 4, 'buffer': 2}
                ]
            }
        })
        assert {'1234': 15, '2345': 10, '3456': 5, '4567': 2, '5678': 15} == \
            dummy_config_handler._PlentySyncConfigHandler__build_buffer_map(
                priority_map=priority_map, config=warehouse[0])


def describe_build_buffer_map():
    def with_multiple_valid_warehouses(dummy_config_handler):
        configs = dummy_config_handler.warehouse_handler.config['warehouses']
        configs[0].update(
            {
                'plenty_warehouse_id': 101,
                'database_column_name': 'stock_warehouse_1',
                'attributes': {
                    'priority_buffer_mapping': [
                        {'level': 1, 'buffer': 10},
                        {'level': -1, 'buffer': 5}
                    ]
                }
            }
        )
        configs.append(
            {
                'plenty_warehouse_id': 102,
                'database_column_name': 'stock_warehouse_2',
                'sheet_column_name': 'BCD',
                'gui_display_name': 'GUI-BCD',
                'sync_with': 'plentymarkets'
            }
        )
        priority_map = {
            '1234': 1, '2345': 2, '3456': 3
        }
        expected = {
            '1234': 10, '2345': 5, '3456': 5
        }
        dummy_config_handler.build_buffer_map(priority_map=priority_map)
        warehouses = dummy_config_handler.warehouse_handler.get()
        assert expected == warehouses[0]['attributes']['variation_buffer_map']
        assert all(
            x not in warehouses[1]
            for x in ['attributes', 'variation_buffer_map']
        )
