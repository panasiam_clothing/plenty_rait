# pylint: disable=unused-variable, redefined-outer-name, protected-access
# pylint: disable=missing-function-docstring
import pytest

from plenty_rait.helper.config_helper import (
    WarehouseConfigHandler, DuplicateDatabaseColumnConfiguration,
    InvalidDatabaseColumnName
)
import plenty_rait.helper.config_helper

@pytest.fixture()
def dummy_warehouse_handler(monkeypatch):
    def init_top_level_config(name, config_type):
        del name, config_type
        return (
            {
                'warehouses': [
                    {
                        'plenty_warehouse_id': 104,
                        'database_column_name': 'stock_warehouse_1',
                        'sheet_column_name': 'ABC',
                        'gui_display_name': 'GUI-ABC',
                        'sync_with': 'plentymarkets'
                    }
                ]
            },
            '/abc/def'
        )
    monkeypatch.setattr(
        plenty_rait.helper.config_helper,
        'init_top_level_config',
        init_top_level_config
    )
    return WarehouseConfigHandler()


def describe__validate_configuration():
    def with_invalid_database_column(dummy_warehouse_handler):
        configs = dummy_warehouse_handler.config['warehouses']
        configs[0].update(
            {
                'plenty_warehouse_id': 104,
                'database_column_name': 'invalid_column'
            }
        )
        with pytest.raises(InvalidDatabaseColumnName):
            dummy_warehouse_handler._validate_configuration()

    def with_duplicate_database_column(dummy_warehouse_handler):
        configs = dummy_warehouse_handler.config['warehouses']
        configs[0].update(
            {
                'plenty_warehouse_id': 101,
                'database_column_name': 'stock_warehouse_1'
            }
        )
        configs.append(
            {
                'plenty_warehouse_id': 102,
                'database_column_name': 'stock_warehouse_1',
                'sheet_column_name': 'BCD',
                'gui_display_name': 'GUI-BCD',
                'sync_with': 'plentymarkets'
            }
        )
        with pytest.raises(DuplicateDatabaseColumnConfiguration):
            dummy_warehouse_handler._validate_configuration()
