# pylint: disable=unused-variable, redefined-outer-name
# pylint: disable=missing-function-docstring
from decimal import Decimal
import datetime
import pytest
import pandas
from sqlalchemy import and_

from plenty_rait.prediction_tool.service import DataFormat
from plenty_rait.helper.database_schema import StatisticModel
from plenty_rait.helper.test_helper import setup_database


PREDICTION_COLUMNS = ['variation_id', 'date_value', 'prediction']


@pytest.fixture
def sample_predictions_total():
    llist = [
        ['1234', datetime.datetime(2021, 1, 1), 9],
        ['1234', datetime.datetime(2021, 2, 1), 11]
    ]
    return pandas.DataFrame(llist, columns=PREDICTION_COLUMNS)


@pytest.fixture
def sample_predictions_one_origin():
    llist = [
        ['1234', datetime.datetime(2021, 1, 1), 5],
        ['1234', datetime.datetime(2021, 2, 1), 8]
    ]
    return pandas.DataFrame(llist, columns=PREDICTION_COLUMNS)


@pytest.fixture
def sample_database_entries():
    stats = [
        StatisticModel(variation_id=1234, month=11, year=2020,
                       origin_id=Decimal('-1'), quantity=5, predicted=False),
        StatisticModel(variation_id=1234, month=12, year=2020,
                       origin_id=Decimal('-1'), quantity=7, predicted=False),
        StatisticModel(variation_id=1234, month=11, year=2020,
                       origin_id=Decimal('1'), quantity=3, predicted=False),
        StatisticModel(variation_id=1234, month=12, year=2020,
                       origin_id=Decimal('1'), quantity=2, predicted=False),
        StatisticModel(variation_id=1234, month=1, year=2021,
                       origin_id=Decimal('1'), quantity=4, predicted=False),
        StatisticModel(variation_id=1234, month=2, year=2021,
                       origin_id=Decimal('1'), quantity=7, predicted=True)
    ]
    return stats


@pytest.fixture
def sample_database_entries_with_dup(sample_database_entries):
    sample_database_entries.append(
        StatisticModel(variation_id=1234, month=1, year=2021,
                       origin_id=Decimal('1'), quantity=4, predicted=False)
    )
    return sample_database_entries


@pytest.fixture
def prep_session(db_session, sample_database_entries):
    setup_database(session=db_session, sources=sample_database_entries)
    return db_session


@pytest.fixture
def dup_prep_session(db_session, sample_database_entries_with_dup):
    setup_database(session=db_session,
                   sources=sample_database_entries_with_dup)
    return db_session


def describe_write_database_entries():
    """
    This test checks the following scenarios:
        * Invalid origin input
          (A origin is invalid if it is not found in the table)
        * No database entry exists -> write new entry
        * predicted Database entry exists -> overwrite quantity
        * actual Database entry exists -> overwrite & set predicted
          (We only create prediction for month >= current month,
           it is impossible to have a combination of:
            - prediction < current_month and an actual entry
            - prediction > current_month and an actual entry
           For the only valid combination:
            - prediction = current_month and an actual entry
           We prefer the prediction.)
        * Duplicated database entries -> Throw warning
    """
    def with_invalid_origin(sample_predictions_total, prep_session):
        origin = Decimal('99')
        prediction = sample_predictions_total
        data_format = DataFormat(format_type='monthly_total',
                                 session=prep_session)
        db_entries_before = prep_session.query(StatisticModel).count()

        result = data_format.write_database_entries(
            data=prediction, origin_id=origin)

        db_entries_after = prep_session.query(StatisticModel).count()

        assert result is False
        assert db_entries_before == db_entries_after

    def with_no_db_entry(sample_predictions_total, prep_session):
        origin = Decimal('-1')
        # Only take the prediction for 1234 origin: -1, 2021-1-1
        prediction = sample_predictions_total.iloc[[0]]
        data_format = DataFormat(format_type='monthly_total',
                                 session=prep_session)
        db_entry_match_before = prep_session.query(
            StatisticModel
        ).filter(
            and_(
                StatisticModel.variation_id == 1234,
                StatisticModel.month == 1,
                StatisticModel.year == 2021,
                StatisticModel.origin_id == Decimal('-1')
            )
        ).count()
        db_entries_before = prep_session.query(StatisticModel).count()

        result = data_format.write_database_entries(
            data=prediction, origin_id=origin)

        db_entry_match_after = prep_session.query(
            StatisticModel
        ).filter(
            and_(
                StatisticModel.variation_id == 1234,
                StatisticModel.month == 1,
                StatisticModel.year == 2021,
                StatisticModel.origin_id == Decimal('-1')
            )
        )
        db_entries_after = prep_session.query(StatisticModel).count()

        assert result is True
        assert (db_entry_match_before + 1) == db_entry_match_after.count()
        assert (db_entries_before + 1) == db_entries_after
        assert db_entry_match_after.one().predicted is True

    def with_predicted_db_entry(sample_predictions_one_origin, prep_session):
        origin = Decimal('1')
        prediction = sample_predictions_one_origin.iloc[[1]]
        data_format = DataFormat(format_type='monthly_total',
                                 session=prep_session)
        db_entry_before = prep_session.query(
            StatisticModel
        ).filter(
            and_(
                StatisticModel.variation_id == 1234,
                StatisticModel.month == 2,
                StatisticModel.year == 2021,
                StatisticModel.origin_id == Decimal('1')
            )
        ).one()
        db_entries_before = prep_session.query(StatisticModel).count()

        result = data_format.write_database_entries(
            data=prediction, origin_id=origin)

        db_entry_after = prep_session.query(
            StatisticModel
        ).filter(
            and_(
                StatisticModel.variation_id == 1234,
                StatisticModel.month == 2,
                StatisticModel.year == 2021,
                StatisticModel.origin_id == Decimal('1')
            )
        ).one()
        db_entries_after = prep_session.query(StatisticModel).count()

        assert result is True
        assert db_entry_before.quantity == db_entry_after.quantity
        assert db_entry_before.predicted == db_entry_after.predicted
        assert db_entries_before == db_entries_after

    def with_actual_db_entry(sample_predictions_one_origin, prep_session):
        origin = Decimal('1')
        # Only take the prediction for 1234 origin: 1, 2021-1-1
        prediction = sample_predictions_one_origin.iloc[[0]]
        data_format = DataFormat(format_type='monthly_total',
                                 session=prep_session)
        db_entry_quanitity_before = prep_session.query(
            StatisticModel
        ).filter(
            and_(
                StatisticModel.variation_id == 1234,
                StatisticModel.month == 1,
                StatisticModel.year == 2021,
                StatisticModel.origin_id == Decimal('1')
            )
        ).one().quantity
        db_entries_before = prep_session.query(StatisticModel).count()

        result = data_format.write_database_entries(
            data=prediction, origin_id=origin)

        db_entry_after = prep_session.query(
            StatisticModel
        ).filter(
            and_(
                StatisticModel.variation_id == 1234,
                StatisticModel.month == 1,
                StatisticModel.year == 2021,
                StatisticModel.origin_id == Decimal('1')
            )
        ).one()
        db_entries_after = prep_session.query(StatisticModel).count()

        assert result is True
        assert db_entry_after.predicted is True
        assert (db_entry_quanitity_before + 1) == db_entry_after.quantity
        assert db_entries_before == db_entries_after

    def with_duplicated_db_entries(sample_predictions_total, dup_prep_session):
        origin = Decimal('1')
        db_entries_before = dup_prep_session.query(StatisticModel).count()

        prediction = sample_predictions_total
        data_format = DataFormat(format_type='monthly_total',
                                 session=dup_prep_session)
        result = data_format.write_database_entries(
            data=prediction, origin_id=origin)

        db_entries_after = dup_prep_session.query(StatisticModel).count()

        assert result is False
        assert db_entries_before == db_entries_after
