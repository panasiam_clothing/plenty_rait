# pylint: disable=unused-variable, redefined-outer-name, protected-access
# pylint: disable=missing-function-docstring
import configparser
import datetime
from decimal import Decimal
from pathlib import Path
import tempfile
import pytest
from sqlalchemy import and_, func
import pandas
from pandas.testing import assert_frame_equal

from plenty_rait.sales_statistic.service import (
    DataCollector, get_quantity
)
from plenty_rait.helper.database_schema import (
    StatisticModel, DailyStatisticModel, AmazonOrdersModel,
    AmazonOrderItemsModel, OrdersModel, OrderItemsModel, IdentificationModel
)
from plenty_rait.helper.test_helper import setup_database


def marked_items(session, table, field: str) -> int:
    return session.query(
        table
    ).filter(
        getattr(table, field) != -1
    ).count()


@pytest.fixture
def amazon_orders():
    orders = [
        AmazonOrdersModel(  # 1
            amazon_id='303-12311512-12313', origin_id='104.02',
            purchase_date='2020-02-13'
        ),
        AmazonOrdersModel(  # 2
            amazon_id='303-12311512-12314', origin_id=-1,
            purchase_date='2019-12-31'
        ),
        AmazonOrdersModel(  # 3
            amazon_id='303-12311512-12315', origin_id='104.03',
            purchase_date='2020-03-15'
        ),
        AmazonOrdersModel(  # 4
            amazon_id='303-12311512-12316', origin_id='104.02',
            purchase_date='2019-04-13'
        ),
        AmazonOrdersModel(  # 5
            amazon_id='303-12311512-12317', origin_id='4.01',
            purchase_date='2020-05-13'
        ),
        AmazonOrdersModel(  # 6
            amazon_id='303-12311512-12318', origin_id='4.01',
            purchase_date='2020-05-18'
        ),
        AmazonOrdersModel(  # 7
            amazon_id='303-12311512-12319', origin_id='104.04',
            purchase_date='2020-06-13'
        )
    ]
    return orders


@pytest.fixture
def orders():
    orders = [
        OrdersModel(  # 1
            origin_id='1', netto_total=1, gross_total=1,
            creation_date='2020-02-13', payment_date='2020-02-13',
            delivery_date='2020-02-13', b2b=False,
            external_id='403-1243415-51423'
        ),
        OrdersModel(  # 2
            origin_id=-1, netto_total=1, gross_total=1,
            creation_date='2020-04-13', payment_date='2020-04-13',
            delivery_date='2020-04-13', b2b=False,
            external_id='12346'
        ),
        OrdersModel(  # 3
            origin_id='104.03', netto_total=1, gross_total=1,
            creation_date='2020-03-15', payment_date='2020-03-15',
            delivery_date='2020-03-15', b2b=False,
            external_id='303-12311512-12315'
        ),
        OrdersModel(  # 4
            origin_id='4', netto_total=1, gross_total=1,
            creation_date='2019-05-20', payment_date='2019-04-20',
            delivery_date='2019-05-20', b2b=False,
            external_id='12347'
        ),
        OrdersModel(  # 5
            origin_id='4.01', netto_total=1, gross_total=1,
            creation_date='2020-05-13', payment_date='2020-05-13',
            delivery_date='2020-05-13', b2b=False,
            external_id='303-12311512-12317'
        ),
        OrdersModel(  # 6
            origin_id='2', netto_total=1, gross_total=1,
            creation_date='2020-06-13', payment_date='2020-06-13',
            delivery_date='2020-06-13', b2b=False,
            external_id='12349'
        ),
        OrdersModel(  # 7
            origin_id='1', netto_total=1, gross_total=1,
            creation_date='2020-06-13', payment_date='2020-06-13',
            delivery_date='2020-06-13', b2b=False,
            external_id='21354'
        )
    ]
    return orders


@pytest.fixture
def amazon_order_items():
    order_items = [
        AmazonOrderItemsModel(
            order_id=1, amazon_id='303-12311512-12313',
            origin_id='104.02', variation_id=1234, asin='B051238590',
            quantity=1, ean='1234567891011'
        ),
        AmazonOrderItemsModel(
            order_id=1, amazon_id='303-12311512-12313',
            origin_id='104.02', variation_id=2345, asin='B051238690',
            quantity=1, ean='1234567891012'
        ),
        AmazonOrderItemsModel(
            order_id=2, amazon_id='303-12311512-12314',
            origin_id=-1, variation_id=3456, asin='B051238790',
            quantity=1, ean='1234567891013'
        ),
        AmazonOrderItemsModel(
            order_id=3, amazon_id='303-12311512-12315',
            origin_id='104.03', variation_id=1234, asin='B051238590',
            quantity=2, ean='1234567891011'
        ),
        AmazonOrderItemsModel(
            order_id=3, amazon_id='303-12311512-12315',
            origin_id='104.03', variation_id=2345, asin='B051238690',
            quantity=1, ean='1234567891012'
        ),
        AmazonOrderItemsModel(
            order_id=4, amazon_id='303-12311512-12316',
            origin_id='104.02', variation_id=1234, asin='B051238590',
            quantity=2, ean='1234567891011'
        ),
        AmazonOrderItemsModel(
            order_id=5, amazon_id='303-12311512-12317',
            origin_id='4.01', variation_id=1234, asin='B051238590',
            quantity=1, ean='1234567891011'
        ),
        AmazonOrderItemsModel(
            order_id=6, amazon_id='303-12311512-12318',
            origin_id='4.01', variation_id=1234, asin='B051238590',
            quantity=1, ean='1234567891011'
        ),
        AmazonOrderItemsModel(
            order_id=7, amazon_id='303-12311512-12319',
            origin_id='104.04', variation_id=4567, asin='B051238890',
            quantity=2, ean='1234567891014'
        )
    ]
    return order_items


@pytest.fixture
def order_items():
    order_items = [
        OrderItemsModel(
            order_id=1, external_id='403-1243415-51423',
            origin_id='1', variation_id=1234, item_id='1010020030',
            name='Testarticle_1', quantity=1, netto=1, gross=1,
            ean='1234567891011'
        ),
        OrderItemsModel(
            order_id=1, external_id='403-1243415-51423',
            origin_id='1', variation_id=2345, item_id='1010020040',
            name='Testarticle_2', quantity=1, netto=1, gross=1,
            ean='1234567891012'
        ),
        OrderItemsModel(
            order_id=2, external_id='12346',
            origin_id=-1, variation_id=3456, item_id='1010020050',
            name='Testarticle_3', quantity=1, netto=1, gross=1,
            ean='1234567891013'
        ),
        OrderItemsModel(
            order_id=3, external_id='303-12311512-12315',
            origin_id='104.03', variation_id=1234, item_id='1010020030',
            name='Testarticle_1', quantity=2, netto=1, gross=1,
            ean='1234567891011'
        ),
        OrderItemsModel(
            order_id=3, external_id='303-12311512-12315',
            origin_id='104.03', variation_id=2345, item_id='1010020040',
            name='Testarticle_2', quantity=1, netto=1, gross=1,
            ean='1234567891012'
        ),
        OrderItemsModel(
            order_id=4, external_id='12347',
            origin_id='4', variation_id=1234, item_id='1010020030',
            name='Testarticle_1', quantity=2, netto=1, gross=1,
            ean='1234567891011'
        ),
        OrderItemsModel(
            order_id=5, external_id='303-12311512-12317',
            origin_id='4.01', variation_id=1234, item_id='1010020030',
            name='Testarticle_1', quantity=1, netto=1, gross=1,
            ean='1234567891011'
        ),
        OrderItemsModel(
            order_id=6, external_id='12349',
            origin_id='2', variation_id=4567, item_id='1010020030',
            name='Testarticle_1', quantity=1, netto=1, gross=1,
            ean='1234567891014'
        ),
        OrderItemsModel(
            order_id=7, external_id='21354',
            origin_id='1', variation_id=4567, item_id='1010020060',
            name='Testarticle_4', quantity=1, netto=1, gross=1,
            ean='1234567891014'
        )
    ]
    return order_items


@pytest.fixture
def identification_table():
    id_table = [
        IdentificationModel(
            variation_id=1234, ean='1234567891011', sku='test_sku_1',
            asin_all='B051238590'
        ),
        IdentificationModel(
            variation_id=2345, ean='1234567891012', sku='test_sku_2',
            asin_all='B051238690'
        ),
        IdentificationModel(
            variation_id=3456, ean='1234567891013', sku='test_sku_3',
            asin_all='B051238790'
        ),
        IdentificationModel(
            variation_id=4567, ean='1234567891014', sku='test_sku_4',
            asin_all='B051238890'
        )
    ]
    return id_table


@pytest.fixture
def source_tables(amazon_orders: list, amazon_order_items: list,
                  orders: list, order_items: list,
                  identification_table: list) -> dict:
    sources = {
        'ama_order': amazon_orders,
        'ama_order_item': amazon_order_items,
        'order': orders,
        'order_item': order_items,
        'id_table': identification_table
    }
    return sources


@pytest.fixture
def source_tables_with_stats(source_tables: dict) -> dict:
    amazon_order_items = source_tables['ama_order_item']
    order_items = source_tables['order_item']
    amazon_order_items[0].statistic_entry = 3
    amazon_order_items[1].statistic_entry = 4
    amazon_order_items[3].statistic_entry = 7
    amazon_order_items[4].statistic_entry = 8
    amazon_order_items[5].statistic_entry = 1
    amazon_order_items[6].statistic_entry = -1
    amazon_order_items[7].statistic_entry = -1
    amazon_order_items[8].statistic_entry = -1
    order_items[0].statistic_entry = 5
    order_items[1].statistic_entry = 6
    order_items[3].statistic_entry = 7
    order_items[4].statistic_entry = 8
    order_items[5].statistic_entry = 2
    order_items[6].statistic_entry = -1
    order_items[7].statistic_entry = -1
    order_items[8].statistic_entry = -1

    return source_tables


@pytest.fixture
def source_tables_with_daily_stats(source_tables: dict) -> dict:
    amazon_order_items = source_tables['ama_order_item']
    order_items = source_tables['order_item']
    amazon_order_items[0].daily_statistic_entry = 3
    amazon_order_items[1].daily_statistic_entry = 4
    amazon_order_items[3].daily_statistic_entry = 7
    amazon_order_items[4].daily_statistic_entry = 8
    amazon_order_items[5].daily_statistic_entry = 1
    order_items[0].daily_statistic_entry = 5
    order_items[1].daily_statistic_entry = 6
    order_items[3].daily_statistic_entry = 7
    order_items[4].daily_statistic_entry = 8
    order_items[5].daily_statistic_entry = 2

    return source_tables


@pytest.fixture
def existing_statistic_entries() -> dict:
    statistic_items = [
        StatisticModel(
            variation_id=1234, origin_id=104.02, month=4, year=2019,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=4, month=5, year=2019,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=104.02, month=2, year=2020,
            quantity=1, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=104.02, month=2, year=2020,
            quantity=1, predicted=False,ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=1, month=2, year=2020,
            quantity=1, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=1, month=2, year=2020,
            quantity=1, predicted=False,ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=104.03, month=3, year=2020,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=104.03, month=3, year=2020,
            quantity=1, predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=4.01, month=5, year=2020,
            quantity=1, predicted=True, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=4567, origin_id=2, month=6, year=2020,
            quantity=3, predicted=True, ean='1234567891014'
        ),
        StatisticModel(
            variation_id=4567, origin_id=104.04, month=6, year=2020,
            quantity=2, predicted=True, ean='1234567891014'
        ),
        StatisticModel(
            variation_id=4567, origin_id=1, month=6, year=2020,
            quantity=3, predicted=True, ean='1234567891014'
        )
    ]
    return statistic_items


@pytest.fixture
def existing_total_statistic_entries() -> dict:
    statistic_items = [
        StatisticModel(
            variation_id=1234, origin_id=-1, month=4, year=2019,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=-1, month=5, year=2019,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=-1, month=2, year=2020,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=-1, month=2, year=2020,
            quantity=2, predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=-1, month=3, year=2020,
            quantity=2, predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=-1, month=3, year=2020,
            quantity=1, predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=-1, month=5, year=2020,
            quantity=1, predicted=True, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=-1, month=6, year=2020,
            quantity=1, predicted=True, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=4567, origin_id=-1, month=6, year=2020,
            quantity=7, predicted=True, ean='1234567891014'
        )
    ]
    return statistic_items


@pytest.fixture
def existing_daily_statistic_entries() -> dict:
    statistic_items = [
        DailyStatisticModel(
            variation_id=1234, origin_id=104.02, month=4, year=2019,
            day=13, quantity=2, ean='1234567891011'
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=4, month=5, year=2019,
            day=20, quantity=2, ean='1234567891011'
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=104.02, month=2, year=2020,
            day=13, quantity=1, ean='1234567891011'
        ),
        DailyStatisticModel(
            variation_id=2345, origin_id=104.02, month=2, year=2020,
            day=13, quantity=1, ean='1234567891012'
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=1, month=2, year=2020,
            day=13, quantity=1, ean='1234567891011'
        ),
        DailyStatisticModel(
            variation_id=2345, origin_id=1, month=2, year=2020,
            day=13, quantity=1, ean='1234567891012'
        ),
        DailyStatisticModel(
            variation_id=1234, origin_id=104.03, month=3, year=2020,
            day=15, quantity=2, ean='1234567891011'
        ),
        DailyStatisticModel(
            variation_id=2345, origin_id=104.03, month=3, year=2020,
            day=15, quantity=1, ean='1234567891012'
        )
    ]
    return statistic_items


@pytest.fixture
def dup_tables() -> dict:
    sources = {
        'ama_order': [
            AmazonOrdersModel(  # 1
                amazon_id='303-12345678-12345', origin_id='4.01',
                purchase_date='2020-02-13'
            ),
            AmazonOrdersModel(  # 2
                amazon_id='303-12345678-12346', origin_id='4.02',
                purchase_date='2020-02-14'
            )
        ],
        'ama_order_item': [
            AmazonOrderItemsModel(
                order_id=1, amazon_id='303-12345678-12345',
                origin_id='4.01', variation_id=1234, asin='B051238590',
                quantity=1, ean='1234567891011'
            ),
            # Duplicate
            AmazonOrderItemsModel(
                order_id=1, amazon_id='303-12345678-12345',
                origin_id='4.01', variation_id=1234, asin='B051238590',
                quantity=1, ean='1234567891011'
            ),
            AmazonOrderItemsModel(
                order_id=2, amazon_id='303-12345678-12346',
                origin_id='4.02', variation_id=1235, asin='B051238591',
                quantity=1
            )
        ],
        'order': [
            OrdersModel(  # 1
                origin_id='4.01', netto_total=1, gross_total=1,
                creation_date='2020-02-13', payment_date='2020-02-13',
                delivery_date='2020-02-13', b2b=False,
                external_id='303-12345678-12345'
            )
        ],
        'order_item': [
            OrderItemsModel(
                order_id=1, external_id='303-12345678-12345',
                origin_id='4.01', variation_id=1234, item_id='1010020060',
                name='Testarticle_1', quantity=1, netto=1, gross=1,
                ean='1234567891011'
            )
        ],
        'id_table': []
    }
    return sources


@pytest.fixture
def statistic_items():
    statistic_items = [
        StatisticModel(
            variation_id=1234, origin_id=4.01, month=8, year=2020, quantity=3,
            predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=4.01, month=7, year=2020, quantity=3,
            predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=1234, origin_id=2, month=6, year=2020, quantity=2,
            predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=1, month=5, year=2020, quantity=1,
            predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=3456, origin_id=104.01, month=4, year=2020,
            quantity=3, predicted=False, ean='1234567891013'
        )
    ]
    return statistic_items


@pytest.fixture
def total_statistic_sample_entries():
    statistic_items = [
        StatisticModel(
            variation_id=1234, origin_id=1.00, month=9, year=2018, quantity=3,
            predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=0, month=9, year=2018, quantity=1,
            predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=1234, origin_id=4.01, month=9, year=2018, quantity=4,
            predicted=False, ean='1234567891011'
        ),
        StatisticModel(
            variation_id=2345, origin_id=104.01, month=3, year=2019,
            quantity=5, predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=2345, origin_id=4.01, month=3, year=2019, quantity=2,
            predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=2345, origin_id=1.00, month=3, year=2019, quantity=1,
            predicted=False, ean='1234567891012'
        ),
        StatisticModel(
            variation_id=3456, origin_id=104.01, month=10, year=2019,
            quantity=2, predicted=False, ean='1234567891013'
        ),
        StatisticModel(
            variation_id=3456, origin_id=4.01, month=10, year=2019, quantity=1,
            predicted=False, ean='1234567891013'
        ),
        StatisticModel(
            variation_id=4567, origin_id=2.01, month=2, year=2020, quantity=3,
            predicted=True, ean='1234567891014'
        )
    ]
    return statistic_items


def test_db_lookup(db_session, source_tables: dict) -> None:
    """
    Verify that all of the used tables work properly.
    """
    stat = StatisticModel(variation_id=1234, month=11, year=2020, quantity=5,
                          predicted=False, ean='1234567891011')
    db_session.add(stat)
    assert db_session.query(StatisticModel).count() == 1

    db_session.add_all(source_tables['ama_order'])
    assert db_session.query(AmazonOrdersModel).count() == 7

    db_session.add_all(source_tables['ama_order_item'])
    assert db_session.query(AmazonOrderItemsModel).count() == 9

    db_session.add_all(source_tables['order'])
    assert db_session.query(OrdersModel).count() == 7

    db_session.add_all(source_tables['order_item'])
    assert db_session.query(OrderItemsModel).count() == 9


def test_db_is_rolled_back(db_session) -> None:
    assert db_session.query(StatisticModel).count() == 0
    assert db_session.query(AmazonOrdersModel).count() == 0
    assert db_session.query(AmazonOrderItemsModel).count() == 0
    assert db_session.query(OrdersModel).count() == 0
    assert db_session.query(OrderItemsModel).count() == 0


def describe_get_order_item_combinations() -> None:
    def with_no_matches(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        data_collector = DataCollector(session=db_session)
        result = data_collector._DataCollector__get_order_item_combinations(
            model=OrderItemsModel, order_ids=['0000'], variations=[9999])

        assert result == {}

    def with_single_match(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        data_collector = DataCollector(session=db_session)
        key = '403-1243415-51423' + ';' + '1234'
        result = data_collector._DataCollector__get_order_item_combinations(
            model=OrderItemsModel, order_ids=['403-1243415-51423'],
            variations=[1234])

        assert list(result.keys()) == [key]
        assert result[key] == {'id': 1, 'amount': 1}

    def with_multi_match(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        data_collector = DataCollector(session=db_session)
        keys = ['403-1243415-51423' + ';' + str(x) for x in [1234, 2345]]
        result = data_collector._DataCollector__get_order_item_combinations(
            model=OrderItemsModel, order_ids=['403-1243415-51423'],
            variations=[1234, 2345])

        assert list(result.keys()) == keys
        assert result[keys[0]] == {'id': 1, 'amount': 1}
        assert result[keys[1]] == {'id': 2, 'amount': 1}

    def with_match_amazon_order_items(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        data_collector = DataCollector(session=db_session)
        key = '303-12311512-12313' + ';' + '1234'
        result = data_collector._DataCollector__get_order_item_combinations(
            model=AmazonOrderItemsModel, order_ids=['303-12311512-12313'],
            variations=[1234])

        assert list(result.keys()) == [key]
        assert result[key] == {'id': 1, 'amount': 1}


def describe_find_duplicate() -> None:
    def with_item_only_in_orders(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        data_collector = DataCollector(session=db_session)
        data = [{'variation_id': 1234, 'external_id': '403-1243415-51423'}]
        key = '403-1243415-51423' + ';' + '1234'
        result = data_collector._DataCollector__find_duplicate(
            amazon_orders=data)

        assert result[key]['order_id'] == -1
        assert result[key]['amazon_id'] == -1

    def with_item_in_both(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        data_collector = DataCollector(session=db_session)
        data = [{'variation_id': 1234, 'external_id': '303-12311512-12315'}]
        key = '303-12311512-12315' + ';' + '1234'
        result = data_collector._DataCollector__find_duplicate(
            amazon_orders=data)

        assert result[key]['order_id'] == 4
        assert result[key]['amazon_id'] == 4

    def with_item_unknown(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        data_collector = DataCollector(session=db_session)
        data = [{'variation_id': 1234, 'external_id': 'unknown_id'}]
        key = 'unknown_id' + ';' + '1234'
        result = data_collector._DataCollector__find_duplicate(
            amazon_orders=data)

        assert result[key]['order_id'] == -1
        assert result[key]['amazon_id'] == -1

    def with_duplicate_in_single_table_match(db_session,
                                             dup_tables: dict) -> None:
        setup_database(session=db_session, sources=dup_tables)
        data_collector = DataCollector(session=db_session)
        data = [{'variation_id': 1234, 'external_id': '303-12345678-12345'}]
        key = '303-12345678-12345' + ';' + '1234'
        result = data_collector._DataCollector__find_duplicate(
            amazon_orders=data)

        assert result[key]['order_id'] == 1
        assert result[key]['amazon_id'] == 1

    def with_duplicate_in_single_table_no_match(db_session,
                                                dup_tables: dict) -> None:
        setup_database(session=db_session, sources=dup_tables)
        data_collector = DataCollector(session=db_session)
        data = [{'variation_id': 1235, 'external_id': '303-12345678-12346'}]
        key = '303-12345678-12346' + ';' + '1235'
        result = data_collector._DataCollector__find_duplicate(
            amazon_orders=data)

        assert result[key]['order_id'] == -1
        assert result[key]['amazon_id'] == -1


def describe_fetch_statistic_data() -> None:
    def with_no_entries_within_range(db_session, source_tables: dict) -> None:
        setup_database(session=db_session, sources=source_tables)
        stat_entries_before = db_session.query(StatisticModel).count()

        data_collector = DataCollector(session=db_session)
        start = datetime.datetime.strptime('2018-10-01', '%Y-%m-%d')
        end = datetime.datetime.strptime('2018-11-01', '%Y-%m-%d')
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        assert stat_entries_before == stat_entries_after

    def with_unique_orders(db_session, source_tables: dict) -> None:
        """
        Get unique orders from the amazon order items and the order items
        table.
        Expected result:
            When the origin is valid exactly one entry should be added
            to the statistics table.
        """
        setup_database(session=db_session, sources=source_tables)

        # There is only one order in april 2019 (AmazonOrderItemsModel)
        stat_entries_before = db_session.query(StatisticModel).count()
        data_collector = DataCollector(session=db_session)
        start = datetime.datetime.strptime('2019-04-01', '%Y-%m-%d')
        end = datetime.datetime.strptime('2019-05-01', '%Y-%m-%d')
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        assert (stat_entries_before + 1) == stat_entries_after

        # There is only one order in may 2019 (OrderItemsModel)
        stat_entries_before = db_session.query(StatisticModel).count()
        start = datetime.datetime.strptime('2019-05-01', '%Y-%m-%d')
        end = datetime.datetime.strptime('2019-06-01', '%Y-%m-%d')
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        assert (stat_entries_before + 1) == stat_entries_after

        # There is only one order in december 2019 (AmazonOrderItemsModel)
        # With invalid an origin id (-1)
        stat_entries_before = db_session.query(StatisticModel).count()
        start = datetime.datetime.strptime('2019-12-01', '%Y-%m-%d')
        end = datetime.datetime.strptime('2020-01-01', '%Y-%m-%d')
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        assert stat_entries_before == stat_entries_after

        # There is only one order in april 2020 (OrderItemsModel)
        # With invalid an origin id (-1)
        stat_entries_before = db_session.query(StatisticModel).count()
        start = datetime.datetime.strptime('2020-04-01', '%Y-%m-%d')
        end = datetime.datetime.strptime('2020-05-01', '%Y-%m-%d')
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        assert stat_entries_before == stat_entries_after

    def with_overlapping_orders(db_session, source_tables: dict) -> None:
        """
        Get orders that are present on both tables.
        Expected result:
            only one of both is added to the statistics table
            but both have a reference to the statistics entry.
        """
        setup_database(session=db_session, sources=source_tables)

        # There are two orders in march 2020 (OrderItemsModel &
        # AmazonOrderItemsModel), only one of both should be added but
        # there has to be one row per table afterwards with statistic_entry
        # filled
        stat_entries_before = db_session.query(StatisticModel).count()
        data_collector = DataCollector(session=db_session)
        start = datetime.datetime.strptime('2020-03-01', '%Y-%m-%d')
        end = datetime.datetime.strptime('2020-04-01', '%Y-%m-%d')
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()

        assert (stat_entries_before + 2) == stat_entries_after

        amazon_with_stat = db_session.query(AmazonOrderItemsModel).filter(
            AmazonOrderItemsModel.statistic_entry > 0
        )
        order_with_stat = db_session.query(OrderItemsModel).filter(
            OrderItemsModel.statistic_entry > 0
        )

        assert (amazon_with_stat.count() + order_with_stat.count()) == 4

    def with_different_origins(db_session, source_tables: dict) -> None:
        """
        Get orders that contain the same variation but from different
        marketplaces.
        Expected result:
            3 orders containing the same variation from different marketplaces
            should create 3 new statistic entries
        """
        setup_database(session=db_session, sources=source_tables)

        stat_entries_before = db_session.query(StatisticModel).count()
        data_collector = DataCollector(session=db_session)
        start = datetime.datetime.strptime('2020-06-01', '%Y-%m-%d')
        end = datetime.datetime.strptime('2020-06-30', '%Y-%m-%d')
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()

        assert (stat_entries_before + 3) == stat_entries_after

    def with_multiple_orders(db_session, source_tables: dict) -> None:
        """
        Get orders that contain the same variation, created in the same month.
        Expected result:
            3 orders containing the same variation from the same marketplace
            from the same month, should create exactly one statistics entry
            with the total sum of the sub quantities.
        """
        setup_database(session=db_session, sources=source_tables)

        # There are 2 orders on the amazon orders table and 1 on the orders
        # table for may 2020. With a total quantity of 4.
        stat_entries_before = db_session.query(StatisticModel).count()
        amazon_marked_items_before = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='statistic_entry')
        orders_marked_items_before = marked_items(
            session=db_session, table=OrderItemsModel,
            field='statistic_entry')

        data_collector = DataCollector(session=db_session)
        start = datetime.datetime.strptime('2020-05-01', '%Y-%m-%d')
        end = datetime.datetime.strptime('2020-05-30', '%Y-%m-%d')
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stat_entries_after = db_session.query(StatisticModel).count()
        amazon_marked_items_after = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='statistic_entry')
        orders_marked_items_after = marked_items(
            session=db_session, table=OrderItemsModel,
            field='statistic_entry')

        assert (stat_entries_before + 1) == stat_entries_after
        assert (amazon_marked_items_before + 2) == amazon_marked_items_after
        assert (orders_marked_items_before + 1) == orders_marked_items_after

        total = db_session.query(
            func.sum(StatisticModel.quantity).label('total')
        ).filter(
                StatisticModel.month == 5,
                StatisticModel.year == 2020,
        ).first().total

        assert total == 2

    def with_multiple_orders_daily(db_session, source_tables: dict) -> None:
        """
        Get orders that contain the same variation, created in the same month.
        Use the daily target to get the total quantities for each day.
        Expected result:
            3 orders containing the same variation from the same marketplace
            from the same month but with two daily statistic entries for two
            different days, the quantity of the statistic entries has to match
            the quantity of the items.
        """
        setup_database(session=db_session, sources=source_tables)

        # There are 2 orders on the amazon orders table and 1 on the orders
        # table for may 2020. With a total quantity of 4. For the 13th there
        # is a total quantity of 3 and for the 18th a quantity of 1.
        daily_entries_before = db_session.query(DailyStatisticModel).count()
        monthly_entries_before = db_session.query(StatisticModel).count()
        amazon_marked_items_before = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='daily_statistic_entry')
        orders_marked_items_before = marked_items(
            session=db_session, table=OrderItemsModel,
            field='daily_statistic_entry')

        data_collector = DataCollector(session=db_session)
        data_collector.set_target_daily()
        start = datetime.datetime.strptime('2020-05-01', '%Y-%m-%d')
        end = datetime.datetime.strptime('2020-05-30', '%Y-%m-%d')
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        daily_entries_after = db_session.query(DailyStatisticModel).count()
        monthly_entries_after = db_session.query(StatisticModel).count()
        amazon_marked_items_after = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='daily_statistic_entry')
        orders_marked_items_after = marked_items(
            session=db_session, table=OrderItemsModel,
            field='daily_statistic_entry')

        assert (daily_entries_before + 2) == daily_entries_after
        assert monthly_entries_before == monthly_entries_after
        assert (amazon_marked_items_before + 2) == amazon_marked_items_after
        assert (orders_marked_items_before + 1) == orders_marked_items_after

        total = db_session.query(
            func.sum(DailyStatisticModel.quantity).label('total')
        ).filter(
                DailyStatisticModel.day == 13,
                DailyStatisticModel.month == 5,
                DailyStatisticModel.year == 2020,
        ).first().total

        assert total == 1

        total = db_session.query(
            func.sum(DailyStatisticModel.quantity).label('total')
        ).filter(
                DailyStatisticModel.day == 18,
                DailyStatisticModel.month == 5,
                DailyStatisticModel.year == 2020,
        ).first().total

        assert total == 1

    def with_overwritting_predictions(db_session, source_tables_with_stats,
                                      existing_statistic_entries):
        """
        Predicted entries should be overwritten by actual entries, when
        new items are analyzed. In this test case, we provide existing
        statistic entries, with predicted entries, the input items overlap
        with those entries, which means all predicted entries have to be
        overwritten.
        """
        source_tables_with_stats['stats'] = existing_statistic_entries
        setup_database(session=db_session, sources=source_tables_with_stats)

        stats_entries_before = db_session.query(StatisticModel).count()
        stats_entries_predicted_before = db_session.query(
            StatisticModel
        ).filter(StatisticModel.predicted.is_(True)).count()
        amazon_marked_orders_before = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='statistic_entry')
        orders_marked_before = marked_items(
            session=db_session, table=OrderItemsModel,
            field='statistic_entry')
        total_before = db_session.query(
            func.sum(StatisticModel.quantity).label('total')
        ).first().total
        data_collector = DataCollector(session=db_session)
        data_collector.target = 'monthly'
        data_collector.statistic_model = StatisticModel
        start = datetime.datetime.strptime('2019-04-01', '%Y-%m-%d')
        end = datetime.datetime.strptime('2020-07-01', '%Y-%m-%d')
        data_collector.fetch_statistic_data(start_date=start, end_date=end)

        stats_entries_after = db_session.query(StatisticModel).count()
        stats_entries_predicted_after = db_session.query(
            StatisticModel
        ).filter(StatisticModel.predicted.is_(True)).count()
        amazon_marked_orders_after = marked_items(
            session=db_session, table=AmazonOrderItemsModel,
            field='statistic_entry')
        orders_marked_after = marked_items(
            session=db_session, table=OrderItemsModel,
            field='statistic_entry')
        total_after = db_session.query(
            func.sum(StatisticModel.quantity).label('total')
        ).first().total

        assert stats_entries_before == stats_entries_after
        assert (stats_entries_predicted_before - 4) ==\
            stats_entries_predicted_after
        assert (amazon_marked_orders_before + 3) == amazon_marked_orders_after
        assert (orders_marked_before + 3) == orders_marked_after
        assert (total_before - 3) == total_after


def setup_total_stats_test(session, scope, statistic_items: list) -> tuple:
    setup_database(session=session, sources=statistic_items)

    model = StatisticModel if scope == 'monthly' else DailyStatisticModel
    entries_before = session.query(model).count()
    data_collector = DataCollector(session=session)
    data_collector.target = scope
    data_collector.statistic_model = model
    return (data_collector, entries_before)


def assert_single_db_entry_qty(session, month, year, variation, expect,
                               origin_id, day=-1) -> None:
    """
    Helper funtion for asserting entry quantity for
    describe_get_custom_origin_statistics
    """
    model = StatisticModel if day < 0 else DailyStatisticModel
    if day > 0:
        query_filter = and_(
            model.variation_id == variation,
            model.origin_id == origin_id,
            model.day == day,
            model.month == month,
            model.year == year
        )
    else:
        query_filter = and_(
            model.variation_id == variation,
            model.origin_id == origin_id,
            model.month == month,
            model.year == year
        )

    actual = session.query(model.quantity).filter(query_filter).all()
    assert len(actual) == 1
    assert expect == actual[0].quantity


def get_single_db_entry(session, variation_id: int, origin_id: int,
                        month: int, year: int, day: int = -1):
    model = StatisticModel if day < 0 else DailyStatisticModel
    if day > 0:
        query_filter = and_(
            model.variation_id == variation_id,
            model.origin_id == origin_id,
            model.day == day,
            model.month == month,
            model.year == year
        )
    else:
        query_filter = and_(
            model.variation_id == variation_id,
            model.origin_id == origin_id,
            model.month == month,
            model.year == year
        )

    query = session.query(model).filter(query_filter)
    match_entries = query.all()
    assert len(match_entries) == 1
    return match_entries[0]


def describe_get_custom_origin_statistics() -> None:
    def with_no_entries_in_date_range(db_session) -> None:
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=3, predicted=False, ean='1234567891011')
        ]
        start_date = datetime.date(2018, 11, 10)
        end_date = datetime.date(2019, 2, 15)
        config = {'-1': '*'}

        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, scope='monthly',
            statistic_items=existing_entries)
        data_collector.get_custom_origin_statistics(
            start_date=start_date, end_date=end_date,
            custom_origin_config=config)
        entries_after = db_session.query(StatisticModel).count()

        assert entries_before == entries_after

    def with_single_variation_monthly(db_session) -> None:
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=3, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=2, month=2, year=2020,
                           quantity=2, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=3, month=2, year=2020,
                           quantity=1, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=4, month=2, year=2020,
                           quantity=4, predicted=False, ean='1234567891011')
        ]
        start_date = datetime.date(2020, 1, 1)
        end_date = datetime.date(2020, 3, 1)
        config = {'-1': '*'}

        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, scope='monthly',
            statistic_items=existing_entries)
        data_collector.get_custom_origin_statistics(
            start_date=start_date, end_date=end_date,
            custom_origin_config=config
        )
        entries_after = db_session.query(StatisticModel).count()

        assert (entries_before + 1) == entries_after

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=1234, expect=10, origin_id='-1')

    def with_single_variation_daily(db_session) -> None:
        existing_entries = [
            DailyStatisticModel(variation_id=1234, origin_id=1, day=10,
                                month=2, year=2020, quantity=3,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=2, day=10,
                                month=2, year=2020, quantity=2,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=3, day=10,
                                month=2, year=2020, quantity=1,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=4, day=10,
                                month=2, year=2020, quantity=4,
                                ean='1234567891011')
        ]
        start_date = datetime.date(2020, 2, 5)
        end_date = datetime.date(2020, 2, 15)
        config = {'-1': '*'}

        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, scope='daily',
            statistic_items=existing_entries)
        data_collector.get_custom_origin_statistics(
            start_date=start_date, end_date=end_date,
            custom_origin_config=config
        )
        entries_after = db_session.query(DailyStatisticModel).count()

        assert (entries_before + 1) == entries_after

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=1234, expect=10, origin_id='-1',
                                   day=10)

    def with_multiple_variations_monthly(db_session) -> None:
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=3, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=2, month=2, year=2020,
                           quantity=2, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=2345, origin_id=1, month=2, year=2020,
                           quantity=1, predicted=False, ean='1234567891012'),
            StatisticModel(variation_id=2345, origin_id=2, month=2, year=2020,
                           quantity=4, predicted=False, ean='1234567891012'),
            StatisticModel(variation_id=3456, origin_id=1, month=4, year=2020,
                           quantity=1, predicted=False, ean='1234567891013'),
            StatisticModel(variation_id=3456, origin_id=2, month=4, year=2020,
                           quantity=4, predicted=False, ean='1234567891013')
        ]
        start_date = datetime.date(2020, 1, 1)
        end_date = datetime.date(2020, 3, 1)
        config = {'-1': '*'}

        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, scope='monthly',
            statistic_items=existing_entries)
        data_collector.get_custom_origin_statistics(
            start_date=start_date, end_date=end_date,
            custom_origin_config=config
        )
        entries_after = db_session.query(StatisticModel).count()

        assert (entries_before + 2) == entries_after

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=1234, expect=5, origin_id='-1')

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=2345, expect=5, origin_id='-1')

    def with_multiple_variations_daily(db_session) -> None:
        existing_entries = [
            DailyStatisticModel(variation_id=1234, origin_id=1, day=10,
                                month=2, year=2020, quantity=3,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=2, day=10,
                                month=2, year=2020, quantity=2,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=2345, origin_id=1, day=10,
                                month=2, year=2020, quantity=1,
                                ean='1234567891012'),
            DailyStatisticModel(variation_id=2345, origin_id=2, day=10,
                                month=2, year=2020, quantity=4,
                                ean='1234567891012'),
            DailyStatisticModel(variation_id=3456, origin_id=1, day=15,
                                month=2, year=2020, quantity=1,
                                ean='1234567891013'),
            DailyStatisticModel(variation_id=3456, origin_id=2, day=15,
                                month=2, year=2020, quantity=4,
                                ean='1234567891013')
        ]
        start_date = datetime.date(2020, 2, 1)
        end_date = datetime.date(2020, 2, 13)
        config = {'-1': '*'}

        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, scope='daily',
            statistic_items=existing_entries)
        data_collector.get_custom_origin_statistics(
            start_date=start_date, end_date=end_date,
            custom_origin_config=config
        )
        entries_after = db_session.query(DailyStatisticModel).count()

        assert (entries_before + 2) == entries_after

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=1234, expect=5, origin_id='-1',
                                   day=10)

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=2345, expect=5, origin_id='-1',
                                   day=10)

    def with_predicted_monthly_values_in_date_range(db_session) -> None:
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=3, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=2, month=2, year=2020,
                           quantity=2, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=3, month=2, year=2020,
                           quantity=1, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=-1, month=2, year=2020,
                           quantity=4, predicted=True, ean='1234567891011')
        ]
        start_date = datetime.date(2020, 1, 1)
        end_date = datetime.date(2020, 3, 1)
        config = {'-1': '*'}

        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, scope='monthly',
            statistic_items=existing_entries)
        data_collector.get_custom_origin_statistics(
            start_date=start_date, end_date=end_date,
            custom_origin_config=config
        )
        entries_after = db_session.query(StatisticModel).count()

        db_entry = get_single_db_entry(session=db_session, variation_id=1234,
                                       origin_id=-1, month=2, year=2020)

        # The predicted entry should be changed to an actual entry
        assert entries_before == entries_after
        assert db_entry.quantity == 6
        assert db_entry.predicted is False

    def with_multiple_custom_origins_monthly(db_session) -> None:
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=3, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=2, month=2, year=2020,
                           quantity=2, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=3, month=2, year=2020,
                           quantity=3, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=4, month=2, year=2020,
                           quantity=2, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=4.01, month=2,
                           year=2020, quantity=3, predicted=False,
                           ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=4.02, month=2,
                           year=2020, quantity=2, predicted=False,
                           ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=-2, month=2,
                           year=2020, quantity=2, predicted=True,
                           ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=-3, month=2,
                           year=2020, quantity=2, predicted=False,
                           ean='1234567891011'),
        ]
        start_date = datetime.date(2020, 2, 1)
        end_date = datetime.date(2020, 3, 1)
        config = {'-1': '*', '-2': '4.*', '-3': '1,2,3'}

        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, scope='monthly',
            statistic_items=existing_entries)
        data_collector.get_custom_origin_statistics(
            start_date=start_date, end_date=end_date,
            custom_origin_config=config
        )
        entries_after = db_session.query(StatisticModel).count()

        # one predicted is updated, one custom origin is updated and one is
        # added => 1 more
        assert (entries_before + 1) == entries_after

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=1234, expect=15, origin_id='-1')

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=1234, expect=7, origin_id='-2')

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=1234, expect=8, origin_id='-3')

    def with_multiple_custom_origins_daily(db_session) -> None:
        existing_entries = [
            DailyStatisticModel(variation_id=1234, origin_id=1, day=10,
                                month=2, year=2020, quantity=3,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=2, day=10,
                                month=2, year=2020, quantity=2,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=3, day=10,
                                month=2, year=2020, quantity=3,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=4, day=10,
                                month=2, year=2020, quantity=2,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=4.01, day=10,
                                month=2, year=2020, quantity=3,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=4.02, day=10,
                                month=2, year=2020, quantity=2,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=-2, day=10,
                                month=2, year=2020, quantity=2,
                                ean='1234567891011')
        ]
        start_date = datetime.date(2020, 2, 10)
        end_date = datetime.date(2020, 2, 13)
        config = {'-1': '*', '-2': '4.*', '-3': '1,2,3'}

        (data_collector, entries_before) = setup_total_stats_test(
            session=db_session, scope='daily',
            statistic_items=existing_entries)
        data_collector.get_custom_origin_statistics(
            start_date=start_date, end_date=end_date,
            custom_origin_config=config
        )
        entries_after = db_session.query(DailyStatisticModel).count()

        # one custom origin is updated and two are added => 2 more
        assert (entries_before + 2) == entries_after

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=1234, expect=15, origin_id='-1',
                                   day=10)

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=1234, expect=7, origin_id='-2',
                                   day=10)

        assert_single_db_entry_qty(session=db_session, month=2, year=2020,
                                   variation=1234, expect=8, origin_id='-3',
                                   day=10)


def describe_cleanup_statistics():
    def with_no_invalid_statistic_entries(
            db_session, source_tables_with_stats: dict,
            existing_statistic_entries: dict, monkeypatch) -> None:

        monkeypatch.setattr('builtins.input', lambda _: 'y')
        source_tables_with_stats['stats'] = existing_statistic_entries
        setup_database(session=db_session, sources=source_tables_with_stats)

        expected = [[x.variation_id, x.origin_id, x.quantity, x.year, x.month, x.ean]
                    for x in db_session.query(StatisticModel).all()]
        data_collector = DataCollector(session=db_session)
        data_collector.cleanup_statistics(print_summary=True)

        result = [[x.variation_id, x.origin_id, x.quantity, x.year, x.month, x.ean]
                  for x in db_session.query(StatisticModel).all()]

        assert expected == result

    def with_invalid_statistic_entries(
            db_session, source_tables_with_stats: dict,
            existing_statistic_entries: dict, monkeypatch) -> None:

        monkeypatch.setattr('builtins.input', lambda _: 'y')
        stats = existing_statistic_entries
        stats[0].variation_id = 2234
        stats[2].variation_id = 3234
        source_tables_with_stats['stats'] = stats
        setup_database(session=db_session, sources=source_tables_with_stats)

        expected_id_set = [1234, 2345, 4567]
        stats_before = db_session.query(StatisticModel)
        stats_count_before = stats_before.count()
        id_set_before = sorted(list({x.variation_id for x in stats_before}))

        data_collector = DataCollector(session=db_session)
        data_collector.cleanup_statistics(print_summary=True)

        stats_after = db_session.query(StatisticModel)
        stats_count_after = stats_after.count()
        id_set_after = sorted(list({x.variation_id for x in stats_after}))

        assert stats_count_after == stats_count_before
        assert id_set_after != id_set_before
        assert expected_id_set == id_set_after

    def with_invalid_daily_statistic_entries(
            db_session, source_tables_with_daily_stats: dict,
            existing_daily_statistic_entries: dict, monkeypatch) -> None:

        monkeypatch.setattr('builtins.input', lambda _: 'y')
        stats = existing_daily_statistic_entries
        stats[0].variation_id = 2234
        stats[2].variation_id = 3234
        source_tables_with_daily_stats['stats'] = stats
        setup_database(session=db_session,
                       sources=source_tables_with_daily_stats)

        expected_id_set = [1234, 2345]
        daily_before = db_session.query(DailyStatisticModel)
        daily_count_before = daily_before.count()
        id_set_before = sorted(list({x.variation_id for x in daily_before}))

        data_collector = DataCollector(session=db_session)
        data_collector.set_target_daily()
        data_collector.cleanup_statistics(print_summary=True)

        daily_after = db_session.query(DailyStatisticModel)
        daily_count_after = daily_after.count()
        id_set_after = sorted(list({x.variation_id for x in daily_after}))

        assert daily_count_after == daily_count_before
        assert id_set_after != id_set_before
        assert expected_id_set == id_set_after

    def with_invalid_total_statistic_entries(
            db_session, source_tables_with_stats: dict,
            existing_statistic_entries: dict,
            existing_total_statistic_entries: dict,
            monkeypatch) -> None:

        monkeypatch.setattr('builtins.input', lambda _: 'y')
        for entry in [x for x in existing_statistic_entries
                      if x.variation_id == 1234]:
            entry.variation_id = 2234
        for entry in [x for x in existing_statistic_entries
                      if x.variation_id == 2345]:
            entry.variation_id = 3345

        total_stats = existing_total_statistic_entries
        for entry in [x for x in total_stats if x.variation_id == 1234]:
            entry.variation_id = 2234
        for entry in [x for x in total_stats if x.variation_id == 2345]:
            entry.variation_id = 3345
        existing_statistic_entries += total_stats
        source_tables_with_stats['stats'] = existing_statistic_entries
        setup_database(session=db_session, sources=source_tables_with_stats)

        expected_id_set = [1234, 2345, 4567]
        totals_before = db_session.query(StatisticModel).filter(
            StatisticModel.origin_id == -1)
        totals_count_before = totals_before.count()
        id_set_before = sorted(list({x.variation_id for x in totals_before}))

        data_collector = DataCollector(session=db_session)
        data_collector.cleanup_statistics(print_summary=True)

        totals_after = db_session.query(StatisticModel).filter(
            StatisticModel.origin_id == -1)
        totals_count_after = totals_after.count()
        id_set_after = sorted(list({x.variation_id for x in totals_after}))

        assert totals_count_after == totals_count_before
        assert id_set_after != id_set_before
        assert expected_id_set == id_set_after

    def with_modified_ean(db_session, monkeypatch) -> None:
        """
        In this case the EAN has been modified, this action is difficult to
        handle for Plenty RAIT as it normally assumes the EAN to be a reliable
        identification property. 

        Warn the user and indicate that he can either detach the old statistics
        from the new ones by modifying nothing or to attach the statistics of
        the old EAN by manually changing the EAN values in the database.
        """
        monkeypatch.setattr('builtins.input', lambda _: 'y')
        identification = [
           IdentificationModel(
               variation_id=1234, ean='1234567891011', sku='test_sku_1',
               asin_all='B051238590'
           ),
           IdentificationModel(
               variation_id=2345, ean='1234567891013', sku='test_sku_2',
               asin_all='B051238591'
           )
        ]
        statistics = [
            StatisticModel(
                variation_id=1234, origin_id=1, month=2, year=2020,
                quantity=1, predicted=False, ean='1234567891011'
            ),
            StatisticModel(
                variation_id=2345, origin_id=1, month=2, year=2020,
                quantity=1, predicted=False, ean='1234567891012'
            )
        ]
        sources = {'ident': identification, 'stats': statistics}
        setup_database(session=db_session, sources=sources)
        # TODO Mark the statistics as archived to reduce redundant actions
        stats_before = db_session.query(StatisticModel).count()
        data_collector = DataCollector(session=db_session)
        data_collector.cleanup_statistics(print_summary=True, ean_check=True)
        stats_after = db_session.query(StatisticModel).count()
        log_path = Path(tempfile.gettempdir()) / 'deprecated_ean_plenty_rait_monthly.log'
        with open(log_path, 'r') as log_file:
            lines = log_file.readlines()
        # Statistcs shouldn't be removed
        assert stats_before == stats_after
        assert lines[0] == "EAN change: variation ID 2345 (1234567891012 -> 1234567891013) (Amount in monthly statistics: 1)\n"


    def with_deleted_variation(db_session, monkeypatch) -> None:
        """
        In this case the EAN and the variation ID have been modified
        for a variation, the proper response here is to throw a
        warning.
        """
        monkeypatch.setattr('builtins.input', lambda _: 'y')
        identification = [
            IdentificationModel(
                variation_id=1234, ean='1234567891011', sku='test_sku_1',
                asin_all='B051238590'
            ),
            IdentificationModel(
                variation_id=2345, ean='1234567891012', sku='test_sku_2',
                asin_all='B051238591'
            )
        ]
        statistics = [
            StatisticModel(
                variation_id=1234, origin_id=1, month=2, year=2020,
                quantity=1, predicted=False, ean='1234567891011'
            ),
            StatisticModel(
                variation_id=3456, origin_id=1, month=2, year=2020,
                quantity=1, predicted=False, ean='1234567891013'
            )
        ]
        sources = {'ident': identification, 'stats': statistics}
        setup_database(session=db_session, sources=sources)
        # TODO Mark the statistics as archived to reduce redundant actions
        stats_before = db_session.query(StatisticModel).count()
        data_collector = DataCollector(session=db_session)
        data_collector.cleanup_statistics(print_summary=True, ean_check=True)
        stats_after = db_session.query(StatisticModel).count()
        log_path = Path(tempfile.gettempdir()) / 'deprecated_ean_plenty_rait_monthly.log'
        with open(log_path, 'r') as log_file:
            lines = log_file.readlines()
        # Statistcs shouldn't be removed
        assert stats_before == stats_after
        assert lines[0] == 'Orphaned: variation ID 3456, EAN 1234567891013 (Amount in monthly statistics: 1)\n'


def describe_parse_origins():
    def with_valid_config(db_session, source_tables_with_stats: dict,
                          existing_statistic_entries: dict) -> None:
        config = configparser.ConfigParser()
        config = {
            '-1': '4.*',
            '-2': '~104',
            '-3': '4.*,104.*',
            '-4': '*',
            '-5': '4.*,1',
            '-6': '4.*,!4.01'
        }
        expected_origins = {
            '-1': ['4.00', '4.01'],
            '-2': ['4.00', '4.01', '1.00', '2.00'],
            '-3': ['4.00', '4.01', '104.02', '104.03', '104.04'],
            '-4': ['4.00', '4.01', '104.02', '104.03', '104.04', '1.00', '2.00'],
            '-5': ['4.00', '4.01', '1.00'],
            '-6': ['4.00']
        }
        collector = DataCollector(session=db_session)
        source_tables_with_stats['stats'] = existing_statistic_entries
        setup_database(session=db_session, sources=source_tables_with_stats)

        result = collector.parse_origins(custom_origin_config=config)

        assert len(result) == len(expected_origins)
        for key, value in expected_origins.items():
            assert len(result[key]) == len(value)
            for origin in value:
                assert origin in result[key]

    def with_origin_id_not_found(db_session, source_tables_with_stats: dict,
                                 existing_statistic_entries: dict) -> None:
        config = configparser.ConfigParser()
        config = {'-1': '99'}
        collector = DataCollector(session=db_session)
        source_tables_with_stats['stats'] = existing_statistic_entries
        setup_database(session=db_session, sources=source_tables_with_stats)

        with pytest.raises(RuntimeError):
            collector.parse_origins(custom_origin_config=config)

    def with_different_input_order(db_session, source_tables_with_stats: dict,
                                   existing_statistic_entries: dict) -> None:
        collector = DataCollector(session=db_session)
        source_tables_with_stats['stats'] = existing_statistic_entries
        setup_database(session=db_session, sources=source_tables_with_stats)

        config = configparser.ConfigParser()
        config = {'-2': '4.*,4.01'}
        result_a = collector.parse_origins(custom_origin_config=config)
        config = {'-2': '4.01,4.*'}
        result_b = collector.parse_origins(custom_origin_config=config)
        assert result_a == result_b


def assert_statistic_entries(collector, entries, result):
    """ Helper function for describe_get_existing_custom_origin_statistics """
    for entry in entries:
        check = [entry.variation_id, entry.origin_id]
        current_check = result[entry.year][entry.month]
        compare_keys = ['variation_id', 'origin_id', 'month', 'year',
                        'quantity', 'ean', 'predicted']
        if collector.target == 'daily':
            check.insert(2, entry.day)
            compare_keys.remove('predicted')
            compare_keys.append('day')
        assert check in current_check['check_list']
        index = current_check['check_list'].index(check)

        for key in compare_keys:
            assert getattr(entry, key) == getattr(current_check[str(index)], key)


def describe_create_lookup_dictionary():
    def with_no_match_monthly(db_session) -> None:
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=1, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=-1, month=12,
                           year=2019, quantity=2, predicted=False,
                           ean='1234567891011')
        ]
        existing = []
        setup_database(session=db_session, sources=existing_entries)
        collector = DataCollector(session=db_session)
        db_entries = db_session.query(StatisticModel).filter(
            StatisticModel.date_value >= '2020-02-01',
            StatisticModel.date_value <= '2020-03-01',
            StatisticModel.origin_id < 0).all()

        result = collector._DataCollector__create_lookup_dictionary(db_entries)

        assert_statistic_entries(collector=collector, entries=existing,
                                 result=result)

    def with_match_monthly(db_session) -> None:
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=1, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=-1, month=3, year=2020,
                           quantity=2, predicted=False, ean='1234567891011')
        ]
        existing = [entry for entry in existing_entries if entry.origin_id < 0]
        setup_database(session=db_session, sources=existing_entries)
        collector = DataCollector(session=db_session)
        db_entries = db_session.query(StatisticModel).filter(
            StatisticModel.date_value >= '2020-02-01',
            StatisticModel.date_value <= '2020-03-01',
            StatisticModel.origin_id < 0).all()

        result = collector._DataCollector__create_lookup_dictionary(db_entries)

        assert_statistic_entries(collector=collector, entries=existing,
                                 result=result)

    def with_match_daily(db_session) -> None:
        existing_entries = [
            DailyStatisticModel(variation_id=1234, origin_id=1, day=10,
                                month=2, year=2020, quantity=1,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=-1, day=12,
                                month=2, year=2020, quantity=2,
                                ean='1234567891011')
        ]
        existing = [entry for entry in existing_entries if entry.origin_id < 0]
        setup_database(session=db_session, sources=existing_entries)
        collector = DataCollector(session=db_session)
        db_entries = db_session.query(DailyStatisticModel).filter(
            DailyStatisticModel.date_value >= '2020-02-10',
            DailyStatisticModel.date_value <= '2020-02-12',
            DailyStatisticModel.origin_id < 0).all()
        collector.set_target_daily()
        result = collector._DataCollector__create_lookup_dictionary(db_entries)

        assert_statistic_entries(collector=collector, entries=existing,
                                 result=result)

    def with_predicted_match_monthly(db_session) -> None:
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=1, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=-1, month=2, year=2020,
                           quantity=2, predicted=True, ean='1234567891011')
        ]
        existing = [entry for entry in existing_entries if entry.origin_id < 0]
        setup_database(session=db_session, sources=existing_entries)
        collector = DataCollector(session=db_session)
        db_entries = db_session.query(StatisticModel).filter(
            StatisticModel.date_value >= '2020-02-01',
            StatisticModel.date_value <= '2020-02-01',
            StatisticModel.origin_id < 0).all()

        result = collector._DataCollector__create_lookup_dictionary(db_entries)

        assert_statistic_entries(collector=collector, entries=existing,
                                 result=result)

    def with_multi_match_monthly(db_session) -> None:
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=1, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=-1, month=2, year=2020,
                           quantity=3, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=-1, month=2, year=2020,
                           quantity=4, predicted=True, ean='1234567891011')
        ]
        setup_database(session=db_session, sources=existing_entries)
        collector = DataCollector(session=db_session)
        db_entries = db_session.query(StatisticModel).filter(
            StatisticModel.date_value >= '2020-02-01',
            StatisticModel.date_value <= '2020-02-01',
            StatisticModel.origin_id < 0).all()

        with pytest.raises(SystemExit):
            collector._DataCollector__create_lookup_dictionary(
                db_entries)


def describe_get_statistic_entries_within_daterange():
    def with_invalid_daterange(db_session, existing_statistic_entries):
        db_session.add_all(existing_statistic_entries)
        start = '2021-07-11'
        end = '2010-07-11'
        origin_list = [Decimal('104.02')]
        collector = DataCollector(session=db_session)

        database = collector._DataCollector__get_statistics_entries_within_date_range(
            start=start, end=end, origin_list=origin_list)

        assert database.all() == []

    def with_empty_origin_list(db_session, existing_statistic_entries):
        db_session.add_all(existing_statistic_entries)
        start = '2020-02-01'
        end = '2020-06-01'
        origin_list = []
        collector = DataCollector(session=db_session)

        database = collector._DataCollector__get_statistics_entries_within_date_range(
            start=start, end=end, origin_list=origin_list)

        assert database.all() == []

    def with_invalid_origins(db_session, existing_statistic_entries):
        db_session.add_all(existing_statistic_entries)
        start = '2020-02-01'
        end = '2020-06-01'
        origin_list = [Decimal('999')]
        collector = DataCollector(session=db_session)

        database = collector._DataCollector__get_statistics_entries_within_date_range(
            start=start, end=end, origin_list=origin_list)

        assert database.all() == []

    def with_predicted_entries(db_session, existing_statistic_entries):
        db_session.add_all(existing_statistic_entries)
        start = '2020-04-01'
        end = '2020-06-01'
        origin_list = [Decimal('4.01')]
        collector = DataCollector(session=db_session)

        database = collector._DataCollector__get_statistics_entries_within_date_range(
            start=start, end=end, origin_list=origin_list)

        assert database.all() == []

    def with_empty_selection(db_session, existing_statistic_entries):
        db_session.add_all(existing_statistic_entries)
        start = '2021-06-01'
        end = '2021-07-01'
        origin_list = [Decimal('104.02')]
        collector = DataCollector(session=db_session)

        database = collector._DataCollector__get_statistics_entries_within_date_range(
            start=start, end=end, origin_list=origin_list)

        assert database.all() == []

    def with_valid_data(db_session, existing_statistic_entries):
        db_session.add_all(existing_statistic_entries)
        start = '2020-01-01'
        end = '2020-12-01'
        origin_list = [Decimal('104.02')]
        collector = DataCollector(session=db_session)

        database = collector._DataCollector__get_statistics_entries_within_date_range(
            start=start, end=end, origin_list=origin_list)

        assert len(database.all()) == 2
        assert database.all()[0].id == 3
        assert database.all()[1].id == 4


def describe_get_quantity():
    def with_variation_not_found_monthly():
        dataframe = pandas.DataFrame(
            [[1234, 1, 2020, 10, '1234567891011'],
             [2345, 2, 2020, 14, '1234567891012']],
            columns = ['variation_id', 'month', 'year', 'quantity', 'ean']
        )
        variation = 3456
        date = datetime.date(year=2020, month=1, day=1)
        target = 'monthly'
        expect = 0
        result = get_quantity(dataframe=dataframe, variation=variation,
                              date=date, target=target)
        assert expect == result

    def with_variation_not_found_daily():
        dataframe = pandas.DataFrame(
            [[1234, 10, 1, 2020, 10, '1234567891011'],
             [2345, 15, 2, 2020, 14, '1234567891012']],
            columns = ['variation_id', 'day', 'month', 'year', 'quantity',
                       'ean']
        )
        variation = 3456
        date = datetime.date(year=2020, month=2, day=15)
        target = 'daily'
        expect = 0
        result = get_quantity(dataframe=dataframe, variation=variation,
                              date=date, target=target)
        assert expect == result

    def with_no_entry_in_range_monthly():
        dataframe = pandas.DataFrame(
            [[1234, 1, 2020, 10, '1234567891011'],
             [2345, 2, 2020, 14, '1234567891012']],
            columns = ['variation_id', 'month', 'year', 'quantity', 'ean']
        )
        variation = 1234
        date = datetime.date(year=2020, month=3, day=1)
        target = 'monthly'
        expect = 0
        result = get_quantity(dataframe=dataframe, variation=variation,
                              date=date, target=target)
        assert expect == result

    def with_multiple_entries_monthly():
        dataframe = pandas.DataFrame(
            [[1234, 1, 2020, 10, '1234567891011'],
             [1234, 1, 2020, 12, '1234567891011'],
             [2345, 2, 2020, 14, '1234567891012']],
            columns = ['variation_id', 'month', 'year', 'quantity', 'ean']
        )
        variation = 1234
        date = datetime.date(year=2020, month=1, day=1)
        target = 'monthly'
        expect = -1
        result = get_quantity(dataframe=dataframe, variation=variation,
                              date=date, target=target)
        assert expect == result

    def with_match_monthly():
        dataframe = pandas.DataFrame(
            [[1234, 1, 2020, 10, '1234567891011'],
             [2345, 2, 2020, 14, '1234567891012']],
            columns = ['variation_id', 'month', 'year', 'quantity', 'ean']
        )
        variation = 1234
        date = datetime.date(year=2020, month=1, day=1)
        target = 'monthly'
        expect = 10
        result = get_quantity(dataframe=dataframe, variation=variation,
                              date=date, target=target)
        assert expect == result

    def with_match_daily():
        dataframe = pandas.DataFrame(
            [[1234, 10, 1, 2020, 3, '1234567891011'],
             [2345, 15, 2, 2020, 4, '1234567891012']],
            columns = ['variation_id', 'day', 'month', 'year', 'quantity',
                       'ean']
        )
        variation = 2345
        date = datetime.date(year=2020, month=2, day=15)
        target = 'daily'
        expect = 4
        result = get_quantity(dataframe=dataframe, variation=variation,
                              date=date, target=target)
        assert expect == result


def describe_get_total_quantity():
    def with_empty_origin_list(db_session):
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=1, predicted=False, ean='1234567891011')
        ]
        setup_database(session=db_session, sources=existing_entries)
        origin_list = []
        start = datetime.date(2020, 2, 1)
        end = datetime.date(2020, 2, 15)
        collector = DataCollector(session=db_session)
        result = collector._DataCollector__get_total_quantity(
            origin_list=origin_list, start=start, end=end)

        assert len(result.index) == 0

    def with_single_match_monthly(db_session):
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=1, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=2, month=2, year=2020,
                           quantity=2, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=3, month=2, year=2020,
                           quantity=3, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=1, month=3, year=2020,
                           quantity=4, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=2345, origin_id=2, month=2, year=2020,
                           quantity=5, predicted=False, ean='1234567891012'),
            StatisticModel(variation_id=3456, origin_id=3, month=3, year=2020,
                           quantity=6, predicted=False, ean='1234567891013')
        ]
        setup_database(session=db_session, sources=existing_entries)
        origin_list = ['1', '4']
        start = datetime.date(2020, 2, 1)
        end = datetime.date(2020, 2, 15)
        expect = pandas.DataFrame(
            [[1234, 2, 2020, 1, '1234567891011']],
            columns=['variation_id', 'month', 'year', 'quantity', 'ean']
        )
        collector = DataCollector(session=db_session)
        result = collector._DataCollector__get_total_quantity(
            origin_list=origin_list, start=start, end=end)

        assert_frame_equal(expect, result)

    def with_multi_match_monthly(db_session):
        """
        Test that only valid monthly entries are picked and that they don't
        overlap between variations, months or years.
        """
        existing_entries = [
            StatisticModel(variation_id=var_id, origin_id=orig_id, month=month,
                           year=year, quantity=qty + (year - 2020),
                           predicted=False, ean=ean)
            for var_id, ean in [(1234, '1234567891011'),
                                (2345, '1234567891012')]
            for orig_id in [1, 2, 3]
            for month, qty in [(2, 3), (3, 6), (4, 9)]
            for year in [2020, 2021]
        ]

        # 3456 should not appear as all entries are invalid.
        existing_entries += [
            StatisticModel(variation_id=3456, origin_id=3, month=4, year=2020,
                           quantity=19, predicted=False, ean='1234567891013'),
            StatisticModel(variation_id=3456, origin_id=2, month=5, year=2021,
                           quantity=20, predicted=False, ean='1234567891013'),
            StatisticModel(variation_id=3456, origin_id=2, month=3, year=2020,
                           quantity=21, predicted=True, ean='1234567891013')
        ]
        setup_database(session=db_session, sources=existing_entries)
        origin_list = ['1', '2']
        start = datetime.date(2020, 2, 1)
        end = datetime.date(2021, 4, 15)
        expect = pandas.DataFrame(
            [[1234, 2, 2020, 6, '1234567891011'],
             [1234, 2, 2021, 8, '1234567891011'],
             [1234, 3, 2020, 12, '1234567891011'],
             [1234, 3, 2021, 14, '1234567891011'],
             [1234, 4, 2020, 18, '1234567891011'],
             [1234, 4, 2021, 20, '1234567891011'],
             [2345, 2, 2020, 6, '1234567891012'],
             [2345, 2, 2021, 8, '1234567891012'],
             [2345, 3, 2020, 12, '1234567891012'],
             [2345, 3, 2021, 14, '1234567891012'],
             [2345, 4, 2020, 18, '1234567891012'],
             [2345, 4, 2021, 20, '1234567891012']],
            columns=['variation_id', 'month', 'year', 'quantity', 'ean']
        )
        collector = DataCollector(session=db_session)
        result = collector._DataCollector__get_total_quantity(
            origin_list=origin_list, start=start, end=end)

        assert_frame_equal(expect, result)

    def with_single_match_daily(db_session):
        existing_entries = [
            DailyStatisticModel(variation_id=1234, origin_id=1, day=10,
                                month=2, year=2020, quantity=1,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=2, day=16,
                                month=2, year=2020, quantity=2,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=2345, origin_id=4, day=10,
                                month=2, year=2020, quantity=3,
                                ean='1234567891012'),
            DailyStatisticModel(variation_id=1234, origin_id=1, day=10,
                                month=3, year=2020, quantity=4,
                                ean='1234567891011')
        ]
        setup_database(session=db_session, sources=existing_entries)
        origin_list = ['1', '2']
        start = datetime.date(2020, 2, 10)
        end = datetime.date(2020, 2, 15)
        expect = pandas.DataFrame(
            [[1234, 10, 2, 2020, 1, '1234567891011']],
            columns=['variation_id', 'day', 'month', 'year', 'quantity', 'ean']
        )
        collector = DataCollector(session=db_session)
        collector.set_target_daily()
        result = collector._DataCollector__get_total_quantity(
            origin_list=origin_list, start=start, end=end)

        assert_frame_equal(expect, result)

    def with_multi_match_daily(db_session):
        """
        Test that only valid daily entries are picked and that they don't
        overlap between variations, days, months or years.
        """
        existing_entries = [
            DailyStatisticModel(variation_id=var_id, origin_id=orig_id,
                                day=day, month=month, year=year,
                                quantity=qty + (year - 2020) + (month - 2),
                                ean=ean)
            for var_id, ean in [(1234, '1234567891011'),
                                (2345, '1234567891012')]
            for orig_id in [1, 2, 3]
            for day, qty in [(10, 3), (11, 6), (12, 9)]
            for month in [2, 3]
            for year in [2020, 2021]
        ]

        # 3456 should not appear as all entries are invalid.
        existing_entries += [
            DailyStatisticModel(variation_id=3456, origin_id=3, month=4,
                                year=2020, quantity=19,
                                ean='1234567891013'),
            DailyStatisticModel(variation_id=3456, origin_id=2, month=5,
                                year=2021, quantity=20,
                                ean='1234567891013')
        ]
        setup_database(session=db_session, sources=existing_entries)
        origin_list = ['1', '2']
        start = datetime.date(2020, 2, 10)
        end = datetime.date(2021, 3, 15)
        expect = pandas.DataFrame(
            [[1234, 10, 2, 2020, 6, '1234567891011'],
             [1234, 11, 2, 2020, 12, '1234567891011'],
             [1234, 12, 2, 2020, 18, '1234567891011'],
             [1234, 10, 3, 2020, 8, '1234567891011'],
             [1234, 11, 3, 2020, 14, '1234567891011'],
             [1234, 12, 3, 2020, 20, '1234567891011'],
             [1234, 10, 2, 2021, 8, '1234567891011'],
             [1234, 11, 2, 2021, 14, '1234567891011'],
             [1234, 12, 2, 2021, 20, '1234567891011'],
             [1234, 10, 3, 2021, 10, '1234567891011'],
             [1234, 11, 3, 2021, 16, '1234567891011'],
             [1234, 12, 3, 2021, 22, '1234567891011'],
             [2345, 10, 2, 2020, 6, '1234567891012'],
             [2345, 11, 2, 2020, 12, '1234567891012'],
             [2345, 12, 2, 2020, 18, '1234567891012'],
             [2345, 10, 3, 2020, 8, '1234567891012'],
             [2345, 11, 3, 2020, 14, '1234567891012'],
             [2345, 12, 3, 2020, 20, '1234567891012'],
             [2345, 10, 2, 2021, 8, '1234567891012'],
             [2345, 11, 2, 2021, 14, '1234567891012'],
             [2345, 12, 2, 2021, 20, '1234567891012'],
             [2345, 10, 3, 2021, 10, '1234567891012'],
             [2345, 11, 3, 2021, 16, '1234567891012'],
             [2345, 12, 3, 2021, 22, '1234567891012']],
            columns=['variation_id', 'day', 'month', 'year', 'quantity', 'ean']
        )
        collector = DataCollector(session=db_session)
        collector.set_target_daily()
        result = collector._DataCollector__get_total_quantity(
            origin_list=origin_list, start=start, end=end)

        assert_frame_equal(expect, result)

    def with_match_over_year_border_monthly(db_session):
        existing_entries = [
            StatisticModel(variation_id=1234, origin_id=1, month=12, year=2019,
                           quantity=1, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=2, month=1, year=2020,
                           quantity=2, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=2, month=12, year=2019,
                           quantity=3, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=1, month=3, year=2020,
                           quantity=4, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=1, month=1, year=2020,
                           quantity=5, predicted=False, ean='1234567891011')
        ]
        setup_database(session=db_session, sources=existing_entries)
        origin_list = ['1', '2']
        start = datetime.date(2019, 11, 1)
        end = datetime.date(2020, 3, 15)
        expect = pandas.DataFrame(
            [[1234, 1, 2020, 7, '1234567891011'],
             [1234, 3, 2020, 4, '1234567891011'],
             [1234, 12, 2019, 4, '1234567891011']],
            columns=['variation_id', 'month', 'year', 'quantity', 'ean']
        )
        collector = DataCollector(session=db_session)
        result = collector._DataCollector__get_total_quantity(
            origin_list=origin_list, start=start, end=end)

        assert_frame_equal(expect, result)

    def with_match_over_year_border_daily(db_session):
        existing_entries = [
            DailyStatisticModel(variation_id=1234, origin_id=1, day=31,
                                month=12, year=2019, quantity=1,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=2, day=31,
                                month=12, year=2019, quantity=2,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=2, day=1,
                                month=1, year=2020, quantity=3,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=1, day=1,
                                month=1, year=2020, quantity=4,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=1, day=3,
                                month=2, year=2020, quantity=5,
                                ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=2, day=3,
                                month=2, year=2020, quantity=6,
                                ean='1234567891011')
        ]
        setup_database(session=db_session, sources=existing_entries)
        origin_list = ['1', '2']
        start = datetime.date(2019, 12, 25)
        end = datetime.date(2020, 3, 10)
        expect = pandas.DataFrame(
            [[1234, 31, 12, 2019, 3, '1234567891011'],
             [1234, 1, 1, 2020, 7, '1234567891011'],
             [1234, 3, 2, 2020, 11, '1234567891011']],
            columns=['variation_id', 'day', 'month', 'year', 'quantity', 'ean']
        )
        collector = DataCollector(session=db_session)
        collector.set_target_daily()
        result = collector._DataCollector__get_total_quantity(
            origin_list=origin_list, start=start, end=end)

        assert_frame_equal(expect, result)


def describe_get_latest_statistic_date() -> None:
    def with_mode_daily_without_daily_statistics(db_session):
        setup_database(session=db_session, sources=[
            StatisticModel(variation_id=1234, origin_id=1, month=1, year=2020,
                           quantity=1, predicted=False, ean='1234567891011')
        ])
        expected = None
        collector = DataCollector(session=db_session)
        result = collector.get_latest_statistic_date(mode='daily')
        assert result == expected

    def with_mode_monthly_without_monthly_statistics(db_session):
        setup_database(session=db_session, sources=[
            DailyStatisticModel(variation_id=1234, origin_id=1, day=12,
                                month=1, year=2020, quantity=1,
                                ean='1234567891011')
        ])
        expected = None
        collector = DataCollector(session=db_session)
        result = collector.get_latest_statistic_date(mode='monthly')
        assert result == expected

    def with_mode_both_without_statistics(db_session):
        expected = None
        collector = DataCollector(session=db_session)
        result = collector.get_latest_statistic_date(mode='both')
        assert result == expected

    def with_mode_daily(db_session):
        setup_database(session=db_session, sources=[
            StatisticModel(variation_id=1234, origin_id=1, month=1, year=2020,
                           quantity=5, predicted=False, ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=1, day=12,
                                month=1, year=2020, quantity=1,
                                ean='1234567891011')
        ])
        expected = datetime.date(2020, 1, 12)
        collector = DataCollector(session=db_session)
        result = collector.get_latest_statistic_date(mode='daily')
        assert result == expected

    def with_mode_monthly(db_session):
        setup_database(session=db_session, sources=[
            StatisticModel(variation_id=1234, origin_id=1, month=1, year=2020,
                           quantity=5, predicted=False, ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=1, day=12,
                                month=1, year=2020, quantity=1,
                                ean='1234567891011')
        ])
        expected = datetime.date(2020, 1, 1)
        collector = DataCollector(session=db_session)
        result = collector.get_latest_statistic_date(mode='monthly')
        assert result == expected

    def with_mode_monthly_with_predictions(db_session):
        setup_database(session=db_session, sources=[
            StatisticModel(variation_id=1234, origin_id=1, month=1, year=2020,
                           quantity=5, predicted=False, ean='1234567891011'),
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=5, predicted=True, ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=1, day=12,
                                month=1, year=2020, quantity=1,
                                ean='1234567891011')

        ])
        expected = datetime.date(2020, 1, 1)
        collector = DataCollector(session=db_session)
        result = collector.get_latest_statistic_date(mode='monthly')
        assert result == expected

    def with_mode_both_with_higher_monthly_date(db_session):
        setup_database(session=db_session, sources=[
            StatisticModel(variation_id=1234, origin_id=1, month=2, year=2020,
                           quantity=5, predicted=False, ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=1, day=12,
                                month=1, year=2020, quantity=1,
                                ean='1234567891011')
        ])
        expected = datetime.date(2020, 2, 1)
        collector = DataCollector(session=db_session)
        result = collector.get_latest_statistic_date(mode='both')
        assert result == expected

    def with_mode_both_with_higher_daily_date(db_session):
        setup_database(session=db_session, sources=[
            StatisticModel(variation_id=1234, origin_id=1, month=1, year=2020,
                           quantity=5, predicted=False, ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=1, day=12,
                                month=1, year=2020, quantity=1,
                                ean='1234567891011')
        ])
        expected = datetime.date(2020, 1, 12)
        collector = DataCollector(session=db_session)
        result = collector.get_latest_statistic_date(mode='both')
        assert result == expected

    def with_mode_both_with_equal_peaks(db_session):
        setup_database(session=db_session, sources=[
            StatisticModel(variation_id=1234, origin_id=1, month=1, year=2020,
                           quantity=5, predicted=False, ean='1234567891011'),
            DailyStatisticModel(variation_id=1234, origin_id=1, day=1,
                                month=1, year=2020, quantity=1,
                                ean='1234567891011')
        ])
        expected = datetime.date(2020, 1, 1)
        collector = DataCollector(session=db_session)
        result = collector.get_latest_statistic_date(mode='both')
        assert result == expected
