# PlentyMarkets Reorder and inventory transfer tool (RAIT)

## Table of contents
[Introduction](#introduction)

[Installation](#installation)

* [Local installation](#local)
* [Docker installation](#local)
* [Database](#database)
    - [Local database setup](#database-setup)
    - [Remote database](#remote-database)
* [Configuration and Usage](#configuration-usage)

[Implementing macros in Libreoffice/Excel](#macro)

## Introduction <a name="introduction"></a>

This is a collection of tools with the unified goal of calculating the correct reorder or redistribution (moving stock in between 2 warehouses) amount. Each tool is designed to handle one sub-section of the problem
1. `plenty_sync` - communicate with Plentymarkets, pull data and create redistributions and reorders
2. `amazon_import` - Pull data from Amazon export files, this is only required if you do not maintain your FBA orders on Plentymarkets
3. `sales_statistic` - Calculate monthly and daily statistics for each variation/date/origin combination, as well as the total of all origins for each month/variation combination
4. `prediction_tool` - Use the statistics to predict future sales for multiple origins or to analyze different variations sale curves
5. `report` - Create a custom sales report for a given time range and place it into either a separate .csv file or directly into a .xlsx/.xlsm sheet
6. `update_spreadsheet` - Update a .xlsx/.xlsm sheet, with the latest data from the database
7. `control_panel` - Graphical user interface that provides the functionality of the tools with an easy interface

Everything revolves around a spreadsheet file, which is used as the user interface for the application. The reason for this choice is simply to use a common and well known and tested interface and fill it with the required data.

## Installation <a name="installation"></a>

### Local installation <a name="local"></a>

The project is managed with poetry package and dependency management tool, to install poetry no matter what system you are using open the terminal and enter:

**Linux**:  
```bash
sudo apt update && sudo apt-get install curl git python3 python3-pip python3-tk
git clone https://gitlab.com/panasiam_clothing/plenty_rait
cd plenty_rait
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3
poetry build
python3 -m pip install .
```

**Windows**:  

- Download & install a recent version of Python from [here](https://www.python.org/downloads/windows/)
- Download & install a recent version of Git from [here](https://git-scm.com/download/win)
- Clone the project and install:
```bash
git clone https://gitlab.com/panasiam_clothing/plenty_rait.git
cd plenty_rait
python -m pip install --upgrade pip && python -m pip install poetry
poetry build
python -m pip install .
```
*Troubleshooting Windows*:  
In case you get the following error while building `cchardet`: **error: Microsoft Visual C++ 14.0 is required.**
Download the [Visual Studio Build Tools](https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2017) and select the following individual components:
- Latest MSVC VS 201X C++ x64/x86 build tools
- Windows 10 SDK
- C++ CMake tools for Windows
- C++/CLI support

### Docker setup <a name="docker"></a>

This project also comes with a docker container setup, which creates an Ubuntu container with all the required dependencies and keyring set up. Currently, the container stores the keyring as plain-text, therefore you should **NOT publish** the image **publicly**.

Get the source files:

```bash
sudo apt update && sudo apt-get install git python3 python3-pip python3-tk
git clone https://gitlab.com/panasiam_clothing/plenty_rait
cd plenty_rait
```

The provided Makefile provides routines for building and running the container. The credentials for the database as well as the Plentymarkets REST API are read from an GPG-encrypted file. Fill out a text file like this:

```txt
db_user=
db_password=
api_user=
api_password=
```

Then encrypt the file with GPG and remove the plain-text file:

```bash
# Encrypt the file
gpg -e -r {MAIL-address from your GPG key} .docker/example_credentials
# Remove the plain-text file
rm .docker/example_credentials
# Test if everything worked
gpg -d .docker/example_credentials.gpg
```

Adjust the `.env` file within the root of the project:

```txt
LOCAL_DOWNLOAD_FOLDER = /home/$USER/Downloads
CREDENTIAL_FILE = .docker/example_credentials.gpg
PLENTY_RAIT_CONFIG_FOLDER = /home/$USER/.config/plenty_rait_tool
PLENTY_RAIT_LIST_FOLDER = /home/$USER/Documents/plenty_rait_lists/
```

Now build the image:
```bash
make build
```

Once finished you can run the container, starting the `ControlPanel` tool, with the paths set up from the `.env` file:

```bash
make run
```

And just as a small reminder ;). Do not publish your image in a public docker repo!

### Database <a name="database"></a>

This project was designed and tested with the **MariaDB** relational
database management system, below is a brief instruction for setting up
a MariaDB instance on your system or how to use a remote instance.
And a description for installing the required dependencies to access it
from the python application.

**Linux**:  
`sudo apt-get install libmariadb3 libmariadb-dev`

**Windows**:  
1.  Go to the [MariaDB Connector/C download page](https://mariadb.com/downloads/?tab=connectors&group=connectors_dataaccess#connectors)
2.  Ensure the "Product" drop-down reads "C connector".
3.  In the "Version" drop-down, select the version you want to download.
4.  In the "OS" drop-down, select either "MS Windows (64-bit)" or "MS Windows (32-bit)", depending on whether you need a 64-bit or 32-bit connector.
5.  Click on the "Download" button to download the MSI package.
6.  When the MSI package finishes downloading, run it and follow the on-screen instructions.


To configure MariaDB on **Debian based systems (Linux)** you will need to open your terminal and enter the following:
```bash
# Import the GnuPG signing key
sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xcbcb082a1bb943db
# Add MariaDB to the sources.list file
sudo add-apt-repository 'deb http://ftp.osuosl.org/pub/mariadb/repo/5.5/ubuntuprecise main'
# Refresh the system
sudo apt-get update
# Install MariaDB
sudo apt-get install mariadb-server
sudo mysql_secure_installation
```

Set up MariaDB on **Windows**:
1.  Go the [MariaDB download page](https://downloads.mariadb.org/)
2.  Download the latest stable version of MariaDB
3.  Run the installer and choose 'MariaDB Server' under 'Custom setup'
5.  Default instance properties: Set your root password and click next. Now tick all boxes and click next again.
6.  Install

#### Setting up database <a name='database-setup'></a>

First you will need to login into the root account of your database.

```bash
mysql -u root -p
```

Now we will create a user, that will be used by Plenty RAIT to access the database:

```bash
CREATE USER {Username} IDENTIFIED BY '{Password}';
```

Next we will create the database for Plenty RAIT, you just need to enter the following into the terminal:

```bash
CREATE DATABASE {Database}
```

At last provide all permissions for the database to our newly created user:

```bash
GRANT ALL PRIVILEGES ON {Database}.* TO '{Username}'@localhost IDENTIFIED BY '{Password}';
```

Finally, declare the database within the configuration file `database_config.ini` within the `plenty_rait_tool` configuration folder.

```ini
host=localhost
database={Database}
```

#### Using a remote database <a name="remote-database"></a>

Declare the host and the name of the database wihtin the `database_config.ini` file, within the `plenty_rait_tool` configuration folder.

```ini
host=https://remote-host.com
database=remote_database
```

### Configuration and Usage <a name="configuration-usage"></a>

Each of the sub-tools comes with a `README.md` file, where configuration and usage is described.

## Plentymarkets REST API configuration

You have to define the URL to REST API endpoint of Plentymarkets within the `plenty_api_config.json` file within the `plenty_rait_tool` configuration folder.
To locate that URL navigate to `Setup->Settings->API->Data` and copy the **Host** field *prepended by 'https://'* into the `plenty_api_base_url` configuration field.

## Warehouse configuration

The recommended method of configuring the warehouses for your setup is the control panel, which provides a convinient graphical user interface for the creation, modification and deletion of warehouses.

Plenty RAIT differentiates between two types of warehouses internal warehouses (synchronize with Plentymarkets and you have control over the locations) and external warehouses (synchronize via Amazon exports, you don't have control over the locations). The `sync_with` option within the JSON configuration or the `Amazon warehouse?` checkbox are utilized to switch between these two types. Every warehouse requires a unique set of the following 4 attributes:
* `plenty_warehouse_id`: The ID assigned to the warehouse by Plentymarkets (we utilize the ID to pull stock and to create redistributions/reorders)
* `database_column_name`: One of the columns used within the `current_stock` database table
* `sheet_column_name`: The name of the column displayed on the stock report sheet/file
* `gui_display_name`: Name used within the graphical user interface

Internal warehouses (`sync_with` = `plentymarkets`), can contain additional optional attributes like:
* `priority_buffer_mapping` (Mapping of priority levels to buffer values, useful for warehouses that are used as source warehouse for a redistribution to ensure a minimum amount of stock for each variation)
* `transit_location_id` (ID assigned to a specific location by Plentymarkets, which points to a location that is used as temporary storage for all inventory moved via an redistribution)
* `empty_threshold_quantity` (Integer value indicating the minimum amount of stock within a source warehouse, every variation with less stock will be emptied from the source warehouse (move to target warehouse))
* `empty_exclusions` (List of variation IDs assigned by Plentymarkets, that are excluded from the `empty_threshold_quantity` rule)

External warehouses (`sync_with` = `amazon`), must contain one additional attribute:
* `amazon_region` (Two letter region code, indicating the region where the Amazon warehouse is located, used for pulling stock from Amazon exports)

## Implementing macros in Libreoffice/Excel <a name="macro"></a>

### Adding a macro to Libreoffice

You will need to move the python script to the python script folder of libreoffice, you will need to do this as root/admin because this directory usually needs root/admin permissions to edit it.

**For Linux:**
```bash
sudo mv {path to Plenty RAIT}/macro/macro.py {Installation}/share/Scripts/python/plenty_rait.py
```

**In my case:**
```bash
sudo mv {path to Plenty RAIT}/macro/macro.py /lib/libreoffice/share/Scripts/python/plenty_rait.py
```

**For Windows:**
First make sure that you opened the terminal in admin mode, then execute following command:
```bash
mv {path to Plenty RAIT}\macro\macro.py {Installation}\share\Scripts\python\plenty_rait.py
```

**NOTE:** {Installation} is referring to the path where you first installed libreoffice on your computer.

You will find the macro in Libreoffice under:

__*Tools>Macros>Organize Macros>Python>LibreOffice Macros>plenty_rait*__

Now you can select the different functions and run them.

### Adding a macro to Excel

We recommend to install xlwing on your computer.
To install xlwings for python enter the following into the cmd terminal:

```bash
pip install xlwings
```

To install xlwings for Excel enter the following into the cmd terminal:

```bash
xlwings addin install
```

To configure you xlwings installation start Excel and navigate to: __*File>Options>Customize>Ribbon*__
Make sure to activate the **"Developer"** checkbox  on the right panel of this menu.

There you will find a checkbox called "Developer" in the right panel.
Make sure to check this box before you go back to your spreadsheet.

Now you should see a button called "Developer" in your top bar, select it.
Click the **"Vistual Basic button"** on the the left side of your sheet
and navigate to __*Tools>References*__ to activate the **xlwings** checkbox.

Now you have successfully configured xlwings to run in Excel.

At last, move the macro located in ```{plenty_rait}/macro/macro.py``` to the same folder as your Plenty RAIT '.xlsm' spreadsheet.

You can run the script by navigating to the xlwings tab in your xlsm sheet and clicking on run.

**Since Windows doesn't allow programms to access open files the current version of Plenty RAIT does not support executing the programm from the spreadsheet. You will need to run "macro.py" from the CLI.**

## Developer <a name='developer'></a>

### Setting up a test database

This step is fairly similar to [this step](#database-setup) with the only difference that you will need to use a fixed name and password for the database, since the unit tests expect specific credentials for a user with access to the test database. This test user will not have access to your real database!

```bash
# Login into mysql
mysql -u root -p
# Create test user
CREATE USER testuser IDENTIFIED BY 'testpw1234';
# Create test database
CREATE DATABASE testdb
# Give the test user access to the test database
GRANT ALL PRIVILEGES ON testdb.* TO 'testuser'@localhost IDENTIFIED BY 'testpw1234';
```
