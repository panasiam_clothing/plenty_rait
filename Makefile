include .env

build:
	@echo "Build the Plenty RAIT application within a docker container"
	gpg --decrypt $(CREDENTIAL_FILE) > .docker/credential_tmp
	docker build --secret id=keyring_creds,src=.docker/credential_tmp --progress=plain -t plenty_rait_production .
	rm -rf .docker/credential_tmp

run:
	@echo "Start the Plenty RAIT application"
	@echo "Fetching lists from folder: $(PLENTY_RAIT_LIST_FOLDER)"
	@if [ ! "$(shell docker ps -q -f name=plenty\_rait\_production)" ]; then \
		docker run --rm --name plenty_rait_production \
		--env="DISPLAY" -v /tmp/.X11-unix/:/tmp/.X11-unix/ \
		-v $(PLENTY_RAIT_CONFIG_FOLDER):/home/raituser/.config/plenty_rait_tool/ \
		-v $(PLENTY_RAIT_LIST_FOLDER):/home/raituser/lists \
		-v $(LOCAL_DOWNLOAD_FOLDER):/home/raituser/amazon_data \
		-it plenty_rait_production python3 -m plenty_rait ControlPanel ;\
	else \
		@echo "Application is already running" ;\
	fi
