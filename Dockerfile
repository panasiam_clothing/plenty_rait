# syntax = docker/dockerfile:1.0-experimental

FROM ubuntu:groovy AS builder
LABEL maintainer "sebastian.fricke@posteo.net"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get -y --no-install-recommends install \
    git \
    python3 \
    python3-pip \
    python3-dev \
    python3-tk \
    python3-setuptools\
    libmariadb3 \
    libmariadb-dev \
    mariadb-server \
    build-essential \
    gedit \
 && rm -rf /var/lib/apt/lists/*\
 && addgroup --gid 1000 raitgroup \
 && adduser raituser --disabled-password --disabled-login --gecos "" --uid 1000 --gid 1000 --shell /bin/bash 

ENV USER raituser
USER raituser

SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN python3 -m pip install --upgrade pip
ENV PATH /home/raituser/.local/bin:$PATH
RUN python3 -m pip install keyrings.alt==4.0.2 poetry==1.1.11 poetry-core==1.0.7 --user

COPY --chown=raituser:raitgroup . /home/raituser/plenty_rait

RUN cd /home/raituser/plenty_rait && python3 -m poetry build && python3 -m pip install .

USER root
ENV PYTHON_KEYRING_BACKEND=keyrings.alt.file.PlaintextKeyring
ENV SFILE /home/raituser/.config/plenty_rait_docker_secrets
ENV DBID plenty_rait_database_identity
ENV PLID plenty-identity

RUN python3 -m pip install keyring keyrings.alt==4.0.2

# Get the credentials from a local file on the host, do that as root as raituser doesn't have permission for the file.
# Read the credentials into the keyring and then move the keyring to the user with the right permissions
# Currently, this process works with a plaintext keyring, which is not the cleanest solution
RUN --mount=type=secret,id=keyring_creds,dst=/home/raituser/.config/plenty_rait_docker_secrets python3 -c "import keyring; keyring.set_password('$DBID', 'user', '$(grep -Po '(?<=db_user\=).+$' $SFILE)'); keyring.set_password('$DBID', 'password', '$(grep -Po '(?<=db_password\=).+$' $SFILE)'); keyring.set_password('$PLID', 'user', '$(grep -Po '(?<=api_user\=).+$' $SFILE)'); keyring.set_password('$PLID', 'password', '$(grep -Po '(?<=api_password\=).+$' $SFILE)')"

RUN mkdir -p /home/raituser/.local/share/python_keyring \
    && mv /root/.local/share/python_keyring/keyring_pass.cfg /home/raituser/.local/share/python_keyring/keyring_pass.cfg \
    && chown -R raituser:raitgroup /home/raituser/.config \
    && chown -R raituser:raitgroup /home/raituser/.local

USER raituser
