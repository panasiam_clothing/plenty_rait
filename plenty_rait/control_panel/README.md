# Control panel of the Plenty RAIT tool

## Introduction

The purpose of this sub-application of the **Plenty RAIT** tool is to act as an interface to Plenty RAIT services for the user, without any required knowledge of the CLI. It currently supports:
1. Collect all data from Plentymarkets and Amazon -> Write data to the database -> Create sales statistics -> Write collected data into spreadsheet.
2. Read redistribution data out of a spreadsheet sheet -> upload redistribution to Plentymarkets.

**Note**: All sub-applications of the **Plenty RAIT** project must be installed for the current user (*plenty_sync*, *amazon_import*, *sales_statistics*, *report*, *update_spreadsheet*, *helper*, *prediction_tool*)

## Installation

Before you can run the program, you will need to install all dependencies to ensure functionality.

```bash
# Clone the the project from GitLab to your computer.
git clone https://gitlab.com/initBasti/plenty_rait.git
# Navigate to the control_panel directory.
cd plenty_rait/control_panel
# Install all dependencies
poetry build
tar -xf dist/control_panel-0.1.1.tar.gz -C ~/
python3 ~/control_panel-0.1.1/setup.py install
python3 -m pip install dist/control_panel-0.1.0.tar.gz
python3 -m plenty_rait ControlPanel -h
```

Alternatively you can run the install.sh script after you cloned the repository from GitLab located in the utils folder of the project.

## Configuration

The configuration file is located under `~/.config/plenty_rait/control_panel/config.ini` on a Linux system and under `C:\users\{$USER}\.config\plenty_rait\control_panel\config.ini` on a Windows system, it will be created automatically when you launch the application for the first time.

The application requires some basic information in order to work properly, you have to provide the path to the **Plenty RAIT** tool, the path to where your amazon data will be located and the PATH variable name of you python installation.

Example:
```ini
[GENERAL]
amazon_data = /home/test/documents/amazon_data
python = python3
```

In addition, the redistribution workflow needs a mapping of your warehouse names to their Plentymarkets IDs (located the warehouse IDs under Setup->Stock->Warehouse->{target warehouse}->Settings):
```ini
[WAREHOUSES]
warehouse_alpha = 105
warehouse_123 = 321
```

## Usage

You can simply call the control panel by typing the following into the cli:
```bash
python3 -m plenty_rait ControlPanel
```
