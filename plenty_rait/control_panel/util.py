"""
User interface to various services of the Plenty RAIT tool
Copyright © 2021 Jan Sallermann & Sebastian Fricke (Panasiam)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import configparser
import datetime
from functools import partial
import json
import os
import pathlib
import sys
import webbrowser
import csv
from tkinter import messagebox
import pandas
import pandas.errors
from loguru import logger

from plenty_rait.helper.file_handling import get_encoding
from plenty_rait.helper.config_helper import (
    ConfigHandler as BaseConfigHandler, WarehouseConfigHandler
)

from plenty_rait.control_panel.config_schema import (
    control_panel_configuration_schema
)

from plenty_rait.plenty_sync.utils import PlentySyncConfigHandler
from plenty_rait.plenty_sync.config_schema import (
    plenty_sync_configuration_schema
)
from plenty_rait.amazon_import.utils import AmazonImportConfigHandler
from plenty_rait.amazon_import.config_schema import (
    amazon_import_configuration_schema
)
from plenty_rait.report.utils import ReportConfigHandler
from plenty_rait.report.config_schema import (
    report_configuration_schema
)
from plenty_rait.update_spreadsheet.utils import UpdateSpreadsheetConfigHandler
from plenty_rait.update_spreadsheet.config_schema import (
    update_spreadsheet_configuration_schema
)
from plenty_rait.inbound_shipment.utils import InboundShipmentConfigHandler
from plenty_rait.inbound_shipment.config_schema import (
    inbound_shipment_configuration_schema
)
from plenty_rait.reorder_document.utils import ReorderDocumentConfigHandler
from plenty_rait.reorder_document.config_schema import (
    reorder_document_configuration_schema
)

class ExportFileHandler:
    def __init__(self, amazon_path):
        self.amazon_path = amazon_path
        # TODO this could be user configurable, if required
        self.required_files = [
            {
                'type': 'order',
                'region': 'EU',
                'search_column': 'sales-channel',
                'match_values': ['Amazon.de', 'Amazon.fr',
                                 'Amazon.it', 'Amazon.es']
            },
            {
                'type': 'order',
                'region': 'US',
                'search_column': 'sales-channel',
                'match_values': ['Amazon.com']
            },
            {
                'type': 'stock',
                'region': 'EU',
                'search_column': 'Currency code',
                'match_values': ['EUR']
            },
            {
                'type': 'stock',
                'region': 'US',
                'search_column': 'Currency code',
                'match_values': ['USD']
            }
        ]

    @staticmethod
    def __read_file(path: pathlib.Path) -> pandas.DataFrame:
        """
        Detect the encoding and delimiter, and read the file into a dataframe.

        Parameters:
            path            [Path]      -   Full path to the file

        Returns:
                            [DataFrame]
        """
        encoding = get_encoding(path=path, logger=logger)
        with open(path, mode='r', encoding=encoding) as export:
            try:
                sep = csv.Sniffer().sniff(
                    export.readline(), [',', '\t']
                ).delimiter
            except csv.Error:
                return pandas.DataFrame()
            export.seek(0)
        return pandas.read_csv(path, sep=sep, encoding=encoding)

    @staticmethod
    def __delete_old_files(folder: pathlib.Path, deletion_interval: int) -> None:
        """
        Delete files without any modification in the last @deletion_interval days.

        Parameters:
            folder              [Path]      -   Path to the folder to rotate
            deletion_interval   [int]       -   Number of days to wait before a
                                                deletion
        """
        now = datetime.datetime.now().date()
        for export in folder.iterdir():
            last_change = datetime.date.fromtimestamp(export.stat().st_mtime)
            if now - last_change >= datetime.timedelta(days=deletion_interval):
                export.unlink()

    def __analyse_file(self, path: pathlib.Path) -> bool:
        """
        Check if the given file matches one of the required file types.

        Parameters:
            path            [path]      -   full path to the file

        Returns:
                            [bool]      -   Indicates if the file matches
                                            the pattern of a required file
        """
        try:
            dataframe = self.__read_file(path=path)
        except pandas.errors.ParserError:
            messagebox.showwarning(
                'WARNING',
                f'Invalid file \'{path.name}\' could not be read.'
            )
            return False
        if len(dataframe.index) == 0:
            messagebox.showwarning(
                'WARNING',
                f'Invalid file \'{path.name}\' is neither a tabulator nor '
                'comma separated value file (.tsv/.csv)'
            )
            return False
        for required_file in self.required_files:
            column = required_file['search_column']
            values = required_file['match_values']
            if column in dataframe.columns:
                if any(value in dataframe[column].to_list()
                       for value in values):
                    if 'dataframe' in required_file:
                        # We reach this either when the user goes back
                        # or when there are two exports for the same
                        # region and type.
                        return True
                    required_file['dataframe'] = dataframe
                    required_file['path'] = path
                    return True
        messagebox.showwarning(
            'WARNING',
            f'The \'{path.name}\' file does not match any of the required file'
            ' patterns.'
        )
        return False

    def fetch_date_range(self) -> dict:
        """
        Get the minimum and maximum dates of all order files and find the
        greatest possible window.

        Returns:
                            [dict]      -   start and end dates of the date
                                            range window
        """
        order_frames = [x['dataframe'] for x in self.required_files
                        if x['type'].lower() == 'order']
        min_date = max_date = 0
        for frame in order_frames:
            dates = pandas.to_datetime(
                frame['purchase-date'], infer_datetime_format=True
            ).apply(lambda x: x.date())
            frame_min_date = dates.min().isoformat()
            frame_max_date = dates.max().isoformat()
            if min_date == 0:
                min_date = frame_min_date
            else:
                if min_date != frame_min_date:
                    logger.warning('Order export files do not cover the same '
                                   'date range')
                min_date = min([min_date, frame_min_date])

            if max_date == 0:
                max_date = frame_max_date
            else:
                if max_date != frame_max_date:
                    logger.warning('Order export files do not cover the same '
                                   'date range')
                max_date = max([max_date, frame_max_date])

        return {'start': min_date, 'end': max_date}

    def check_files(self) -> bool:
        """
        Analyse all files within @amazon_path to check if all files configured
        within @required_files are found.
        The conditions are defined within the @required_files structure.
        """
        if not self.amazon_path.exists():
            messagebox.showerror('ERROR', f'Folder at {self.amazon_path} does '
                                 'not exist.')
            return False
        invalid_files_folder = self.amazon_path / 'invalid_files'
        invalid_files_folder.mkdir(exist_ok=True)
        for file_path in self.amazon_path.iterdir():
            if file_path.is_dir():
                continue
            if file_path.suffix not in ['.csv', '.txt', '.tsv']:
                continue
            current_file = self.amazon_path / file_path
            status = self.__analyse_file(path=current_file)
            if not status:
                current_file.replace(invalid_files_folder / file_path.name)
        self.__delete_old_files(
            folder=invalid_files_folder, deletion_interval=14
        )

        if any('dataframe' not in x for x in self.required_files):
            # pylint: disable=invalid-string-quote
            required_types = [
                (f"{x['type']}_{x['region']}", 'dataframe' in x)
                for x in self.required_files
            ]
            message = str(
                "To update the list all required Amazon exports have to be "
                f'placed into:\n{self.amazon_path}\n\n'
                f"Required files:\n{[x[0] for x in required_types]}.\n\n"
                f"Missing files:\n{[x[0] for x in required_types if not x[1]]}"
            )
            messagebox.showerror('ERROR', message)
            return False

        return True

    def get_path(self, data_type: str, region: str) -> pathlib.Path:
        for required_file in self.required_files:
            if (
                'path' in required_file and
                required_file['type'].lower() == data_type and
                required_file['region'] == region
            ):
                return required_file['path']
        raise KeyError(f'No path found for the {data_type}_{region} export')

    def move_files_to_old_files(self) -> None:
        """
        Move all required files to the 'old_files' folder after processing
        them
        """
        # pylint: disable=invalid-string-quote
        old_files_folder = self.amazon_path / 'old_files'
        old_files_folder.mkdir(exist_ok=True)
        today = datetime.date.today()
        for required_file in self.required_files:
            extension = 'csv' if required_file['type'] == 'stock' else 'txt'
            type_region = f"{required_file['type']}_{required_file['region']}"
            required_file['path'].replace(
                old_files_folder / str(f'{type_region}_{today}.{extension}')
            )
        self.__delete_old_files(folder=old_files_folder, deletion_interval=14)


# Custom errors for the ConfigHandler
class InvalidConfigurationError(Exception):
    def __init__(self, app_name: str, reason: str):
        super().__init__('')
        self.app_name = app_name
        self.reason = reason

    def __str__(self):
        return str(f'The configuration for the {self.app_name} '
                   f'application is invalid, {self.reason}')


class MissingConfigurationError(Exception):
    def __init__(self, app_name: str, path: str):
        super().__init__('')
        self.app_name = app_name
        self.path = path

    def __str__(self):
        return str(f'The configuration for the {self.app_name} '
                   'application was missing and has been created '
                   f'automatically at {self.path}.\nPlease fill out the '
                   'configuration and restart the application.')


class MissingConfigurationOptionError(Exception):
    def __init__(self, app_name: str, section: str, option: str):
        super().__init__('')
        self.app_name = app_name
        self.section = section
        self.option = option

    def __str__(self):
        return str(f'Unable to locate option \'{self.option}\' in the '
                   f'\'{self.section}\' section within the configuration for '
                   f'the \'{self.app_name}\' application.')


class ControlPanelConfigHandler(BaseConfigHandler):
    def __init__(self, name: str, config_type: str, config_schema: dict):
        self.config: dict
        self.warehouse_handler: WarehouseConfigHandler
        super().__init__(name=name, config_type=config_type,
                         config_schema=config_schema)

class ConfigHandler:
    def __init__(self, applications: list, base_path: pathlib.Path):
        """
        Parameters:
            applications    [list]      -   List of tuples containing the
                                            name of the application and the
                                            type of configuration
            base_path       [Path]      -   Folder location where the
                                            configurations of all applications
                                            are stored
        """
        control_panel_config_handler = ControlPanelConfigHandler(
            name='control_panel', config_type='json',
            config_schema=control_panel_configuration_schema
        )
        self.configs: dict = {
            'control_panel': {
                'data': control_panel_config_handler.config,
                'type': 'json',
                'path': base_path / 'control_panel' / 'config.json'
            }
        }
        self.warehouse_handler = control_panel_config_handler.warehouse_handler
        for app_name, config_type in applications:
            folder = base_path / app_name
            folder.mkdir(exist_ok=True)
            if config_type == 'ini':
                path = folder / 'config.ini'
            elif config_type == 'json':
                path = folder / 'config.json'
            else:
                raise RuntimeError('Use of unsupported configuration type '
                                   f'{config_type}')
            if not path.exists():
                path.touch()
                raise MissingConfigurationError(app_name=app_name, path=path)
            config_data = self.__read_config_data(
                path=path, app_name=app_name, config_type=config_type
            )
            self.configs[app_name] = {
                'data': config_data, 'type': config_type, 'path': path
            }

    @staticmethod
    def __read_config_data(path: pathlib.Path, app_name: str, config_type: str) -> dict:
        func_map = {
            'plenty_sync': partial(
                PlentySyncConfigHandler, 'plenty_sync', 'json',
                plenty_sync_configuration_schema
            ),
            'amazon_import': partial(
                AmazonImportConfigHandler, 'amazon_import', 'json',
                amazon_import_configuration_schema
            ),
            'report': partial(
                ReportConfigHandler, 'report', 'json',
                report_configuration_schema
            ),
            'update_spreadsheet': partial(
                UpdateSpreadsheetConfigHandler, 'update_spreadsheet', 'json',
                update_spreadsheet_configuration_schema
            ),
            'inbound_shipment': partial(
                InboundShipmentConfigHandler, 'inbound_shipment', 'json',
                inbound_shipment_configuration_schema
            ),
            'reorder_document': partial(
                ReorderDocumentConfigHandler, 'reorder_document', 'json',
                reorder_document_configuration_schema
            ),
            'control_panel': partial(
                ControlPanelConfigHandler, 'control_panel', 'json',
                control_panel_configuration_schema
            )
        }
        if config_type == 'ini':
            config = configparser.ConfigParser()
            config.read(path)
        else:
            config_handler = func_map[app_name]()
            config = config_handler.config
        return config

    def __update_config(self, app_name: str) -> None:
        """
        Update the in-memory representation of the config data for @app_name.

        Parameters:
            app_name        [str]       -   Name of the sub-application
        """
        if app_name not in self.configs:
            raise RuntimeError(f'Invalid application name [{app_name}]')
        config_path = self.configs[app_name]['path']
        config_type = self.configs[app_name]['type']
        config_data = self.__read_config_data(path=config_path,
                                              app_name=app_name,
                                              config_type=config_type)
        if not config_data:
            raise RuntimeError(f'Empty {app_name} configuration found.')

        self.configs[app_name]['data'] = config_data

    def edit_config(self, app_name: str) -> None:
        """
        Open the configuration of the @app_name sub-application in an editor.

        Parameters:
            app_name            [str]       -   Name of the sub-application
        """
        path = self.configs[app_name]['path']
        if sys.platform == 'linux':
            os.system('gedit ' + str(path))
        else:
            # webbrowser will open the default windows editor
            webbrowser.open(path)
        self.__update_config(app_name=app_name)

    def write_config(self, app_name) -> None:
        if app_name not in self.configs:
            raise RuntimeError(f'Invalid application name [{app_name}]')
        config = self.configs[app_name]
        with open(config['path'], 'w', encoding='UTF-8') as config_file:
            if config['type'] == 'ini':
                config['data'].write(config_file)
            elif config['type'] == 'json':
                config_file.write(json.dumps(config['data'], indent=4))

    def get_ini_value(self, app_name: str, section: str, option: str) -> str:
        """ Get the value of an INI configuration file """
        if app_name not in self.configs:
            raise RuntimeError(f'Invalid application name [{app_name}]')
        config = self.configs[app_name]
        if config['type'] != 'ini':
            raise RuntimeError(f'Configuration for [{app_name}] is not an '
                               'INI config')
        if section not in config['data']:
            raise MissingConfigurationOptionError(app_name=app_name,
                                                  section=section,
                                                  option=option)
        if option not in config['data'][section]:
            raise MissingConfigurationOptionError(app_name=app_name,
                                                  section=section,
                                                  option=option)
        return config['data'][section][option]

    def add_section(self, app_name: str, section_name: str) -> None:
        """ Add a new section to the configuration if it doesn't exist """
        if app_name not in self.configs:
            raise RuntimeError(f'Invalid application name [{app_name}]')
        config = self.configs[app_name]
        if config['type'] == 'ini':
            if not config['data'].has_section(section_name):
                config['data'].add_section(section_name)
        elif config['type'] == 'json':
            config['data'].update({section_name: {}})
