# pylint: disable=invalid-string-quote, line-too-long
control_panel_configuration_schema = {
    "title": "Control Panel configuration schema",
    "type": "object",
    "required": [
        "amazon_export_path",
        "fba_extern"
    ],
    "properties": {
        "amazon_export_path": {
            "type": "string",
            "title": "Amazon export file folder",
            "description": "Folder to check for Amazon export files"
        },
        "fba_extern": {
            "type": "string",
            "title": "Fba external handling",
            "description":
            "Determines if FBA orders and Amazon warehouse stock is imported"
            " from export files (yes) or from Plentymarkets (no)",
            "default": "yes",
            "enum": ["yes", "no"]
        },
        "output_directory": {
            "type": "string",
            "title": "Inbound output destination",
            "description":
            "Destination folder for the inbound plans created by the "
            "inbound_shipment tool."
        }
    }
}
