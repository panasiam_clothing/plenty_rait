"""
User interface to various services of the Plenty RAIT tool
Copyright © 2021 Jan Sallermann & Sebastian Fricke (Panasiam)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
from tkinter import messagebox
from plenty_rait.control_panel.service import Interface
from plenty_rait.control_panel.util import (
    ConfigHandler, InvalidConfigurationError, MissingConfigurationError,
    MissingConfigurationOptionError
)
from plenty_rait.helper.setup_helper import CommandLineSetup
from plenty_rait.helper.config_helper import init_config


class ControlPanelCommandLine(CommandLineSetup):
    @staticmethod
    def setup_parser(subparser) -> None:
        subparser.add_parser(
            'ControlPanel',
            help='Graphical user interface for working with the orderlist'
        )

    @staticmethod
    def handle_command(args: argparse.Namespace,
                       connection_string: str) -> bool:
        del args
        config_path = init_config(name='control_panel')
        application_names = [
            ('plenty_sync', 'json'),
            ('amazon_import', 'json'),
            ('sales_statistic', 'ini'),
            ('prediction_tool', 'ini'),
            ('report', 'json'),
            ('update_spreadsheet', 'json'),
            ('inbound_shipment', 'json'),
            ('reorder_document', 'json')
        ]
        try:
            config_handler = ConfigHandler(applications=application_names,
                                           base_path=config_path.parent.parent)
        except InvalidConfigurationError as err:
            messagebox.showerror('ERROR', err)
            return False
        except MissingConfigurationError as err:
            messagebox.showerror('ERROR', err)
            return False
        except MissingConfigurationOptionError as err:
            messagebox.showerror('ERROR', err)
            return False

        interface = Interface(config_handler=config_handler,
                              database_connection_string=connection_string)
        interface.root.mainloop()
        return True
