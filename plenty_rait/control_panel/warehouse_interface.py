
from collections import defaultdict
from copy import deepcopy
import re
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import awesometkinter as atk

from plenty_rait.helper.warehouse_config_schema import (
    warehouse_config_schema
)
from plenty_rait.control_panel.interface import (
    ENTRY_COLOR, ERROR_COLOR, CustomFrame, LabeledEntry, FRAME_COLOR,
    TEXT_COLOR, BUTTON_COLOR, ACTIVE_BUTTON_COLOR
)


class WarehouseEdit(tk.Frame):
    def __init__(self, parent, warehouse: dict) -> None:
        tk.Frame.__init__(self, parent, background=FRAME_COLOR)
        self.parent = parent
        self.warehouse = warehouse
        self.logic_layer = parent.parent.logic_layer
        self.remove_button: tk.Button
        self.label_length = 220
        self.entry_length = 90
        self.columns = [0, 370]
        self.__add_warehouse_frame()
        self.entries: dict
        self.set_values()

    def __add_warehouse_frame(self) -> None:
        self.entries = {
            'plenty_warehouse_id': {
                'name': 'Plentymarkets warehouse ID:', 'column': 1, 'row': 1,
                'tooltip': 'ID assigned to the warehouse by Plentymarkets'
            },
            'database_column_name': {
                'name': 'Database column name:', 'column': 1, 'row': 2,
                'tooltip': 'Generic database column name in the\n'
                '`current_stock` database table'
            },
            'gui_display_name': {
                'name': 'Display name:', 'column': 1, 'row': 3,
                'tooltip': 'Name used within a report or\nas column name in '
                'the orderlist'
            },
            'sheet_column_name': {
                'name': 'Spreadsheet column name:', 'column': 1, 'row': 4,
                'tooltip': 'Name used within a report or\nas column name in '
                'the orderlist'
            },
            'priority_buffer_mapping': {
                'name': 'Buffer-priority mapping:', 'column': 1, 'row': 5,
                'tooltip': 'Assignment of a buffer quantity to a priority '
                'level,\nexample: ' '1:30;2:20;*:10'
            },
            'empty_threshold_quantity': {
                'name': 'Empty locations with stock lower than:', 'column': 2,
                'row': 1, 'tooltip': 'Useful option when this warehouse\n'
                'should contain a minimum amount of stock\nat each location '
                'to avoid wasting space'
            },
            'empty_exclusions': {
                'name': 'Empty location exceptions:', 'column': 2, 'row': 2,
                'tooltip': 'Comma-separated list of Plentymarkets variation '
                'IDs\nthat are excluded from the empty threshold.'
            },
            'transit_location_id': {
                'name': 'Transit location ID:', 'column': 2, 'row': 3,
                'tooltip': 'ID of the storage location in the warehouse\nthat '
                'is used as temporary storage of products',
                'sync_with': 'plentymarkets'
            },
            'amazon_region': {
                'name': 'Region identifier:',
                'column': 2, 'row': 4,
                'tooltip': '2 letter country code in upper case letters for '
                'region (EU, US, UK, etc.)',
                'sync_with': 'amazon'
            }
        }

        for value in self.entries.values():
            value['object'] = LabeledEntry(
                parent=self, label_text=value['name'],
                label_len=self.label_length, entry_len=self.entry_length
            )
            value['object'].label.config(anchor='e')
            value['object'].place(
                x=self.columns[value['column'] - 1],
                y=(value['row'] * 25) - 20,
                width=self.label_length + self.entry_length + 20, height=25
            )
            atk.tooltip(value['object'], text=value['tooltip'])
        self.entries['sync_with'] = {}
        self.entries['sync_with']['value'] = tk.BooleanVar()
        self.entries['sync_with']['value'].trace(
            'w', self.__warehouse_type_switch
        )
        self.entries['sync_with']['object'] = tk.Checkbutton(
            self,
            text='Amazon warehouse?',
            justify='right',
            variable=self.entries['sync_with']['value'],
            background=FRAME_COLOR, foreground=TEXT_COLOR,
            activebackground=ACTIVE_BUTTON_COLOR,
            highlightthickness=0
        )
        self.entries['sync_with']['object'].place(
            x=self.columns[-1], y=(5 * 25) - 20,
            width=self.label_length, height=25
        )
        atk.tooltip(
            self.entries['sync_with']['object'],
            text='Choose this option if the warehouse stock should not be\n'
            'synchronized with Plentymarkets'
        )
        if self.warehouse:
            self.add_remove_button()
        # Deactivate amazon region field by default as the default warehouse
        # type is plentymarkets
        self.entries['amazon_region']['object'].entry['state'] = 'disabled'

    def add_remove_button(self) -> None:
        self.remove_button = tk.Button(
            self, text='Delete',
            command=self.__remove_warehouse,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.remove_button.place(
            x=self.columns[-1] + self.label_length + 5, y=(5 * 25) - 20,
            width=self.entry_length, height=20
        )

    def __remove_warehouse(self):
        self.parent.remove_warehouse(
            warehouse_id=self.warehouse['plenty_warehouse_id']
        )

    def set_values(self) -> None:
        if not self.warehouse:
            return
        for option in self.entries:
            try:
                warehouse_option = self.warehouse[option]
            except KeyError:
                try:
                    warehouse_option = self.warehouse['attributes'][option]
                except KeyError:
                    continue
            if option == 'sync_with':
                self.entries[option]['value'].set(
                    warehouse_option == 'amazon'
                )
                continue
            if option == 'priority_buffer_mapping':
                # pylint: disable=invalid-string-quote
                self.entries[option]['object'].entry_text.set(
                    self.logic_layer.prio_buffer_config_to_str(
                        config=warehouse_option
                    )
                )
                continue
            if option == 'empty_exclusions':
                self.entries[option]['object'].entry_text.set(
                    ', '.join(warehouse_option)
                )
                continue
            self.entries[option]['object'].entry_text.set(
                warehouse_option
            )

    def __warehouse_type_switch(self, *args) -> None:
        del args
        if not self.amazon_warehouse():
            for field in [
                'transit_location_id', 'priority_buffer_mapping',
                'empty_threshold_quantity', 'empty_exclusions'
            ]:
                self.entries[field]['object'].entry['state'] = 'normal'
            self.entries['amazon_region']['object'].entry['state'] = 'disabled'
        else:
            for field in [
                'transit_location_id', 'priority_buffer_mapping',
                'empty_threshold_quantity', 'empty_exclusions'
            ]:
                self.entries[field]['object'].entry['state'] = 'disabled'
            self.entries['amazon_region']['object'].entry['state'] = 'normal'

    def amazon_warehouse(self) -> bool:
        return self.entries['sync_with']['value'].get()

    def get(self):
        return {
            name: entry['object'].get()
            if name != 'sync_with' else self.amazon_warehouse()
            for name, entry in self.entries.items()
        }


class WarehouseChooser(tk.Frame):
    def __init__(self, parent, warehouses: list) -> None:
        tk.Frame.__init__(self, parent, background=FRAME_COLOR)
        self.parent = parent
        self.warehouses = warehouses
        self.logic_layer = parent.logic_layer
        self.menu = ttk.Notebook(self)
        self.menu.place(x=0, y=0, width=700, height=150)
        self.menu.bind('<<NotebookTabChanged>>', self.__verify_correct)
        self.ready_to_save = tk.BooleanVar()
        self.warehouse_frames = {}
        for warehouse in self.warehouses:
            frame = WarehouseEdit(parent=self, warehouse=warehouse)
            self.menu.add(frame, text=warehouse['gui_display_name'])
            self.warehouse_frames[warehouse['plenty_warehouse_id']] = frame
        frame = WarehouseEdit(parent=self, warehouse={})
        self.menu.add(frame, text='+')
        self.warehouse_frames[-1] = frame
        self.__verify_correct()

        for frame in self.warehouse_frames.values():
            self.__trace_input_fields(frame=frame)

    def __trace_input_fields(self, frame: WarehouseEdit) -> None:
        for field in frame.entries.values():
            if 'name' in field:
                field['object'].entry_text.trace('w', self.__verify_correct)

    def __get_current_frame(self) -> tuple:
        """
        Get the WarehouseEdit object related to the current notebook selection.

        Returns:
                                [tuple]         -   WarehouseEdit object and
                                                    the corresponding index
                                                    within the list of
                                                    warehouse frames
        """
        current_tab = self.menu.tab(self.menu.select(), 'text')
        for index, warehouse in enumerate(self.warehouses):
            if warehouse['gui_display_name'] == current_tab:
                warehouse_id = int(warehouse['plenty_warehouse_id'])
                return (self.warehouse_frames[warehouse_id], index)
        return (self.warehouse_frames[-1], -1)

    @staticmethod
    def __get_required_keys() -> list:
        required = deepcopy(
            warehouse_config_schema[
                'properties']['warehouses']['items']['required']
        )
        return [key for key in required if key != 'sync_with']

    def __verify_correct(self, *args) -> None:
        """ Determine if the current input can be saved """
        del args
        (matching_frame, _) = self.__get_current_frame()
        matching_values = matching_frame.get()
        # copy the required attributes to avoid mutating the original
        required = self.__get_required_keys()
        if matching_frame.amazon_warehouse():
            required.append('amazon_region')
        if all(
            matching_values[option] for option in required
            if option != 'sync_with'
        ):
            self.__set_entries_background(
                frame=matching_frame, options=required, color=ENTRY_COLOR
            )
            self.ready_to_save.set(True)
        else:
            incorrect_fields = [
                option for option in required if not matching_values[option]
            ]
            self.__set_entries_background(
                frame=matching_frame, options=incorrect_fields,
                color=ERROR_COLOR
            )
            self.__set_entries_background(
                frame=matching_frame,
                options=[x for x in required if x not in incorrect_fields],
                color=ENTRY_COLOR
            )
            self.ready_to_save.set(False)

    @staticmethod
    def __set_entries_background(
        frame: WarehouseEdit, options: list, color: str
    ) -> None:
        # The sync_with field is a checkbox and not an entry field, remove it
        if 'sync_with' in options:
            options.remove('sync_with')
        for option in options:
            frame.entries[option]['object'].entry.config(
                {'background': color}
            )

    def get_current_warehouse_configuration(self) -> list:
        (matching_frame, index) = self.__get_current_frame()
        warehouse = self.get_warehouse_configuration(frame=matching_frame)
        if not warehouse:
            return []
        duplicates = self.__duplication_check(warehouse=warehouse, index=index)
        if duplicates:
            messagebox.showwarning(
                'Duplicate warehouse elements',
                f'The following elements: {duplicates} already exist.\n'
                'Entires must be unique.'
            )
            self.__set_entries_background(
                frame=matching_frame, options=duplicates, color=ERROR_COLOR
            )
            return []
        try:
            warehouse_index = [
                index for index, wh in enumerate(self.warehouses)
                if int(wh['plenty_warehouse_id']) ==
                int(warehouse['plenty_warehouse_id'])
            ][0]
            self.warehouses[warehouse_index] = warehouse
        except IndexError:
            self.warehouses.append(warehouse)
        return self.warehouses

    def get_warehouse_configuration(self, frame: WarehouseEdit) -> dict:
        values = frame.get()
        warehouse = defaultdict(dict)
        for key, value in values.items():
            if key == 'sync_with':
                warehouse[key] = 'amazon' if value else 'plentymarkets'
                continue
            if key == 'priority_buffer_mapping':
                if not value:
                    continue
                buffer_config = self.logic_layer.prio_buffer_str_to_config(
                    user_input=value
                )
                if not buffer_config:
                    self.__set_entries_background(
                        frame=frame, options=[key], color=ERROR_COLOR
                    )
                else:
                    self.__set_entries_background(
                        frame=frame, options=[key], color=ENTRY_COLOR
                    )
                warehouse['attributes'][key] = buffer_config
                continue
            if key == 'empty_exclusions':
                if not value:
                    continue
                if 'empty_threshold_quantity' not in values:
                    messagebox.showwarning(
                        'Useless exceptions',
                        'Defining exceptions from the location emptying rule '
                        'is only useful if a threshold is defined within the '
                        '\'Empty locations with stock lower than:\' field.'
                    )
                warehouse['attributes'][key] = [
                    int(variation)
                    for variation in re.sub('[^0-9,]', '', value).split(',')
                ]
                continue
            if key in [
                'plenty_warehouse_id', 'transit_location_id',
                'empty_threshold_quantity'
            ]:
                if not value:
                    continue
                if key == 'plenty_warehouse_id':
                    warehouse[key] = int(value)
                else:
                    warehouse['attributes'][key] = int(value)
                continue
            if key == 'amazon_region' and frame.amazon_warehouse():
                if not value:
                    self.__set_entries_background(
                        frame=frame, options=[key], color=ERROR_COLOR
                    )
                    return {}
                warehouse['attributes'][key] = value.upper()
            elif (
                key == 'amazon_region' and
                not frame.amazon_warehouse()
            ):
                continue
            warehouse[key] = value
        return warehouse

    def add_new_warehouse(self) -> None:
        (_, index) = self.__get_current_frame()
        if index != -1:
            return
        new_frame = self.warehouse_frames[-1]
        values = new_frame.get()
        warehouse_id = int(values['plenty_warehouse_id'])
        warehouse = [
            wh for wh in self.warehouses
            if int(wh['plenty_warehouse_id']) == warehouse_id
        ][0]
        new_frame.warehouse = warehouse
        new_frame.add_remove_button()
        self.warehouse_frames[warehouse_id] = new_frame

        self.menu.tab(
            tab_id=self.menu.index(tab_id='end') - 1,
            text=values['gui_display_name']
        )
        frame = WarehouseEdit(parent=self, warehouse={})
        self.__trace_input_fields(frame=frame)
        self.menu.add(frame, text='+')
        self.warehouse_frames[-1] = frame

    def remove_warehouse(self, warehouse_id: int) -> None:
        try:
            index = [
                index for index, wh in enumerate(self.warehouses)
                if wh['plenty_warehouse_id'] == warehouse_id
            ][0]
        except IndexError:
            return
        if self.logic_layer.remove_warehouse(index=index):
            self.menu.forget(self.menu.tabs().index(self.menu.select()))

    def __duplication_check(self, warehouse: dict, index: int) -> list:
        # New warehouse compare with all warehouses
        if index == -1:
            warehouses = self.warehouses
        # Existing warehouse compare only with other existing warehouses
        else:
            warehouses = self.warehouses[:index] + self.warehouses[index + 1:]
        incorrect_keys = []
        for existing_warehouse in warehouses:
            for key in self.__get_required_keys():
                if existing_warehouse[key] == warehouse[key]:
                    incorrect_keys.append(key)
        return incorrect_keys


class WarehouseFrame(CustomFrame):
    def __init__(self, parent, logic_layer):
        CustomFrame.__init__(self, parent, 'Warehouse configuration')
        self.logic_layer = logic_layer

        self.run_save_changes = tk.Button(
            self, text='Save',
            command=logic_layer.save_changes,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.back_button = tk.Button(
            self, text='Back',
            command=lambda: parent.show_frame('MENU'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )

        warehouses = parent.config_handler.warehouse_handler.get()
        self.chooser = WarehouseChooser(parent=self, warehouses=warehouses)
        self.chooser.place(x=25, y=70, width=700, height=160)
        self.chooser.ready_to_save.trace(
            'w', self.__activate_save_button
        )

        self.run_save_changes.place(x=90, y=230, width=150, height=50)
        self.back_button.place(x=500, y=230, width=150, height=50)

    def __activate_save_button(self, *args) -> None:
        del args
        if self.chooser.ready_to_save.get():
            self.run_save_changes['state'] = 'normal'
        else:
            self.run_save_changes['state'] = 'disabled'

    def get(self) -> list:
        return self.chooser.get_current_warehouse_configuration()

    def signal_success(self) -> None:
        background = self.cget('background')
        active_background = self.run_save_changes['activebackground']
        self.run_save_changes.config(
            {
                'background': 'Green', 'activebackground': 'Green'
            }
        )
        label = tk.Label(
            self, text='successfully saved.', font=('Helvetica bold', 8),
            background=FRAME_COLOR, foreground=TEXT_COLOR
        )
        label.place(x=240, y=245, width=200, height=25)
        self.after(1500, self.parent.root.update())
        self.run_save_changes.config(
            {
                'background': background, 'activebackground': active_background
            }
        )
        label.destroy()
        # In case a new warehouse was created adjust the menu
        self.chooser.add_new_warehouse()
