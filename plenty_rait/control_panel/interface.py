"""
User interface to various services of the Plenty RAIT tool
Copyright © 2021 Jan Sallermann Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
# pylint: disable=too-many-ancestors
from functools import partial
import math
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import pathlib


HEADER_FONT_SIZE = 25
HEADER_FONT = ('Helvetica Bold', HEADER_FONT_SIZE)
STATUS_FONT = ('Helvetica Bold', 13)
LABEL_FONT = ('Helvetica', 13)
NOTE_FONT = ('Arial', 9)


# Colors
FRAME_COLOR = '#f4cccc'
TEXT_COLOR = '#000000'
ENTRY_COLOR = '#ffffff'
BUTTON_COLOR = '#fffaed'
ACTIVE_BUTTON_COLOR = '#cfe2f3'
ERROR_COLOR = '#ffb6a1'


class CustomFrame(tk.Frame):
    """ Base class for each page that adds a centered title """
    def __init__(self, parent, title):
        tk.Frame.__init__(self, parent.container, background=FRAME_COLOR)
        self.parent = parent
        tk.Label(
            self, text=title, font=HEADER_FONT, justify='center',
            background=FRAME_COLOR, foreground=TEXT_COLOR
        ).place(x=375 - (len(title) * HEADER_FONT_SIZE // 2), y=20,
                width=len(title) * HEADER_FONT_SIZE, height=30)
        ttk.Separator(
            self, orient='horizontal'
        ).place(x=50, y=60, width=650, height=2)


class LabeledEntry(tk.Frame):
    """ Entry widget with a description on the right side """
    def __init__(self, parent, label_text: str, label_len: int,
                 entry_len: int, font_size: int = 10):
        tk.Frame.__init__(self, parent, background=FRAME_COLOR)
        self.label = tk.Label(
            self, text=label_text, font=('Helvetica', font_size),
            background=FRAME_COLOR, foreground=TEXT_COLOR
        )
        self.label.place(x=0, y=0, width=label_len, height=20)
        self.entry_text = tk.StringVar()
        self.entry = tk.Entry(
            self, textvariable=self.entry_text,
            background=ENTRY_COLOR, foreground=TEXT_COLOR
        )
        self.entry.place(x=label_len + 5, y=0, width=entry_len, height=20)

    def get(self):
        """ Retrieve the content of the text field """
        return self.entry_text.get()

    def set(self, text):
        """ Set the content of the text field """
        self.entry_text.set(text)


class LabeledOptionMenu(tk.Frame):
    """ OptionMenu widget with a description on top """
    def __init__(self, parent, label_text: str, options: list,
                 menu_width: int = 200):
        tk.Frame.__init__(self, parent, background=FRAME_COLOR)

        self.label = tk.Label(
            self, text=label_text, justify='left',
            background=FRAME_COLOR, foreground=TEXT_COLOR
        )
        self.options = options
        self.option_value = tk.StringVar()
        self.option_menu = tk.OptionMenu(
            self, self.option_value, *options
        )
        self.option_value.set(options[0])
        self.label.place(x=0, y=0, width=menu_width, height=25)
        self.option_menu.place(x=0, y=25, width=menu_width, height=25)

    def get(self):
        """ Retrieve the current selection from the option menu """
        return self.option_value.get()


class MenuFrame(CustomFrame):
    def __init__(self, parent, logic_layer):
        CustomFrame.__init__(self, parent, 'Menu')

        self.list_update = tk.Button(
            self, text='List update', command=logic_layer.choose_date,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.reorder = tk.Button(
            self, text='Reorder',
            command=lambda: parent.show_frame('CREATE_REORDER'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.redistribution = tk.Button(
            self, text='Redistribution',
            command=lambda: parent.show_frame('CREATE_REDISTRIBUTION'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.inbound_shipment = tk.Button(
            self, text='Amazon Inbound',
            command=lambda: parent.show_frame('CREATE_INBOUND'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.reorder_document = tk.Button(
            self, text='Reorder Document',
            command=lambda: parent.show_frame('CREATE_DOCUMENT'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )

        last_date = logic_layer.latest_update_date
        if last_date:
            tk.Label(
                self, text=f'List is up-to-date until {last_date}',
                background=FRAME_COLOR, foreground=TEXT_COLOR,
                font=LABEL_FONT
            ).place(x=160, y=80, width=400, height=25)
        self.list_update.place(x=90, y=130, width=150, height=50)
        self.reorder.place(x=295, y=130, width=150, height=50)
        self.redistribution.place(x=500, y=130, width=150, height=50)
        self.inbound_shipment.place(x=90, y=190, width=150, height=50)
        self.reorder_document.place(x=295, y=190, width=150, height=50)


class DateRangeChooserFrame(CustomFrame):
    def __init__(self, parent, logic_layer):
        CustomFrame.__init__(self, parent, 'Choose date-range')

        self.start = LabeledEntry(parent=self, label_text='Start:',
                                  label_len=50, entry_len=100, font_size=13)
        self.end = LabeledEntry(parent=self, label_text='End:',
                                label_len=50, entry_len=100, font_size=13)
        self.run = tk.Button(
            self, text='Start', command=logic_layer.prepare_update,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.back_button = tk.Button(
            self, text='Back',
            command=lambda: parent.show_frame('MENU'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )

        self.start.place(x=90, y=80, width=150, height=50)
        self.end.place(x=490, y=80, width=150, height=50)
        tk.Label(
            self, text='Note: date-format = yyyy-mm-dd', font=NOTE_FONT,
            background=FRAME_COLOR, foreground=TEXT_COLOR,
        ).place(x=90, y=100, width=200, height=50)
        self.run.place(x=90, y=230, width=150, height=50)
        self.back_button.place(x=500, y=230, width=150, height=50)

    def get(self):
        return {'start': self.start.get(), 'end': self.end.get()}

    def set(self, dates: dict):
        self.start.set(dates['start'])
        self.end.set(dates['end'])


class CreateRedistributionFrame(CustomFrame):
    def __init__(self, parent, logic_layer):
        CustomFrame.__init__(self, parent, 'Create a new redistribution')

        configs = parent.config_handler.configs
        self.warehouses = parent.config_handler.warehouse_handler.get()
        source_warehouse_names = [
            wh['gui_display_name'] for wh in self.warehouses
            if wh['sync_with'] == 'plentymarkets'
        ]
        target_warehouse_names = [
            wh['gui_display_name'] for wh in self.warehouses
        ]
        self.source_warehouse = LabeledOptionMenu(
            parent=self, label_text='Source warehouse',
            options=source_warehouse_names
        )
        self.target_warehouse = LabeledOptionMenu(
            parent=self, label_text='Target warehouse',
            options=target_warehouse_names
        )

        spreadsheet = pathlib.Path(
            configs['update_spreadsheet']['data']['file']
        )
        if not spreadsheet.exists():
            messagebox.showerror('ERROR', 'Spreadsheet at path '
                                 f'{str(spreadsheet)} does not exist.')

        sheets = [
            sheet['name'] for sheet in
            configs['update_spreadsheet']['data']['redistribution_sheets']
        ]
        self.sheets = LabeledOptionMenu(
            parent=self, label_text='Sheets', options=sheets, menu_width=300
        )
        self.sheets.option_value.trace('w', self.__change_warehouses)
        self.__change_warehouses()

        self.transit = tk.IntVar()
        transit_location_button = tk.Checkbutton(
            self,
            text='Book all products into the\n'
                 'transit location of the\nreceiver warehouse.',
            justify='left',
            variable=self.transit,
            background=FRAME_COLOR, foreground=TEXT_COLOR,
            activebackground=ACTIVE_BUTTON_COLOR,
            highlightthickness=0
        )
        self.run = tk.Button(
            self, text='Create',
            command=logic_layer.run_redistribution,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.back_button = tk.Button(
            self, text='Back', command=lambda: parent.show_frame('MENU'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )

        self.source_warehouse.place(x=90, y=80, width=200, height=60)
        tk.Label(
            self, text='►►►►►►►',
            background=FRAME_COLOR, foreground=TEXT_COLOR,
        ).place(x=330, y=80, width=100, height=30)
        self.target_warehouse.place(x=460, y=80, width=200, height=60)
        self.sheets.place(x=90, y=140, width=300, height=60)
        transit_location_button.place(x=460, y=140, width=200, height=50)
        self.run.place(x=90, y=230, width=150, height=50)
        self.back_button.place(x=500, y=230, width=150, height=50)

    def get_warehouse_ids(self):
        source = self.source_warehouse.get()
        target = self.target_warehouse.get()
        try:
            assert source != target
        except AssertionError:
            messagebox.showerror('ERROR', 'Source and target warehouses '
                                 'must be different.')
            return (None, None)

        source_id = target_id = None
        for warehouse in self.warehouses:
            if warehouse['gui_display_name'] == source:
                source_id = warehouse['plenty_warehouse_id']
            elif warehouse['gui_display_name'] == target:
                target_id = warehouse['plenty_warehouse_id']
        return (source_id, target_id)

    def __change_warehouses(self, *args):
        del args
        target_sheet = self.sheets.get()
        sheets = self.parent.config_handler.configs['update_spreadsheet'][
            'data']['redistribution_sheets']
        sheet = [sheet for sheet in sheets if sheet['name'] == target_sheet]
        for wh_type in ['source_warehouse', 'target_warehouse']:
            getattr(self, wh_type).option_value.set([
                wh['gui_display_name'] for wh in self.warehouses
                if wh['plenty_warehouse_id'] == sheet[0][f'{wh_type}_id']
            ][0])


class CreateReorderFrame(CustomFrame):
    def __init__(self, parent, logic_layer):
        CustomFrame.__init__(self, parent, 'Create a new reorder')

        self.warehouses = parent.config_handler.warehouse_handler.get(
            warehouse_type='plentymarkets'
        )
        warehouse_names = [wh['gui_display_name'] for wh in self.warehouses]
        self.target_warehouse = LabeledOptionMenu(
            parent=self, label_text='Target warehouse', options=warehouse_names
        )
        self.run = tk.Button(
            self, text='Create', command=logic_layer.run_reorder,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.back_button = tk.Button(
            self, text='Back', command=lambda: parent.show_frame('MENU'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.target_warehouse.place(x=250, y=80, width=200, height=60)
        self.run.place(x=90, y=230, width=150, height=50)
        self.back_button.place(x=500, y=230, width=150, height=50)

    def get_warehouse_id(self):
        target = self.target_warehouse.get()
        target_id = None
        for warehouse in self.warehouses:
            if warehouse['gui_display_name'] == target:
                target_id = warehouse['plenty_warehouse_id']
        return target_id


class CreateInboundShipmentFrame(CustomFrame):
    def __init__(self, parent, logic_layer):
        CustomFrame.__init__(self, parent,
                             'Announce a new Amazon Inbound shipment')

        config = parent.config_handler.configs['inbound_shipment']['data']
        # pylint: disable=invalid-string-quote
        addresses = [
            f"{address['name']} - {address['street']} - {address['city']}"
            for address in config['sender_addresses']
        ]

        def round_up_to_nearest_divider_of_50(x: int) -> int:
            return int(50 * math.ceil(x / 50))

        def cut_out_middle(x: str) -> str:
            def front_quarter(x: str) -> str:
                return x[:int(len(x) / 4)]

            def back_quarter(x: str) -> str:
                return x[(int(len(x) / 4) * 3):]

            return f"{front_quarter(x)}...{back_quarter(x)}"

        optimal_width = round_up_to_nearest_divider_of_50(
            x=len(max(addresses, key=len))
        )
        if optimal_width >= 150:
            addresses = [
                f"{address['name']} - {cut_out_middle(address['street'])} - "
                f"{address['city']}" if len(address['street']) > 70 else
                f"{address['name']} - {address['street']} - {address['city']}"
                for address in config['sender_addresses']
            ]
            optimal_width = round_up_to_nearest_divider_of_50(
                x=len(max(addresses, key=len))
            )
        optimal_width = max(optimal_width, 100)
        self.senders = LabeledOptionMenu(
            parent=self, label_text='Sender addresses', options=addresses,
            menu_width=optimal_width + 200
        )
        self.receiver_country = LabeledEntry(
            parent=self, label_text='Shipment destination',
            label_len=250, entry_len=100
        )
        self.receiver_country_state = tk.BooleanVar()
        if 'default_receiver' in config:
            self.receiver_country.entry_text.set(config['default_receiver'])
            self.__valid_receiver_country()
        self.order_id = LabeledEntry(
            parent=self, label_text='ID of the redistribution order',
            label_len=250, entry_len=100
        )
        self.order_id_state = tk.BooleanVar()

        self.order_id.entry_text.trace('w', self.__valid_id)
        self.receiver_country.entry_text.trace(
            'w', self.__valid_receiver_country
        )
        self.receiver_country_state.trace('w', self.__button_state_check)
        self.order_id_state.trace('w', self.__button_state_check)
        self.inbound_plan = tk.Button(
            self, text='Create inbound plan',
            command=logic_layer.create_inbound_plan,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.inbound_plan['state'] = 'disabled'
        self.full_view = tk.Button(
            self, text='Create full view CSV',
            command=logic_layer.create_full_view,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.full_view['state'] = 'disabled'
        self.package_list = tk.Button(
            self, text='Fill package_list',
            command=logic_layer.fill_package_list,
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.package_list['state'] = 'disabled'
        self.package_list_deprecation_note = tk.Label(
            self, text='Old option', font=NOTE_FONT,
            background=FRAME_COLOR, foreground=TEXT_COLOR,
        )
        self.back_button = tk.Button(
            self, text='Back', command=lambda: parent.show_frame('MENU'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )

        self.senders.place(x=200, y=80, width=optimal_width + 200, height=60)
        self.order_id.place(x=50, y=140, width=300, height=60)
        self.receiver_country.place(x=350, y=140, width=300, height=60)
        self.inbound_plan.place(x=70, y=230, width=150, height=50)
        self.full_view.place(x=240, y=230, width=150, height=50)
        self.package_list.place(x=410, y=230, width=150, height=50)
        self.package_list_deprecation_note.place(x=435, y=205, width=100, height=20)
        self.back_button.place(x=580, y=230, width=150, height=50)

    def get_order_id(self) -> int:
        return int(self.order_id.get())

    def __valid_id(self, *args) -> None:
        del args
        try:
            order_id = self.order_id.get()
            if len(order_id) < 5 or int(order_id) < 10000:
                self.order_id_state.set(False)
                return
        except ValueError:
            self.order_id_state.set(False)
            return
        self.order_id_state.set(True)

    def __valid_receiver_country(self, *args) -> None:
        del args
        receiver_country = self.receiver_country.get()
        if len(receiver_country) != 2 or not receiver_country.isalpha():
            self.receiver_country_state.set(False)
            return
        self.receiver_country_state.set(True)

    def __button_state_check(self, *args) -> None:
        del args
        if self.order_id_state.get():
            self.package_list['state'] = 'normal'
            self.full_view['state'] = 'normal'
        else:
            self.package_list['state'] = 'disabled'
            self.full_view['state'] = 'disabled'

        if self.order_id_state.get() and self.receiver_country_state.get():
            self.inbound_plan['state'] = 'normal'
        else:
            self.inbound_plan['state'] = 'disabled'

    def get_sender_address_index(self) -> int:
        return self.senders.options.index(self.senders.get())

    def get_receiver_country(self) -> str:
        return self.receiver_country.get()


class CreateReorderDocumentFrame(CustomFrame):
    def __init__(self, parent, logic_layer):
        CustomFrame.__init__(self, parent, 'Generate a reorder document')
        self.order_id = LabeledEntry(
            parent=self, label_text='ID of the reorder',
            label_len=150, entry_len=120
        )
        self.order_id.entry_text.trace('w', self.__valid_id)
        self.order_id_state = tk.BooleanVar()
        self.order_id_state.trace('w', self.__button_state_check)

        self.generate_csv_button = tk.Button(
            self, text='Generate CSV', command=partial(
                logic_layer.generate_document, 'csv'
            ),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.generate_csv_button['state'] = 'disabled'
        self.back_button = tk.Button(
            self, text='Back', command=lambda: parent.show_frame('MENU'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        self.order_id.place(x=170, y=130, width=300, height=60)
        self.generate_csv_button.place(x=90, y=230, width=150, height=50)
        self.back_button.place(x=500, y=230, width=150, height=50)

    def get_order_id(self) -> int:
        return int(self.order_id.get())

    def __valid_id(self, *args) -> None:
        del args
        try:
            order_id = self.order_id.get()
            if len(order_id) < 5 or int(order_id) < 10000:
                self.order_id_state.set(False)
                return
        except ValueError:
            self.order_id_state.set(False)
            return
        self.order_id_state.set(True)

    def __button_state_check(self, *args) -> None:
        del args
        if self.order_id_state.get():
            self.generate_csv_button['state'] = 'normal'
        else:
            self.generate_csv_button['state'] = 'disabled'


class ExecutionFrame(CustomFrame):
    def __init__(self, parent):
        CustomFrame.__init__(self, parent, 'Execute')
        self.status = tk.Label(
            self, text='', font=STATUS_FONT,
            background=FRAME_COLOR, foreground=TEXT_COLOR,
        )
        self.back_button = tk.Button(
            self, text='Back', command=lambda: parent.show_frame('MENU'),
            background=BUTTON_COLOR, activebackground=ACTIVE_BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )

        self.status.place(x=90, y=130, width=650, height=50)
        self.back_button.place(x=500, y=230, width=150, height=50)
