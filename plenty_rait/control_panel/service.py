"""
User interface to various services of the Plenty RAIT tool
Copyright © 2021 Jan Sallermann Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import datetime
from datetime import date
from functools import partial
import re
import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog
import pathlib
from typing import Union
from platformdirs import user_documents_dir

from plenty_rait.control_panel.util import ExportFileHandler
from plenty_rait.control_panel.interface import (
    MenuFrame, DateRangeChooserFrame, CreateRedistributionFrame,
    CreateReorderFrame, CreateInboundShipmentFrame, CreateReorderDocumentFrame,
    ExecutionFrame, FRAME_COLOR, BUTTON_COLOR, TEXT_COLOR
)
from plenty_rait.control_panel.warehouse_interface import WarehouseFrame
from plenty_rait.plenty_sync.service import (
    Interface as PlentySyncInterface
)
from plenty_rait.amazon_import.service import (
    Interface as AmazonImportInterface
)
from plenty_rait.sales_statistic.service import (
    Interface as SalesStatisticInterface
)
from plenty_rait.report.service import (
    Interface as ReportInterface
)
from plenty_rait.update_spreadsheet.service import (
    Interface as UpdateSpreadsheetInterface
)
from plenty_rait.inbound_shipment.service import (
    Interface as InboundShipmentInterface
)
from plenty_rait.reorder_document.service import (
    Interface as ReorderDocumentInterface
)


WINDOW_WIDTH = 750
WINDOW_HEIGHT = 300


class Interface:
    def __init__(self, config_handler: object,
                 database_connection_string: str):
        self.root = tk.Tk()
        self.root.wm_title('Plenty RAIT')
        self.root.geometry(f'{WINDOW_WIDTH}x{WINDOW_HEIGHT}')
        self.root.resizable(width=False, height=False)
        self.config_handler = config_handler
        self.database_connection_string = database_connection_string
        cp_config = self.config_handler.configs['control_panel']['data']
        self.fba_external_handling = cp_config['fba_extern'] == 'yes'
        if self.fba_external_handling:
            try:
                amazon_path = pathlib.Path(cp_config['amazon_export_path'])
            except KeyError as err:
                raise RuntimeError(
                    'With external FBA order & stock handling a folder '
                    'location for the export files is needed.'
                ) from err
            self.file_handler = ExportFileHandler(amazon_path=amazon_path)
        self.frames = {}
        self.build_interface()

    def show_frame(self, frame: str) -> None:
        """
        Move the specified frame to the top of the frame stack maintained by
        tkinter (active window for the user).

        Parameters:
            frame        [str]      -       Frame that is moved up in stack
        """
        self.frames[frame].frame.tkraise()

    def __set_up_pages(self, page_template: list) -> None:
        for name, page in page_template:
            frame = page(self)
            self.frames[name] = frame
            frame.frame.place(x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)

    def build_interface(self):
        self.container = tk.Frame(
            self.root, width=WINDOW_WIDTH, height=WINDOW_HEIGHT,
            background=FRAME_COLOR
        )
        self.container.place(x=0, y=0, width=WINDOW_WIDTH,
                             height=WINDOW_HEIGHT)

        pages = [
            ('MENU', MenuLogic),
            ('DATE_RANGE_PICK', DateRangeLogic),
            ('CREATE_REDISTRIBUTION', RedistributionLogic),
            ('CREATE_REORDER', ReorderLogic),
            ('CREATE_INBOUND', InboundLogic),
            ('CREATE_DOCUMENT', ReorderDocumentLogic),
            ('EXECUTION_STATUS', ExecutionLogic),
            ('ADD_WAREHOUSE', WarehouseLogic)
        ]
        self.__set_up_pages(page_template=pages)

        menubar = tk.Menu(
            self.root, background=FRAME_COLOR, activebackground=BUTTON_COLOR,
            foreground=TEXT_COLOR, activeforeground=TEXT_COLOR
        )
        config_menu = tk.Menu(
            menubar, tearoff=0, background=FRAME_COLOR,
            activebackground=BUTTON_COLOR, foreground=TEXT_COLOR,
            activeforeground=TEXT_COLOR
        )
        for app_name in list(self.config_handler.configs):
            config_menu.add_command(
                label=f'Edit {app_name}',
                command=partial(
                    self.config_handler.edit_config, app_name=app_name
                )
            )
        menubar.add_cascade(label='Edit config', menu=config_menu)

        warehouse_menu = tk.Menu(
            menubar, tearoff=0, background=FRAME_COLOR,
            activebackground=BUTTON_COLOR, foreground=TEXT_COLOR,
            activeforeground=TEXT_COLOR
        )
        warehouse_menu.add_command(
            label='Warhouse configuration',
            command=lambda: self.show_frame('ADD_WAREHOUSE')
        )
        menubar.add_cascade(label='Warehouses', menu=warehouse_menu)

        self.root.configure(menu=menubar)
        self.show_frame('MENU')

    def get_output_folder(self) -> pathlib.Path:
        config = self.config_handler.configs['control_panel']['data']
        try:
            output_folder = pathlib.Path(config['output_folder'])
        except KeyError:
            output_folder = pathlib.Path(user_documents_dir())
        output_folder.mkdir(parents=True, exist_ok=True)
        return output_folder


class MenuLogic:
    def __init__(self, parent):
        self.parent = parent
        self.latest_update_date: Union[date, None] =\
            self.__get_latest_update_date()
        self.frame = MenuFrame(parent=parent, logic_layer=self)
        self.frame.place(x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)

    def __get_latest_update_date(self) -> Union[date, None]:
        sales_statistic = SalesStatisticInterface(
            connection_string=self.parent.database_connection_string,
            mode='both'  # both daily and monthly stats
        )
        return sales_statistic.get_latest_statistic_date()

    def choose_date(self):
        if self.parent.fba_external_handling:
            if not self.parent.file_handler.check_files():
                return
            date_range = self.parent.file_handler.fetch_date_range()
        else:
            today = date.today()
            if self.latest_update_date:
                date_range = {
                    'start': str(self.latest_update_date),
                    'end': today
                }
            else:
                date_range = {
                    'start': '',
                    'end': today
                }
        self.parent.frames['DATE_RANGE_PICK'].frame.set(dates=date_range)
        self.parent.show_frame('DATE_RANGE_PICK')


class ListUpdateLogic:
    def __init__(self, parent):
        self.parent = parent

    def update_list(self, date_range: dict, files: dict = None):
        execute_menu = self.parent.frames['EXECUTION_STATUS']
        configs = self.parent.config_handler.configs
        spreadsheet_config = configs['update_spreadsheet']['data']
        spreadsheet_path = pathlib.Path(spreadsheet_config['file'])

        if not spreadsheet_path.exists():
            messagebox.showerror('ERROR', 'Spreadsheet at path '
                                 f'{str(spreadsheet_path)} does not exist.')
            return
        stock_sheet_name = spreadsheet_config['stock_sheet']['name'].replace(
            ' ', '_')
        plenty_sync = PlentySyncInterface(
            connection_string=self.parent.database_connection_string,
            modes=['all']
        )
        sales_statistic = SalesStatisticInterface(
            connection_string=self.parent.database_connection_string,
            mode='both'  # both daily and monthly stats
        )
        report = ReportInterface(
            connection_string=self.parent.database_connection_string,
            ident='asin'
        )
        update_spreadsheet = UpdateSpreadsheetInterface(
            connection_string=self.parent.database_connection_string
        )
        actions = [
            {
                'status': 'Plentymarkets identification data synchronization',
                'func': plenty_sync.identity_update
            },
            {
                'status': 'Clean up deprecated variation IDs in `order_items`',
                'func': plenty_sync.database.cleanup_variation_ids
            },
            {
                'status': 'Clean up deprecated variation IDs in statistics',
                'func': partial(sales_statistic.cleanup, False, True)
            },
            {
                'status': 'Plentymarkets order synchronization',
                'func': partial(
                    plenty_sync.order_update,
                    (date_range['start'], date_range['end'])
                ),
            },
            {
                'status': 'Plentymarkets Stock data synchronization',
                'func': plenty_sync.stock_update
            },
            {
                'status': 'Plentymarkets Producer synchronization',
                'func': plenty_sync.producer_update
            },
            {
                'status': 'Plentymarkets additional data synchronization',
                'func': plenty_sync.additional_update
            },
            {
                'status': 'Plentymarkets pending incoming variation sync',
                'func': plenty_sync.incoming_update
            },
            {
                'status': 'Generating sales statistics',
                'func': partial(
                    sales_statistic.process_statistics,
                    (date.fromisoformat(date_range['start']),
                     date.fromisoformat(date_range['end']))
                )
            },
            {
                'status': 'Write stock report to file',
                'func': partial(
                    report.write_stock_report_to_file, str(spreadsheet_path),
                    stock_sheet_name
                )
            },
            {
                'status': 'Update the spreadsheet',
                'func': partial(
                    update_spreadsheet.update,
                    ['user_interaction_sheets', 'incoming_sheets']
                )
            }
        ]

        if files:
            self.__external_amazon_handling(actions=actions, files=files)

        for sales_sheet in spreadsheet_config['sales_sheets']:
            actions.insert(
                -2,
                {
                    # pylint: disable=invalid-string-quote
                    'status': f"Generate {sales_sheet['name']} report",
                    'func': partial(
                        report.write_order_report_to_file,
                        str(spreadsheet_path),
                        sales_sheet['name'],
                        (date.today() - datetime.timedelta(
                            sales_sheet['days_range']), date.today())
                    )
                }
            )
        execute_menu.target = 'complete'
        self.parent.show_frame('EXECUTION_STATUS')
        execute_menu.execute(actions=actions)

    def __external_amazon_handling(self, actions: list, files: dict) -> None:
        amazon_import = AmazonImportInterface(
            connection_string=self.parent.database_connection_string)
        commands = [
            (2, {
                'status': 'Clean up deprecated variation IDs in '
                '`amazon_order_items`',
                'func': amazon_import.importer.cleanup_variation_ids
            }),
            (9, {
                'status': 'Importing Amazon orders EU',
                'func': partial(
                    amazon_import.import_orders_from_export,
                    files['order_eu']()
                )
            }),
            (10, {
                'status': 'Importing Amazon orders US',
                'func': partial(
                    amazon_import.import_orders_from_export,
                    files['order_us']()
                )
            }),
            (11, {
                'status': 'Importing Amazon Stock EU',
                'func': partial(
                    amazon_import.import_stock_from_export,
                    files['stock_eu'](), 'EU'
                )
            }),
            (12, {
                'status': 'Importing Amazon Stock UK',
                'func': partial(
                    amazon_import.import_stock_from_export,
                    files['stock_eu'](), 'UK'
                )
            }),
            (13, {
                'status': 'Importing Amazon Stock US',
                'func': partial(
                    amazon_import.import_stock_from_export,
                    files['stock_us'](), 'US'
                )
            })
        ]
        for command in commands:
            actions.insert(command[0], command[1])


class DateRangeLogic:
    def __init__(self, parent):
        self.frame = DateRangeChooserFrame(parent=parent, logic_layer=self)
        self.frame.place(x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)
        self.parent = parent

    def prepare_update(self) -> None:
        """
        Prepare the list updating process by assembling the commands to the
        sub applications and the matching status.
        """
        date_range_frame = self.parent.frames['DATE_RANGE_PICK'].frame
        date_range = date_range_frame.get()

        if not date_range['start'] or not date_range['end']:
            messagebox.showerror('ERROR', 'Please choose a start date and an '
                                 'end date')
            return

        files = {}
        if self.parent.fba_external_handling:
            file_handler = self.parent.file_handler
            try:
                files = {
                    'order_eu': partial(
                        file_handler.get_path, 'order', 'EU'
                    ),
                    'order_us': partial(
                        file_handler.get_path, 'order', 'US'
                    ),
                    'stock_eu': partial(
                        file_handler.get_path, 'stock', 'EU'
                    ),
                    'stock_us': partial(
                        file_handler.get_path, 'stock', 'US'
                    )
                }
            except KeyError as err:
                messagebox.showerror('ERROR', err)
                return

        list_update_logic = ListUpdateLogic(parent=self.parent)
        list_update_logic.update_list(date_range=date_range, files=files)


class RedistributionLogic:
    def __init__(self, parent):
        self.frame = CreateRedistributionFrame(parent=parent, logic_layer=self)
        self.frame.place(x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)
        self.parent = parent
        self.last_sheet = None

    def run_redistribution(self):
        parent = self.parent
        (source, target) = self.frame.get_warehouse_ids()
        if any(x is None for x in [source, target]):
            return
        sheet = self.frame.sheets.get()
        if not source or not target or not sheet:
            messagebox.showerror('ERROR', 'Please select a source and a target'
                                 ' warehouse and an excel sheet from the '
                                 'dropdown menu')
            return
        if len(sheet.split(' ')) > 1:
            messagebox.showerror('ERROR', 'The redistribution sheet name '
                                 'must not contain any spaces')
            return

        self.last_sheet = sheet

        config = parent.config_handler.configs['update_spreadsheet']['data']
        sheet_config = [
            x for x in config['redistribution_sheets'] if x['name'] == sheet
        ][0]
        plenty_sync = PlentySyncInterface(
            connection_string=self.parent.database_connection_string, modes=[]
        )
        action = {
            'status': 'Create a redistribution on Plentymarkets',
            'func': partial(plenty_sync.redistribution_creation,
                            config['file'], int(source), int(target),
                            sheet, self.frame.transit.get() is not None,
                            False,
                            {'name': sheet_config['identifier_column'],
                             'type': sheet_config['identifier_type']},
                            sheet_config['quantity_column'])
        }

        parent.frames['EXECUTION_STATUS'].target = 'redistribution'
        parent.show_frame('EXECUTION_STATUS')
        parent.frames['EXECUTION_STATUS'].execute(actions=[action])

    def clear_user_input(self):
        if not self.last_sheet:
            return
        config = self.parent.config_handler.configs['update_spreadsheet']
        sheet_config = [
            x for x in config['data']['redistribution_sheets']
            if x['name'] == self.last_sheet
        ][0]
        # pylint: disable=invalid-string-quote
        if messagebox.askyesno(
            'Reset input',
            f"Do you want to clear the `{sheet_config['quantity_column']}` "
            "column?"
        ):
            update_spreadsheet = UpdateSpreadsheetInterface(
                connection_string=self.parent.database_connection_string
            )
            update_spreadsheet.update_service.clear_user_input(
                sheet_name=sheet_config['name'],
                column_name=sheet_config['quantity_column'])


class ReorderLogic:
    def __init__(self, parent):
        self.frame = CreateReorderFrame(parent=parent, logic_layer=self)
        self.frame.place(x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)
        self.parent = parent

    def run_reorder(self):
        parent = self.parent
        target = self.frame.get_warehouse_id()
        if not target:
            messagebox.showerror('ERROR', 'Please select a target warehouse')
            return

        config = parent.config_handler.configs['update_spreadsheet']['data']
        sheet_config = config['reorder_sheet']
        plenty_sync = PlentySyncInterface(
            connection_string=self.parent.database_connection_string, modes=[]
        )
        action = {
            'status': 'Create a reorder on Plentymarkets',
            'func': partial(plenty_sync.reorder_creation,
                            config['file'], int(target),
                            sheet_config['producer_column'],
                            {'name': sheet_config['identifier_column'],
                             'type': sheet_config['identifier_type']},
                            sheet_config['quantity_column'],
                            sheet_config['name'])
        }

        parent.frames['EXECUTION_STATUS'].target = 'reorder'
        parent.show_frame('EXECUTION_STATUS')
        parent.frames['EXECUTION_STATUS'].execute(actions=[action])

    def clear_user_input(self):
        config = self.parent.config_handler.configs['update_spreadsheet']
        sheet_config = config['data']['reorder_sheet']
        # pylint: disable=invalid-string-quote
        if messagebox.askyesno(
            'Reset input',
            f"Do you want to clear the `{sheet_config['quantity_column']}` "
            "column?"
        ):
            update_spreadsheet = UpdateSpreadsheetInterface(
                connection_string=self.parent.database_connection_string
            )
            update_spreadsheet.update_service.clear_user_input(
                sheet_name=sheet_config['name'],
                column_name=sheet_config['quantity_column'])


class InboundLogic:
    def __init__(self, parent):
        self.frame = CreateInboundShipmentFrame(parent=parent,
                                                logic_layer=self)
        self.frame.place(x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)
        self.parent = parent
        self.output_folder = self.parent.get_output_folder()

    def create_inbound_plan(self):
        inbound_shipment = InboundShipmentInterface(
            connection_string=self.parent.database_connection_string,
            output_folder=self.output_folder,
            receiver_country=self.frame.get_receiver_country(),
            sender_index=self.frame.get_sender_address_index()
        )
        order_id = self.frame.get_order_id()
        action = {
            'status': f'Create an Amazon inbound shipment plan for {order_id}',
            'func': partial(
                inbound_shipment.create_inbound_plan, order_id
            )
        }
        self.parent.frames['EXECUTION_STATUS'].target = 'inbound_plan'
        self.parent.show_frame('EXECUTION_STATUS')
        self.parent.frames['EXECUTION_STATUS'].execute(actions=[action])

    def create_full_view(self):
        inbound_shipment = InboundShipmentInterface(
            connection_string=self.parent.database_connection_string,
            output_folder=self.output_folder,
            receiver_country=self.frame.get_receiver_country(),
            sender_index=self.frame.get_sender_address_index()
        )
        order_id = self.frame.get_order_id()
        action = {
            'status': f'Create a full-view CSV (packages) for {order_id}',
            'func': partial(
                inbound_shipment.create_package_summary_csv, order_id
            )
        }
        self.parent.frames['EXECUTION_STATUS'].target = 'inbound_plan'
        self.parent.show_frame('EXECUTION_STATUS')
        self.parent.frames['EXECUTION_STATUS'].execute(actions=[action])

    def fill_package_list(self):
        inbound_shipment = InboundShipmentInterface(
            connection_string=self.parent.database_connection_string,
            output_folder=self.output_folder
        )
        order_id = self.frame.get_order_id()
        path = pathlib.Path(filedialog.askopenfilename(
            initialdir=self.parent.file_handler.amazon_path),
            title='Amazon inbound package list',
            filetypes=(('.tsv files', '.tsv'), ('all files', '*.*'))
        )
        action = {
            'status': str(
                f'Fill the Amazon inbound shipment package list for {order_id}'
            ),
            'func': partial(
                inbound_shipment.fill_package_list, order_id, path
            )
        }
        self.parent.frames['EXECUTION_STATUS'].target = 'inbound_plan'
        self.parent.show_frame('EXECUTION_STATUS')
        self.parent.frames['EXECUTION_STATUS'].execute(actions=[action])


class WarehouseLogic:
    def __init__(self, parent):
        self.frame = WarehouseFrame(parent=parent, logic_layer=self)
        self.frame.place(x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)
        self.parent = parent

    def save_changes(self):
        data = self.frame.get()
        if not data:
            return
        warehouse_handler = self.parent.config_handler.warehouse_handler
        warehouse_handler.config = {'warehouses': data}
        warehouse_handler.write()
        self.frame.signal_success()

    def remove_warehouse(self, index: int) -> bool:
        warehouse_handler = self.parent.config_handler.warehouse_handler
        warehouses = warehouse_handler.config['warehouses']
        if messagebox.askyesno(
            'Remove warehouse?',
            'Do you really want to delete the current warehouse?'
        ):
            warehouses.pop(index)
            warehouse_handler.config = {'warehouses': warehouses}
            warehouse_handler.write()
            return True
        return False

    @staticmethod
    def prio_buffer_config_to_str(config: list) -> str:
        """ Convert the JSON configuration section to a contiguous string """
        # pylint: disable=invalid-string-quote
        return '; '.join(
            [
                f"{level['level']}:{level['buffer']}" if level['level'] != -1
                else f"*:{level['buffer']}" for level in config
            ]
        )

    @staticmethod
    def prio_buffer_str_to_config(user_input: str) -> list:
        """
        Convert the user input to a valid JSON configuration section

        Notice, this function simply ignores all invalid characters.
        """
        # Remove all invalid characters and convert to integer
        text = user_input.replace('*', '-1')
        try:
            prio_buf_mapping = [
                dict(zip(
                    ['level', 'buffer'],
                    [int(x) for x in mapping.split(':')]))
                for mapping in re.sub('[^0-9;:-]', '',  text).split(';')
            ]
        except ValueError:
            return []
        smallest_entry = min(prio_buf_mapping, key=len)
        # pylint: disable=invalid-string-quote
        if len(smallest_entry) == 1:
            messagebox.showerror(
                'Missing buffer mapping',
                f"Level {smallest_entry['level']} doesn't contain a "
                'buffer mapping, use the syntax `level`:`buffer`'
            )
            return []
        level_sort = sorted(prio_buf_mapping, key=lambda x: x['level'])
        if level_sort[0]['level'] == -1:
            level_sort.append(level_sort.pop(0))
        buffer_sort = list(reversed(
            sorted(prio_buf_mapping, key=lambda x: x['buffer'])
        ))
        if level_sort != buffer_sort:
            messagebox.showwarning(
                'Dubious priority buffer mapping',
                'The priority mapping is not set up in order of '
                'highest priority-highest buffer, '
                'please validate your input.'
            )
        return prio_buf_mapping


class ReorderDocumentLogic:
    def __init__(self, parent):
        self.frame = CreateReorderDocumentFrame(
            parent=parent, logic_layer=self
        )
        self.frame.place(x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)
        self.parent = parent

    def generate_document(self, document_type: str = 'csv') -> None:
        assert document_type in ['csv', 'pdf']
        order_id = self.frame.get_order_id()
        output_folder = self.parent.get_output_folder()
        interface = ReorderDocumentInterface(
            connection_string=self.parent.database_connection_string,
            output_folder=output_folder, output_type=document_type
        )
        action = {
            'status': str(
                f'Generate a reorder document for reorder ID {order_id}'
            ),
            'func': partial(
                interface.create_reorder_document, order_id
            )
        }
        self.parent.frames['EXECUTION_STATUS'].target = 'reorder_document'
        self.parent.show_frame('EXECUTION_STATUS')
        self.parent.frames['EXECUTION_STATUS'].execute(actions=[action])


class ExecutionLogic:
    def __init__(self, parent):
        self.frame = ExecutionFrame(parent=parent)
        self.frame.place(x=0, y=0, width=WINDOW_WIDTH, height=WINDOW_HEIGHT)
        self.parent = parent
        self.target = None

    def execute(self, actions: list):
        back_button = self.frame.back_button
        back_button['state'] = 'disabled'
        for action in actions:
            self.frame.status['text'] = action['status']
            self.parent.root.update()
            if action['func']() is False:
                # pylint: disable=invalid-string-quote
                self.frame.status['text'] =\
                    f"failed at {action['status']}"
                back_button['state'] = 'normal'
                return
        self.frame.status['text'] = 'complete'
        back_button['state'] = 'normal'

        if self.target == 'complete' and self.parent.fba_external_handling:
            self.parent.file_handler.move_files_to_old_files()
        elif self.target in ['redistribution', 'reorder']:
            logic_layer = self.parent.frames[f'CREATE_{self.target.upper()}']
            logic_layer.clear_user_input()
