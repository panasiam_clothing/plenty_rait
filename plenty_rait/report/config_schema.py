# pylint: disable=invalid-string-quote, line-too-long
report_configuration_schema = {
    "title": "Report tool configuration schema",
    "type": "object",
    "required": [
        "format"
    ],
    "properties": {
        "format": {
            "title": "Report format",
            "type": "array",
            "default": [],
            "items": {
                "title": "Report format column",
                "type": "object",
                "required": [
                    "name",
                    "content"
                ],
                "properties": {
                    "name": {
                        "title": "Column Name",
                        "type": "string"
                    },
                    "content": {
                        "oneOf": [
                            {
                                "title": "Plentymarkets origin ID",
                                "type": "number"
                            },
                            {
                                "title": "Sum of columns by column index",
                                "description": "Indeces excluding all sum columns and the identifier column",
                                "type": "array",
                                "items": {"type": "number"},
                                "minItems": 1,
                                "uniqueItems": True
                            }
                        ]
                    }
                }
            }
        }
    }
}
