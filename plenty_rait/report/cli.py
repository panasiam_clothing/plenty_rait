"""
Sub application of the Plenty RAIT tool with the purpose of reporting
sales for a given time-range as a sheet or separate csv.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import datetime
import os
import pathlib
import tempfile

from loguru import logger

from plenty_rait.report.service import Interface

from plenty_rait.helper.setup_helper import CommandLineSetup


def setup_sales_parser(subparser) -> None:
    sales_parser = subparser.add_parser('Sales', help='Get a report of the '
                                        'sales-statistics in the given'
                                        ' time range')
    sales_subparser = sales_parser.add_subparsers(
        title='Report by a specific category', dest='report_sales_command'
    )
    range_parser = sales_subparser.add_parser(
        'Range', help='Number of days to report from today into the past.'
    )
    range_parser.add_argument(
        'report_days', help='Number of days into the past', type=int)
    date_range_parser = sales_subparser.add_parser(
        'DateRange', help='Specific window for a report between two dates.')
    date_range_parser.add_argument(
        '--range_start', '--start', '-s', required=True,
        help='Date range start for sales reports.', dest='report_start_date',
        type=datetime.date.fromisoformat)
    date_range_parser.add_argument(
        '--range_end', '--end', '-e', required=True, dest='report_end_date',
        help='Date range end for sales reports.',
        type=datetime.date.fromisoformat)
    year_parser = sales_subparser.add_parser(
        'Year', help='Create a report for a full year.')
    year_parser.add_argument(
        'target_year', help='The year to create a report for', type=int)


def setup_stock_parser(subparser):
    subparser.add_parser(
        'Stock',
        help='Get a report of the stock in all warehouses configured in the '
        'configuration.')


def get_destination(args: argparse.Namespace) -> tuple:
    if args.report_export_excel_path:
        return (args.report_export_excel_path, args.report_export_excel_sheet)
    return (args.report_export_path, '')


class ReportCommandLine(CommandLineSetup):
    @staticmethod
    def setup_parser(subparser) -> None:
        parser = subparser.add_parser('Report')
        subparsers = parser.add_subparsers(
            title='Options for the report tool', dest='report_command')
        setup_sales_parser(subparser=subparsers)
        setup_stock_parser(subparser=subparsers)
        parser.add_argument(
            '--file', '-f', required=False, dest='report_export_path',
            help='Target destination for the finished report',
            default=os.path.join(tempfile.gettempdir(), 'report.csv'))
        parser.add_argument(
            '--excel', '--xlsx', '--xlsm', required=False,
            help='Save the report within an xlsx/xlsm file to the given '
            'sheet @ `--sheet`', dest='report_export_excel_path')
        parser.add_argument(
            '--sheet', required=False, dest='report_export_excel_sheet',
            help='Specifiy the name of the sheet within the given xlsx/xlsm '
            'file @ `--excel`')
        parser.add_argument(
            '--reset', '-r', required=False, help='Reset the system keyring',
            action='store_true', dest='reset')
        parser.add_argument(
            '--identifier', '-i', required=False, dest='report_ident',
            help='Choose the identifier for the report, default is ASIN',
            choices=['asin', 'sku', 'ean', 'variation_id'])

    @staticmethod
    def validate_parser(args: argparse.Namespace) -> bool:
        if (
            bool(args.report_export_excel_path) ^
            bool(args.report_export_excel_sheet)
        ):
            logger.error('The excel sheet argument requires both a file '
                         'path and a sheet name.')
            return False
        if (
            args.report_export_excel_path and not
            pathlib.Path(args.report_export_excel_path).is_file
        ):
            logger.error('No excel sheet found at'
                         f'{args.report_export_excel_path}')
            return False
        return True

    @staticmethod
    def handle_command(args: argparse.Namespace,
                       connection_string: str) -> bool:
        if not args.report_ident:
            args.report_ident = 'asin'

        interface = Interface(connection_string=connection_string,
                              ident=args.report_ident)

        (path, sheet) = get_destination(args=args)
        if args.report_command == 'Stock':
            return interface.write_stock_report_to_file(path=path, sheet=sheet)

        if args.report_command == 'Sales':
            if args.report_sales_command in ['Range', 'DateRange']:
                if args.report_sales_command == 'Range':
                    date_range = (datetime.date.today() -
                                  datetime.timedelta(days=args.report_days),
                                  datetime.date.today())
                elif args.report_sales_command == 'DateRange':
                    date_range = (args.report_start_date, args.report_end_date)
                year = 0
            else:
                date_range = None
                year = args.target_year

            return interface.write_order_report_to_file(
                path=path, date_range=date_range, year=year, sheet=sheet)
        return True
