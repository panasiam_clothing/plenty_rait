"""
Sub application of the Plenty RAIT tool with the purpose of reporting
sales for a given time-range as a sheet or separate csv.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from collections import defaultdict
import datetime
import decimal
import pathlib
import numpy
import openpyxl
from openpyxl.utils.dataframe import dataframe_to_rows
import pandas
from sqlalchemy.orm import Query

from loguru import logger

from plenty_rait.helper.database_schema import (
    BaseDatabaseService, DailyStatisticModel, IdentificationModel, StockModel
)
from plenty_rait.helper.config_helper import WarehouseConfigHandler
from plenty_rait.helper.file_handling import write_workbook_with_retry
from plenty_rait.report.utils import ReportConfigHandler
from plenty_rait.report.config_schema import report_configuration_schema


class Report(BaseDatabaseService):
    def __init__(self, session=None, ident: str = 'asin'):
        """
        The date range is fixed during the instantiation of the object.

        When month is -1 (default), then the whole year is used.
        """
        super().__init__(session)
        self.ident = ident
        self.ident_map = {'asin': 'asin_all',
                          'ean': 'ean',
                          'sku': 'sku',
                          'variation_id': 'variation_id'}

    def select_by_date_range(self, date_range: tuple) -> pandas.DataFrame:
        """
        Get all orders within the given date range, that were created on
        a specific marketplace.

        Parameters:
            date_range          [tuple]     -   Start and end date of the date
                                                range

        Return:
                                [DataFrame] -   A pandas dataframe with the
                                                columns:
                                                    [ASIN, quantity]
        """
        try:
            if not date_range[0] and not date_range[1]:
                raise RuntimeError(
                    'Invalid date range contains two empty dates'
                )
        except IndexError as error:
            raise RuntimeError(
                'date_range must be a tuple with two date objects'
            ) from error
        if date_range[0] and not date_range[1]:
            date_range[1] = datetime.date.today()
        items = self.session.query(
            DailyStatisticModel
        ).filter(DailyStatisticModel.date_value <= date_range[1],
                 DailyStatisticModel.date_value >= date_range[0]).all()

        return self.__build_dataframe(items=items)

    def select_by_year(self, year: int) -> pandas.DataFrame:
        """
        Get all orders within the given year, that were created on a specific
        marketplace.

        Parameters:
            year                [int]       -   Year to filter by

        Return:
                                [DataFrame] -   A pandas dataframe with the
                                                columns:
                                                    [ASIN, quantity]
        """
        items = self.session.query(DailyStatisticModel).filter(
            DailyStatisticModel.year == year).all()

        return self.__build_dataframe(items=items)

    def get_items_without_sales(self, sold_items: list,
                                columns: list) -> pandas.DataFrame:
        identifier = self.ident_map[self.ident]
        remaining_items = self.session.query(IdentificationModel).filter(
            getattr(IdentificationModel, identifier).notin_(sold_items),
            getattr(IdentificationModel, identifier) != '0',
            IdentificationModel.ean != '0'
        )

        raw_data = []
        for item in remaining_items.all():
            entry = [getattr(item, identifier)]
            entry += [0 for x in columns]
            raw_data.append(entry)

        return pandas.DataFrame(raw_data, columns=[self.ident] + columns)

    def __get_identifier(self, variations: list) -> str:
        identifier = self.ident_map[self.ident]
        items = self.session.query(IdentificationModel).filter(
            IdentificationModel.ean.in_(variations)
        ).all()
        ident_dict = {}
        for item in items:
            ident_value = str(getattr(item, identifier))
            if ident_value:
                ident_dict[str(item.ean)] = ident_value
        return ident_dict

    def __build_dataframe(self, items: Query) -> pandas.DataFrame:
        raw_data = {}
        variations = list({x.ean for x in items})
        identifiers = self.__get_identifier(variations=variations)
        skip_dict = defaultdict(lambda: 0)
        for item in items:
            if not item:
                break
            try:
                identifier = identifiers[str(item.ean)]
            except KeyError:
                skip_dict[item.ean] += item.quantity
                continue
            qty = item.quantity
            key = f'{identifier}{item.origin_id}'
            if key not in raw_data:
                raw_data[key] = [identifier, qty, item.origin_id]
            else:
                raw_data[key][1] += qty

        for ean, qty in skip_dict.items():
            logger.warning(f'No {self.ident} found for ean: '
                           f'{ean}, skip (qty: {qty})')
        return pandas.DataFrame.from_dict(raw_data, orient='index',
                                          columns=[self.ident, 'quantity',
                                                   'origin_id'])

    def get_stock(self, warehouse_handler: WarehouseConfigHandler) -> pandas.DataFrame:
        raw_data = {}
        stock = self.session.query(StockModel).all()
        variations = list({x.ean for x in stock})
        identifiers = self.__get_identifier(variations=variations)
        warehouse_mapping = {
            wh['database_column_name']: wh['sheet_column_name']
            for wh in warehouse_handler.get()
        }
        for item in stock:
            if not item:
                break
            try:
                identifier = identifiers[str(item.ean)]
            except KeyError:
                logger.warning(f'No {self.ident} found for '
                               f'{item.ean}')
                continue
            try:
                row = [
                    getattr(item, warehouse) for warehouse in warehouse_mapping
                ]
            except AttributeError:
                error_list = [
                    x for x in warehouse_mapping
                    if x not in list(item.__dict__.keys())
                ]
                logger.error(
                    'Columns given in the config are not found in the'
                    f'database table, columns: \n {error_list}')
                return pandas.DataFrame()
            raw_data[identifier] = [identifier] + row
        return pandas.DataFrame.from_dict(
            raw_data, orient='index',
            columns=[self.ident] + list(warehouse_mapping.values())
        )


class Interface:
    def __init__(self, connection_string: str, ident: str) -> None:
        self.ident = ident
        self.config_handler = ReportConfigHandler(
            name='report', config_type='json',
            config_schema=report_configuration_schema
        )

        self.origins = self.__parse_origins()
        if not self.origins:
            raise RuntimeError(
                'No report format configuration found. For an example look '
                'into the \'README.md\' file.')
        self.report = Report(ident=ident)
        self.report.create_database_session(
            connection_string=connection_string)

    def __parse_origins(self) -> dict:
        """
        Read a map of column name to Plentymarkets origin ID combinations and
        create a dictionary of origin ID keys and column name values.

        Exchange key and value, as the configuration format is more natural for
        the user, while the ID acts as input for the select_by_date_range
        method.

        Return:
                            [dict]                  -   ID - column name
        """
        self.sum_columns = {
            format_mapping['name']: format_mapping['content']
            for format_mapping in self.config_handler.config['format']
            if(isinstance(format_mapping['content'], list))
        }
        return {
            format_mapping['content']: format_mapping['name']
            for format_mapping in self.config_handler.config['format']
            if not isinstance(format_mapping['content'], list)
        }

    def __rename_origins(self, result_table: pandas.DataFrame,
                         dataframe: pandas.DataFrame) -> pandas.DataFrame:
        for origin, name in self.origins.items():
            origin_id = decimal.Decimal(str(origin)).quantize(
                decimal.Decimal('1.00'))
            table = dataframe[dataframe['origin_id'] == origin_id].copy()
            if len(table.index) == 0:
                continue
            table.rename(columns={'quantity': name},
                         inplace=True)
            table.fillna(0, inplace=True)
            table = table.drop(['origin_id'], axis=1)
            if len(result_table.index) == 0:
                result_table = table
            else:
                result_table = result_table.merge(table, how='outer',
                                                  on=self.ident)
        return result_table

    def write_stock_report_to_file(self, path: pathlib.Path,
                                   sheet: str = '') -> bool:
        result_table = self.report.get_stock(
            warehouse_handler=self.config_handler.warehouse_handler
        )
        return self.__save_to_file(dataframe=result_table, path=path,
                                   sheet_name=sheet)

    def write_order_report_to_file(self, path: pathlib.Path, sheet: str = '',
                                   date_range: tuple = None,
                                   year: int = 0) -> bool:
        tables = pandas.DataFrame()
        result_table = pandas.DataFrame()
        if date_range:
            tables = self.report.select_by_date_range(date_range=date_range)
        elif year:
            tables = self.report.select_by_year(year=year)
        if len(tables.index) == 0:
            logger.warning('Empty order fetch. (date_range: '
                           f'{date_range}, year: {year})')
            return False
        result_table = self.__rename_origins(result_table=result_table,
                                             dataframe=tables)

        result_table = result_table.fillna(value=0)
        result_table.reset_index(inplace=True, drop=True)
        items = result_table[self.ident].values.tolist()
        cols = result_table.columns.values.tolist()[1:]
        remaining = self.report.get_items_without_sales(
            sold_items=items, columns=cols)
        result_table = pandas.concat([result_table, remaining])
        # FIXME: This shouldn't be required, check why we get statistics without
        # an identifier
        result_table[result_table.columns[0]].replace('', numpy.nan, inplace=True)
        result_table.dropna(subset=[result_table.columns[0]], inplace=True)

        result_table[cols] = result_table[cols].applymap(int)

        sums = {}
        for name, columns in self.sum_columns.items():
            sums[name] = result_table.iloc[:,columns].sum(numeric_only=True, axis=1)

        index = 1
        for name, sum in sums.items():
            result_table.insert(loc=index, column=name, value=sum)
            index += 1

        result_table.sort_values(inplace=True, ascending=False,
                                 ignore_index=True, by=result_table.columns[1])
        return self.__save_to_file(dataframe=result_table, path=path,
                                   sheet_name=sheet)

    def __get_excel_column_by_index(self, index: int):
        if index <= 0:
            return ''
        return self.__get_excel_column_by_index(index // 26) +\
            chr(index % 26 + ord('A'))

    @staticmethod
    def __remove_special_chars(name: str) -> str:
        special_chars = ['?', '!', ':', '.', ',', '/', '\\',
                         'ä', 'ö', 'ü', 'Ä', 'Ü', 'Ö', 'ß']
        name = ''.join([c for c in name if c not in special_chars])
        name = name.replace(' ', '_').lower()
        return name

    def __save_to_file(self, dataframe: pandas.DataFrame, path: pathlib.Path,
                       sheet_name: str = '') -> bool:
        if not sheet_name:
            dataframe.to_csv(path, sep=';', na_rep='0', index=False)
            logger.info(f'Saved report at {path}.')
        else:
            workbook = openpyxl.load_workbook(path)
            if sheet_name not in workbook.get_sheet_names():
                sheet = workbook.create_sheet(title=sheet_name)
            else:
                workbook.remove_sheet(workbook[sheet_name])
                sheet = workbook.create_sheet(title=sheet_name)

            for row in dataframe_to_rows(dataframe, index=False,
                                         header=True):
                sheet.append(row)

            named_range_name = self.__remove_special_chars(
                name=sheet_name + '_range')
            end_column = self.__get_excel_column_by_index(
                index=len(self.origins)+2)
            named_range_range = str(
                f'$A$2:${end_column}${len(dataframe.index) + 1}'
            )
            try:
                if workbook.get_named_range(name=named_range_name):
                    del workbook.defined_names[named_range_name]
            except KeyError:
                logger.info(f'Creating named range: {named_range_name}')
            workbook.create_named_range(named_range_name,
                                        sheet, named_range_range)

            write_workbook_with_retry(workbook=workbook, path=path)
            logger.info(f'Saved report at {path} on sheet {sheet_name}')
        return True
