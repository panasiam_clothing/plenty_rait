# Report tool

This tool is part of the Plentymarkets Reorder And Inventory Transfer tool, it is used to save the sales within a given time-range into a sheet or a separate `csv` file.

## Configuration

### Custom order-origins output-names

You have to configure which origins you want to present and the assigned column-name in the output file.

Example:
```json
{
    'format': [
        {'name': 'amazon_fbm_ger", 'content': 4.01},
        {'name': 'amazon_fbm_ger_b2b", 'content': 4.21},
        {'name': 'amazon_fba_ger", 'content': 104.01}
    ]
}
```
In this case `amazon_fbm_ger` will be the column name and it will contain the sales for the origin `4.01`.

#### Adding sums of columns

You can add arbitrary sums of columns to the report.

Example:
```json
{
    'format': [
        {'name': 'sum', 'content': [1,2]}
        {'name': 'amazon_fbm_ger", 'content': 4.01},
        {'name': 'amazon_fba_ger", 'content': 104.01}
    ]
}
```

Which will add together all columns mentioned by index, an index of 1 means the first non identifier & non sum column. E.g in this example `amazon_fbm_ger`.

## Logging

The application logs by default to a standardized location on your platform. On Linux this will usually be: `~/.cache/report/log`. The file will grow to a maximum of 2 MB, at which point it will start to rotate out old entries.

## Usage

There are 3 different report date-ranges:
* Number of days in the past (`--vk 30` (now: 2020-12-10) -> (until: 2020-11-10))
* Specific range between two dates (`--start 2020-11-10 --end 2020-12-10`)
* A whole year (`--year 2020` (2020-01-01 - 2020-12-31 (unless the year is not over yet)))

You can specify a file location or use the default path, which saves the file as `report.csv` into the `.config/plenty_rait` directory.

### Examples
**Report the orders from the configured origins for the last 30 days, write the results into the default location**  
```bash
python3 -m plenty_rait Report Sales Range 30
```
**Report the orders from the configured origins for the given date range, write the results into a custom location**  
```bash
python3 -m plenty_rait Report --file ~/test_report.csv DateRange --start 2020-11-10 --end 2020-12-30
```
**Report the orders from the configured origins for the whole year 2019**  
```bash
python3 -m plenty_rait Report --excel ~/test_report.xlsx --sheet test_sheet Year 2019
```
**Report the stock for all warehouses within the database, write the results into the default location**  
```bash
python3 -m plenty_rait Report Stock
```
