# pylint: disable=invalid-string-quote, line-too-long
reorder_document_configuration_schema = {
    "title": "Reorder Document generator configuration",
    "type": "object",
    "required": [
        "export_language",
        "columns"
    ],
    "properties": {
        "default_format": {
            "title": "Default output format",
            "type": "string",
            "enum": ["csv", "pdf"]
        },
        "export_language": {
            "title": "Language of the export fields",
            "type": "string",
            "enum": ["de", "en", "fr", "it", "es"]
        },
        "csv_settings": {
            "title": "Style and configuration for the CSV output type",
            "type": "object",
            "properties": {
                "separator": {
                    "title": "value separator character",
                    "type": "string",
                    "enum": [",", ";", "\t"]
                }
            }
        },
        "columns": {
            "title": "Document columns",
            "description": "Define the columns of the reorder document",
            "type": "array",
            "default": [],
            "items": {
                "title": "Document column items",
                "type": "object",
                "required": [
                    "name",
                    "type"
                ],
                "properties": {
                    "name": {
                        "title": "Column name",
                        "description": "Name of the column in the resulting "
                        "document",
                        "type": "string"
                    },
                    "type": {
                        "title": "Data type",
                        "description": "Describes the source of the data to "
                        "be inserted for each variation",
                        "type": "string",
                        "enum": ["reorder_data", "feature", "picture",
                                 "identification", "empty"]
                    },
                    "fallback": {
                        "title": "Fallback",
                        "type": "object",
                        "required": [
                            "type"
                        ],
                        "properties": {
                            "type": {
                                "title": "Data type",
                                "description": "Describes the source of the "
                                "data to be inserted for each variation",
                                "type": "string",
                                "enum": ["general", "feature", "picture",
                                         "identification", "empty"]
                            },
                        }
                    }

                }
            },
            "dependencies": {
                "type": {
                    "oneOf": [
                        {
                            "properties": {
                                "type": {
                                    "enum": ["empty"]
                                }
                            }
                        },
                        {
                            "properties": {
                                "type": {
                                    "enum": ["reorder_data"]
                                },
                                "field_name": {
                                    "title": "Response field name",
                                    "description": "Name of the field within "
                                    "the JSON reorder response",
                                    "type": "string"
                                }
                            },
                            "required": ["field_name"]
                        },
                        {
                            "properties": {
                                "type": {
                                    "enum": ["feature"]
                                },
                                "feature_id": {
                                    "title": "Feature_id",
                                    "description": "ID of the feature if type "
                                    "== feature",
                                    "type": "integer",
                                    "minimum": 1
                                }
                            },
                            "required": ["feature_id"]
                        },
                        {
                            "properties": {
                                "type": {
                                    "enum": ["picture"]
                                },
                                "picture_index": {
                                    "title": "Picture index",
                                    "description": "Index of the picture of "
                                    "all available images for the variation",
                                    "type": "integer",
                                    "minimum": 0
                                }
                            },
                            "required": ["picture_index"]
                        },
                        {
                            "properties": {
                                "type": {
                                    "enum": ["identification"]
                                },
                                "identification_type": {
                                    "title": "Identification type",
                                    "description": "Name of the column within "
                                    "the `identification` database table",
                                    "type": "string",
                                    "enum": ["sku", "ean", "asin_all", "name"]
                                }
                            },
                            "required": ["identification_type"]
                        }
                    ]
                }
            }
        }
    }
}
