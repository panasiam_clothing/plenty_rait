"""
Prepare a simple document for a reorder to summarize information about each
order variation.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import pathlib
import platformdirs

from plenty_rait.reorder_document.service import Interface
from plenty_rait.helper.setup_helper import CommandLineSetup


class ReorderDocumentCommandLine(CommandLineSetup):
    @staticmethod
    def setup_parser(subparser):
        parser = subparser.add_parser(
            'ReorderDocument',
            help='Generate a custom document for a reorder on Plentymarktes'
        )
        parser.add_argument(
            'Order_id', type=int,
            help='ID of the redistribution in Plentymarkets'
        )
        parser.add_argument(
            '--custom_output_folder', '--output_folder', '--output',
            required=False, dest='output', type=pathlib.Path,
            default=pathlib.Path(platformdirs.user_documents_dir())
        )
        parser.add_argument(
            '--output_type', '--type', required=False, dest='output_type',
            type=str, default='csv', choices=['csv', 'pdf']
        )

    @staticmethod
    def validate_parser(args: argparse.Namespace) -> bool:
        args.output.mkdir(exist_ok=True, parents=True)
        return True

    @staticmethod
    def handle_command(args: argparse.Namespace,
                       connection_string: str) -> bool:
        return Interface(
            connection_string=connection_string,
            output_folder=args.output, output_type=args.output_type
        ).create_reorder_document(order_id=args.Order_id)
