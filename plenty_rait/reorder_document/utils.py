"""
Prepare an Amazon Inbound shipment from a Plentymarkets redistribution.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from plenty_rait.helper.config_helper import ConfigHandler


class ReorderDocumentConfigHandler(ConfigHandler):
    def __init__(self, name: str, config_type: str, config_schema: dict):
        super().__init__(name=name, config_type=config_type,
                         config_schema=config_schema)
