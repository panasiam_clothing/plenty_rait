"""
Prepare an Amazon Inbound shipment from a Plentymarkets redistribution.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from datetime import date, timedelta
from pathlib import Path
from typing import Any
from loguru import logger
import pandas
import platformdirs
import plenty_api

from plenty_rait.reorder_document.utils import ReorderDocumentConfigHandler
from plenty_rait.reorder_document.config_schema import (
    reorder_document_configuration_schema
)
from plenty_rait.helper.config_helper import get_plenty_api_base_url
from plenty_rait.helper.database_schema import (
    BaseDatabaseService, IdentificationModel
)


class PlentyApiFetcher:
    """ Service to pull the redistribution order from the REST API """
    def __init__(self, base_url: str) -> None:
        self.api = plenty_api.PlentyApi(base_url=base_url)
        self.api.cli_progress_bar = True

    def pull_order(self, order_id: int) -> dict:
        """
        Pull the order matching the ID with package assignments attached
        """
        reorder = self.api.plenty_api_get_pending_reorder(
            order_id=order_id
        )
        if isinstance(reorder, list):
            reorder = reorder[0]
        return reorder

    def pull_variation_data(self, variations: list, additional: list) -> list:
        additional_map = {
            'feature': 'properties',
            'picture': 'images'
        }
        assert all(x in additional_map for x in additional)
        additional = [additional_map[x] for x in additional]
        return self.api.plenty_api_get_variations(
            refine={
                'id': ','.join(
                    [str(variation) for variation in variations]
                )
            }, additional=additional
        )

    def pull_selection_mapping(self, export_language: str) -> pandas.DataFrame:
        """
        Fetch a mapping of variation-properties (features) to corresponding
        selections with values for the configured language.

        Return:
                            [DataFrame]
        """
        selection_mapping = self.api.plenty_api_get_property_selections()

        if selection_mapping == {}:
            raise RuntimeError('Selection mapping pull request failed.')

        selection_list = [
            [property_id, selection_id, values[export_language]]
            for property_id, selections in selection_mapping.items()
            for selection_id, values in selections.items()
            if export_language in values
        ]
        return pandas.DataFrame(
            selection_list, columns=['property_id', 'selection_id', 'value']
        )


class SelectionCacheHandler:
    """ Service to cache the feature selection mapping from the REST API """
    def __init__(self):
        self.cache_dir = Path(
            platformdirs.user_data_dir()
        ) / 'plenty_rait_cache'
        self.cache_dir.mkdir(exist_ok=True, parents=True)

    def cache_selections(
        self, dataframe: pandas.DataFrame
    ) -> None:
        """ Serialize and cache the feature selections """
        full_path = self.cache_dir / f'feature_selections.pkl'
        dataframe.to_pickle(full_path)

    def read_selections(self) -> pandas.DataFrame:
        """ Deserialize and read the feature selections """
        today = date.today()
        valid_date = today - timedelta(days=7)
        full_path = self.cache_dir / f'feature_selections.pkl'
        if not full_path.exists():
            return pandas.DataFrame()
        if date.fromtimestamp(full_path.stat().st_mtime) < valid_date:
            try:
                full_path.unlink()
            except PermissionError:
                logger.error(
                    f'Unable to delete old cache file at {full_path} due to '
                    'insufficient permissions.'
                )
            return pandas.DataFrame()
        return pandas.read_pickle(full_path)


class ReorderDocumentBuilder:
    """
    Service to build a custom reorder document from a dataframe.
    """
    def __init__(self, config: dict) -> None:
        self.config = config

    def build_csv(
        self, dataframe: pandas.DataFrame, output_path: Path
    ) -> bool:
        sep = ';'
        encoding = 'utf-8'
        if 'csv_settings' in self.config:
            settings = self.config['csv_settings']
            if 'separator' in settings:
                sep = settings['separator']

        path = output_path.with_suffix('.csv')
        dataframe.to_csv(path, sep=sep, encoding=encoding, index=False)
        return True


class DataFrameBuilder:
    """
    Service to construct a dataframe from the given reorder and variation data.
    """
    def __init__(
        self, config: dict, reorder_data: dict, variation_data: list,
        ident_mapping: dict = None, selection_map: pandas.DataFrame = None
    ):
        self.config = config
        self.reorder_data = reorder_data
        self.variation_data = variation_data
        self.language = config['export_language']
        self.ident_mapping = ident_mapping
        self.selection_map = selection_map

    def __get_matching_variation_data(self, variation: int) -> dict:
        try:
            return [
                data for data in self.variation_data if data['id'] == variation
            ][0]
        except IndexError as err:
            raise RuntimeError(
                f'No variation data found for variation ID {variation} within '
                'the Plentymarkets variation REST API request'
            ) from err

    def __get_reorder_data(self, variation: int, **kwargs) -> Any:
        try:
            reorder_variation_data = [
                variation_data
                for variation_data in self.reorder_data['orderItems']
                if variation == variation_data['itemVariationId']
            ][0]
        except IndexError:
            logger.warning(
                f'Variation ID {variation} not found in reorder data'
            )
            return ''
        try:
            return reorder_variation_data[kwargs['field_name']]
        except KeyError as err:
            raise RuntimeError(
                f"Field {kwargs['field_name']} not found in reorder item data "
                f'for variation ID {variation}'
            ) from err

    def __get_feature(self, variation: int, **kwargs) -> Any:
        variation_data = self.__get_matching_variation_data(
            variation=variation
        )
        property_id = kwargs['feature_id']
        try:
            feature = [
                prop
                for prop in variation_data['properties']
                if prop['propertyId'] == property_id
            ][0]
        except IndexError:
            logger.warning(
                f'Feature ID {property_id} not found in variation data of '
                f'variation ID {variation}'
            )
            return ''
        cast = feature['propertyRelation']['cast']
        entries = feature['relationValues']
        if not entries:
            return ''
        if cast == 'selection':
            if self.selection_map is None:
                raise RuntimeError(
                    f'Selection feature found for variation ID {variation} '
                    'but no selection mapping found.'
                )
            try:
                selection_id = int(entries[0]['value'])
            except IndexError:
                return ''
            try:
                return self.selection_map.loc[
                    (self.selection_map['property_id'] == property_id) &
                    (self.selection_map['selection_id'] == selection_id)
                ]['value'].item()
            except KeyError:
                return ''
        elif cast in ['shortText', 'longText']:
            for entry in entries:
                if entry['lang'].lower() == self.language:
                    return entry['value']
        elif cast in ['int', 'float', 'date', 'file']:
            try:
                return entries[0]['value']
            except IndexError:
                return ''
        return ''

    def __get_picture(self, variation: int, **kwargs) -> str:
        variation_data = self.__get_matching_variation_data(
            variation=variation
        )
        images = variation_data['images']
        if len(images) <= kwargs['picture_index']:
            return ''
        return images[kwargs['picture_index']]['url']

    def __get_identification(self, variation: int, **kwargs) -> Any:
        if self.ident_mapping is None:
            raise RuntimeError(
                'No identification mapping found.'
            )
        try:
            return self.ident_mapping[variation][kwargs['identification_type']]
        except KeyError:
            logger.warning(
                f'Variation ID {variation} not found in database '
                'identification table. (Either a deleted variation or the '
                'database is not up-to-date)'
            )
            return ''

    # pylint: disable=no-self-use
    def __get_empty(self, variation: int, **kwargs) -> str:
        del variation, kwargs
        return ''

    def __get_data_for_data_type(
        self, data_type: str, variation: int, **kwargs
    ) -> Any:
        valid_types = reorder_document_configuration_schema[
            'properties']['columns']['items']['properties']['type']['enum']
        assert data_type in valid_types
        return getattr(self, f'_DataFrameBuilder__get_{data_type}')(
            variation=variation, **kwargs
        )

    def __get_data_for_variation(self, variation: int, column: dict) -> Any:
        additional_parameter_map = {
            'reorder_data': 'field_name',
            'feature': 'feature_id',
            'picture': 'picture_index',
            'identification': 'identification_type',
            'empty': ''
        }
        column_type = column['type']
        parameter = additional_parameter_map[column_type]
        if parameter:
            parameter_value = column[parameter]
            return self.__get_data_for_data_type(
                data_type=column_type, variation=variation,
                **{parameter: parameter_value}
            )
        return self.__get_data_for_data_type(
            data_type=column_type, variation=variation
        )

    def build_dataframe(self) -> pandas.DataFrame:
        variations = [
            item['itemVariationId'] for item in self.reorder_data['orderItems']
        ]
        dataframe_columns = [
            column['name'] for column in self.config['columns']
        ]
        dataframe_data = []
        for variation in variations:
            dataframe_row = []
            for column in self.config['columns']:
                content = self.__get_data_for_variation(
                    variation=variation, column=column
                )
                if not content and column['type'] != 'empty':
                    if 'fallback' not in column:
                        logger.warning(
                            f"No {column['name']} data found for variation "
                            f'{variation}. Leave the field empty.'
                        )
                        dataframe_row.append('')
                        continue
                    content = self.__get_data_for_variation(
                        variation=variation, column=column['fallback']
                    )
                    if not content and column['fallback']['type'] != 'empty':
                        logger.warning(
                            f'Neither the main nor the fallback value found '
                            f"on column {column['name']} for variation "
                            f'{variation}. Leave the field empty.'
                        )
                        dataframe_row.append('')
                        continue
                dataframe_row.append(content)
            dataframe_data.append(dataframe_row)
        return pandas.DataFrame(dataframe_data, columns=dataframe_columns)


class DatabaseService(BaseDatabaseService):
    """ Service for getting the identification mapping from the database """
    def __init__(self, session=None):
        super().__init__(session)

    def get_ident_mapping(self, variation_ids: list, elements: list) -> dict:
        """
        Map the elements requested by the configuration to the
        Plentymarkets Variation ID of the provided variations.

        The elements are guranteed to match the database column names through
        the JSON schema.
        """
        return {
            getattr(row, 'variation_id'): {
                column: getattr(row, column) for column in elements
            }
            for row in self.session.query(
                IdentificationModel
            ).filter(
                IdentificationModel.variation_id.in_(variation_ids)
            )
        }


class Interface:
    """ Interface to the services of the inbound shipment tool """
    def __init__(
        self, connection_string: str, output_folder: Path,
        output_type: str = 'csv'
    ) -> None:
        self.config_handler = ReorderDocumentConfigHandler(
            name='reorder_document', config_type='json',
            config_schema=reorder_document_configuration_schema
        )
        self.output_folder = output_folder
        self.output_type = output_type
        self._database = DatabaseService()
        self._database.create_database_session(
            connection_string=connection_string
        )
        self._plenty_api_fetcher = PlentyApiFetcher(
            base_url=get_plenty_api_base_url()
        )
        self.cache_handler = SelectionCacheHandler()
        self.additional_variation_data =\
            self.__determine_required_additional_request_data()

    def __get_selections(self) -> pandas.DataFrame:
        cached_data = self.cache_handler.read_selections()
        if len(cached_data.index) == 0:
            data = self._plenty_api_fetcher.pull_selection_mapping(
                export_language=self.config_handler.config['export_language']
            )
            if len(data.index) > 0:
                self.cache_handler.cache_selections(dataframe=data)
        else:
            return cached_data
        return data


    def __determine_required_additional_request_data(self) -> list:
        config = self.config_handler.config
        plenty_variation_data_types = ['feature', 'picture']
        config_types = [
            column['type'] for column in config['columns']
        ] + [
            column['fallback']['type'] for column in config['columns']
            if 'fallback' in column
        ]
        return [
            column_type for column_type in set(config_types)
            if column_type in plenty_variation_data_types
        ]

    def __determine_required_identification_fields(self) -> list:
        config = self.config_handler.config
        return list(
            set(
                [
                    column['identification_type']
                    for column in config['columns']
                    if column['type'] == 'identification'
                ] + [
                    column['fallback']['identification_type']
                    for column in config['columns']
                    if 'fallback' in column and
                    column['fallback']['type'] == 'identification'
                ]
            )
        )

    def create_reorder_document(self, order_id: int) -> bool:
        reorder = self._plenty_api_fetcher.pull_order(order_id=order_id)
        if not reorder:
            raise RuntimeError('Plentymarkets reorder REST API request failed')
        variations = [
            item['itemVariationId'] for item in reorder['orderItems']
        ]
        variation_data = self._plenty_api_fetcher.pull_variation_data(
            variations=variations, additional=self.additional_variation_data
        )
        required_ident = self.__determine_required_identification_fields()
        if required_ident:
            ident_mapping = self._database.get_ident_mapping(
                variation_ids=variations, elements=required_ident
            )
        else:
            ident_mapping = None
        if 'feature' in self.additional_variation_data:
            selections = self.__get_selections()
        else:
            selections = None

        builder = DataFrameBuilder(
            config=self.config_handler.config, reorder_data=reorder,
            variation_data=variation_data, ident_mapping=ident_mapping,
            selection_map=selections
        )
        dataframe = builder.build_dataframe()
        document_builder = ReorderDocumentBuilder(
            config=self.config_handler.config
        )
        id_date = f'{order_id}_{date.today()}'
        path = self.output_folder / f'reorder_document_{id_date}.csv'
        return document_builder.build_csv(
            dataframe=dataframe, output_path=path
        )
