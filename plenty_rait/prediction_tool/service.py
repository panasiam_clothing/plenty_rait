"""
Sub application of the Plenty RAIT tool with the purpose of predicting
future sales and place the results back into the database.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
from decimal import Decimal
import pathlib
import dateutil.relativedelta
import matplotlib.pyplot as plt
import numpy as np
import pandas
import seaborn as sns
import sqlalchemy.sql
from sqlalchemy import func, and_, or_, inspect
from sqlalchemy.ext.serializer import loads, dumps
from statsmodels.tsa.api import SimpleExpSmoothing, Holt, ExponentialSmoothing
import tqdm

from loguru import logger

from plenty_rait.helper.database_schema import (
    BaseDatabaseService, DailyStatisticModel, StatisticModel, dal, Base
)


VALID_FORMAT_TYPES = ['monthly_progression', 'monthly_total', 'daily_total']
MONTHLY_PLOT_LINE_WIDTH = 1
DAILY_PLOT_LINE_WIDTH = 0.3


def valid_format_type(format_type: str) -> bool:
    if format_type not in VALID_FORMAT_TYPES:
        return False
    return True


def to_date(year: int, month: int, day: int = 0) -> datetime.date:
    try:
        if day > 0:
            return datetime.date(int(year), int(month), int(day))
        return datetime.date(int(year), int(month), 1)
    except ValueError:
        logger.error(f'Invalid year ({year}), month ({month}) combination')
        return None
    return None


def find_best_algorithm(actual_sum: int, diff_sums: dict,
                        predicted_sum: dict) -> str:
    best = [-1, -1, '']

    for key in diff_sums.keys():
        diff_sum = diff_sums[key]
        diff_to_actual = abs(actual_sum - predicted_sum[key])
        if best[0] == -1 and best[1] == -1:
            best[0] = diff_sum
            best[1] = diff_to_actual
            best[2] = key
            continue
        if best[0] >= diff_sum and best[1] >= diff_to_actual:
            best[0] = diff_sum
            best[1] = diff_to_actual
            best[2] = key

    return best[2]


def add_to_dataset(dataframe: pandas.DataFrame, series: pandas.Series,
                   name: str, with_diff=False) -> pandas.DataFrame:
    dataframe = dataframe.assign(temp_name=series)
    dataframe = dataframe.rename(columns={'temp_name': name})
    if with_diff:
        dataframe = dataframe.assign(
            temp_diff=lambda x: abs(x['total'] - x[name])
        )
        dataframe = dataframe.rename(
            columns={'temp_diff': str(f'diff_{name}')}
        )
    return dataframe


def get_sub_axis(num_variations: int, axes, x: int, y: int):
    if num_variations == 1:
        sub_axis = axes
    elif num_variations == 2:
        sub_axis = axes[x]
    else:
        sub_axis = axes[x, y]
    return sub_axis


class PredictionSession(BaseDatabaseService):
    def __init__(self, session=None):
        super().__init__(session)
        self.tables = [StatisticModel, DailyStatisticModel]

    def backup_table(self, table: str, dest_file: pathlib.Path) -> bool:
        if inspect(dal.engine).has_table(table):
            orm_table = [t for t in self.tables if t.__tablename__ == table]
            if not orm_table:
                logger.error(f'Invalid table name for backup [{table}]')
                return False
            query = self.session.query(orm_table[0])
            serialized_data = dumps(query.all())
            with open(dest_file, 'wb') as backup_file:
                backup_file.write(serialized_data)
            logger.info(f'Backup saved at {dest_file}')
            return True
        logger.warning(f'Back-up of table [{table}] failed. Table '
                       'does not exist.')
        return False

    def rollback_table(self, table: str, source_file: pathlib.Path) -> bool:
        if inspect(dal.engine).has_table(table):
            logger.warning('Are you sure that you want to rollback? All data '
                           'added after the backup will be permanently lost!')
            choice = ''
            while not choice:
                choice = input('(y/N)')
                if not choice:
                    continue
                if choice[0].lower() not in ['y', 'j']:
                    return False
            with open(source_file, 'rb') as backup_file:
                serialized_data = backup_file.read()
            restore_query = loads(serialized_data, Base.metadata, self.session)
            for query_element in restore_query:
                self.session.merge(query_element)
            self.session.commit()
            return True
        logger.warning(f'Rollback of table [{table}] failed. Table '
                       'does not exist.')
        return False


class Forecaster:
    def __init__(self, format_type, session=None, algorithms=None):
        self.session = session
        self.algorithms = algorithms
        if not valid_format_type(format_type=format_type):
            logger.warning(f'Invalid data format type {format_type}.')
            return
        self.format_type = format_type

    def get_origins_for_variation(self, variation_id: int) -> list:
        origins = []
        query = self.session.query(
            func.distinct(StatisticModel.origin_id).label('origins')
        ).filter(
            StatisticModel.variation_id == variation_id
        ).all()
        for origin in query:
            origins.append(origin[0])
        return origins

    def algorithm_compare(self, data: pandas.DataFrame, fcasts: int,
                          write_to: str = '') -> dict:
        """
        Find out which algorithm provides the best result for a given data set.

        Currently valid algorithms are:
            [simple02, simple04, simpleauto, holtlinear, holtexpo]

        Parameters:
            data            [DataFrame]     -   Input data to be analyzed
            write_to        [str]           -   Filename (optional)
            qty_field       [str]           -   Name of the column to analyze
            fcasts          [int]           -   Amount of iterations to
                                                forecast

        Return:
                            [dict]          -
                            Dictionary containing:
                                * The sum of differences between the predicted
                                  and actual values
                                * The sum of predicted values
                                * The forecasts for the given time interval
                                * The best algorithm (the algorithm with the
                                  lowest difference sum and total sum closest
                                  to the actual)
                                * The updated dataset
        """
        qty_field = 'total'
        result = {'diff_to_actual_sums': {}, 'total_sums': {}, 'fcasts': {},
                  'best': '', 'data': pandas.DataFrame()}

        if len(data.index) == 0:
            return result

        data.reset_index(drop=True, inplace=True)

        forecast_algorithm = Algorithm(data=data[qty_field],
                                       seasonal_periods=12)

        for algorithm in self.algorithms:
            fit_result = forecast_algorithm.function[algorithm]()
            if fit_result is None:
                # pylint: disable=invalid-string-quote
                logger.warning(f"Running the {algorithm} algorithm for the "
                               f"{data['variation_id'].tolist()[0]} variation "
                               "failed. Skip.")
                continue
            fcast = fit_result.forecast(steps=fcasts).rename(algorithm)
            predict_dates = self.__date_series(
                recent_data=data['date_value'].max(),
                months=fcasts)
            data = add_to_dataset(dataframe=data, series=fit_result.fittedvalues,
                                  name=algorithm, with_diff=True)
            result['diff_to_actual_sums'][algorithm] =\
                data[str(f'diff_{algorithm}')].sum()
            result['total_sums'][algorithm] = data[algorithm].sum()
            fcasts_df = fcast.to_frame(name='total')
            fcasts_df.loc[:, 'date_value'] = predict_dates
            result['fcasts'][algorithm] = fcasts_df

        if write_to:
            data.to_csv(str(f'{write_to}.csv'), sep=';', index=False)

        result['best'] = find_best_algorithm(
            actual_sum=data['total'].sum(),
            diff_sums=result['diff_to_actual_sums'],
            predicted_sum=result['total_sums'])
        result['data'] = data

        return result

    def get_total_prediction(self, data: pandas.DataFrame, fcasts: int,
                             algorithm: str) -> int:
        """
        Calculate the sum of forecasts for a given range in months.

        Parameters:
            data                [DataFrame]     -   data set containing the
                                                    sales for a single
                                                    variation.
            fcasts              [int]           -   Amount of months to
                                                    forecast
            algorithm           [str]           -   String identifier for
                                                    a specific algorithm
        Return:
                                [int]           -   Total rounded sum
        """
        predictions = self.get_predictions(data=data, fcasts=fcasts,
                                           algorithm=algorithm)
        if len(predictions.index) == 0:
            return 0
        return round(predictions['prediction'].sum())

    def get_predictions(self, data: pandas.DataFrame, fcasts: int,
                        algorithm: str) -> int:
        """
        Calculate the sum of forecasts for a given range in months.

        Parameters:
            data                [DataFrame]     -   data set containing the
                                                    sales for a single
                                                    variation-origin
                                                    combination.
            fcasts              [int]           -   Amount of months to
                                                    forecast
            algorithm           [str]           -   String identifier for
                                                    a specific algorithm
        Return:
                                [DataFrame]     -   predictions for a series of
                                                    dates on a single variation
                                                    origin combinations.
        """
        qty_field = 'total'
        if len(data.index) == 0:
            return pandas.DataFrame()

        data.reset_index(drop=True, inplace=True)

        forecast_algorithm = Algorithm(data=data[qty_field],
                                       seasonal_periods=12)
        fit_result = forecast_algorithm.function[algorithm]()
        if fit_result is None:
            # pylint: disable=invalid-string-quote
            logger.warning(f"Running the {algorithm} algorithm for the "
                           f"{data['variation_id'].tolist()[0]} variation "
                           "failed. Skip.")
            return pandas.DataFrame()
        fcast = fit_result.forecast(steps=fcasts).rename('prediction')
        fcasts_df = fcast.to_frame(name='prediction')
        fcasts_df = fcasts_df.reset_index(drop=True)
        fcasts_df['prediction'] = fcasts_df['prediction'].apply(round)
        fcasts_df['variation_id'] = data['variation_id'].unique()[0]
        fcasts_df.loc[:, 'date_value'] = self.__date_series(
            recent_data=data['date_value'].max(), months=fcasts)
        return fcasts_df

    def create_plot(self, data: pandas.DataFrame, forecasts: dict,
                    best_algorithm: dict) -> None:
        """
        Create a plot to visualize, how well a certain algorithm is able to
        adjust to the actual data. Additionally, visualize the forecasts of
        all the algorithm from the algorithm comparision.

        Parameter:
            data                [DataFrame]     -   data set from
                                                    'algorithm_compare',
                                                    containing the actual data
                                                    and the algorithm results
                                                    for each given variation.
            forecasts:          [dict]          -   forecast data from the
                                                    set of algorithm for each
                                                    variation.
            best_algorithm      [dict]          -   Mapping of variations to
                                                    their respective best
                                                    performing algorithm.
        """
        if len(data.index) == 0:
            logger.warning('Empty data-set, abort. ')
            return
        sns.set_theme(style='darkgrid')
        unique_variations = data['variation_id'].unique()
        if len(unique_variations) > 6:
            logger.warning('More than 6 variations are currently not '
                           'supported by create_plot')
            return

        if self.format_type == 'daily_total':
            linewidth = DAILY_PLOT_LINE_WIDTH
        else:
            linewidth = MONTHLY_PLOT_LINE_WIDTH

        if len(unique_variations) > 4:
            axes_x = 2
            axes_y = 3
        elif len(unique_variations) > 2:
            axes_x = 2
            axes_y = 2
        elif len(unique_variations) == 2:
            axes_x = 2
            axes_y = 1
        elif len(unique_variations) == 1:
            axes_x = 1
            axes_y = 1

        fig, axes = plt.subplots(axes_x, axes_y, sharex=True, dpi=1000)
        # pylint: disable=invalid-name
        x = 0
        y = 0
        for variation in unique_variations:
            variation_data = data[data['variation_id'] == variation]
            sub_axis = get_sub_axis(num_variations=len(unique_variations),
                                    axes=axes, x=x, y=y)
            sub_axis.set_title(str(variation), loc='center')
            sub_axis.legend(loc='upper right')
            y_max = variation_data['total'].max()
            y_max = y_max + (y_max // 2)
            sub_axis.set_ylim(0, y_max)
            plot_x = variation_data['date_value']
            plot_y = variation_data['total']
            sns.lineplot(x=plot_x, y=plot_y, ax=sub_axis,
                         color='blue', data=variation_data, label='actual',
                         linewidth=linewidth)
            algorithm = best_algorithm[str(variation)]
            plot_y = variation_data[algorithm]
            sns.lineplot(x=plot_x, y=plot_y, ax=sub_axis,
                         legend='full', color='red',
                         data=variation_data,
                         label=str(f'{algorithm} [best algorithm]'),
                         linewidth=linewidth)
            for algorithm in self.algorithms:
                fcasts = forecasts[str(variation)][algorithm]
                if fcasts.total.max() == fcasts.total.mean():
                    continue
                plot_x = fcasts['date_value']
                plot_y = fcasts['total']
                sns.lineplot(x=plot_x, y=plot_y, ax=sub_axis,
                             legend='full', color=np.random.rand(3,),
                             data=fcasts, label=str(f'forecast_{algorithm}'),
                             linewidth=linewidth)
            x += 1
            if x == axes_x:
                x = 0
                y += 1
        fig = plt.gcf()
        fig.set_size_inches((15, 11), forward=False)
        fig.savefig(
            # pylint: disable=invalid-string-quote
            str("algorithm_compare_"
                f"{datetime.datetime.now().strftime('%Y-%m-%d')}.jpg"),
            dpi=500
        )

    @staticmethod
    def visualize_sales(data: pandas.DataFrame,
                        variations: list) -> None:
        new_data = pandas.DataFrame()
        data_with_date = data.set_index('date_value')
        for variation in variations:
            variation_data = data_with_date[
                data_with_date['variation_id'] == variation]['total']
            variation_data = variation_data.asfreq(freq='MS', fill_value=0.0)
            variation_data = variation_data.to_frame()
            variation_data['variation_id'] = variation
            new_data = new_data.append(variation_data)

        # Facet grid with one plot for each variation
        new_data['date_value'] = new_data.index
        grid = sns.FacetGrid(new_data, col='variation_id', col_wrap=3)
        grid.map_dataframe(sns.lineplot, x='date_value', y='total')
        plt.show()

    @staticmethod
    def __date_series(recent_data: datetime.datetime,
                      months: int) -> np.array:
        predict_dates = pandas.Series(
            [recent_data + dateutil.relativedelta.relativedelta(months=+x)
                for x in range(1, months+1)])
        return predict_dates.values


class DataFormat:
    """
    There are three valid formats:
        - monthly_progression (1.{first_year}, 1.2016, .. 1.{current_year})
        - monthly_total       (1.{first_year}, 2.{first_year} ..
                               {current_month}.{current_year})
        - daily_total         (Same as monthly total but with every day)
    """
    def __init__(self, format_type: str, session=None):
        if format_type not in ['monthly_progression', 'monthly_total',
                               'daily_total']:
            logger.error('Invalid format type used, see the documentation '
                         '@DataFormat class')
            return
        self.format_type = format_type
        self.session = session

    def get_database_entries(self, variation_id: list = None,
                             origin_id: list = None,
                             **kwargs) -> pandas.DataFrame:
        empty_df = pandas.DataFrame()
        if self.format_type == 'monthly_progression':
            if 'month' not in kwargs:
                logger.error(f'Format type: {self.format_type} requires a '
                             'month parameter')
                return empty_df
            query = self.__variation_sales_month_progression(
                variation_id=variation_id, origin_id=origin_id,
                month=kwargs['month'])
        elif self.format_type == 'monthly_total':
            query = self.__variation_sales_total_per_month(
                variation_id=variation_id, origin_id=origin_id
            )
        elif self.format_type == 'daily_total':
            query = self.__variation_sales_total_per_day(
                variation_id=variation_id, origin_id=origin_id
            )
        return self.__create_dataframe(query=query)

    def write_database_entries(self, data: pandas.DataFrame,
                               origin_id: Decimal = -1) -> bool:
        if self.format_type == 'daily_total':
            logger.warning('Predictions are currently not implemented for the '
                           'daily format.')
            return False

        origins = self.session.query(func.distinct(StatisticModel.origin_id))
        if origin_id not in [x[0] for x in origins.all()]:
            logger.warning(f'Invalid origin: {origin_id}')
            return False

        logger.info(f'Write {len(data.index)} rows to the database.')
        new_entries = []
        for row in tqdm.tqdm(data.itertuples()):
            month = row.date_value.month
            year = row.date_value.year
            variation = row.variation_id
            qty = row.prediction

            local_match = [x for x in new_entries if x.month == month
                           and x.year == year and x.variation_id == variation]
            if len(local_match) > 0:
                continue

            db_match = self.session.query(StatisticModel).filter(
                StatisticModel.variation_id == variation,
                StatisticModel.month == month,
                StatisticModel.year == year,
                StatisticModel.origin_id == origin_id
            )

            if db_match.count() == 0:
                entry = StatisticModel(variation_id=variation, month=month,
                                       year=year, origin_id=origin_id,
                                       quantity=qty, predicted=True)
                new_entries.append(entry)
            elif db_match.count() == 1:
                db_entry = db_match.one()
                if db_entry.predicted:
                    db_entry.quantity = qty
                else:
                    db_entry.quantity = qty
                    db_entry.predicted = 1
            else:
                logger.warning(f'Duplicate database entries for\n(year: {year}'
                               f'month: {month}, variation: {variation}, '
                               f'origin_id: {origin_id})\nIDs: '
                               f'[{[x.id for x in db_match.all()]}]')
                return False

        self.session.add_all(new_entries)
        self.session.commit()

        return True

    def __variation_scope_filter(self, variations: list) -> bool:
        if variations is None:
            return sqlalchemy.sql.true()
        if isinstance(variations, list):
            if self.format_type == 'daily_total':
                return DailyStatisticModel.variation_id.in_(variations)
            return StatisticModel.variation_id.in_(variations)
        if isinstance(variations, int):
            if self.format_type == 'daily_total':
                return DailyStatisticModel.variation_id == variations
            return StatisticModel.variation_id == variations
        logger.warning(f'Invalid variations {variations}')
        return sqlalchemy.sql.false()

    def __origin_scope_filter(self, origins: list) -> bool:
        if origins is None:
            if self.format_type == 'daily_total':
                return sqlalchemy.sql.true()
            return StatisticModel.origin_id == Decimal('-1')
        if isinstance(origins, list):
            if self.format_type == 'daily_total':
                return DailyStatisticModel.origin_id.in_(origins)
            return StatisticModel.origin_id.in_(origins)
        if isinstance(origins, Decimal):
            if self.format_type == 'daily_total':
                return DailyStatisticModel.origin_id == origins
            return StatisticModel.origin_id == origins
        logger.warning(f'Invalid origins {origins}')
        return sqlalchemy.sql.false()

    def __variation_sales_month_progression(
            self, month: int, variation_id: list = None,
            origin_id: list = None) -> pandas.DataFrame:

        if not month:
            return None

        query = self.session.query(
            StatisticModel.quantity.label('total'),
            StatisticModel.variation_id,
            StatisticModel.month, StatisticModel.year
        ).filter(
            and_(
                StatisticModel.month == month,
                self.__variation_scope_filter(variations=variation_id),
                self.__origin_scope_filter(origins=origin_id),
                StatisticModel.predicted.is_(False),
                or_(
                    StatisticModel.month != datetime.datetime.now().month,
                    StatisticModel.year != datetime.datetime.now().year
                )
            )
        )
        return query

    def __variation_sales_total_per_month(
            self, variation_id: list = None,
            origin_id: list = None) -> pandas.DataFrame:
        query = self.session.query(
            StatisticModel.quantity.label('total'),
            StatisticModel.variation_id,
            StatisticModel.month, StatisticModel.year
        ).filter(
            and_(
                self.__variation_scope_filter(variations=variation_id),
                self.__origin_scope_filter(origins=origin_id),
                StatisticModel.predicted.is_(False),
                or_(
                    StatisticModel.month != datetime.datetime.now().month,
                    StatisticModel.year != datetime.datetime.now().year
                )
            )
        )
        return query

    def __variation_sales_total_per_day(
            self, variation_id: list = None,
            origin_id: list = None) -> pandas.DataFrame:
        query = self.session.query(
            DailyStatisticModel.quantity.label('total'),
            DailyStatisticModel.variation_id,
            DailyStatisticModel.month, DailyStatisticModel.year,
            DailyStatisticModel.day
        ).filter(
            self.__variation_scope_filter(variations=variation_id),
            self.__origin_scope_filter(origins=origin_id)
        )
        return query

    def __create_dataframe(self, query) -> pandas.DataFrame:
        if not query:
            return pandas.DataFrame()
        dataframe = pandas.read_sql(query.statement, self.session.bind)
        if len(dataframe.index) == 0:
            return pandas.DataFrame()
        if self.format_type == 'daily_total':
            dataframe['date_value'] = dataframe.apply(
                lambda x: to_date(year=x.year, month=x.month, day=x.day),
                axis=1)
        else:
            dataframe['date_value'] = dataframe.apply(
                lambda x: to_date(year=x.year, month=x.month), axis=1)
        dataframe['date_value'] = pandas.to_datetime(dataframe['date_value'],
                                                     format='%Y-%m-%d')
        dataframe.dropna(inplace=True)
        dataframe = dataframe.groupby(
            ['variation_id', 'date_value']
        ).agg({'total': 'sum'}).reset_index()
        return dataframe


class Algorithm:
    def __init__(self, data, seasonal_periods: int):
        self.data = data
        self.seasonal_periods = seasonal_periods
        self.function = {
            'simple02': lambda: self.__simple_exponential_smoothing(level=0.2),
            'simple04': lambda: self.__simple_exponential_smoothing(level=0.4),
            'simpleauto': self.__simple_exponential_smoothing,
            'holtlinear': lambda: self.__holt_exponential_smoothing(
                level=0.8, trend=0.2, expo=False),
            'holtexpo': lambda: self.__holt_exponential_smoothing(
                level=0.8, trend=0.2, expo=True),
            'winterlinear': lambda: self.__winter_exponential_smoothing(
                trend='add', seasonal='add')
        }

    def __simple_exponential_smoothing(self, level=None, optimized=False):
        try:
            if not level:
                return SimpleExpSmoothing(
                    self.data, initialization_method='estimated'
                ).fit()
            return SimpleExpSmoothing(
                self.data, initialization_method='estimated'
            ).fit(
                smoothing_level=level, optimized=optimized
            )
        except ValueError as err:
            logger.warning(f'SimpleExpSmoothing function failed => {err}')
            return None

    def __holt_exponential_smoothing(self, level=None, trend=None, expo=False):
        try:
            return Holt(
                self.data, initialization_method='estimated', exponential=expo
            ).fit(
                smoothing_level=level, smoothing_trend=trend
            )
        # pylint: disable=broad-except
        except Exception as err:
            logger.warning(f'HoltExpSmoothing function failed => {err}')
            return None

    def __winter_exponential_smoothing(self, trend=None, seasonal=None):
        try:
            return ExponentialSmoothing(
                self.data, initialization_method='estimated',
                trend=trend, seasonal=seasonal,
                seasonal_periods=self.seasonal_periods
            ).fit()
        # pylint: disable=broad-except
        except Exception as err:
            logger.warning(f'WinterExpSmoothing function failed => {err}')
            return None
