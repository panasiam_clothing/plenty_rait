# Prediction tool

Use sales statistics data to calculate possible sales in the future.

## Configuration

## Logging

The application logs by default to a standardized location on your platform. On Linux this will usually be: `~/.cache/prediction_tool/log`. The file will grow to a maximum of 2 MB, at which point it will start to rotate out old entries.

## Usage

Prediction is not a straight forward process, in order to make a good guess which algorithm fits best to your data, you have to be able to observe how the data behaves.
This step can be performed with the following command:
```bash
python3 -m plenty_rait PredictionTool Action --data_format monthly_total --forecast_range 10 --variation_id 1234,2345,3456 Analysis
```

Here we say that we want to make an analysis on the variations {1234,2345,3456} (Plentymarkets variation IDs) for all origins, which means that we apply all the available algorithms, predict 10 months into the future, calculate which algorithms has the lowest average error (difference to the actual data) and plot the actual data together with the best algorithm with matplotlib. The `data_format` in this case is `monthly_total`, which means that we take the total sales for each variation/origin combination for each month. Other possible formats are [`daily_total` and `monthly_progression`], but currently `monthly_total` provides the best results.

If you want to compare multiple variations with one another you can use the `visual_sales` action.

```bash
python3 -m plenty_rait PredictionTool Action --data_format monthly_total --variation_id 1234,2345,3456 Visual
```

This action will simply plot the sales of the different variations, in order to search for patterns.

When you are done with your analysis you can start the prediction, you have to choose an algorithm, a set of variations, a origin, the amount of months to predict and a output target.
Here are a few examples:
```bash
# Predict 10 months into the future for a single variation and all origins and write the result into a `.csv` (use the default holt-winter-exponential smoothing algorithm)
python3 -m plenty_rait PredictionTool Action --data_format monthly_total --variation_id 1234 --forecast_range 10 Prediction --output file --custom_file_location /path/to/file.csv

# Predict 15 months into the future with the holt linear exponential smoothing algorithm for all variations, a single origin and write the result to the database (a backup is created automatically)
python3 -m plenty_rait PredictionTool Action --algorithm holtlinear --data_format monthly_total --origin_id 4.01 --forecast_range 15 Prediction --output database

# Roll back a recent database write
python3 -m plenty_rait PredictionTool Backup --path ~/.config/plenty_rait/prediction_tool/.statistics_2021-03-04_backup.db --rollback
```
