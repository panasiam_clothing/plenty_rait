"""
Sub application of the Plenty RAIT tool with the purpose of predicting
future sales and place the results back into the database.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import argparse
import configparser
import datetime
from decimal import Decimal, InvalidOperation
import os
import pathlib
import tempfile
import pandas
from loguru import logger


from plenty_rait.prediction_tool.service import (
    Forecaster, PredictionSession, DataFormat
)


from plenty_rait.helper.setup_helper import CommandLineSetup
from plenty_rait.helper.config_helper import init_config


# pylint: disable=missing-function-docstring
def setup_prediction_parser(subparser, default_path: str):
    prediction_parser = subparser.add_parser(
        'Prediction',
        help='Predicte sales into the future and write results to the database'
    )
    prediction_parser.add_argument(
        '--output', '--out', '-o', required=False,
        help='Write the prediction results to the specified destination.',
        choices=['file', 'database'], default='file',
        dest='prediction_output_type')
    prediction_parser.add_argument(
        '--custom_file_location', '--file_location', '--file',
        required=False,
        help='custom file location for the prediction results.',
        type=argparse.FileType('w', encoding='utf-8'),
        dest='prediction_file_path', default=default_path)


# pylint: disable=missing-function-docstring
def setup_analysis_parser(subparser, default_path: str):
    analysis_parser = subparser.add_parser(
        'Analysis',
        help='Predicte sales into the future and create a plot for the results'
    )
    analysis_parser.add_argument(
        '--output', '--out', '-o', required=False,
        help='Write the prediction results to the specified destination.',
        choices=['file', 'database'], default='file',
        dest='prediction_output_type')
    analysis_parser.add_argument(
        '--custom_file_location', '--file_location', '--file',
        required=False,
        help='custom file location for the prediction results.',
        type=argparse.FileType('w', encoding='utf-8'),
        dest='prediction_file_path', default=default_path)


# pylint: disable=missing-function-docstring
def setup_visual_parser(subparser):
    subparser.add_parser(
        'Visual',
        help='Create a graph representing the sales development of variations'
    )


# pylint: disable=missing-function-docstring
def setup_action_parser(subparser):
    now = datetime.date.today()
    default_file_path = str(f'./prediction_results_{now}.csv')
    action_parser = subparser.add_parser(
        'Action',
        help='Choose the mode of the prediction tool'
    )
    sub_action_parser = action_parser.add_subparsers(
        title='Sub-Commands of the prediction tool',
        dest='prediction_action_command'
    )
    setup_prediction_parser(subparser=sub_action_parser,
                            default_path=default_file_path)
    setup_analysis_parser(subparser=sub_action_parser,
                          default_path=default_file_path)
    setup_visual_parser(subparser=sub_action_parser)

    action_parser.add_argument(
        '--algorithm', '-a', required=False,
        help='Choose a specific algorithm to predict sales.',
        choices=['simple02', 'simple04', 'simpleauto', 'holtlinear',
                 'holtexpo', 'winterlinear'],
        type=str, dest='prediction_algorithm', default='winterlinear')
    action_parser.add_argument(
        '--format', '--data_format', '-f', required=False,
        help='The type of format for the inital data',
        choices=['monthly_progression', 'monthly_total', 'daily_total'],
        dest='prediction_dataformat', default='monthly_total')
    action_parser.add_argument(
        '--forecast_range', '--forecasts', '--fcasts', '-c',
        required=False, help='Number of months to forecasts',
        type=int, dest='prediction_fcasts', default=12)
    action_parser.add_argument(
        '--month', '--mon', required=False,
        help='Month for the \'montly_progression\' dataformat.',
        type=int, dest='prediction_month')
    action_parser.add_argument(
        '--origin_id', '--origin', '--origins', '-o', required=False,
        help='Comma-separated list of origins to predict/analyse. (-1 for all)',
        default='-1', type=str, dest='prediction_origins')
    action_parser.add_argument(
        '--variation_id', '--variation', '--var', '-v' '--variations',
        required=False, default='-1', type=str, dest='prediction_variations',
        help='Comma-separated list of variations to predict/analyse. '
        '(-1 for all)')


# pylint: disable=missing-function-docstring
def setup_backup_parser(subparser):
    backup_parser = subparser.add_parser(
        'Backup',
        help='Create a backup or rollback to a backup'
    )
    backup_parser.add_argument(
        '--table', '-t', required=False,
        help='Select which table to backup or rollback.',
        type=str, choices=['daily', 'monthly'], default='monthly',
        dest='backup_type'
    )
    backup_parser.add_argument(
        '--custom_location', '--path', '-p', required=False,
        help='Select a custom destination for the backup file',
        type=pathlib.Path, dest='prediction_backup_custom_location'
    )
    backup_parser.add_argument(
        '--rollback', '-r', required=False,
        help='Rollback a certain table to a previous state',
        type=argparse.FileType('r'), dest='prediction_rollback')


# pylint: disable=missing-function-docstring
def create_backup_filename(base_path: pathlib.Path,
                           tablename: str) -> pathlib.Path:
    now = datetime.date.today()
    return base_path / f'.{tablename}_{now}_backup.db'


# pylint: disable=missing-function-docstring
def parse_comma_separated_list(string: str) -> list:
    result = []
    for sub_str in string.split(','):
        try:
            result.append(int(sub_str))
        except ValueError:
            result.append(Decimal(sub_str))
        except InvalidOperation:
            logger.error(f'Invalid value: {sub_str}. The provided '
                         'argument must be a comma-separated string of integer'
                         '/decimal IDs')
            return None

    if -1 in result:
        return None
    return result


# pylint: disable=missing-class-docstring
class PredictionToolCommandLine(CommandLineSetup):
    @staticmethod
    def setup_parser(subparser) -> argparse.ArgumentParser:
        parser = subparser.add_parser('PredictionTool')
        subparsers = parser.add_subparsers(
            title='Sub-Commands',
            help='The different modes of the prediction tool',
            dest='prediction_tool_command'
        )
        setup_action_parser(subparser=subparsers)
        setup_backup_parser(subparser=subparsers)

    @staticmethod
    def validate_parser(args: argparse.Namespace) -> bool:
        month = args.prediction_month
        if month and month not in range(1, 13):
            logger.error('Invalid \'month\' parameter must be an integer '
                         f'between 1-12. Got: {month}.')
            return False
        if args.prediction_dataformat == 'monthly_progression' and not month:
            logger.error('The \'monthly_progression\' data-format requires the'
                         ' \'month\' parameter.')
            return False
        if args.prediction_command == 'Backup':
            if (
                args.prediction_rollback and
                not os.path.exists(args.prediction_rollback.name)
            ):
                logger.error('Invalid file for rollback. '
                             f'[{args.prediction_rollback}]')
                return False
            if (
                args.prediction_backup_custom_location and
                not args.prediction_backup_custom_location.exists() or
                not args.prediction_backup_custom_location.isdir()
            ):
                logger.error('Invalid custom backup folder does not exist or '
                             'is not a directory '
                             f'({args.prediction_backup_custom_location})')
                return False
        return True

    @staticmethod
    def handle_command(args: argparse.Namespace,
                       connection_string: str) -> bool:
        config_path = init_config(name='prediction_tool')
        config = configparser.ConfigParser()
        config.read(config_path)
        variations = parse_comma_separated_list(
            string=args.prediction_variations)
        origins = parse_comma_separated_list(string=args.prediction_origins)

        session = PredictionSession()
        session.create_database_session(connection_string=connection_string)

        if (
            args.prediction_command == 'Backup' or
            (
                args.prediction_action_command in ['Prediction', 'Analysis']
                and args.prediction_output_type == 'database'
            )
        ):
            if args.backup_type == 'daily':
                tablename = 'daily_statistics'
            else:
                tablename = 'statistics'
            if args.prediction_backup_custom_location:
                base_path = args.prediction_backup_custom_location
            else:
                base_path = tempfile.gettempdir()
            filename = create_backup_filename(tablename=tablename,
                                              base_path=base_path)
            session.backup_table(table=tablename, dest_file=filename)

            if args.prediction_rollback:
                session.rollback_table(
                    table=tablename, source_file=args.prediction_rollback.name)
            if args.prediction_command == 'Backup':
                return True

        data = DataFormat(format_type=args.prediction_dataformat,
                          session=session.session)
        dataframe = data.get_database_entries(month=args.prediction_month,
                                       variation_id=variations,
                                       origin_id=origins)
        if len(dataframe.index) == 0:
            # pylint: disable=invalid-string-quote
            logger.warning(
                "No data entries found for variations: "
                f"{'ALL' if variations is None else variations} and "
                f"origins: {'ALL' if origins is None else origins}."
            )
            return True
        if variations is None:
            variations = dataframe['variation_id'].unique().tolist()

        # For test purposes defined as constant list
        algorithms = ['simple02', 'simpleauto',
                      'holtlinear', 'winterlinear']
        forecast_algorithm = args.prediction_algorithm

        forecaster = Forecaster(session=session.session, algorithms=algorithms,
                                format_type=args.prediction_dataformat)

        if args.action_command == 'Visual':
            forecaster.visualize_sales(data=dataframe, variations=variations)

        if args.action_command == 'Prediction':
            fcasts = args.prediction_fcasts
            if args.prediction_dataformat == 'daily_total':
                fcasts *= 30
            result_table = pandas.DataFrame()
            for variation in variations:
                predictions = forecaster.get_predictions(
                    data=dataframe[dataframe['variation_id'] == variation],
                    fcasts=fcasts, algorithm=forecast_algorithm
                )
                result_table = result_table.append(predictions)

            if args.prediction_output_type == 'file':
                result_table.to_csv(args.prediction_file_path, sep=';',
                                    na_rep='', index=False)

            if args.prediction_output_type == 'database':
                origin = Decimal('-1') if origins is None else origins[0]
                if not data.write_database_entries(data=result_table,
                                                   origin_id=origin):
                    logger.error('Database write failed.')

        if args.action_command == 'Analysis':
            result_table = pandas.DataFrame()
            forecasts = {}
            best_algorithm = {}
            for variation in variations:
                tmp_data = forecaster.algorithm_compare(
                    data=dataframe[dataframe['variation_id'] == variation],
                    write_to=str(f'monthly_progress_{variation}'),
                    fcasts=args.prediction_fcasts)
                if len(tmp_data['data'].index) == 0:
                    continue
                forecasts[str(variation)] = tmp_data['fcasts']
                if not tmp_data['best']:
                    continue
                best_algorithm[str(variation)] = tmp_data['best']
                result_table = result_table.append(tmp_data['data'])

            forecaster.create_plot(data=result_table, forecasts=forecasts,
                                   best_algorithm=best_algorithm)
        return True
