# Synchronize a database with data from Plentymarkets and import redistributions

## Introduction

This sub-application of the **Plenty RAIT** tool has two purposes:
1. Update the MariaDB database with the latest stock, order, priority (mapping of variations to certain tags which symbolize their importance) and identification data (mapping of ASIN, Plentymarkets Variation ID, EAN, SKU, NAME)
2. Create new redistributions from a given list of variation IDs and redistribution quantities between two warehouses on Plentymarkets

It is basically the interface to a Plentymarkets system through the REST API for the Plenty RAIT project.

## Configuration

The configuration file is located under `~/.config/plenty_rait/plenty_sync/config.json` on a Linux system and under `C:\users\{$USER}\AppData/Local/plenty_rait_tool/plenty_sync/config.json` on a Windows system, it will be created automatically when you launch the application for the first time.

#### Basic configuration

The application requires some basic information in order to work properly, you need to define the `ean_barcode_id` and `asin_id` to determine which of the many possible barcodes and ASINs shall be used for the `identification` table.

The `b2b_customer_id` is required to identify, which orders are *business to business* and which orders are *business to customer* sales. In order to find the correct ID you can use the following series of python instructions:
```python
import plenty_api
import requests
plenty = plenty_api.PlentyApi("https://company.plentymarkets-cloud01.com")
print(requests.get(plenty.url + '/rest/accounts/contacts/classes', headers=plenty.creds).json())
```

If there are variations you don't want to export from PlentyMarkets, you will need to define them with their variation IDs within the configuration in the following format:

```json
{
    ...
    "excluded_variations": [4617, 4836, 4838, 4839, 4896, 5155, 5156, 5686],
    ...
}
```

#### Priority configuration

If you want to use priorities you have to create a certain amount of variation tags in Plentymarkets under (Setup->Settings->Tags) for the variation section. Then you have to assign those tags to the variations. And finally you have to map the IDs of those tags to the correct priority level (with 1 being the highest level) within the configuration.

Example priority configuration:
```bash
{
    ...
    "priorities": [
        {"level": 1, "plenty_tag_id": 1},
        {"level": 2, "plenty_tag_id": 2},
        {"level": 3, "plenty_tag_id": 3},
        {"level": 4, "plenty_tag_id": 4}
    ],
    ...
}
```

#### Warehouse configuration

Besides the basic mapping of Plentymarkets barcodes and IDs you need to configure the warehouses you intend to maintain for stock exports and for the creation of redistributions. In order to fetch the correct stock data, you need to map the ID of the warehouse in Plentymarkets (locate the warehouse IDs under Setup->Stock->Warehouse->{target warehouse}->Settings) to a couple of attributes.

For instance, you have to assign a column used in the `current_stock` database-table. You can also declare additional parameters to refine your redistributions, for example by assigning a certain buffer to the different priority levels, you can ensure that you always have a certain minimum of variations depending on their importance. You can define a certain threshold where the algorithm empties a location in a certain warehouse, which is useful for some warehouses to not block space with near-empty boxes. A list of variation IDs can be defined to create exceptions to that rule.

And finally by defining a `transit_location` ID you can optionally enable the redistribution creation to automatically create incoming transactions into the defined transit location of the target warehouse (which means all picked products are dumped onto a single location in the target warehouse and the redistributed into their final locations).

#### Redistribution configuration

Most of the configuration for a redistribution is performed within the warehouse configuration but you can define the column headers within the configuration to skip the CLI options `--ident_column`, `--ident_type` and `--quantity`. This is useful when you always work with the same files.

```json
{
    ...
    "redistribution_id_column": "EAN",
    "redistribution_id_type": "ean",
    "redistribution_qty_column": "Amount"
    ...
}
```

Example configuration:
```json
{
    "ean_barcode_id": 1,
    "asin_id": 0,
    "b2b_customer_id": 3,
    "general_buffer": 0,
    "plenty_id": 12345,
    "priorities": [
        {"level": 1, "plenty_tag_id": 1},
        {"level": 2, "plenty_tag_id": 2},
        {"level": 3, "plenty_tag_id": 3}
    ],
    "excluded_variations": [1234, 1235, 1237],
    "exclude_reorder_status": 19.8,
    "warehouses": [
        {
            "plenty_warehouse_id": 100,
            "database_column_name": "stock_warehouse_1",
            "priority_buffer_mapping": [
                {"level": 1, "buffer": 30},
                {"level": -1, "buffer": 20}
            ],
            "transit_location_id": 12345
        },
        {
            "plenty_warehouse_id": 101,
            "database_column_name": "stock_warehouse_2",
            "empty_threshold_quantity": 20,
            "empty_exclusions": [3456, 4567],
            "transit_location_id": 23456
        }
    ]

}
```

The example above resembles a possible setup of two warehouses, where warehouse 101 is a warehouse for incoming deliveries and warehouse 100 is a warehouse for outgoing shipments. In this scenario, the configuration dictates that the warehouse for incoming deliveries tries to only contain  amounts of products that are larger than 20 as inventory for refueling the warehouse for outgoing shipments, while warehouse 100 tries to always keep a certain amount of stock in order to be operational. Additionally, warehouse 101 has excluded variations 3456 and 4567 from the empty threshold (these IDs are Plentymarkets variation IDs), this means even if the inventory of those products is below 20, it will not be automatically emptied on the next redistribution (which is useful for products that are sold very rarely).
The `exclude_reorder_status` field allows the user to specify the ID of an order status, that will mark a reorder as excluded from the synchronization.

## Logging

The application logs by default to a standardized location on your platform. On Linux this will usually be: `~/.cache/plenty_sync/log`. The file will grow to a maximum of 2 MB, at which point it will start to rotate out old entries.

## Usage

In order to use the application, you need to create a REST API user with the appropriate rights on Plentymarkets under: Setup->Settings->User->Accounts.
When the application is started you will be prompted for the user name and password and these credentials will be saved into the system-wide keyring.

Here are some examples, you can either perform **Export**s or **Import**s. You can find all possible types of exports by executing `python3 -m plenty_rait PlentySync Export -h`.


* Create the database tables in case they don't exist
```bash
python3 -m plenty_rait PlentySync --create_tables
```
* Synchronize the stock for the mapped warehouses
```bash
python3 -m plenty_rait PlentySync Export Stock
```
* Update the identification table
```bash
python3 -m plenty_rait PlentySync Export Identity
```
* Maintain a list of producers with a connected delivery time
```bash
python3 -m plenty_rait PlentySync Export Producer
```
* Synchronize helpful information for each variation like priority, producer and creation date
```bash
python3 -m plenty_rait PlentySync Export Additional
```
* Pull pending incoming variations from reorders
```bash
python3 -m plenty_rait PlentySync Export Incoming
```
* Import orders into the MariaDB database from Plentymarkets
```bash
python3 -m plenty_rait PlentySync Export Order --from 2021-03-05 --to 2021-04-05
```
* Export all possible elements the date range is required for the orders
```bash
python3 -m plenty_rait PlentySync Export All --from 2021-03-05 --to 2021-04-05
```
* Import a redistribution between warehouses 104 and 105, which are the Plentymarkets IDs for the warehouses, to Plentymarkets via the REST API from the given file with the EAN barcode identifier found in the `ean` column and the amount to be redistributed in the `qty` column. Use the given transit location of the target warehouse from the configuration as destination for all transactions.
```bash
python3 -m plenty_rait PlentySync Import \
    --file /tmp/redistribution_template.csv\
    --identifier ean\
    --identifier_type ean\
    --quantity qty\
    Redistribution \
    --source 104\
    --target 105\
    --with_transit
```

* Import a reorder from a producer contact in Plentymarkets with ID 1234 to a receiving warehouse with ID 104. Fetch the reorder quantity from an excel sheet.
```bash
python3 -m plenty_rait PlentySync Import \
    --excel /tmp/excel_file.xlsx \
    --identifier sku \
    --identifier_type sku \
    --quantity reorder_quantity \
    Reorder \
    --source 1234 \
    --target 104
```

* Clean up deprecated variations, which means variations that have been deleted on Plentymarkets and recreated onto a new variation ID.
```bash
python3 -m plenty_rait PlentySync Clean --cleanup_variations
```
