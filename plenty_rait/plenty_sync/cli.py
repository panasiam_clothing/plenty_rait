"""
Synchronize Plentymarkets orders, stock etc. with the MariaDB database.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import datetime
from functools import partial
import pathlib
import dateutil.parser as date_parser
from dateutil.parser._parser import ParserError

from loguru import logger

from plenty_rait.plenty_sync import service
from plenty_rait.helper.setup_helper import CommandLineSetup


class InvalidDate(Exception):
    def __init__(self, date_type: str, date_str: str):
        # pylint: disable=invalid-string-quote
        if date_type not in ['start', 'end']:
            raise RuntimeError("date type for InvalidDate Exception must be "
                               f"'start' or 'end'. Got: {date_type}")
        self.date_type = date_type
        self.date_str = date_str
        super().__init__()

    def __str__(self):
        return f'Invalid {self.date_type} date [{self.date_str}]'


def setup_export_parsers(subparser) -> None:
    """
    Create parsers for order, stock and identification data export.

    Order and All export require setting the start and end date for the fetch.

    Parameters:
        subparser           [SubParsersAction]- Collection of sub-parsers from
                                                the Argument Parser
    """
    export_parser = subparser.add_parser('Export')
    export_subparsers = export_parser.add_subparsers(
        title='Different types of export actions',
        help='Define which kind of export to perform [Order, Stock, Identity]',
        dest='plenty_sync_export_command')
    order_parser = export_subparsers.add_parser(
        'Order', help='Import orders from Plentymarkets to the database. '
                      'The [start] & [end] dates are required.')
    order_parser.add_argument(
        '-s', '--start', '--from', '-f', required=True,
        help='start date for the time range to fetch data',
        dest='plenty_sync_order_start')
    order_parser.add_argument(
        '-e', '--end', '--to', '-t', required=True,
        help='end date for the time range to fetch data',
        dest='plenty_sync_order_end')
    export_subparsers.add_parser(
        'Stock', help='Import stock from Plentymarkets to the database.')
    identity_parser = export_subparsers.add_parser(
        'Identity',
        help='Import identification data from Plentymarkets to the database.')
    identity_parser.add_argument(
        '--identity', help=argparse.SUPPRESS, dest='identity',
        action='store_true', default=True)
    export_subparsers.add_parser(
        'Additional',
        help='Import additional information for each variation like priority, '
        'release date and producer ID.')
    export_subparsers.add_parser(
        'Producer',
        help='Import producer contact information, including delivery time')
    export_subparsers.add_parser(
        'Incoming',
        help='Import incoming variations from reorders')
    all_parser = export_subparsers.add_parser(
        'All', help='Import orders, stock and identification data from '
                    'Plentymarkets. The [start] & [end] dates are required.')
    all_parser.add_argument(
        '-s', '--start', '--from', '-f', required=True,
        help='start date for the time range to fetch data', dest='start')
    all_parser.add_argument(
        '-e', '--end', '--to', '-t', required=True,
        help='end date for the time range to fetch data', dest='end')


def setup_import_parsers(subparser) -> None:
    """
    Create a parser for the creation of redistributions.

    Parameters:
        subparser           [SubParsersAction]- Collection of sub-parsers from
                                                the Argument Parser
    """
    import_parser = subparser.add_parser(
        'Import',
        help='Provide a template file which contains a column with an identity'
             'column (ean, sku, asin, variation ID) found in Plentymarkets and'
             ' a column containing the desired import quantity.\n'
             'Announce the name of those columns in the configuration or with '
             '`ident_column`, `ident_type` and `qty_column` CLI arguments.\n'
             '(otherwise the program assumes `ean` & `qty` as the column '
             'names).'
    )
    import_parser.add_argument(
        '-f', '--file', '--file-path', required=False,
        help='Path to the template file for the import creation',
        dest='plenty_sync_import_file', type=pathlib.Path)
    import_parser.add_argument(
        '-e', '--excel', '--excel_path', required=False,
        help='Path to the template spreadsheet for the import creation',
        dest='plenty_sync_excel', type=pathlib.Path)
    import_parser.add_argument(
        '--sheet', '--excel_sheet', required=False,
        help='Name of the spreadsheet at the give path',
        dest='plenty_sync_excel_sheet', type=str)
    import_parser.add_argument(
        '-i', '--ident_column', required=False,
        help='Name of the column where the identifier is located',
        dest='plenty_sync_ident_column')
    import_parser.add_argument(
        '--ident_type', required=False,
        help='Type of the identifier',
        choices=['ean', 'sku', 'variation_id', 'asin'],
        dest='plenty_sync_ident_type')
    import_parser.add_argument(
        '-q', '--qty_column', required=False,
        help='Name of the column where the quantity for the import is located',
        dest='plenty_sync_qty_column')

    import_subparsers = import_parser.add_subparsers(
        title='Different types of import actions', dest='import_command')
    redistribution_parser = import_subparsers.add_parser(
        'Redistribution',
        help='Declare the source warehouse and the target warehouse with their'
             ' IDs from Plentymarkets.'
    )
    redistribution_parser.add_argument(
        '-s', '--source', '--source-warehouse', required=True,
        help='ID of the warehouse from which the inventory is taken',
        dest='redistribution_source', type=int)
    redistribution_parser.add_argument(
        '-t', '--target', '--target-warehouse', required=True,
        help='ID of the warehouse in which the inventory is placed',
        dest='redistribution_target', type=int)
    redistribution_parser.add_argument(
        '-b', '--book_out', required=False,
        help='Perform bookings of outgoing and incoming transactions '
             'automatically in Plentymarkets (WARNING no changes can be '
             'performed on the order after this action)',
        dest='redistribution_booking', action='store_true')
    redistribution_parser.add_argument(
        '--transit_location', '--automatic_incoming', '--transit',
        required=False,
        help='Use the mapped transit location from the configuration to create'
             ' incoming transaction automatically',
        dest='transit_location', action='store_true')

    reorder_parser = import_subparsers.add_parser(
        'Reorder',
        help='Declare the contact ID of the producer from Plentymarkets, as '
             'well as the Plentymarkets ID of the receiving warehouse.'
    )
    reorder_parser.add_argument(
        '-s', '--source', '--producer_contact', '--producer', required=True,
        help='ID of the producer contact from Plentymarkets',
        dest='reorder_source', type=int)
    reorder_parser.add_argument(
        '-t', '--target', '--target-warehouse', required=True,
        help='ID of the warehouse in which the inventory is placed',
        dest='reorder_target', type=int)


def setup_cleanup_parsers(subparser) -> None:
    """
    Create a parser for cleaning up the database tables.

    Parameters:
        subparser           [SubParsersAction]- Collection of sub-parsers from
                                                the Argument Parser
    """
    cleanup_parser = subparser.add_parser('Clean')
    cleanup_parser.add_argument('--cleanup_variations', '--variations',
                                '--var',
                                help='Replace deprecated variations in'
                                     'order items',
                                dest='clean_variations', action='store_true')


def get_time_range(parser: argparse.Namespace) -> tuple:
    date_format = '%Y-%m-%dT%H:%M:%S'
    start = parser.plenty_sync_order_start
    end = parser.plenty_sync_order_end

    if start:
        try:
            start_date = date_parser.parse(start)
        except ParserError as err:
            raise InvalidDate(date_type='start',
                              date_str=start) from err
    if end:
        try:
            end_date = date_parser.parse(end)
        except ParserError as err:
            raise InvalidDate(date_type='end', date_str=parser.end) from err

    if start and end:
        return (start_date.strftime(date_format),
                end_date.strftime(date_format))

    now = datetime.datetime.now()
    now_str = now.strftime(date_format)
    if start and not end:
        return (start_date.strftime(date_format), now_str)

    if not start and end:
        start_date = end_date - datetime.timedelta(days=1)
        return (start_date.strftime(date_format),
                end_date.strftime(date_format))

    start_date = now - datetime.timedelta(hours=12)
    return (start_date.strftime(date_format), now_str)


def get_optional_columns(args: argparse.Namespace) -> tuple:
    if args.plenty_sync_ident_column and args.plenty_sync_qty_column:
        ident_column = {'name': args.plenty_sync_ident_column,
                        'type': args.plenty_sync_ident_type}
        qty_column = args.plenty_sync_qty_column
    else:
        ident_column = None
        qty_column = ''
    return (ident_column, qty_column)


def get_input_file(args: argparse.Namespace) -> tuple:
    if args.plenty_sync_excel:
        path = args.plenty_sync_excel
        sheet = args.plenty_sync_excel_sheet
    else:
        path = args.plenty_sync_import_file
        sheet = ''
    return (path, sheet)


class PlentySyncCommandLine(CommandLineSetup):
    @staticmethod
    def setup_parser(subparser):
        parser = subparser.add_parser(
            'PlentySync',
            help='Update the database with data from the Plentymarkets REST API'
            'and upload redistributions and reorders'
        )
        subparsers = parser.add_subparsers(
            title='Options for plenty_sync', dest='plenty_sync_command')
        setup_export_parsers(subparser=subparsers)
        setup_import_parsers(subparser=subparsers)
        setup_cleanup_parsers(subparser=subparsers)

        parser.add_argument(
            '--create_tables', required=False,
            help='Create the initial database tables skip creation if exists',
            action='store_true', dest='plenty_sync_create_tables')

    @staticmethod
    def validate_parser(args: argparse.Namespace) -> bool:
        if args.plenty_sync_command == 'Import':
            import_file = args.plenty_sync_import_file
            excel = args.plenty_sync_excel
            if not import_file and not excel:
                logger.error('No file location for template given')
                return False
            if import_file and not import_file.exists():
                logger.error(f'No file found at {import_file}')
                return False
            if excel and not excel.exists():
                logger.error(f'No file found at {excel}')
                return False
            if excel and not args.excel_sheet:
                logger.error('No sheet name given for the spreadsheet at '
                             f'{excel}')
                return False
            # Either both or neither must be given
            if (
                bool(args.plenty_sync_ident_column) ^
                bool(args.plenty_sync_ident_type)
            ):
                logger.error('Identifier column option requires the '
                             'identifier type option.')
                return False
        return True

    @staticmethod
    def handle_command(args: argparse.Namespace,
                       connection_string: str) -> bool:
        if args.plenty_sync_command == 'Export':
            modes = [args.plenty_sync_export_command.lower()]
        else:
            modes = []
        interface = service.Interface(connection_string=connection_string,
                                      modes=modes)
        if args.plenty_sync_create_tables:
            tables = ['OrdersModel', 'OrderItemsModel', 'StockModel',
                      'IdentificationModel', 'VariationInformationModel']
            interface.database.create_tables(tables=tables)

        if args.plenty_sync_command == 'Clean':
            if args.clean_variations:
                interface.database.cleanup_variation_ids()

        if args.plenty_sync_command == 'Export':
            function_map = {
                'Identity': interface.identity_update,
                'Order': partial(interface.order_update,
                                 partial(get_time_range, args)),
                'Stock': interface.stock_update,
                'Producer': interface.producer_update,
                'Additional': interface.additional_update,
                'Incoming': interface.incoming_update
            }
            if args.plenty_sync_export_command == 'All':
                for function in function_map:
                    if not function():
                        return False
            else:
                if not function_map[args.plenty_sync_export_command]():
                    return False
        elif args.plenty_sync_command == 'Import':
            (ident_column, qty_column) = get_optional_columns(args=args)
            (path, sheet) = get_input_file(args=args)
            function_map = {
                'Redistribution': partial(interface.redistribution_creation,
                                          path, args.redistribution_source,
                                          args.redistribution_target, sheet,
                                          args.transit_location,
                                          args.redistribution_booking,
                                          ident_column, qty_column),
                'Reorder': partial(interface.reorder_creation,
                                   path, args.reorder_source,
                                   args.reorder_target,
                                   ident_column, qty_column, sheet)
            }
            if not function_map[args.plenty_sync_import_command]():
                return False
        return True
