"""
Synchronize Plentymarkets orders, stock etc. with the MariaDB database.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from collections import defaultdict
import datetime
from decimal import Decimal
import functools
import sys
import pathlib
import numpy
import pandas
import plenty_api
from loguru import logger
from sqlalchemy.orm.session import Session
from sqlalchemy import null
from sqlalchemy.sql import case

from plenty_rait.plenty_sync.utils import PlentySyncConfigHandler
from plenty_rait.plenty_sync.config_schema import (
    plenty_sync_configuration_schema
)
from plenty_rait.helper.config_helper import get_plenty_api_base_url
from plenty_rait.helper.file_handling import get_encoding
from plenty_rait.helper.database_schema import (
    BaseDatabaseService, OrdersModel, OrderItemsModel, StockModel,
    IdentificationModel, VariationInformationModel, ProducerModel,
    IncomingVariationModel
)


###################################################################
# Database input and output services
###################################################################


def transform_to_dateobject(date_str: str) -> datetime.date:
    """
    Get a date object from a given W3C date string.

    Parameter:
        date_str        [str]   -   Date as string from `pull_purchase_orders`

    Return:
              [datetime.date]
    """
    if not date_str or len(date_str) != 25:
        return None

    return datetime.date.fromisoformat(date_str[:-15])


class DatabaseService(BaseDatabaseService):
    """
    Responsible for handling in and output with the database instance.
    (Optimized for MariaDB)
    """
    def __init__(self, session: Session = Session()):
        self.session: Session
        super().__init__(session=session)

    def cleanup_variation_ids(self) -> None:
        """
        Search and replace old variation IDs with their new counterparts.

        Search for variation IDs that were removed from the identification
        table (removed on Plentymarkets) and replace them with the current
        matching variation ID for the EAN.
        """
        variations = dict(
            self.session.query(
                IdentificationModel.ean, IdentificationModel.variation_id
            )
        )
        for model in [OrderItemsModel, IncomingVariationModel,
                      VariationInformationModel]:
            self.session.query(
                model
            ).filter(
                model.variation_id != -1,
                model.variation_id.notin_(variations.values()),
                model.ean.in_(variations)
            ).update(
                {'variation_id': case(variations, value=model.ean)},
                synchronize_session=False
            )
            # TODO decide if variations deleted from Plentymarkets should be
            # deleted from all tables.
            # self.session.query(
            #     model
            # ).filter(
            #     model.ean.notin_(variations)
            # ).delete(synchronize_session='fetch')
        self.session.commit()


    def __update_non_order_table(self, dataframe: pandas.DataFrame,
                                 model) -> None:
        """
        Synchronize a database table with a given pandas DataFrame.

        The export stems from Plentymarkets, delete rows that were deleted on
        Plentymarkets and add rows that are found in Plentymarkets but not
        in the database.

        This method should only be used for tables where an EAN is found at
        most once.

        Parameters:
            dataframe   [DataFrame] -   Export from the Plentymarkets REST API
            model       [Declarative
                         Meta]      -   Class identifier for the database
                                        schema
        """
        db_eans = []
        # Delete all variations removed from Plentymarkets
        self.session.query(model).filter(
            model.ean.notin_(dataframe['ean'])
        ).delete(synchronize_session='fetch')

        # Update existing rows
        for db_row in self.session.query(model).all():
            db_row.update_by_dataframe(dataframe=dataframe)
            db_eans.append(db_row.ean)

        # Add rows that were newly created on Plentymarkets
        new_rows = dataframe[~dataframe['ean'].isin(db_eans)]
        if len(new_rows.index) > 0:
            self.session.add_all(
                [model(**x) for x in new_rows.to_dict(orient='records')]
            )
        self.session.commit()

    def add_stock(self, dataframe: pandas.DataFrame) -> None:
        """
        Update the stock information for each product on the database.

        The export dictates how the database is filled, therefore all
        entries missing in the export are removed from the database.

        Parameters:
            dataframe   [DataFrame] -   Generated dataframe from
                                        `pull_stock_data`
        """
        self.__update_non_order_table(dataframe=dataframe, model=StockModel)

    def sync_identification_data(self, dataframe: pandas.DataFrame) -> None:
        """
        Update the identification entries for each product on the database.

        The export dictates how the database is filled, therefore all
        entries missing in the export are removed from the database.

        Parameters:
            dataframe   [DataFrame] -   Generated dataframe from
                                        `pull_identification_data`
        """
        self.__update_non_order_table(dataframe=dataframe,
                                      model=IdentificationModel)

    def handle_order(self, dataframe):
        existing_orders = [x for x, in self.session.query(OrdersModel.id)]
        new_entries = dataframe[~dataframe['order_id'].isin(existing_orders)]
        if len(new_entries.index) == 0:
            return
        new_entries = self.__append_ean_barcode(dataframe=new_entries)
        if new_entries['ean'].isna().any():
            # pylint: disable=invalid-string-quote
            logger.warning("Variation ID of the following orders not found in "
                           "the identification table:\n"
                           f"{new_entries[new_entries['ean'].isna()]}\n"
                           "Skipping order items..")
            new_entries.dropna(subset=['ean'], inplace=True)

        for order in new_entries['order_id'].unique():
            order_match_rows = new_entries[new_entries['order_id'] == order]
            assert len(order_match_rows['origin_id'].unique()) == 1
            assert len(order_match_rows['external_id'].unique()) == 1
            (total_net, total_gross) = [
                sum([
                    price * qty for price, qty in zip(
                        order_match_rows[type], order_match_rows['quantity'])
                ]) for type in ['net_price', 'gross_price']
            ]
            order_items = [
                OrderItemsModel(
                    id=v.order_item_id,
                    external_id=v.external_id, origin_id=v.origin_id,
                    variation_id=v.variation_id, ean=v.ean, item_id=v.item_id,
                    name=v.name, quantity=v.quantity, netto=v.net_price,
                    gross=v.gross_price
                ) for v in order_match_rows.itertuples()
            ]
            # Take the first entry that matches the order id, as the attributes
            # required for the order creation are the same for each entry.
            entry = order_match_rows.iloc[0]
            order = OrdersModel(
                id=entry.order_id,
                origin_id=entry.origin_id, netto_total=total_net,
                gross_total=total_gross,
                creation_date=transform_to_dateobject(entry.creation_date),
                payment_date=transform_to_dateobject(entry.payment_date),
                delivery_date=transform_to_dateobject(entry.delivery_date),
                b2b=entry.b2b,
                external_id=entry.external_id,
                order_items=order_items
            )
            self.session.add(order)

        self.session.commit()

    # pylint: disable=dangerous-default-value
    def get_id_map(self, id_type: str,
                   additional_map: list = ['name']) -> dict:
        """
        Get a dictionary export of the identification table for all variations.

        The variation ID is required to work with the Plentymarkets API, fetch
        it from the database for any identifier that is not a variation ID.

        Parameters:
            id_type             [str]       -   Name of the identifier
                                                [ean, asin, sku, variation_id]
            additional_map      [list]      -   Map more elements from the
                                                identification table to
                                                resulting dictionary

        Return:
                                [dict]      -   Mapping of identifier to the
                                                variation name and if the
                                                identifier isn't a variation ID
                                                to the matching variation ID
        """
        # TODO Make this configurable over the configuration (needs to be
        # aligned with the other sub-projects of Plenty RAIT)
        db_map = {'variation_id': 'variation_id',
                  'sku': 'sku',
                  'asin': 'asin_all',
                  'ean': 'ean',
                  'name': 'name'}

        db_column = db_map[id_type]
        # Convert additional fields to the expected column names
        additional_map = [db_map[x] for x in additional_map]
        table_column = getattr(IdentificationModel, db_column)
        columns = [table_column]
        dict_keys = []
        if id_type != 'variation_id':
            columns += [IdentificationModel.variation_id]
            dict_keys += ['variation_id']
        for field in additional_map:
            columns += [getattr(IdentificationModel, field)]
            dict_keys += [field]

        return {
            str(getattr(x, db_column)):
            {key: str(getattr(x, key)) for key in dict_keys}
            for x in self.session.query(*columns)
        }

    def sync_variation_information_map(self, dataframe) -> None:
        """
        Write the variation attributes for all variation to the database.

        Parameters:
            dataframe       [DataFrame] -   Generated dataframe from
                                            `pull_variation_information`
        """
        dataframe['creation_date'] = dataframe['creation_date'].apply(
            transform_to_dateobject)
        self.__update_non_order_table(dataframe=dataframe,
                                      model=VariationInformationModel)

    def sync_producers(self, dataframe) -> None:
        """
        Insert or update producer contact information from Plentymarkets.

        Parameters:
            dataframe       [DataFrame] -   Generated dataframe from
                                            `pull_priority_tags`
        """
        db_ids = []
        # Delete all variations removed from Plentymarkets
        self.session.query(ProducerModel).filter(
            ProducerModel.plenty_id.notin_(dataframe['plenty_id'])
        ).delete(synchronize_session='fetch')

        # Update existing rows
        for db_row in self.session.query(ProducerModel).all():
            df_row = dataframe[dataframe['plenty_id'] == db_row.plenty_id]
            db_row.update_producer(
                producer_name=df_row.producer_name.item(),
                delivery_time=df_row.delivery_time.item()
            )
            db_ids.append(db_row.plenty_id)

        # Add rows that were newly created on Plentymarkets
        new_rows = dataframe[~dataframe['plenty_id'].isin(db_ids)]
        if len(new_rows.index) > 0:
            self.session.add_all(
                [
                    ProducerModel(**x)
                    for x in new_rows.to_dict(orient='records')
                ]
            )
        self.session.commit()

    def sync_incoming_variations(self, dataframe) -> None:
        # Map the EAN barcode to the variation ID
        if len(dataframe.index) > 0:
            dataframe = self.__append_ean_barcode(dataframe=dataframe)
            reordered_columns = list(dataframe.columns)
            reordered_columns.insert(1, reordered_columns.pop(-1))
            dataframe = dataframe.reindex(columns=reordered_columns)

        # Mark all variations where the reorder is finished but which have not
        # been marked yet
        self.session.query(IncomingVariationModel).filter(
            IncomingVariationModel.order_id.notin_(dataframe['order_id']),
            IncomingVariationModel.receive_date.is_(None)
        ).update(
            {IncomingVariationModel.receive_date: datetime.date.today()},
            synchronize_session='fetch'
        )

        if len(dataframe.index) == 0:
            self.session.commit()
            return

        existing_combinations = []
        # Update quantity and producer
        for db_row in self.session.query(IncomingVariationModel).all():
            db_row.update_by_dataframe(dataframe=dataframe)
            existing_combinations.append(
                f'{db_row.order_id}_{db_row.variation_id}')

        dataframe['match'] = dataframe.apply(
            # pylint: disable=invalid-string-quote
            lambda x: f"{x['order_id']}_{x['variation_id']}", axis=1)
        dataframe['order_date'] = dataframe['order_date'].apply(
            transform_to_dateobject)
        dataframe = dataframe[~dataframe['match'].isin(existing_combinations)]
        if len(dataframe.index) > 0:
            dataframe.drop('match', axis=1, inplace=True)
            dataframe.fillna(null(), inplace=True)
            self.session.add_all(
                [
                    IncomingVariationModel(**x)
                    for x in dataframe.to_dict(orient='records')
                ]
            )
        self.session.commit()

    def get_priority_map(self) -> dict:
        """
        Pull priorities for all variations from the database.

        The priority indicates the importance of a variation in comparision to
        other variations for purposes like determining the correct buffer
         within a warehouse.

        Return:
                                [dict]      -   Mapping of variation ID to
                                                priority integer
        """
        return {
            str(x.variation_id): x.priority for x in self.session.query(
                VariationInformationModel.variation_id,
                VariationInformationModel.priority
            )
        }

    def __append_ean_barcode(self,
                             dataframe: pandas.DataFrame) -> pandas.DataFrame:
        if 'variation_id' not in dataframe.columns:
            # pylint: disable=invalid-string-quote
            logger.warning("Unable to apply EAN to dataframe as no "
                           "'variation_id' column can be located")
            return dataframe
        ean_table = pandas.read_sql(
            self.session.query(IdentificationModel.variation_id,
                               IdentificationModel.ean).statement,
            self.session.bind,
            index_col='variation_id'
        )
        dataframe.index = dataframe['variation_id']
        dataframe = dataframe.assign(ean=ean_table['ean'])
        return dataframe

    def get_producer_map(self) -> dict:
        return {
            x.producer_name: x.plenty_id for x in self.session.query(
                ProducerModel.producer_name, ProducerModel.plenty_id
            )
        }


###################################################################
# Data collection services
###################################################################


ORDER_TYPE_SALES_ORDER = 1
ORDER_ITEM_TYPE_VARIATION = 1


def retrieve_value_by_id(json: dict, id_value: int, section: str,
                         match_field: str, target_field: str) -> str:
    """
    Retrieve a specific value matching to a ID from a JSON response.

    Parameters:
        json            [dict]  -   Response from Plentymarkets API call
        id_value        [int]   -   ID to match in the elements of the given
                                    section
        section         [str]   -   Section within the JSON to search at
        match_field     [str]   -   Field within the section where the
                                    value has to match the @id_value
        target_field    [str]   -   Section to take the return value from, if
                                    the id matches

    Return:
                        [str]   -   The found value represented as a string
    """
    if section not in json:
        raise KeyError(f'Invalid JSON section {section}')

    if not isinstance(id_value, int):
        try:
            id_value = int(id_value)
        except ValueError:
            logger.warning(f'Invalid ID value: {id_value} must be an integer.')
            return ''

    for element in json[section]:
        if element[match_field] == id_value:
            return str(element[target_field])

    return ''


def get_ean(json: dict, ean_id: int) -> str:
    """
    Retrieve the correct barcode from the JSON response

    Parameters:
        json            [dict]  -   Response from Plentymarkets API call
        ean_id          [int]   -   ID of the barcode assigned by
                                        Plentymarkets.
                                        (Lookup: Setup -> Article -> Barcodes)

    Return:
                        [str]   -   The EAN barcode as a string
    """
    return retrieve_value_by_id(json=json, id_value=ean_id,
                                section='variationBarcodes',
                                match_field='barcodeId',
                                target_field='code')


def get_asin(json: dict, asin_id: int) -> str:
    """
    Retrieve the correct ASIN code from the JSON response.

    Parameters:
        json            [dict]  -   Response from Plentymarkets API call
        asin_id         [int]   -   ID of the country connected to the ASIN

    Return:
                        [str]   -   The ASIN code as a string
    """
    return retrieve_value_by_id(json=json, id_value=asin_id,
                                section='marketItemNumbers',
                                match_field='countryId',
                                target_field='value')


def get_stock(variation: dict, warehouse_ids: list) -> list:
    """
    Retrieve the stock of a variation from a specific set of warehouses.

    Parameters:
        variation       [dict]  -   Part of the plentymarkets API response
        warehouse_ids   [list]  -   set of available Plentymarkets warehouse
                                    IDs from the warehouse configuration

    Return:
                        [list]  -   Stock values in the order of the warehouses
    """
    if not warehouse_ids or not variation:
        return []

    stock_map: dict = {}
    for stock in variation['stock']:
        stock_id = stock['warehouseId']
        if stock_id not in warehouse_ids:
            continue
        stock_map[stock_id] = stock['physicalStock']

    # Sort the values into the correct order
    return_values: list = []
    for warehouse_id in warehouse_ids:
        if warehouse_id not in stock_map:
            return_values.append(0)
            continue
        return_values.append(int(stock_map[warehouse_id]))

    return return_values


def get_date_by_type(json: dict, date_type: str) -> str:
    """
    Get a specific date from the list of dates associated with an order.

    Parameters:
        json            [dict]  -   Part of the plentymarkets API response
        date_type       [str]   -   date type name
                                    (see valid values below)

    Return:
                        [str]  -   date string in the W3C date format
    """
    valid_values = {
        'deleted': 1,
        'created': 2,
        'paid': 3,
        'last_update': 4,
        'completed': 5,
        'returned': 6,
        'payment_due_to': 7,
        'estimated_shipping': 8,
        'started': 9,
        'ended': 10,
        'estimated_delivery': 11,
        'purchased': 16,
        'finished': 17
    }
    if date_type not in valid_values:
        raise KeyError(f'Invalid date type {type}')
    return retrieve_value_by_id(json=json, id_value=valid_values[date_type],
                                section='dates', match_field='typeId',
                                target_field='date')


def get_property_by_type(json: dict, property_type: str) -> str:
    """
    Get a specific property value from the order properties by its type.

    Parameters:
        json            [dict]  -   Part of the plentymarkets API response
        property_type   [str]   -   property type name
                                    (see valid values below)

    Return:
                        [str]  -    external order number from the marketplace
    """
    valid_values = {
        'warehouse': 1,
        'shipping_profile': 2,
        'payment_method': 3,
        'payment_status': 4,
        'external_shipping_profile': 5,
        'document_language': 6,
        'external_order_id': 7,
        'customer_sign': 8,
        'dunning_level': 9,
        'seller_account': 10,
        'weight': 11,
        'width': 12,
        'length': 13,
        'height': 14,
        'flag': 15,
        'external_token_id': 16,
        'external_item_id': 17,
        'coupon_code': 18,
        'coupon_type': 19,
        'sales_tax_id_number': 34,
        'main_document_number': 33,
        'payment_transaction_id': 45,
        'external_tax_service': 47,
        'merchant_id': 60,
        'report_id': 61,
        'preferred_storage_location_id': 63,
        'amazon_shipping_label': 64,
        'ebay_plus': 994,
        'fulfillment_service': 995
    }
    if property_type not in valid_values:
        raise KeyError(f'Invalid property type {property_type}')
    return retrieve_value_by_id(json=json,
                                id_value=valid_values[property_type],
                                section='properties', match_field='typeId',
                                target_field='value')


def get_amount(json: dict, price_type: str) -> Decimal:
    """
    Get either the net or gross price from an order item.

    Parameters:
        json            [dict]  -   Order item within a order REST API response
        price_type      [str]   -   price type name
                                    (either 'net' or 'gross')

    Return:
                        [Decimal]-  Price in Euro with 2 decimal places
    """
    valid_values = {'net': 'priceNet', 'gross': 'priceGross'}
    if price_type not in valid_values:
        raise KeyError(f'Invalid price amount type {type}')

    for amount in json['amounts']:
        if amount['currency'] != 'EUR':
            continue
        return Decimal(
            amount[valid_values[price_type]]
        ).quantize(Decimal('0.01'))

    return Decimal(0)


def is_b2b_customer(json: dict, b2b_class_id: int) -> bool:
    """
    Check if the receiver contact is a buisness to business customer.

    Parameters:
        json            [dict]  -   Part of the plentymarkets API response
        b2b_class_id    [int]   -   User-configured ID of the customer class
                                    associated with b2b customers

    Return:
                        [bool]
    """
    try:
        if not json['contactReceiver']:
            logger.warning(
                f"Order {json['id']} is a guest order, assume B2C customer")
            return False
        return json['contactReceiver']['classId'] == b2b_class_id
    except TypeError:
        return False


def get_priority_level(json: dict, priority_config: dict) -> int:
    """
    Search for the highest available priority variation tag.

    If a product does not contain any of the priority tags, we assume the
    lowest priority.

    Parameters:
        json                [dict]      -   Part of the plentymarkets API
                                            response
        priority_config     [dict]      -   Configuration section that maps
                                            variation tag ID numbers to
                                            priority numbers

    Return:
                            [int]       -   Priority level of the variation
    """
    if 'tags' not in json.keys():
        logger.error('Missing tags section in the REST API response')
        return -1

    tag_ids = [int(tag['tagId']) for tag in json['tags']]
    sorted_config = dict(sorted(priority_config.items(), key=lambda x: x[1]))

    for tag_id, priority in sorted_config.items():
        if tag_id in tag_ids:
            return priority

    return list(sorted_config.values())[-1]


def get_producer_id(json: dict) -> int:
    """
    Get the ID of the producer of the variation if it is configured in
    Plentymarkets.

    Parameters:
        json                [dict]      -   Part of the plentymarkets API
                                            response
    Return:
                            [int]       -   ID of the producer contact
    """
    if 'variationSuppliers' not in json:
        logger.error('Missing variationSuppliers section in the REST API '
                     'response')
        return -1

    try:
        return json['variationSuppliers'][0]['supplierId']
    except IndexError:
        return -1


def get_excluded_variations(config):
    if 'excluded_variations' not in config:
        return []
    return config['excluded_variations']


class DataCollector:
    """ Collect data objects from the Plentymarkets REST API.  """
    def __init__(self, config, warehouse_handler, collect_options: dict):
        self.config = config
        self.warehouse_handler = warehouse_handler
        self.collect_options = collect_options
        self.variations: list = []
        self.excluded_variations = get_excluded_variations(config=config)

        self.api = plenty_api.PlentyApi(base_url=get_plenty_api_base_url())
        self.api.cli_progress_bar = True

    def pull_stock_data(self) -> pandas.DataFrame:
        """
        Get the stock data of the given warehouses for all variations.

        Return:
                        [DataFrame]
        """
        if not self.variations:
            self.variations = self.api.plenty_api_get_variations(
                additional=self.collect_options['variation']
            )
        database_column_names = self.warehouse_handler.get(
            key='database_column_name', warehouse_type='plentymarkets'
        )
        columns = ['variation_id', 'ean'] + database_column_names

        dataframe_data = []
        ean_id = self.config['ean_barcode_id']
        warehouse_ids = self.warehouse_handler.get(
            key='plenty_warehouse_id', warehouse_type='plentymarkets'
        )
        for variation in self.variations:
            if variation['id'] in self.excluded_variations:
                continue
            ean = get_ean(json=variation, ean_id=ean_id)
            if not ean or ean == '0':
                continue
            row = [variation['id'], ean]
            row += get_stock(
                variation=variation, warehouse_ids=warehouse_ids
            )
            dataframe_data.append(row)

        dataframe = pandas.DataFrame(dataframe_data, columns=columns)
        return dataframe.fillna(0)

    def pull_identification_data(self) -> pandas.DataFrame:
        """
        Get identification data (barcodes, skus, name) for all variations.

        Both parameters are supplied by the user via configuration.

        Return:
                        [DataFrame]
        """
        columns = ['variation_id', 'ean', 'sku', 'asin_all', 'name']
        if not self.variations:
            self.variations = self.api.plenty_api_get_variations(
                additional=self.collect_options['variation']
            )
        dataframe_data = []
        ean_id = self.config['ean_barcode_id']
        asin_id = self.config['asin_id']
        for variation in self.variations:
            if variation['id'] in self.excluded_variations:
                continue
            ean = get_ean(json=variation, ean_id=ean_id)
            if not ean or ean == '0':
                continue
            row = [
                variation['id'], get_ean(json=variation, ean_id=ean_id),
                variation['number'], get_asin(json=variation, asin_id=asin_id),
                variation['name']
            ]
            dataframe_data.append(row)
        return pandas.DataFrame(dataframe_data, columns=columns)

    def pull_purchase_orders(self, date_range: tuple) -> pandas.DataFrame:
        """
        Prepare a dataframe with a row for each order item for orders in range.

        Use only Euro currency prices.

        You can retrieve the customer classes on your system via the
        `/rest/accounts/contacts/classes` route

        Parameters:
            date_range  [tuple]     -   start and end dates for order fetch
                                        supplied by the other via CLI arguments
            b2b_class_id[int]       -   Id of the the customer class associated
                                        with business to business customers,
                                        supplied by the user via configuration

        Return:
                        [DataFrame]
        """
        columns = [
            'order_id', 'order_item_id', 'variation_id', 'item_id',
            'external_id', 'name', 'origin_id', 'net_price', 'gross_price',
            'quantity', 'creation_date', 'payment_date', 'delivery_date', 'b2b'
        ]
        if len(date_range) != 2:
            logger.error('Invalid date range: {date_range}')
            return pandas.DataFrame()

        (start, end) = date_range
        b2b_class_id = self.config['b2b_customer_id']
        # Pull all orders from 7.0 to 8.0 (inclusive), exclude 8.0 manually
        # below.
        orders = self.api.plenty_api_get_orders_by_date(
            start=start, end=end, date_type='creation',
            additional=self.collect_options['order'],
            refine={'orderType': ORDER_TYPE_SALES_ORDER,
                    'statusFrom': 7, 'statusTo': 8})
        if orders is None:
            return pandas.DataFrame()

        dataframe_data = []
        for order in orders:
            external_id = get_property_by_type(
                json=order, property_type='external_order_id')
            payment_date = get_date_by_type(json=order, date_type='paid')
            delivery_date = get_date_by_type(json=order, date_type='completed')
            b2b = is_b2b_customer(json=order, b2b_class_id=b2b_class_id)

            # Status 8 means cancelation, so these should be excluded, but the
            # Plentymarkets REST API uses '<=' for the 'statusTo' parameter,
            # while we need '<'.
            if order['statusId'] == 8:
                continue
            for item in order['orderItems']:
                if item['typeId'] != ORDER_ITEM_TYPE_VARIATION:
                    continue
                variation_id = item['itemVariationId']
                if variation_id in self.excluded_variations:
                    continue
                try:
                    variation_name = item['variation']['name']
                    item_id = item['variation']['itemId']
                except TypeError:
                    # pylint: disable=invalid-string-quote
                    logger.warning(f"Invalid order item {item['id']} in order "
                                   f"{order['id']} with variation ID: "
                                   f"{item['itemVariationId']}")
                    continue
                item_net = get_amount(json=item, price_type='net')
                item_gross = get_amount(json=item, price_type='gross')
                dataframe_data.append([
                    order['id'], item['id'], variation_id, item_id,
                    external_id, variation_name, order['referrerId'],
                    item_net, item_gross, item['quantity'], order['createdAt'],
                    payment_date, delivery_date, b2b
                ])

        return pandas.DataFrame(dataframe_data, columns=columns)

    def pull_storage_locations(self, warehouse_id: int,
                               variations: list = None) -> dict:
        """
        Get all locations from a warehouse for each variation.

        Parameters:
            warehouse_id    [int]       -   Plentymarkets IDs of the warehouse
        Optional:
            variations      [list]      -   A list of strings containing
                                            Plentymarkets variations IDs
                                            restricting the locations

        Return:
                            [dict]      -   Mapping of variations ID to all
                                            locations for the given warehouse
        """
        locations = {}
        response = self.api.plenty_api_get_storagelocations(
            warehouse_id=warehouse_id)
        if not response:
            logger.warning('No locations found @ warehouse {warehouse_id}')
            return {}

        for entry in response:
            variation = str(entry['variationId'])
            if variation in self.excluded_variations:
                continue
            if variations and variation not in variations:
                continue
            if int(entry['quantity']) <= 0:
                continue
            loc_id = str(entry['storageLocationId'])
            if variation not in locations:
                locations.update(
                    {variation: {loc_id: entry['quantity']}})
            else:
                locations[variation].update(
                    {loc_id: entry['quantity']})

        return locations

    def pull_variation_information(self,
                                   priority_config: dict) -> pandas.DataFrame:
        """
        Get the priorities, producer and release date for each variation from
        the Plentymarkets REST API.

        The priorities are set via a variation tag in Plentymarkets, the
        tag-id of each priority is mapped to an integer within the
        configuration, a priority of 1 depicts the highest possible priority.

        priority_config     [dict]  -   Configuration section that maps
                                        variation tag ID numbers to priority
                                        numbers

        Return:
                        [DataFrame]
        """
        if not self.variations:
            self.variations = self.api.plenty_api_get_variations(
                additional=self.collect_options['variation']
            )

        dataframe_data = []
        ean_id = self.config['ean_barcode_id']
        for variation in self.variations:
            if variation['id'] in self.excluded_variations:
                continue
            ean = get_ean(json=variation, ean_id=ean_id)
            if not ean or ean == '0':
                continue
            priority_level = get_priority_level(
                json=variation, priority_config=priority_config)
            if priority_level < 0:
                return pandas.DataFrame()
            producer_id = get_producer_id(json=variation)
            if producer_id == -1:
                producer_id = null()
            dataframe_data.append([variation['id'], ean, priority_level,
                                   producer_id, variation['releasedAt']])

        return pandas.DataFrame(dataframe_data,
                                columns=['variation_id', 'ean', 'priority',
                                         'producer_id', 'creation_date'])

    def pull_producer_contacts(self) -> pandas.DataFrame:
        """
        Pull name and shipping time from the supplier contacts from
        Plentymarkets.

        Return:
                        [DataFrame]
        """
        # The ID for supplier contacts is 4 within Plentymarkets
        producers = self.api.plenty_api_get_contacts(refine={'typeId': 4})
        # Take 6 months as the fallback delivery duration
        default_delivery_time = 182
        if 'default_delivery_time' in self.config:
            default_delivery_time = self.config['default_delivery_time']

        producer_list = []
        for producer in producers:
            try:
                delivery_time = producer['accounts'][0]['deliveryTime']
            except IndexError:
                delivery_time = default_delivery_time
            if delivery_time == 0:
                delivery_time = default_delivery_time
            producer_list.append(
                [producer['id'], producer['fullName'], delivery_time]
            )

        return pandas.DataFrame(producer_list, columns=['plenty_id',
                                                        'producer_name',
                                                        'delivery_time'])

    def pull_incoming_variations(self):
        """
        Pull variations from pending incoming reorders.

        Return:
                        [DataFrame]
        """
        reorders = self.api.plenty_api_get_pending_reorder()
        if not reorders:
            logger.error('Pending incoming variation pull from REST API '
                         'failed')
            sys.exit(1)

        reorder_list = []
        for order in reorders:
            if 'exclude_reorder_status' in self.config:
                if order['statusId'] == self.config['exclude_reorder_status']:
                    # pylint: disable=invalid-string-quote
                    logger.info(f"Reorder {order['id']} has been excluded.")
                    continue
            try:
                producer_id = [
                    relation['referenceId']
                    for relation in order['relations']
                    if relation['relation'] == 'sender'
                ][0]
            except IndexError:
                # pylint: disable=invalid-string-quote
                logger.warning(f"Reorder {order['id']} doesn't contain a "
                               "sender contact. Skip.")
                continue

            # Order date type ID 2 == creation date
            for order_item in order['orderItems']:
                booked_quantity = sum(
                    [
                        transaction['quantity']
                        for transaction in order_item['transactions']
                        if transaction['receiptId']
                    ]
                )
                reorder_list.append(
                    [
                        order_item['itemVariationId'], order['id'],
                        order_item['quantity'] - booked_quantity,
                        order_item['createdAt'], producer_id
                    ]
                )

        return pandas.DataFrame(reorder_list, columns=['variation_id',
                                                       'order_id',
                                                       'quantity',
                                                       'order_date',
                                                       'producer_id'])


###################################################################
# Redistribution calculation
###################################################################


#pylint: disable=dangerous-default-value
def get_subset_sum_locations(locations: list, request: int, partial=[],
                             partial_sum=0) -> tuple:
    """
    Implementation of a subset sum algorithm as a generator, returns all
    location combinations where the quantity sum equals the request quantity.

    Based on algorithm by Manuel Salvadores
    https://stackoverflow.com/a/4633515

    Parameters:
        locations           [list]      -   List of tuples containing the stock
                                            location ID as first element and
                                            the stock quantity as second
        requests            [int]       -   desired shipment amount
    Internal:
        partial             [list]      -   Current combination of locations
                                            to check
        partial_sum         [int]       -   Current sum of the partial location
                                            list

    Return:
                            [tuple]     -   pair of stock location ID to
                                            quantity
    """
    if partial_sum == request:
        yield partial

    if partial_sum >= request:
        return

    for i, n in enumerate(locations):
        remaining = locations[i+1:]
        yield from get_subset_sum_locations(
            locations=remaining, request=request, partial=partial+[n],
            partial_sum=partial_sum+n[1])


def find_matching_location_combinations(locations: dict, request: int) -> dict:
    """
    Search for the smallest combination of locations that meet the request.

    Parameters:
        locations           [dict]      -   Stocklocation ID to stock amount
                                            mapping
        requests            [int]       -   desired shipment amount

    Return:
                            [dict]      -   Stocklocation ID to stock amount
                                            mapping and the sum of all
                                            those locations
    """
    if not locations or not request:
        return {}

    combinations = list(
        get_subset_sum_locations(locations=list(locations.items()),
                                 request=request))
    if not combinations:
        return {}
    return dict(min(combinations, key=len))


def distribute_request_on_locations(locations: dict, request: int) -> dict:
    """
    Walk through the locations in ascending order until the request is met.

    Parameters:
        locations           [dict]      -   Stocklocation ID to stock amount
                                            mapping
        requests            [int]       -   desired shipment amount

    Return:
                            [dict]      -   Stocklocation ID to stock amount
                                            mapping and the sum of all
                                            those locations
    """
    remaining_request = request
    dist_loc = {}

    sorted_locations = dict(sorted(locations.items(), key=lambda x: x[1]))
    for loc_id, loc_qty in sorted_locations.items():
        qty = min(remaining_request, loc_qty)
        dist_loc.update({loc_id: qty})
        remaining_request -= qty
        if remaining_request == 0:
            break

    return dist_loc


def find_locations_below_threshold(threshold: int, locations: dict,
                                   picked: dict) -> dict:
    """
    Search for locations were the stock is below or equal to a given threshold.

    Parameters:
        threshold           [int]       -   stock threshold
        locations           [dict]      -   Stocklocation ID to stock amount
                                            mapping
        picked              [dict]      -   Mapping of locations to the
                                            quantity already picked by the
                                            algorithm and the sum of those
                                            quantities

    Return:
                            [dict]      -   Stocklocation ID to stock amount
                                            mapping and the sum of all
                                            those locations
    """
    remaining = {loc: locations[loc] - picked['locations'][loc]
                 if loc in picked['locations'] else locations[loc]
                 for loc in locations}

    for loc, qty in remaining.items():
        if qty <= threshold:
            total_at_loc = locations[loc]
            try:
                picked_from_loc = picked['locations'][loc]
            except KeyError:
                picked_from_loc = 0
            picked['total_qty'] += (total_at_loc - picked_from_loc)
            picked['locations'][loc] = total_at_loc

    return picked


class RedistributionService:
    """
    Distribute a requested amount for each variation upon the available
    locations, focus on touching as few locations as possible while emptying
    locations if possible.
    """
    def __init__(self, source_warehouse_config: dict):
        try:
            self.attributes = source_warehouse_config['attributes']
        except KeyError:
            self.attributes = {}

    def find_best_fit_locations(self, locations: dict, requests: dict,
                                source_id: int,
                                general_buffer: int = 0) -> dict:
        """
        Determine an optimal set of locations that meet the given requirements.

        Parameters:
            locations       [dict]      -   source warehouse storage locations
                                            for each variation
            requests        [dict]      -   Mapping of desired shipment amount
                                            from the source warehouse to the
                                            target warehouse to variation IDs
            source_id       [int]       -   ID of the source warehouse
        Optional:
            general_buffer  [int]       -   Configuration option describing
                                            the amount of inventory that can
                                            not be taken from the source
                                            warehouse

        Return:
                            [dict]      -   Mapping of variation IDs to a
                                            dictionary of locations with the
                                            calculated amount that tries to
                                            meet the requirements
        """
        best_fit_locations = {}
        for variation, request in requests.items():
            request_qty = request['quantity']
            exact_match = {}
            try:
                var_loc = locations[variation]
            except KeyError:
                logger.warning(f'Cannot redistribute variation ID {variation} '
                               f'no locations found in warehouse {source_id}')
                continue

            if 'variation_buffer_map' in self.attributes:
                source_buffer = self.attributes['variation_buffer_map']
                source_buffer = max(
                    int(source_buffer[variation]), int(general_buffer)
                )
            else:
                source_buffer = int(general_buffer)

            # Use the unreserved stock in case of insufficiant inventory
            request = min(sum(var_loc.values()) - source_buffer, request_qty)

            exact_match = find_matching_location_combinations(
                locations=var_loc, request=request)
            if exact_match:
                best_fit_locations[variation] = {'total_qty': request,
                                                 'locations': exact_match}
            else:
                if request > 0:
                    dist_loc = distribute_request_on_locations(
                        locations=var_loc, request=request)

                    best_fit_locations[variation] = {
                        'total_qty': sum(dist_loc.values()),
                        'locations': dist_loc}
                else:
                    continue

            # Empty a location automatically when the stock at the location is
            # below a certain threshold. Do this only if the variation is not
            # excluded from the action.
            if (
                'empty_threshold' in self.attributes and
                (
                    (
                        'empty_exclusions' in self.attributes
                        and int(variation) not in
                        self.attributes['empty_exclusions']
                    )
                    or 'empty_exclusions' not in self.attributes
                )
            ):
                best_fit_locations[variation] = find_locations_below_threshold(
                    threshold=self.attributes['empty_threshold'],
                    locations=var_loc,
                    picked=best_fit_locations[variation]
                )

        return best_fit_locations


class PlentyImport:
    """ Import local data objects to Plentymarkets via the REST API """
    def __init__(self, collector: DataCollector, database: DatabaseService,
                 ident_column: dict = None, qty_column: str = ''):
        """
            collector           [object]    -   DataCollector instance for
                                                pulling data from Plentymarkets
            database            [object]    -   DatabaseService instance for
                                                reading and writing to the
                                                database
        OPTIONAL:
            ident_column        [dict]      -   Name and type of the identifier
                                                column
            qty_column          [str]       -   Name of the quantity column
        """
        self.collector = collector
        self.database = database
        self.config = collector.config
        self.warehouse_handler = collector.warehouse_handler
        self.file_type = ''
        self.ident_column = ident_column
        self.qty_column = qty_column

    def __get_file_columns(self) -> dict:
        """
        Choose the column names for the template file with the highest priority

        Optional arguments > Configuration arguments > Default arguments

        Return:
                                [dict]       -  identifier column and quantity
                                                column mapping
        """
        assert self.file_type.lower() in ['redistribution', 'reorder']
        default = {'ident_column': 'ean', 'ident_type': 'ean',
                   'qty_column': 'qty'}
        if self.ident_column and self.qty_column:
            return {'ident_column': self.ident_column['name'],
                    'ident_type': self.ident_column['type'],
                    'qty_column': self.qty_column}

        # The configuration schema gurantees that if one is present all are
        # present
        if self.file_type + '_id_column' in self.config:
            return {
                'ident_column': self.config[self.file_type + '_id_column'],
                'ident_type': self.config[self.file_type + '_id_type'],
                'qty_column': self.config[self.file_type + '_qty_column']
            }
        logger.info('Use default columns (ean, qty)')
        return default

    @staticmethod
    def __read_import_request(path: pathlib.Path, columns: dict, id_map: dict,
                              sheet: str = '',
                              extra_columns: list = None) -> dict:
        """
        Read out the amount to transfer with a Plentymarkets import.

        The file must contain either an EAN barcode, ASIN barcode, SKU or
        Plentymarkets variation ID. The identifier is detected through the
        columns dictionary provided by `get_file_columns`.

        Parameters:
            columns             [dict]      -   Map of the column names for the
                                                identifier and quantity
            id_map              [dict]      -   Mapping of the identifier used
                                                within the csv file to
                                                identification data from the
                                                database
            extra_columns       [list]      -   Additional columns to fetch
                                                from the import file

        Return:
                                [dict]      -   Mapping of variation_id to
                                                the transfer amount
        """
        ident = columns['ident_column']
        qty = columns['qty_column']
        if extra_columns:
            required_columns = [ident, qty] + extra_columns
        else:
            required_columns = [ident, qty]
        if sheet:
            try:
                dataframe = pandas.read_excel(
                    path, dtype=str, sheet_name=sheet
                ).dropna(subset=[ident, qty])
            except FileNotFoundError:
                logger.error(f'Sheet {sheet} does not exist at path {path}.')
                return {}
            except KeyError:
                logger.error(
                    f'Both columns {ident} and {qty} must be present on the '
                    f'sheet {sheet} within the file at {path}.'
                )
                return {}
        else:
            encoding = get_encoding(path=path, logger=logger)
            dataframe = pandas.read_csv(path, sep=';', dtype=str,
                                        encoding=encoding)

        if any(x not in dataframe.columns for x in required_columns):
            logger.error('Header of the import template file does not match '
                         f'the required column names [{required_columns}]')
            return {}

        try:
            dataframe = dataframe.astype({qty: int})
        except ValueError:
            logger.error(f'The {qty} column of import template file contains '
                         'non numeric values')
            return {}

        if columns['ident_type'] != 'variation_id':
            required_columns.remove(ident)
            required_columns.insert(0, 'variation_id')
            dataframe_with_map = dataframe[
                dataframe[ident].isin(id_map.keys())
            ]
            if len(dataframe.index) != len(dataframe_with_map.index):
                missing_ident = dataframe[
                    ~dataframe[ident].isin(id_map.keys())
                ][ident].to_list()
                logger.warning(
                    'No Identification entry found for the following '
                    f'Identifier:\n{missing_ident}'
                )
            dataframe['variation_id'] = dataframe[ident].apply(
                lambda x: id_map[x]['variation_id'])
            dataframe = dataframe[required_columns].reset_index(drop=True)
        else:
            dataframe = dataframe[required_columns].reset_index(drop=True)
            dataframe.rename(columns={ident: 'variation_id'}, inplace=True)
        column_map = {qty: 'quantity'}
        if extra_columns:
            column_map.update({x: x for x in extra_columns})
        return {
            data.pop('variation_id'): {
                column_map[key]: value for key, value in data.items()
            } for data in dataframe.to_dict(orient='records')
        }

    def _get_data(self, path: pathlib.Path, sheet: str = '',
                  extra_columns: list = None):
        """
        Fetch the data from the file and prepare the mapping table.

        Returns:
            requests        [dict]      -   Mapping of variation ID to quantity
            name_map        [dict]      -   Mapping of variation ID to name
        """
        columns = self.__get_file_columns()
        id_map = self.database.get_id_map(id_type=columns['ident_type'])

        requests = self.__read_import_request(path=path, columns=columns,
                                              id_map=id_map, sheet=sheet,
                                              extra_columns=extra_columns)

        if 'variation_id' in list(id_map.values())[0].keys():
            name_map = {str(pair['variation_id']): pair['name']
                        for pair in id_map.values()}
        else:
            name_map = {key: id_map[key]['name'] for key in id_map}

        return (requests, name_map)


class ImportRedistribution(PlentyImport):
    def __init__(self, collector: DataCollector, database: DatabaseService,
                 ident_column: dict = None, qty_column: str = ''):
        super().__init__(collector=collector, database=database,
                         ident_column=ident_column, qty_column=qty_column)
        self.file_type = 'redistribution'

    def create_redistribution(
        self, path: pathlib.Path, source_id: int, target_id: int,
        sheet: str = '', use_transit_location: bool = False,
        booking: bool = False
    ) -> bool:
        """
        Calculate and import a redistribution to Plentymarkets.

        The quantities for the variations are pulled from a csv and an
        algorithm calculates the optimal set of storage locations to fulfill
        the requests.

        Return:
                                [bool]
        """
        warehouse_ids = self.warehouse_handler.get(
            key='plenty_warehouse_id'
        )
        if any(x not in warehouse_ids for x in [source_id, target_id]):
            logger.error('Both source and target warehouses require a '
                         'configuration section')
            return False

        (requests, name_map) = self._get_data(path=path, sheet=sheet)
        if not requests:
            logger.error('Empty/Invalid redistribution template file')
            return False

        locations = self.collector.pull_storage_locations(
            warehouse_id=source_id)
        if not locations:
            logger.error('Fetching storage locations from Plentymartkets '
                         'failed.')
            return False

        general_buffer = 0
        if 'general_buffer' in self.config:
            general_buffer = self.config['general_buffer']

        source_warehouse_config = self.warehouse_handler.get_with_id(
            warehouse_id=source_id
        )
        redistribution = RedistributionService(
            source_warehouse_config=source_warehouse_config)
        best_fit = redistribution.find_best_fit_locations(
            locations=locations, requests=requests,
            source_id=source_id, general_buffer=general_buffer)

        if not best_fit:
            logger.warning('No locations found that match the requests')
            return True

        target_warehouse_config = self.warehouse_handler.get_with_id(
            warehouse_id=target_id
        )
        if use_transit_location:
            # pylint: disable=invalid-string-quote
            try:
                transit_location = target_warehouse_config['attributes'][
                    'transit_location']
                logger.info("Creating incoming transactions for warehouse "
                            f"{target_id} to location {transit_location}.")
                target_location = transit_location
            except KeyError:
                logger.info('No transit location configured for warehouse '
                            f'{target_id}.')
                target_location = -1
        else:
            target_location = -1

        if not self.__request_redistribution(
            locations=best_fit, source_id=source_id,
            target_id=target_id, name_map=name_map,
            target_location=target_location,
            book_out=booking
        ):
            logger.error('REST API redistribution creation request failed')
            return False
        return True

    def __request_redistribution(self, locations: dict, source_id: int,
                                 target_id: int, name_map: dict,
                                 target_location: int = -1,
                                 book_out: bool = False) -> bool:
        """
        Build & provide the required template data structure for the REST API.

        Parameters:
            locations       [dict]      -   Dictionary of each variation mapped
                                            to the total quantity and the
                                            quantity at each location
            source_id       [int]       -   Plentymarkets ID of the warehouse,
                                            where the inventory is taken from
            target_id       [int]       -   Plentymarkets ID of the warehouse,
                                            where the inventory is placed
            name_map        [dict]      -   Mapping of variation IDs to names
        OPTIONAL:
            target_location [int]       -   ID of the location where the
                                            inventory should be booked in
                                            temporarily (transit location)
            book_out        [bool]      -   Decide whether to perform bookings
                                            of outgoing and incoming
                                            transactions automatically
        Return:
                            [bool]      -   Signal if the REST API request was
                                            successful
        """
        template = {
            'plenty_id': self.config['plenty_id'],
            'sender': source_id,
            'receiver': target_id,
            'variations': [
                {
                    'variation_id': int(variation),
                    'total_quantity': locations[variation]['total_qty'],
                    'name': name_map[variation],
                    'locations': [
                        {'location_id': int(loc_id), 'quantity': int(loc_qty)}
                        if target_location == -1 else
                        {'location_id': int(loc_id), 'quantity': int(loc_qty),
                         'targets': [
                             {'location_id': target_location,
                              'quantity': int(loc_qty)}
                         ]}
                        for loc_id, loc_qty in
                        locations[variation]['locations'].items()
                    ]
                } for variation in locations
            ]
        }

        response = self.collector.api.plenty_api_create_redistribution(
            template=template, book_out=book_out)
        if 'error' in response.keys():
            # pylint: disable=invalid-string-quote
            logger.error("Redistribution creaton failed with "
                         f"{response['error']}")
            return False

        return True


class ImportReorder(PlentyImport):
    def __init__(self, collector: DataCollector, database: DatabaseService,
                 ident_column: dict = None, qty_column: str = ''):
        super().__init__(collector=collector, database=database,
                         ident_column=ident_column, qty_column=qty_column)
        self.file_type = 'reorder'

    def create_reorder(self, path: pathlib.Path, target_id: int,
                       producer_column: str, sheet: str = '') -> bool:
        warehouse_ids = self.warehouse_handler.get(key='plenty_warehouse_id')
        if target_id not in warehouse_ids:
            logger.error('Target warehouse requires a configuration section')
            return False

        (requests, name_map) = self._get_data(
            path=path, sheet=sheet, extra_columns=[producer_column])

        producer_dict = defaultdict(dict)
        producer_map = self.database.get_producer_map()
        missing_producer_list = []
        for variation, values in requests.items():
            producer = values[producer_column]
            if not producer or (isinstance(producer, float) and numpy.isnan(producer)):
                missing_producer_list.append(variation)
                continue
            producer_id = producer_map[producer]
            producer_dict[producer_id].update({variation: values})

        if missing_producer_list:
            logger.warning(
                'The following variations do not have a mapped producer in '
                f"the '{producer_column}' column.\n{missing_producer_list}\n"
                'Skipping these variations.'
            )

        for producer_id, variations in producer_dict.items():
            if not self.__request_reorder(variations=variations,
                                          source_id=producer_id,
                                          target_id=target_id,
                                          name_map=name_map):
                logger.error('REST API reorder creation request failed')
                return False
        return True

    def __request_reorder(self, variations: list, source_id: int,
                          target_id: int, name_map: dict) -> bool:
        """
        Build & provide the required template data structure for the REST API.

        Parameters:
            variations      [list]      -   Plentymarkets Variation IDs mapped
                                            to a reorder quantity and optional
                                            additional attributes
            source_id       [int]       -   Plentymarkets ID of the contact,
                                            that is instructed to produce the
                                            products
            target_id       [int]       -   Plentymarkets ID of the warehouse,
                                            where the inventory is placed
            name_map        [dict]      -   Mapping of variation IDs to names

        Return:
                            [bool]      -   Signal if the REST API request was
                                            successful
        """
        template = {
            'plenty_id': self.config['plenty_id'],
            'sender': source_id,
            'receiver': target_id,
            'variations': [
                {
                    'variation_id': int(variation),
                    'total_quantity': variations[variation]['quantity'],
                    'name': name_map[variation]
                } for variation in variations
            ]
        }

        response = self.collector.api.plenty_api_create_reorder(
            template=template)
        if 'error' in response.keys():
            # pylint: disable=invalid-string-quote
            logger.error(f"Reorder creaton failed with {response['error']}")
            return False

        return True


class Interface:
    def __init__(self, connection_string: str, modes: list) -> None:
        self.config_handler = PlentySyncConfigHandler(
            name='plenty_sync', config_type='json',
            config_schema=plenty_sync_configuration_schema
        )
        self.database = DatabaseService()
        self.database.create_database_session(
            connection_string=connection_string)

        self.collector = DataCollector(
            config=self.config_handler.config,
            warehouse_handler=self.config_handler.warehouse_handler,
            collect_options=self.__get_collect_options(modes=modes))

    @staticmethod
    def __get_collect_options(modes: list) -> dict:
        """
        Set up the additional elements to be pulled from the REST API.

        In order to reduce API calls, get a response that is sufficient for as
        many modes(stock, identity, order) as possible. Reuse that response for
        all appropriate modes.

        Parameters:
            modes           [list]      -   List of modes to evalute the
                                            required elements of the response
                                            body

        Return:
                            [dict]      -   Mapping of REST API request route to
                                            additional element list
        """
        if not modes:
            return {}
        valid_modes = ['order', 'stock', 'identity', 'additional', 'incoming',
                       'all']
        invalid_modes = [mode for mode in modes if mode not in valid_modes]
        if invalid_modes:
            raise RuntimeError(f'Invalid modes: {invalid_modes}.')

        options = {'order': [], 'variation': ['variationBarcodes']}
        if any(mode in ['order', 'all'] for mode in modes):
            options['order'] += ['orderItems.variation', 'contactReceiver']
        if any(mode in ['stock', 'all'] for mode in modes):
            options['variation'] += ['stock']
        if any(mode in ['identity', 'all'] for mode in modes):
            options['variation'] += ['marketItemNumbers']
        if any(mode in ['additional', 'all'] for mode in modes):
            options['variation'] += ['tags', 'variationSuppliers']

        return options

    def identity_update(self) -> bool:
        dataframe = self.collector.pull_identification_data()
        if len(dataframe.index) == 0:
            logger.error(
                'Identification data pull from REST API failed.')
            return False
        self.database.sync_identification_data(dataframe=dataframe)
        return True

    def order_update(self, daterange: tuple) -> bool:
        if isinstance(daterange, functools.partial):
            daterange = daterange()
        dataframe = self.collector.pull_purchase_orders(
            date_range=daterange)
        if len(dataframe.index) == 0:
            logger.error('Order data pull from REST API failed.')
            return False
        self.database.handle_order(dataframe=dataframe)
        return True

    def stock_update(self) -> bool:
        dataframe = self.collector.pull_stock_data()
        if len(dataframe.index) == 0:
            logger.error('Stock data pull from REST API failed.')
            return False

        self.database.add_stock(dataframe=dataframe)
        return True

    def producer_update(self) -> bool:
        dataframe = self.collector.pull_producer_contacts()
        if len(dataframe.index) == 0:
            logger.error('Producer data pull from REST API failed.')
            return False
        self.database.sync_producers(dataframe=dataframe)
        return True

    def additional_update(self) -> bool:
        priority_config = self.config_handler.get_priority_config()
        if not priority_config:
            logger.error('Invalid priority configuration')
            return False
        dataframe = self.collector.pull_variation_information(
            priority_config=priority_config)
        if len(dataframe.index) == 0:
            logger.error('Additional data pull from REST API failed.')
            return False
        self.database.sync_variation_information_map(dataframe=dataframe)
        return True

    def incoming_update(self) -> bool:
        dataframe = self.collector.pull_incoming_variations()
        self.database.sync_incoming_variations(dataframe=dataframe)
        return True

    def redistribution_creation(
        self, path: pathlib.Path, source_id: int, target_id: int,
        sheet: str = '', use_transit_location: bool = False,
        booking: bool = False, ident_column: dict = None, qty_column: str = ''
    ) -> bool:
        priority_map = self.database.get_priority_map()
        if len(priority_map) > 0:
            self.config_handler.build_buffer_map(priority_map=priority_map)
        plenty_import = ImportRedistribution(collector=self.collector,
                                             database=self.database,
                                             ident_column=ident_column,
                                             qty_column=qty_column)
        if not plenty_import.create_redistribution(
            path=path, source_id=source_id, target_id=target_id,
            sheet=sheet, use_transit_location=use_transit_location,
            booking=booking,
        ):
            return False
        return True

    def reorder_creation(
        self, path: pathlib.Path, target_id: int, producer_column: str,
        ident_column: dict = None, qty_column: str = '', sheet: str = ''
    ) -> bool:
        plenty_import = ImportReorder(collector=self.collector,
                                      database=self.database,
                                      ident_column=ident_column,
                                      qty_column=qty_column)
        if not plenty_import.create_reorder(path=path, target_id=target_id,
                                            producer_column=producer_column,
                                            sheet=sheet):
            return False
        return True
