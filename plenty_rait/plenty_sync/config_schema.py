# pylint: disable=invalid-string-quote, line-too-long
plenty_sync_configuration_schema = {
    "type": "object",
    "title": "Plentymarkets Sync Configuration",
    "required": [
        "plenty_id",
        "ean_barcode_id",
        "asin_id",
        "b2b_customer_id"
    ],
    "dependentRequired": {
        "redistribution_id_column": ["redistribution_qty_column",
                                     "redistribution_id_type"],
        "reorder_id_column": ["reorder_qty_column", "reorder_id_type"],
        "redistribution_id_type": ["redistribution_qty_column",
                                   "redistribution_id_column"],
        "reorder_id_type": ["reorder_qty_column", "reorder_id_column"],
        "redistribution_qty_column": ["redistribution_id_column",
                                      "redistribution_id_type"],
        "reorder_qty_column": ["reorder_id_column", "reorder_id_type"]
    },
    "properties": {
        "ean_barcode_id": {
            "id": "ean_id",
            "type": "integer",
            "title": "Plentymarkets EAN barcode ID",
            "description": "ID of the barcode within Plentymarkets that is used as EAN barcode",
            "default": 1
        },
        "asin_id": {
            "id": "asin_id",
            "type": "integer",
            "title": "Plentymarkets ASIN barcode ID",
            "description": "ID of ASIN barcode used for matching variations",
            "default": 0
        },
        "b2b_customer_id": {
            "id": "b2b_id",
            "type": "integer",
            "title": "Plentymarkets B2B customer ID",
            "description": "ID of the customer class that is used for B2B",
            "default": 3
        },
        "general_buffer": {
            "id": "general_buffer",
            "type": "integer",
            "title": "General warehouse quantity buffer",
            "description": "Used for redistribution to determine a general minimum quantity to remain at the source warehouse",
            "default": 0
        },
        "plenty_id": {
            "id": "plenty_id",
            "type": "integer",
            "title": "Plentymarkets system ID",
            "description": "ID of the Plentymarkets system"
        },
        "redistribution_id_column": {
            "id": "redistribution_id_column",
            "type": "string",
            "title": "redistribution input file ID column name",
            "description": "The exact name of the column used for identification of each variation",
            "default": "variation_id"
        },
        "redistribution_id_type": {
            "id": "redistribution_id_type",
            "type": "string",
            "enum": ["variation_id", "ean", "sku", "asin"]
        },
        "redistribution_qty_column": {
            "id": "redistribution_qty_column",
            "type": "string",
            "title": "redistribution input file quantity column name",
            "description": "The exact name of the column used for the redistribution amount of each variation",
            "default": "quantity"
        },
        "reorder_id_column": {
            "id": "reorder_id_column",
            "type": "string",
            "title": "reorder input file ID column name",
            "description": "The exact name of the column used for identification of each variation",
            "default": "variation_id"
        },
        "reorder_id_type": {
            "id": "reorder_id_type",
            "type": "string",
            "enum": ["variation_id", "ean", "sku", "asin"]
        },
        "reorder_qty_column": {
            "id": "reorder_qty_column",
            "type": "string",
            "title": "reorder input file quantity column name",
            "description": "The exact name of the column used for the reorder amount of each variation",
            "default": "quantity"
        },
        "priorities": {
            "id": "priority_mapping",
            "type": "array",
            "title": "Priority level mapping",
            "description": "Mapping of Plentymarkets Tag IDs to priority levels",
            "default": [],
            "examples": [
                [
                    {
                        "level": 1,
                        "plenty_tag_id": 1
                    },
                    {
                        "level": 2,
                        "plenty_tag_id": 15
                    }
                ]
            ],
            "additionalItems": False,
            "uniqueItems": True,
            "items": {
                "anyOf": [
                    {
                        "type": "object",
                        "title": "Priority mapping attributes",
                        "default": {},
                        "examples": [
                            {
                                "level": 1,
                                "plenty_tag_id": 1
                            }
                        ],
                        "required": [
                            "level",
                            "plenty_tag_id"
                        ],
                        "properties": {
                            "level": {
                                "id": "priority_mapping_level",
                                "type": "integer",
                                "title": "Priority level",
                                "description": "The priority level with 1 being the highest priority"
                            },
                            "plenty_tag_id": {
                                "id": "priority_mapping_plenty_tag_id",
                                "type": "integer",
                                "title": "Plentymarkets Tag ID",
                                "description": "The ID of the tag assigned to variations to indicate the priority level"
                            }
                        },
                        "additionalProperties": True
                    }
                ]
            }
        },
        "excluded_variations": {
            "id": "excluded_variations",
            "type": "array",
            "title": "Excluded Plentymarkets variations",
            "description": "Plentymarkets variation IDs excluded from synchronization, suitable for test variations.",
            "default": [],
            "additionalItems": False,
            "uniqueItems": True,
            "items": {
                "anyOf": [
                    {
                        "type": "integer",
                        "title": "Plentymarkets Variation ID",
                        "description": "ID of a single variation"
                    }
                ]
            }
        },
        "exclude_reorder_status": {
            "type": "number",
            "title": "Excluded reorder status",
            "description": "Exclude the reorder status from synchronization",
            "minimum": 18.0
        },
    },
    "additionalProperties": True
}
