"""
Synchronize Plentymarkets orders, stock etc. with the MariaDB database.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from plenty_rait.helper.config_helper import (
    ConfigHandler, WarehouseConfigHandler
)


class MissingPriorityBufferMapping(Exception):
    def __init__(self, warehouse_id: int, expected: list, missing: list):
        self.warehouse_id = warehouse_id
        self.expected = expected
        self.missing = missing
        super().__init__()

    def __str__(self):
        return str(f'Invalid configuration for warehouse {self.warehouse_id},'
                   ' missing a priority mapping for priority level/s '
                   f'{self.missing}.\nExpected priority buffer mappings for '
                   f'the following priorities: {self.expected}.')


class PlentySyncConfigHandler(ConfigHandler):
    def __init__(self, name: str, config_type: str, config_schema: dict):
        self.config: dict
        self.warehouse_handler: WarehouseConfigHandler
        super().__init__(name=name, config_type=config_type,
                         config_schema=config_schema)

    def _validate_configuration(self):
        if any('priority_buffer_mapping' in warehouse
               for warehouse in self.warehouse_handler.get()):
            if 'priorities' not in self.config:
                raise RuntimeError(
                    'Priority level-buffer mapping given for warehouses '
                    'but no priority level-tag mapping found.'
                )

    @staticmethod
    def __build_buffer_map(priority_map: dict, config: dict) -> dict:
        """
        Construct a mapping of inventory buffers to variations depending on
        their priority and the assigned buffer value for the given priority.

        Parameters:
            priority_map        [dict]      -   Mapping of variation ID to
                                                priority level
            config              [dict]      -   Configuration for a specific
                                                warehouse as JSON

        Return:
                                [dict]      -   Mapping of variation_id to
                                                the buffer value
        """
        max_prio_level = max(priority_map.values())
        mapping = {
            entry['level']: entry['buffer']
            for entry in config['attributes']['priority_buffer_mapping']
        }
        levels = mapping.keys()
        if -1 in levels:
            buffer_value = mapping.pop(-1)
            if not levels:
                max_prio_level_from_map = 0
            else:
                max_prio_level_from_map = max(levels)
            for level in range(max_prio_level_from_map + 1,
                               max_prio_level + 1):
                mapping.update({level: buffer_value})
        else:
            expected_priority_levels = range(1, max_prio_level + 1)
            if not all(x in list(mapping.keys())
                       for x in expected_priority_levels):
                missing_priority_mapping = [x for x in expected_priority_levels
                                            if x not in list(mapping.keys())]
                raise MissingPriorityBufferMapping(
                    warehouse_id=config['plenty_warehouse_id'],
                    expected=list(expected_priority_levels),
                    missing=missing_priority_mapping
                )

        return {key: mapping[value] for key, value in priority_map.items()}

    def build_buffer_map(self, priority_map: dict) -> None:
        """
        Read out and validate user-configured warehouses from the
        configuration.

        Parameters:
            priority_map        [dict]      -   Mapping of variation ID to
                                                priority level
        """
        for warehouse in self.warehouse_handler.get():
            if (
                'attributes' in warehouse and
                'priority_buffer_mapping' in warehouse['attributes']
            ):
                buffer_map = self.__build_buffer_map(
                    priority_map=priority_map, config=warehouse
                )
                warehouse['attributes']['variation_buffer_map'] = buffer_map

    def get_priority_config(self) -> dict:
        """
        Parse and prepare the priority mapping configuration.

        Return:
                                [dict]      -   Mapping of variation tag
                                                IDs to priority levels as
                                                integers
        """
        if 'priorities' not in self.config:
            return {}
        return {
            entry['plenty_tag_id']: entry['level']
            for entry in self.config['priorities']
        }
