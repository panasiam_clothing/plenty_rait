"""
Various helper functions used within the tools of the Plenty RAIT project.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
def setup_database(session, sources) -> None:
    try:
        if isinstance(sources, dict):
            session.add_all([entry for section in sources.values()
                             for entry in section])
        elif isinstance(sources, list):
            session.add_all(sources)
        else:
            print('ERROR invalid test sources {sources}')
    # pylint: disable=broad-except
    except Exception:
        session.rollback()
    else:
        session.commit()
