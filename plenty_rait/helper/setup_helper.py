"""
Various helper functions used within the tools of the Plenty RAIT project.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import sys
import getpass
from collections import namedtuple
import platformdirs
import keyring


def setup(args, db_config, logger) -> str:
    """
    Prepare the connection to the database.

    Return:
                            [str]     -     DB connection string
    """

    setup_logging(tool_name='plenty_sync', logger=logger)

    (host, db_name) = get_database(config=db_config,
                                   args=args, logger=logger)
    (user, password) = get_database_credentials(reset=args.reset,
                                                host=host,
                                                database=db_name)
    connection_string = str(
        f'mysql+pymysql://{user}:{password}@{host}/{db_name}')
    return connection_string


def setup_logging(tool_name: str, logger) -> None:
    """ Copy log messages to a specified file depending on the platform """
    logger.add(platformdirs.user_log_dir(tool_name), rotation='2 MB')


def get_database(config: dict, args: namedtuple, logger) -> tuple:
    """
    Provide two ways for the declaration of the database location(host + name).

    Parameters:
        config              [dict]      -   Dictionary of the configuration
                                            options
        args                [namedtuple]-   Argument parser options
        logger              [Logger]    -   Logger object from the loguru
                                            module

    Return:
                            [tuple]     -   Database name and Database host
                                            as strings
    """
    if args.db:
        db_name = args.db
    elif config.has_option(section='DATABASE', option='database_name'):
        db_name = config['DATABASE']['database_name']
    else:
        logger.error('Database name required either from the database '
                     'configuration or from CLI argument.')
        sys.exit(1)

    if args.host:
        host = args.host
    elif config.has_option(section='DATABASE', option='host'):
        host = config['DATABASE']['host']
    else:
        logger.info('No host declared in the configuration or CLI '
                    'arguments, fall back to default \'localhost\'')
        host = 'localhost'

    return (host, db_name)


def get_database_credentials(reset: bool, host: str, database: str) -> tuple:
    """
    Get or set the credentials for the database user from a keyring.

    Parameters:
        reset               [bool]      -   Force reset the user and password
        host                [str]       -   Name of the database host
        database            [str]       -   Name of the specific database@host

    Return:
                            [tuple]     -   User and password as strings
    """
    user = keyring.get_password('plenty_rait_database_identity', 'user')
    password = keyring.get_password('plenty_rait_database_identity',
                                    'password')
    if not user or not password or reset:
        print(f'Database login credentials for {database}@{host}:')
        user = input('Username: ')
        password = getpass.getpass()
        keyring.set_password('plenty_rait_database_identity', 'user', user)
        keyring.set_password('plenty_rait_database_identity', 'password',
                             password)
        return (user, password)
    return (user, password)


class CommandLineSetup:
    """ Abstract base class for CLI implementations of sub modules """
    @staticmethod
    def setup_parser(subparser) -> None:
        """ Prepare the specific CLI parameter for the sub module """
        raise NotImplementedError('No parser set up method implemented.')

    @staticmethod
    def validate_parser(args: namedtuple) -> bool:
        """
        Optional check for specific parameter conditions that cannot be
        handled directly with argparse
        """
        del args
        return True

    @staticmethod
    def handle_command(args: namedtuple, connection_string: str) -> bool:
        """ Execute specific actions depending on the parsed arguments """
        raise NotImplementedError('No command handling method implemented.')
