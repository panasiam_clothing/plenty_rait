# pylint: disable=invalid-string-quote, line-too-long
warehouse_config_schema = {
    "definitions": {},
    "title": "Warehouse configuration schema",
    "type": "object",
    "required": [
        "warehouses"
    ],
    "properties": {
        "warehouses": {
            "title": "Warehouses configurations",
            "type": "array",
            "items": {
                "title": "Warehouse instances",
                "type": "object",
                "required": [
                    "plenty_warehouse_id",
                    "database_column_name",
                    "sheet_column_name",
                    "gui_display_name",
                    "sync_with"
                ],
                "properties": {
                    "plenty_warehouse_id": {
                        "type": "integer",
                        "title": "Plentymarkets warehouse_id",
                        "description": "ID of the warehouse assigned by "
                                       "Plentymarkets"
                    },
                    "database_column_name": {
                        "type": "string",
                        "title": "Database column name",
                        "description": "Name of the column used within the "
                                       "database, the names are generic"
                    },
                    "sheet_column_name": {
                        "type": "string",
                        "title": "Sheet_column_name",
                        "description": "Name of the column in the Stock sheet",
                    },
                    "gui_display_name": {
                        "type": "string",
                        "title": "Gui_display_name",
                        "description":
                        "Name of the option in the control panel",
                    },
                    "sync_with": {
                        "type": "string",
                        "description":
                        "Declare which stock values are used for the database "
                        "either from an Amazon warehouse via the export or "
                        "from an internal Plentymarkets warehouse via REST API"
                        ". If you Plentymarkets to synchronize Amazon FBA "
                        "orders, then you should always synchronize with "
                        "Plentymarkets",
                        "enum": ["plentymarkets", "amazon"]
                    },
                    "attributes": {
                        "title": "Warehouse attributes",
                        "type": "object",
                        "properties": {
                            "priority_buffer_mapping": {
                                "id": "priority_buffer_mapping",
                                "type": "array",
                                "title": "Priority Buffer mapping",
                                "description":
                                "Map a buffer value for variations of "
                                "different priorities for the specific "
                                "warehouse, -1 describes everything else",
                                "default": [],
                                "examples": [
                                    [
                                        {
                                            "level": 1,
                                            "buffer": 30
                                        },
                                        {
                                            "level": -1,
                                            "buffer": 20
                                        }
                                    ]
                                ],
                                "additionalItems": False,
                                "uniqueItems": True,
                                "items": {
                                    "anyOf": [
                                        {
                                            "type": "object",
                                            "title": "Priority Buffer map attributes",
                                            "required": [
                                                "level",
                                                "buffer"
                                            ],
                                            "properties": {
                                                "level": {
                                                    "type": "integer",
                                                    "title": "Priority level",
                                                    "description":
                                                    "The priority level with 1"
                                                    "being the highest priority"
                                                },
                                                "buffer": {
                                                    "type": "integer",
                                                    "title": "Buffer quantity",
                                                    "description":
                                                    "Amount of stock that "
                                                    "should remain in the "
                                                    "source warehouse"
                                                }
                                            },
                                            "additionalProperties": True
                                        }
                                    ]
                                }
                            },
                            "transit_location_id": {
                                "type": "integer",
                                "title": "Plentymarkets Transit location ID",
                                "description":
                                "Location ID of a warehouse that is used as "
                                "transit"
                            },
                            "empty_threshold_quantity": {
                                "type": "integer",
                                "title": "Source location empty threshold",
                                "description": "Quantity threshold where a "
                                               "location should be emptied"
                            },
                            "empty_exclusions": {
                                "type": "array",
                                "title": "Excluded variations from empty "
                                         "threshold",
                                "description": "Array of variations that "
                                               "should not be emptied "
                                               "automatically",
                                "default": [],
                                "additionalItems": False,
                                "uniqueItems": True,
                                "items": {
                                    "anyOf": [
                                        {
                                            "type": "integer",
                                            "title":
                                            "Plentymarkets Variation ID",
                                            "description":
                                            "ID of a single variation"
                                        }
                                    ]
                                }
                            },
                            "amazon_region": {
                                "type": "string",
                                "title": "Amazon warehouse region",
                                "description": "two letter country code",
                                "enum": ["EU", "UK", "US"]
                            }
                        }
                    }
                }
            }
        }
    }
}
