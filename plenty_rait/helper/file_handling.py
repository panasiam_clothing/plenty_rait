"""
Various helper functions used within the tools of the Plenty RAIT project.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import os
from pathlib import Path
import tkinter
from tkinter.messagebox import askokcancel
import chardet
from openpyxl.workbook.workbook import Workbook


def get_encoding(path, logger) -> str:
    """
    Sniff the encoding used within a given file.

    Parameters:
        path            [Path/str]  -   filepath to the given file
        logger      [loguru object] -   logger object of the module calling
                                        this function

    Return:
                        [str]       -   Name of the encoding
    """
    with open(path, mode='rb') as data:
        if not data:
            logger.error(f'No file found at {path}')
            return ''
        return chardet.detect(data.read())['encoding']


def write_workbook_with_retry(workbook: Workbook, path: Path) -> None:
    """
    Special wrapper around the save method of an openpyxl Workbook object to
    retry saving a file, in case it is already open on a Windows system.
    """
    while 1:
        try:
            workbook.save(path)
            break
        except PermissionError as err:
            if os.name == 'nt':
                root = tkinter.Tk()
                root.overrideredirect(1)
                root.withdraw()
                response = askokcancel(
                    'Cannot write to an open file',
                    f'File at {path} cannot be written to as it is already '
                    'open, please close the file and click OK.'
                )
                root.destroy()
                if response:
                    continue
                return
            raise err
