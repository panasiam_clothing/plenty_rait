# pylint: disable=too-few-public-methods, missing-function-docstring
import importlib
import datetime
import sqlalchemy.orm
from sqlalchemy.exc import OperationalError, StatementError
from sqlalchemy.orm import sessionmaker, relationship, scoped_session
from sqlalchemy.orm.query import Query as _Query
from sqlalchemy.orm.session import Session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy_utils import generic_repr
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy import (
    Column, Integer, Text, String, Date, Numeric, Boolean, create_engine,
    ForeignKey, Index, func, inspect
)
from loguru import logger
from time import sleep


Base = declarative_base()


@generic_repr
class OrdersModel(Base):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True, nullable=False)
    origin_id = Column(Numeric(precision=8, scale=2), nullable=False)
    netto_total = Column(Numeric(precision=8, scale=2), nullable=False)
    gross_total = Column(Numeric(precision=8, scale=2), nullable=False)
    creation_date = Column(Date, nullable=False)
    payment_date = Column(Date)
    delivery_date = Column(Date)
    b2b = Column(Boolean, nullable=False)
    external_id = Column(Text)
    order_items = relationship('OrderItemsModel', backref='orders')

    Index('order_index', id, origin_id, creation_date)


@generic_repr
class OrderItemsModel(Base):
    __tablename__ = 'order_items'

    id = Column(Integer, primary_key=True, nullable=False)
    order_id = Column(Integer, ForeignKey(OrdersModel.id), nullable=False)
    external_id = Column(String(50))
    origin_id = Column(Numeric(precision=8, scale=2), nullable=False)
    variation_id = Column(Integer, nullable=False)
    ean = Column(String(13))
    item_id = Column(Integer, nullable=False)
    name = Column(Text)
    quantity = Column(Integer, nullable=False)
    netto = Column(Numeric(precision=8, scale=2), nullable=False)
    gross = Column(Numeric(precision=8, scale=2), nullable=False)
    statistic_entry = Column(Integer, nullable=True, default=-1)
    daily_statistic_entry = Column(Integer, nullable=True, default=-1)
    order = relationship('OrdersModel',
                         foreign_keys='OrderItemsModel.order_id',
                         lazy='joined', overlaps='order_items,orders')

    Index('order_item_index', id, order_id, external_id, origin_id,
          variation_id, quantity, statistic_entry, daily_statistic_entry)


@generic_repr
class AmazonOrdersModel(Base):
    __tablename__ = 'amazon_orders'

    id = Column(Integer, primary_key=True, nullable=False)
    amazon_id = Column(String(length=25), nullable=False)
    origin_id = Column(Numeric(precision=8, scale=2))
    purchase_date = Column(Date)
    netto_total = Column(Numeric(precision=8, scale=2))
    gross_total = Column(Numeric(precision=8, scale=2))
    b2b = Column(Boolean)
    order_items = relationship('AmazonOrderItemsModel',
                               back_populates='order')

    Index('amazon_order_index', id, amazon_id, origin_id, purchase_date)


@generic_repr
class AmazonOrderItemsModel(Base):
    __tablename__ = 'amazon_order_items'

    id = Column(Integer, primary_key=True, nullable=False)
    order_id = Column(Integer, ForeignKey(AmazonOrdersModel.id),
                      nullable=False)
    amazon_id = Column(String(length=25), nullable=False)
    origin_id = Column(Numeric(precision=8, scale=2))
    variation_id = Column(Integer, nullable=False)
    ean = Column(String(13))
    asin = Column(String(length=10), nullable=False)
    netto = Column(Numeric(precision=8, scale=2))
    gross = Column(Numeric(precision=8, scale=2))
    quantity = Column(Integer, nullable=False)
    statistic_entry = Column(Integer, nullable=True, default=-1)
    daily_statistic_entry = Column(Integer, nullable=True, default=-1)
    returned = Column(Boolean, default=False)
    order = relationship('AmazonOrdersModel',
                         foreign_keys='AmazonOrderItemsModel.order_id',
                         lazy='joined', overlaps='amazon_orders,order_items',
                         back_populates='order_items')

    Index('amazon_order_item_index', id, order_id, amazon_id, origin_id,
          variation_id, quantity, statistic_entry, daily_statistic_entry)


@generic_repr
class StockModel(Base):
    __tablename__ = 'current_stock'

    variation_id = Column(Integer, primary_key=True, autoincrement=False)
    ean = Column(String(13))
    stock_warehouse_1 = Column(Integer, default=0)
    stock_warehouse_2 = Column(Integer, default=0)
    stock_warehouse_3 = Column(Integer, default=0)
    stock_warehouse_4 = Column(Integer, default=0)
    stock_warehouse_5 = Column(Integer, default=0)
    stock_warehouse_6 = Column(Integer, default=0)
    stock_warehouse_7 = Column(Integer, default=0)
    stock_warehouse_8 = Column(Integer, default=0)
    stock_warehouse_9 = Column(Integer, default=0)

    def update_row(self, row):
        for key, value in row.items():
            setattr(self, key, value)

    def update_by_dataframe(self, dataframe):
        """
        Get a matching row from a dataframe and update the database entry.
        """
        # pylint: disable=invalid-string-quote
        row = dataframe[dataframe['ean'] == self.ean].to_dict(orient='records')
        if len(row) > 1:
            logger.warning(f"[{self.__tablename__}] Multiple rows match ean "
                           f"{self.ean}.\nVariation ID: "
                           f"{[x['variation_id'] for x in row]}")
        try:
            for key, value in row[0].items():
                setattr(self, key, value)
        except IndexError:
            logger.warning(f'[{self.__tablename__}] No row matches ean '
                           f'{self.ean}')


@generic_repr
class IdentificationModel(Base):
    __tablename__ = 'identification'

    variation_id = Column(Integer, primary_key=True, autoincrement=False)
    ean = Column(String(13))
    sku = Column(Text(50))
    asin_all = Column(String(10))
    name = Column(String(300))

    Index('identification_index', variation_id, ean, name)

    def update_by_dataframe(self, dataframe):
        """
        Get a matching row from a dataframe and update the database entry.
        """
        # pylint: disable=invalid-string-quote
        row = dataframe[dataframe['ean'] == self.ean].to_dict(orient='records')
        if len(row) > 1:
            logger.warning(f"[{self.__tablename__}] Multiple rows match ean "
                           f"{self.ean}.\nVariation ID: "
                           f"{[x['variation_id'] for x in row]}")
        try:
            for key, value in row[0].items():
                setattr(self, key, value)
        except IndexError:
            logger.warning(f'[{self.__tablename__}] No row matches ean '
                           f'{self.ean}')


@generic_repr
class StatisticModel(Base):
    __tablename__ = 'statistics'

    id = Column(Integer, primary_key=True)
    variation_id = Column(Integer)
    ean = Column(String(13))
    origin_id = Column(Numeric(precision=8, scale=2))
    month = Column(Integer)
    year = Column(Integer)
    quantity = Column(Integer)
    predicted = Column(Boolean)

    Index('statistics_index', id, variation_id, ean, origin_id, month,
          year, quantity, predicted)

    @hybrid_property
    def date_value(self):
        return func.str_to_date(func.concat(self.year, '-', self.month, '-',
                                            '1'), '%Y-%m-%d')

    @hybrid_property
    def date_obj(self):
        return datetime.date.fromisoformat(f'{self.year}-{self.month:02}-01')


@generic_repr
class DailyStatisticModel(Base):
    __tablename__ = 'daily_statistics'

    id = Column(Integer, primary_key=True)
    variation_id = Column(Integer)
    ean = Column(String(13))
    origin_id = Column(Numeric(precision=8, scale=2))
    day = Column(Integer)
    month = Column(Integer)
    year = Column(Integer)
    quantity = Column(Integer)

    Index('daily_statistics_index', id, variation_id, ean, origin_id, day,
          month, year, quantity)

    @hybrid_property
    def date_value(self):
        return func.str_to_date(func.concat(self.year, '-', self.month, '-',
                                            self.day), '%Y-%m-%d')

    @hybrid_property
    def date_obj(self):
        return datetime.date.fromisoformat(
            f'{self.year}-{self.month:02}-{self.day:02}')


@generic_repr
class ProducerModel(Base):
    __tablename__ = 'producers'

    plenty_id = Column(Integer, primary_key=True, autoincrement=False)
    producer_name = Column(String(50))
    delivery_time = Column(Integer, nullable=False)

    def update_producer(self, producer_name: str = '',
                        delivery_time: int = -1):
        if producer_name:
            self.producer_name = producer_name
        if delivery_time:
            self.delivery_time = delivery_time

@generic_repr
class VariationInformationModel(Base):
    __tablename__ = 'variation_information'

    variation_id = Column(Integer, primary_key=True, autoincrement=False)
    ean = Column(String(13), nullable=False)
    priority = Column(Integer)
    producer_id = Column(Integer, ForeignKey(ProducerModel.plenty_id))
    creation_date = Column(Date, nullable=True)
    producer = relationship('ProducerModel', lazy='joined')

    def update_by_dataframe(self, dataframe):
        """
        Get a matching row from a dataframe and update the database entry.
        """
        # pylint: disable=invalid-string-quote
        row = dataframe[dataframe['ean'] == self.ean].to_dict(orient='records')
        if len(row) > 1:
            logger.warning(f"[{self.__tablename__}] Multiple rows match ean "
                           f"{self.ean}.\nVariation ID: "
                           f"{[x['variation_id'] for x in row]}")
        try:
            for key, value in row[0].items():
                setattr(self, key, value)
        except IndexError:
            logger.warning(f'[{self.__tablename__}] No row matches ean '
                           f'{self.ean}')


@generic_repr
class IncomingVariationModel(Base):
    __tablename__ = 'incoming_variations'

    variation_id = Column(Integer, primary_key=True, autoincrement=False)
    ean = Column(String(13))
    order_id = Column(Integer, primary_key=True, autoincrement=False)
    quantity = Column(Integer, nullable=False)
    order_date = Column(Date, nullable=False)
    receive_date = Column(Date, nullable=True)
    producer_id = Column(Integer, ForeignKey(ProducerModel.plenty_id),
                         nullable=False)
    producer = relationship('ProducerModel', lazy='joined')

    def update_by_dataframe(self, dataframe):
        """
        Get a matching row from a dataframe and update the database entry.
        """
        if self.receive_date or not self.quantity:
            return
        row = dataframe[
            ((dataframe['ean'] == self.ean) &
             (dataframe['order_id'] == self.order_id))
        ].to_dict(orient='records')
        if len(row) > 1:
            logger.warning(f'[{self.__tablename__}] Multiple rows match ean '
                           f'{self.ean} and order ID {self.order_id}.\n'
                           f'[{row}]')
        try:
            for key, value in [(key, value) for key, value in row[0].items()
                               if key in ['quantity', 'producer_id']]:
                setattr(self, key, value)
        except IndexError:
            logger.warning(f'[{self.__tablename__}] No row matches ean '
                           f'{self.ean}')


class RetryingQuery(_Query):
    __max_retry_count__ = 3

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __iter__(self):
        attempts = 0
        while True:
            attempts += 1
            try:
                return super().__iter__()
            except OperationalError as ex:
                if "server closed the connection unexpectedly" not in str(ex):
                    raise
                if attempts <= self.__max_retry_count__:
                    sleep_for = 2 ** (attempts - 1)
                    logger.error(
                        "Database connection error: retrying Strategy => "
                        "sleeping for {}s and will retry (attempt #{} of {})"
                        " \n Detailed query impacted: {}".format(
                        sleep_for, attempts, self.__max_retry_count__, ex)
                )
                    sleep(sleep_for)
                    continue
                else:
                    raise
            except StatementError as ex:
                if "reconnect until invalid transaction is rolled back" not in str(ex):
                    raise
                self.session.rollback()


class DataAccessLayer:
    def __init__(self):
        self.connection = ''
        self.engine = None
        # pylint: disable=invalid-name
        self.s_session: scoped_session = None
        self.session: Session = Session()

    def connect(self):
        self.engine = create_engine(self.connection)
        Base.metadata.bind = self.engine
        Base.metadata.create_all(self.engine)
        self.session = sessionmaker(bind=self.engine, query_cls=RetryingQuery)


dal = DataAccessLayer()


class BaseDatabaseService:
    def __init__(self, session: Session = Session()):
        self.session = session

    def create_database_session(self, connection_string: str) -> None:
        dal.connection = connection_string
        dal.connect()

        dal.s_session = scoped_session(sessionmaker(bind=dal.engine,
                                                    query_cls=RetryingQuery))
        dal.session = dal.s_session()
        dal.s_session.registry.clear()

        self.session: sqlalchemy.orm.session.Session = dal.session

    @staticmethod
    def create_tables(tables: list) -> None:
        """
        Create the given list of database tables, which match the class names
        in schema.py.

        Parameters:
            tables          [list]      -   List of tables names matching the
                                            class names in schema.py
            module          [str]       -   Name of the schema module to be
                                            imported
        """
        table_model = importlib.import_module(__name__)
        for table in tables:
            if not inspect(dal.engine).has_table(table):
                orm_table = getattr(table_model, table)
                orm_table.__table__.create(bind=dal.engine, checkfirst=True)
                orm_table.__table__.create(bind=dal.engine, checkfirst=True)
