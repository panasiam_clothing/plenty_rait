"""
Various helper functions used within the tools of the Plenty RAIT project.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import configparser
import json
import pathlib
from typing import Union
import jsonschema
import platformdirs

from plenty_rait.helper.database_schema import StockModel
from plenty_rait.helper.warehouse_config_schema import warehouse_config_schema


def get_plenty_api_base_url() -> str:
    (api_config, api_config_path) = init_top_level_config(
        name='plenty_api', config_type='json'
    )
    if 'plenty_api_base_url' not in api_config:
        raise RuntimeError(
            'No REST API URL found within the configuration at '
            f'{api_config_path}'
        )
    return api_config['plenty_api_base_url']


def read_configuration(
    path: pathlib.Path, config_type: str
) -> Union[dict, configparser.ConfigParser]:
    if not path.stat().st_size:
        return {}
    if config_type.lower() == 'json':
        with path.open(mode='r', encoding='utf-8') as config_file:
            return json.load(config_file)
    if config_type.lower() == 'ini':
        data = configparser.ConfigParser()
        data.read(path)
        return data
    return {}


def init_top_level_config(name: str, config_type: str) -> tuple:
    """
    Locate or create a configuration file within the `plenty_rait_tool` folder.

    Return:
                            [dict]      -   (configuration data / path)
    """
    plenty_rait_config = pathlib.Path(
        platformdirs.user_config_dir('plenty_rait_tool'))
    plenty_rait_config.mkdir(parents=True, exist_ok=True)
    path = plenty_rait_config / f'{name}_config.{config_type}'
    if path.exists():
        try:
            config = read_configuration(path=path, config_type=config_type)
        except json.decoder.JSONDecodeError as err:
            raise InvalidConfiguration(
                name=name, error='Invalid JSON'
            ) from err
        if config:
            return (config, path)
    path.touch(exist_ok=True)
    raise RuntimeError(
        f'{name} config empty, please fill the file found at {path}'
    )


def init_config(name: str, config_type: str = 'ini') -> pathlib.Path:
    """
    Locate or create the configuration for @name tool.

    The only viable location for the configuration is the @config_folder
    directory within the home directory of the current user.

    Parameters:
        name                [str]       -   Name of the specific tool
    Optional:
        config_type         [str]       -   Type of the configuration INI or
                                            JSON

    Return:
                            [Path]      -   Path to the tool configuration
    """
    plenty_rait_config = pathlib.Path(
        platformdirs.user_config_dir('plenty_rait_tool'))
    base_path = plenty_rait_config / name
    base_path.mkdir(parents=True, exist_ok=True)

    if config_type.lower() == 'ini':
        config_path = base_path / 'config.ini'
    elif config_type.lower() == 'json':
        config_path = base_path / 'config.json'
    else:
        raise RuntimeError(f'Invalid config type {config_type} used.')

    if not config_path.exists():
        config_path.touch()

    return config_path


class InvalidConfiguration(Exception):
    def __init__(self, name: str, error: str):
        self.name = name
        self.error = error
        super().__init__()

    def __str__(self):
        return f'Invalid {self.name} configuration, error: {self.error}'


class InvalidDatabaseColumnName(Exception):
    def __init__(self, warehouse_id: int, database_column_name: str):
        self.warehouse_id = warehouse_id
        self.database_column_name = database_column_name
        super().__init__()

    def __str__(self):
        return str(f'Invalid database column name {self.database_column_name}'
                   f' for the warehouse with ID {self.warehouse_id}')


class DuplicateDatabaseColumnConfiguration(Exception):
    def __init__(self, warehouse_id: int, duplicate_id: int, column_name: str):
        self.warehouse_id = warehouse_id
        self.duplicate_id = duplicate_id
        self.column_name = column_name
        super().__init__()

    def __str__(self):
        return str(f'Duplicate database column [{self.column_name}] found at '
                   f'{self.warehouse_id} and {self.duplicate_id}.')


class WarehouseConfigHandler:
    def __init__(self):
        (self.config, self.path) = init_top_level_config(name='warehouse',
                                                         config_type='json')
        jsonschema.validate(instance=self.config,
                            schema=warehouse_config_schema)
        self._validate_configuration()

    def _validate_configuration(self) -> None:
        assigned_db_columns = []
        for warehouse in self.config['warehouses']:
            if warehouse['sync_with'] == 'amazon':
                if (
                    'attributes' in warehouse and
                    'amazon_region' in warehouse['attributes']
                ):
                    continue
                raise InvalidConfiguration(
                    name='warehouses',
                    error='Every amazon warehouse requires a region to '
                          'identify the correct export file'
                )
            warehouse_id = warehouse['plenty_warehouse_id']
            db_column = warehouse['database_column_name']
            if db_column not in StockModel.__dict__:
                raise InvalidDatabaseColumnName(
                    warehouse_id=warehouse_id,
                    database_column_name=db_column
                )
            duplicate_db_column = [
                x for x in assigned_db_columns if x[1] == db_column
            ]
            if duplicate_db_column:
                raise DuplicateDatabaseColumnConfiguration(
                    warehouse_id=warehouse_id,
                    duplicate_id=duplicate_db_column[0],
                    column_name=db_column
                )
            assigned_db_columns.append((warehouse_id, db_column))

    def get(self, key: str = '', warehouse_type: str = '') -> list:
        assert warehouse_type in ['', 'plentymarkets', 'amazon']
        warehouses = self.config['warehouses']
        if warehouse_type:
            warehouses = [
                warehouse for warehouse in self.config['warehouses']
                if warehouse['sync_with'] == warehouse_type
            ]
        if key:
            try:
                return [
                    value for warehouse in warehouses
                    for w_key, value in warehouse.items() if w_key == key
                ]
            except KeyError as err:
                raise KeyError(
                    f'No section named {key} found in the warehouse configuration'
                ) from err
        return warehouses

    def get_with_id(self, warehouse_id: int) -> dict:
        try:
            return [
                warehouse for warehouse in self.config['warehouses']
                if warehouse['plenty_warehouse_id'] == warehouse_id
            ][0]
        except IndexError:
            return {}

    def write(self) -> None:
        with open(self.path, 'w', encoding='UTF-8') as config_file:
            config_file.write(
                json.dumps(self.config, indent=4)
            )


class ConfigHandler:
    def __init__(self, name: str, config_type: str,
                 config_schema: dict = None):
        path = init_config(name=name, config_type=config_type)
        try:
            self.config = read_configuration(
                path=path, config_type=config_type
            )
        except json.decoder.JSONDecodeError as err:
            raise InvalidConfiguration(
                name=name, error='Invalid JSON'
            ) from err
        if not self.config:
            raise RuntimeError(
                f'{name} config empty, please fill the file found at {path}'
            )
        if config_schema:
            jsonschema.validate(instance=self.config, schema=config_schema)
        self.warehouse_handler = WarehouseConfigHandler()
        try:
            self._validate_configuration()
        except Exception as err:
            raise InvalidConfiguration(name=name, error=err.args) from err

    # pylint: disable=no-self-use
    def _validate_configuration(self):
        return
