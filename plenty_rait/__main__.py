import argparse
import sys
from loguru import logger

from plenty_rait.helper.setup_helper import setup
from plenty_rait.helper.config_helper import init_top_level_config
from plenty_rait.plenty_sync.cli import PlentySyncCommandLine
from plenty_rait.amazon_import.cli import AmazonImportCommandLine
from plenty_rait.sales_statistic.cli import SalesStatisticCommandLine
from plenty_rait.report.cli import ReportCommandLine
from plenty_rait.prediction_tool.cli import PredictionToolCommandLine
from plenty_rait.update_spreadsheet.cli import UpdateSpreadsheetCommandLine
from plenty_rait.control_panel.cli import ControlPanelCommandLine
from plenty_rait.inbound_shipment.cli import InboundShipmentCommandLine
from plenty_rait.reorder_document.cli import ReorderDocumentCommandLine


def setup_module_map() -> dict:
    return {
        'PlentySync': PlentySyncCommandLine,
        'AmazonImport': AmazonImportCommandLine,
        'SalesStatistic': SalesStatisticCommandLine,
        'Report': ReportCommandLine,
        'PredictionTool': PredictionToolCommandLine,
        'UpdateSpreadsheet': UpdateSpreadsheetCommandLine,
        'ControlPanel': ControlPanelCommandLine,
        'InboundShipment': InboundShipmentCommandLine,
        'ReorderDocument': ReorderDocumentCommandLine
    }


def setup_argparser(modules):
    """ Return: [Argparse Object] """
    parser = argparse.ArgumentParser(prog='plenty_rait')
    subparsers = parser.add_subparsers(
        title='Options for plenty_rait',
        help='Collection of tools for the calculation and creation of '
             'redistributions and reorders', dest='command')
    parser.add_argument('--reset-credentials', '-r', '--reset', required=False,
                        help='Reset the credentials within your system-wide'
                             ' keyring',
                        action='store_true', dest='reset')
    parser.add_argument('--host', '-l', required=False,
                        help='The location of the database remote server or '
                             'localhost',
                        dest='host')
    parser.add_argument('--database', '--db', '-d', required=False,
                        help='Name of the database',
                        dest='db')
    for module in modules.values():
        module.setup_parser(subparser=subparsers)
    args = parser.parse_args()

    if not args.command and not args.reset:
        logger.warning(
            'No command provided, specify the tool to run.\nChoices: '
            f'{list(modules)}'
        )
        sys.exit(0)

    if args.command:
        if not modules[args.command].validate_parser(args=args):
            logger.error(f'Invalid CLI parameter for command {args.command}')
            sys.exit(1)
    return args


def main():
    module_map = setup_module_map()
    args = setup_argparser(modules=module_map)
    (db_config, _) = init_top_level_config(name='database', config_type='ini')
    init_top_level_config(name='plenty_api', config_type='json')
    connection_string = setup(args=args, db_config=db_config, logger=logger)

    if not args.command:
        sys.exit(0)

    if not module_map[args.command].handle_command(
        args=args, connection_string=connection_string
    ):
        sys.exit(1)


if __name__ == '__main__':
    main()
