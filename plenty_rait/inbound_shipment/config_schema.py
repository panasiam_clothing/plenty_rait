# pylint: disable=invalid-string-quote, line-too-long
inbound_shipment_configuration_schema = {
    "title": "Inbound shipment preparation configuration",
    "type": "object",
    "required": [
        "sender_addresses"
    ],
    "properties": {
        "default_receiver": {
            "title": "Default receiver",
            "description": "Two letter country code for the country, where the goods are shipped to.",
            "type": "string",
            "examples": [
                "DE", "US", "UK"
            ],
            "pattern": "^[A-Z]{2}$"
        },
        "sender_addresses": {
            "title": "Sender addresses",
            "description": "List of pre-configured sender addresses for inbound plans",
            "type": "array",
            "items": {
                "title": "Sender address attributes",
                "type": "object",
                "required": [
                    "name",
                    "street",
                    "city",
                    "country_code",
                    "state",
                    "postal_code"
                ],
                "properties": {
                    "name": {
                        "title": "Sender name",
                        "type": "string",
                        "examples": [
                            "Test Name"
                        ],
                        "pattern": r"^\w+\s\w+$"
                    },
                    "street": {
                        "title": "Sender street",
                        "type": "string",
                        "examples": [
                            "test street nr 52"
                        ]
                    },
                    "additional_street": {
                        "title": "Additional Sender street line",
                        "type": "string",
                        "examples": [
                            "second floor"
                        ]
                    },
                    "city": {
                        "title": "Sender city",
                        "type": "string",
                        "examples": [
                            "test city"
                        ]
                    },
                    "country_code": {
                        "title": "Sender country code",
                        "type": "string",
                        "examples": [
                            "DE"
                        ],
                        "pattern": "^[A-Z]{2}$"
                    },
                    "state": {
                        "title": "State",
                        "type": "string",
                        "examples": [
                            "Bayern"
                        ]
                    },
                    "postal_code": {
                        "title": "Sender postal code",
                        "type": "string",
                        "examples": [
                            "12345"
                        ]
                    },
                    "district": {
                        "title": "Sender address district",
                        "type": "string"
                    }
                }
            }

        }
    }
}
