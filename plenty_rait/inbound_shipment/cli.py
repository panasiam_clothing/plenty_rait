"""
Prepare an Amazon Inbound shipment from a Plentymarkets redistribution.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import pathlib
import platformdirs

from loguru import logger

from plenty_rait.inbound_shipment.service import Interface
from plenty_rait.helper.setup_helper import CommandLineSetup


def setup_inbound_plan_parser(subparser) -> None:
    parser = subparser.add_parser(
        'Inbound',
        help='Create a new inbound plan for a redistribution to announce the '
        'shipment to Amazon, the sender information is taken from the config.'
    )
    parser.add_argument(
        '--ship_to_country', '--ship_to', '--country', '-c', required=False,
        dest='shipment_delivery_country', type=str,
        help='Choose the destination country as ISO 3166-1 Alpha-2 code '
        '(`GB`, `DE`, `FR`, etc)',
        choices=['DE', 'FR', 'GB', 'IT', 'ES', 'US', 'TR', 'NL']
    )
    parser.add_argument(
        '--sender_address', '--sender', '-s', required=False,
        dest='inbound_sender_address',
        help='Index of the sender address within the `sender_adresses` array '
        'of the configuration.', type=int, default=1
    )


def setup_package_list_parser(subparser) -> None:
    parser = subparser.add_parser(
        'PackageList',
        help='Fill a package list export from the Amazon Seller-central, with'
        'data from a Plentymarkets Redistribution'
    )
    parser.add_argument(
        '--package_list_path', '--file_path', '--path', '-p',
        type=pathlib.Path, required=True, dest='package_list_path',
        help='Path to the downloaded package list from the seller-central on '
        'your local system'
    )


def setup_full_view_parser(subparser) -> None:
    parser = subparser.add_parser(
        'FullView',
        help='Create a combined list of inbound plan and package assignment'
    )


class InboundShipmentCommandLine(CommandLineSetup):
    @staticmethod
    def setup_parser(subparser):
        parser = subparser.add_parser(
            'InboundShipment',
            help='Generate/Fill the necessary documents for announcing a new '
            'FBA inbound shipment from a Plentymarkets redistribution'
        )
        subparsers = parser.add_subparsers(
            title='Options for InboundShipment',
            dest='inbound_shipment_command'
        )

        setup_inbound_plan_parser(subparser=subparsers)
        setup_package_list_parser(subparser=subparsers)
        setup_full_view_parser(subparser=subparsers)

        parser.add_argument(
            'Order_id', type=int,
            help='ID of the redistribution in Plentymarkets'
        )
        parser.add_argument(
            '--custom_output_folder', '--output_folder', '--output', '-p',
            required=False, dest='custom_inbound_destination',
            type=pathlib.Path,
            help='By default the finished file will be stored into your'
                 ' Documents directory. You can choose a different location'
                 ' with this argument.'
        )

    @staticmethod
    def validate_parser(args: argparse.Namespace) -> bool:
        if not args.custom_inbound_destination:
            default = pathlib.Path(platformdirs.user_documents_dir())
            args.custom_inbound_destination = default
        args.custom_inbound_destination.mkdir(parents=True, exist_ok=True)

        if args.inbound_shipment_command == '':
            if (
                not args.package_list_path.exists() or
                not args.package_list_path.isfile()
            ):
                logger.error('Invalid file path {args.package_list_path} '
                             'does not lead to a valid file.')
                return False

        return True

    @staticmethod
    def handle_command(args: argparse.Namespace,
                       connection_string: str) -> bool:
        try:
            receiver_country = args.shipment_delivery_country
            sender_address_index = args.inbound_sender_address
        except AttributeError:
            receiver_country = None
            sender_address_index = 0
        interface = Interface(
            connection_string=connection_string,
            output_folder=args.custom_inbound_destination,
            receiver_country=receiver_country,
            sender_index=sender_address_index
        )
        if args.inbound_shipment_command == 'Inbound':
            return interface.create_inbound_plan(order_id=args.Order_id)
        if args.inbound_shipment_command == 'PackageList':
            return interface.fill_package_list(
                order_id=args.Order_id,
                path=args.package_list_path
            )
        if args.inbound_shipment_command == 'FullView':
            return interface.create_package_summary_csv(
                order_id=args.Order_id
            )
        return True
