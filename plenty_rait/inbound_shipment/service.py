"""
Prepare an Amazon Inbound shipment from a Plentymarkets redistribution.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import datetime
import pathlib
import tempfile
from loguru import logger
import pandas
import pickle
import plenty_api

from plenty_rait.inbound_shipment.utils import InboundShipmentConfigHandler
from plenty_rait.inbound_shipment.config_schema import (
    inbound_shipment_configuration_schema
)
from plenty_rait.helper.config_helper import get_plenty_api_base_url
from plenty_rait.helper.database_schema import (
    BaseDatabaseService, IdentificationModel
)
from typing import Tuple


class OrderFetcher:
    """ Service to pull the redistribution order from the REST API """
    def __init__(self) -> None:
        self.api = plenty_api.PlentyApi(base_url=get_plenty_api_base_url())
        self.api.cli_progress_bar = True

    def pull_order(self, order_id: int) -> dict:
        """
        Pull the order matching the ID with package assignments attached
        """
        return self.api.plenty_api_get_pending_redistribution(
            order_id=order_id, shipping_packages='minimal')


class DuplicateOrderItemsError(Exception):
    def __init__(self, error_df: pandas.DataFrame):
        super().__init__()
        self.error_df = error_df

    def __str__(self):
        return str(
            'The following products have been created multiple times within '
            'the redistribution, please adjust the order items accordingly:'
            # pylint: disable=invalid-string-quote
            f"\n{self.error_df['MerchantSKU'].unique().tolist()}"
        )


class InboundPlanCreator:
    """
    Service to build the inbound plan to announce the shipment to Amazon
    """
    def __init__(self, sender_config: dict, receiver_country: str) -> None:
        self.header_section = self.__build_header_section(
            sender_config=sender_config, receiver_country=receiver_country
        )

    @staticmethod
    def __build_header_section(
        sender_config: dict, receiver_country: str
    ) -> list:
        """
        Build the section of the inbound plan with metadata for Amazon.

        Parameters:
            sender_config       [dict]      -   Member of the
                                                'sender_addresses' section from
                                                the JSON config
            receiver_country    [str]       -   Two letter country code for the
                                                destination of the shipment

        Returns:
                                [list]      -   List of rows to be prepended to
                                                the inbound plan
        """
        header_section = []
        today = datetime.date.today()
        header_section.append(['PlanName', f'Shipment_{today}'])
        header_section.append(['ShipToCountry', receiver_country])

        config_map = {
            'AddressName': 'name',
            'AddressFieldOne': 'street',
            'AddressFieldTwo': 'additional_street',
            'AddressCity': 'city',
            'AddressCountryCode': 'country_code',
            'AddressStateOrRegion': 'state',
            'AddressPostalCode': 'postal_code',
            'AddressDistrict': 'district'
        }

        for field, config_key in config_map.items():
            # The required fields are guranteed to be filled through the JSON
            # schema
            try:
                config_value = sender_config[config_key]
            except KeyError:
                config_value = ''
            header_section.append([field, config_value])
        header_section.append(['', ''])
        header_section.append(['MerchantSKU', 'Quantity'])
        header_section.append(['', ''])
        return header_section

    def create_inbound_plan(
        self, order_response: dict, sku_mapping: dict
    ) -> pandas.DataFrame:
        """
        Build the inbound plan with the data from Plentymarkets order and the
        configured sender address and shipment info section.

        Parameters:
            order_response      [dict]      -   Redistribution order from the
                                                Plentymarkets REST API
            sku_mapping         [dict]      -   Mapping of variation IDs to
                                                SKUs from the 'identification'
                                                database table

        Returns:
                                [DataFrame]
        """
        item_list = []
        if 'orderItems' not in order_response:
            logger.error(
                "Invalid REST API order response, doesn't contain 'orderItems'"
                f"\n(contains: [{order_response.keys()}])"
            )
            return pandas.DataFrame()
        for item in order_response['orderItems']:
            if item['quantity'] == 0:
                continue
            item = [sku_mapping[item['itemVariationId']], item['quantity']]
            item_list.append(item)
        complete_frame_list = self.header_section + item_list
        inbound_plan = pandas.DataFrame(
            complete_frame_list, columns=['MerchantSKU', 'Quantity']
        )
        self.__duplicate_items_check(inbound_plan=inbound_plan)
        return inbound_plan

    def __duplicate_items_check(self, inbound_plan: pandas.DataFrame) -> None:
        """
        Check if the inbound plan is valid by searching for duplicate SKUs.

        Parameters:
            inbound_plan        [DataFrame] -   Complete inbound plan with the
                                                header section
        """
        duplicate_sku = inbound_plan.loc[
            len(self.header_section):, 'MerchantSKU'
        ].duplicated(keep=False)
        duplicates = inbound_plan.loc[len(self.header_section):][duplicate_sku]
        if len(duplicates.index) > 0:
            raise DuplicateOrderItemsError(error_df=duplicates)

    @staticmethod
    def validate_with_package_list(
        inbound_plan: pandas.DataFrame, assignment: pandas.DataFrame
    ) -> pandas.DataFrame:
        """
        Check if the inbound plan is valid by comparing the announced quantity
        with the packaged quantity.

        This check can detect errors, which occur through an incorrect order
        creation process.

        Parameters:
            inbound_plan        [DataFrame] -   Complete inbound plan with the
                                                header section
            assignment          [DataFrame] -   Mapping of variations to
                                                packages with the
                                                respective amount

        Returns:
                                [bool]
        """
        error_list = []
        packaged_skus = []
        # Find deviations from the announced amount
        for sku in assignment.groupby('sku'):
            variation_sku = sku[0]
            packaged_skus.append(variation_sku)
            packaged_qty = int(sku[1]['quantity'].sum())
            announced_qty = int(inbound_plan[
                inbound_plan['MerchantSKU'] == variation_sku
            ]['Quantity'].to_list()[0])
            if packaged_qty != announced_qty:
                error_list.append([variation_sku, announced_qty, packaged_qty])
        # Find products that have not been packaged at all
        announced_skus = [str(sku) for sku in inbound_plan['MerchantSKU'].loc[13:].to_list()]
        unpackaged_skus = [sku for sku in announced_skus if sku not in packaged_skus]
        unpackaged_rows = inbound_plan[
            inbound_plan['MerchantSKU'].isin(unpackaged_skus)
        ][['MerchantSKU', 'Quantity']]
        for row in unpackaged_rows.itertuples():
            error_list.append([row.MerchantSKU, int(row.Quantity), 0])
        return pandas.DataFrame(error_list, columns=['sku', 'announced', 'packaged'])


class InsufficientPackagesError(Exception):
    def __init__(self, error_df: pandas.DataFrame):
        super().__init__()
        self.error_df = error_df

    def __str__(self):
        return str(
            'Invalid packaging of products, the following amount of '
            'products must be packaged to fulfill the announced '
            f'quantity.\n{self.error_df}'
        )


class PackageListFiller:
    """
    Service to fill out the generated package assignment form from Amazon
    """
    def __init__(self) -> None:
        self.header_section = []
        self.package_list = pandas.DataFrame()
        self.assignment = pandas.DataFrame()

    def get_package_assignment(
        self, order_response: dict, sku_mapping: dict
    ) -> pandas.DataFrame:
        """
        Create a content list of each package from the API response.

        A single continous range of package numbers will be created by the
        method. For example, the package number of the first package within the
        second pallet starts at last package number of the first pallet + 1.

        Parameters:
            order_response      [dict]      -   Redistribution order from the
                                                Plentymarkets REST API
            sku_mapping         [dict]      -   Mapping of variation IDs to
                                                SKUs from the 'identification'
                                                database table

        Returns:
                                [DataFrame]
        """
        packages = order_response['shippingPackages']
        if not packages:
            return pandas.DataFrame()
        offset_map = {}
        previous_offset = 0
        for key in sorted(packages['pallets']):
            offset_map.update({key: previous_offset})
            previous_offset = len(packages['pallets'][key])

        package_assignment_list = []
        for variation, content in packages['content'].items():
            for pallet, packages in content['packages'].items():
                offset = offset_map[pallet]
                for package in packages.values():
                    package_no = package['packageNo'] + offset
                    quantity = package['quantity']
                    package_assignment_list.append(
                        [sku_mapping[variation], quantity, package_no]
                    )
        self.assignment = pandas.DataFrame(
            package_assignment_list, columns=['sku', 'quantity', 'packageNo']
        )
        self.assignment.sort_values(
            by='packageNo', ignore_index=True, inplace=True
        )
        return self.assignment

    def split_package_list_into_parts(self, path: str) -> tuple:
        """
        Read the package list file and split it into the header section and
        the package list section.

        This routine only works with the .tsv package list format.

        Parameters:
            path                [str/Path]  -   File path to the downloaded
                                                package list from Amazon

        Returns:
                                [tuple]     -   List of rows from the header
                                                section and a dataframe for
                                                package list table
        """

        with open(path, mode='r', encoding='ISO-8859-1') as fileobj:
            self.header_section = [
                fileobj.readline().strip('\n').split('\t') for _ in range(9)
            ]
        self.package_list = pandas.read_csv(
            path, sep='\t', encoding='ISO-8859-1', header=7, na_filter=False
        )
        return (self.header_section, self.package_list)

    def fill_out_packagelist(self) -> bool:
        """
        Fill out the Amazon package assignment form with the package assignment
        from the Plentymarkets order.
        """
        self.package_list.index = pandas.Index(
            self.package_list['Merchant SKU'], dtype=str
        )
        for package in self.assignment.groupby('packageNo'):
            package_no = package[0]
            try:
                quantity_column = [
                    col for col in self.package_list.columns
                    if col.endswith(f'{package_no} - Unit Quantity')
                ][0]
            except IndexError as err:
                raise RuntimeError(
                    'Invalid package list template, Package number '
                    f'{package_no} is not listed. Please recreate the package '
                    'list with the correct amount of packages.'
                ) from err
            package_df = package[1]
            package_df.index = pandas.Index(package_df['sku'], dtype=str)
            self.package_list[quantity_column] = package_df['quantity']

        self.__validate_package_list()
        self.package_list.fillna(0, inplace=True)
        last_column = self.package_list.columns[-1]
        self.package_list.loc[:, 'Shipped':last_column] =\
            self.package_list.loc[:, 'Shipped':last_column].apply(
                pandas.to_numeric, errors='ignore', downcast='integer'
            )
        self.package_list.reset_index(drop=True, inplace=True)
        return True

    def __validate_package_list(self) -> None:
        """
        Check if the package list is valid, by comparing the announced quantity
        with the packaged quantity.
        """
        diff_df = self.package_list['Shipped'].to_frame(
            name='announced_quantity'
        )
        diff_df['packaged_quantity'] = pandas.to_numeric(
            self.package_list.filter(like='Unit Quantity').sum(axis=1)
        ).fillna(0)
        diff_df['required_difference'] = diff_df['announced_quantity'] -\
            diff_df['packaged_quantity']
        if not (diff_df['required_difference'] == 0).all():
            diff_df.index = self.package_list['Merchant SKU']
            diff_df = diff_df[diff_df['required_difference'] != 0]
            raise InsufficientPackagesError(error_df=diff_df)

    def join_package_list_parts(self) -> pandas.DataFrame:
        """ Add the extracted header back to the filled out package list """
        header_frame = pandas.DataFrame(
            [
                row + ['' for _ in range(
                    len(self.package_list.columns) - len(row))
                ] for row in self.header_section],
            columns=self.package_list.columns
        )
        self.package_list = header_frame.append(self.package_list,
                                                ignore_index=True)
        return self.package_list

    def create_full_view_packagelist(self) -> pandas.DataFrame:
        """
        Create a table of variations showing the total quantity of the
        variation spread upon all packages.
        """
        total_packages = self.assignment['packageNo'].max()
        variations = {
            sku: [0 for _ in range(total_packages)]
            for sku in self.assignment['sku'].unique()
        }

        for package in self.assignment.iterrows():
            package_df = package[1]
            package_no = package_df['packageNo'] - 1
            variations[package_df['sku']][package_no] = package_df['quantity']

        dataframe = pandas.DataFrame.from_dict(variations, orient='index')
        dataframe.columns = [f'package_{num + 1}' for num in range(total_packages)]
        dataframe.insert(0, 'sku', dataframe.index)
        dataframe.sort_values(by='sku', inplace=True)
        dataframe.reset_index(drop=True, inplace=True)
        return dataframe


class CacheHandler:
    """
    Service to cache the package assignment from the REST API order or the SKU
    mapping from Plentymarkets, in case it is already present during the
    inbound plan creation.
    """
    def __init__(self):
        self.temp_dir = self.__locate_existing_dir()
        if not self.temp_dir:
            self.temp_dir = pathlib.Path(
                tempfile.mkdtemp(prefix='plenty_rait_inbound_orders_')
            )

    @staticmethod
    def __locate_existing_dir():
        """ Try to reuse an existing directory if possible """
        try:
            return [
                p for p in pathlib.Path(tempfile.gettempdir()).iterdir()
                if p.is_dir() and p.match('*/plenty_rait_inbound_orders_*')
            ][0]
        except IndexError:
            return None

    def cache_order_response(
        self, order_id: int, order_response: dict
    ) -> None:
        """ Serialize and cache the order response in a temporary file """
        full_path = self.temp_dir / f'{order_id}_order_response.pkl'
        with open(full_path, mode="wb") as f:
            pickle.dump(order_response, f, pickle.HIGHEST_PROTOCOL)

    def read_order_response(self, order_id: int) -> dict:
        """ Deserialize and read the Plentymarkets REST API order response"""
        order_response = {}
        full_path = self.temp_dir / f'{order_id}_order_response.pkl'
        # Do not handle the error here, the caller is expected to handle it.
        with open(full_path, mode="rb") as f:
            order_response = pickle.load(f)
        return order_response

    def cache_order_packages(
        self, order_id: int, dataframe: pandas.DataFrame
    ) -> None:
        """ Serialize and cache the package assignment in a temporary file """
        full_path = self.temp_dir / f'{order_id}_packages.pkl'
        dataframe.to_pickle(full_path)

    def read_order_packages(self, order_id: int) -> pandas.DataFrame:
        """ Deserialize and read the package assignment """
        full_path = self.temp_dir / f'{order_id}_packages.pkl'
        # Do not handle the error here, the caller is expected to handle it.
        return pandas.read_pickle(full_path)

    def cache_sku_mapping(self, order_id: int, sku_mapping: dict) -> None:
        """
        Serialize and cache the SKU to variation ID mapping in a temporary file
        """
        full_path = self.temp_dir / f'{order_id}_sku_mapping.pkl'
        with open(full_path, mode="wb") as f:
            pickle.dump(sku_mapping, f, pickle.HIGHEST_PROTOCOL)

    def read_sku_mapping(self, order_id: int) -> dict:
        """ Deserialize and read the SKU to variation ID mapping """
        sku_mapping = {}
        full_path = self.temp_dir / f'{order_id}_sku_mapping.pkl'
        # Do not handle the error here, the caller is expected to handle it.
        with open(full_path, mode="rb") as f:
            sku_mapping = pickle.load(f)
        return sku_mapping


class DatabaseService(BaseDatabaseService):
    """ Service for getting the identification mapping from the database """
    def __init__(self, session=None):
        super().__init__(session)

    def get_sku_mapping(self, response: dict) -> dict:
        """
        Map the SKU stored as 'variation number' in Plentymarkets to the
        Plentymarkets Variation ID.
        """
        variation_ids = [
            item['itemVariationId'] for item in response['orderItems']
        ]
        return {
            getattr(row, 'variation_id'): getattr(row, 'sku')
            for row in self.session.query(
                IdentificationModel.variation_id, IdentificationModel.sku
            ).filter(
                IdentificationModel.variation_id.in_(variation_ids)
            )
        }


class Interface:
    """ Interface to the services of the inbound shipment tool """
    def __init__(
        self, connection_string: str, output_folder: pathlib.Path,
        receiver_country: str = '', sender_index: int = 1
    ) -> None:
        self.config_handler = InboundShipmentConfigHandler(
            name='inbound_shipment', config_type='json',
            config_schema=inbound_shipment_configuration_schema
        )
        config = self.config_handler.config
        self.output_folder = output_folder
        if receiver_country:
            destination = receiver_country
        elif config.get('default_receiver'):
            destination = config['default_receiver']
        else:
            raise RuntimeError(
                'No destination picked for the inbound shipment'
            )
        self._order_fetcher = OrderFetcher()
        try:
            sender = config['sender_addresses'][sender_index - 1]
        except IndexError as err:
            raise RuntimeError(
                f'No sender address found with index {sender_index} in the '
                'configuration'
            ) from err

        self._inbound_plan_creator = InboundPlanCreator(
            sender_config=sender, receiver_country=destination
        )
        self._package_list_filler = PackageListFiller()
        self._cache_handler = CacheHandler()
        self._database = DatabaseService()
        self._database.create_database_session(
            connection_string=connection_string
        )

    def create_inbound_plan(self, order_id: int) -> bool:
        """
        Create a new inbound plan to announce a shipment to Amazon from
        a Plentymarkets order.

        Parameters:
            order_id            [int]       -   ID of the redistribution on
                                                Plentymarkets

        Returns:
                                [bool]
        """
        (order_response, assignment, sku_mapping) =\
            self.__get_internal_data(order_id=order_id, overwrite=True)
        inbound_plan = self._inbound_plan_creator.create_inbound_plan(
            order_response=order_response, sku_mapping=sku_mapping
        )
        if len(inbound_plan.index) == 0:
            return False

        errors = self._inbound_plan_creator.validate_with_package_list(
            inbound_plan=inbound_plan, assignment=assignment
        )
        if len(errors.index) > 0:
            with pandas.option_context("display.max_rows", None):
                logger.error(f'Not all announced quantities have been packaged.\n{errors}')
            return False

        id_date = f'{order_id}_{datetime.date.today()}'
        path = self.output_folder / f'{id_date}.csv'
        inbound_plan.to_csv(path, sep='\t', index=False, header=False)
        return True

    def fill_package_list(self, order_id: int, path: pathlib.Path) -> bool:
        """
        Fill out the Amazon package assignment form with the package assignment
        from the Plentymarkets order.

        Parameters:
            order_id            [int]       -   ID of the redistribution on
                                                Plentymarkets
            path                [Path]      -   File path to read the package
                                                list from, the file will be
                                                overwritten.

        Returns:
                                [bool]
        """
        _, assignment, _ = self.__get_internal_data(order_id=order_id)
        if len(assignment.index) == 0:
            logger.error(
                f'No package assignment found within order {order_id}.'
            )
            return False
        self._package_list_filler.split_package_list_into_parts(path=path)
        self._package_list_filler.fill_out_packagelist()
        package_list = self._package_list_filler.join_package_list_parts()
        package_list.to_csv(path, sep='\t', index=False, header=False)
        return True

    def create_package_summary_csv(self, order_id: int) -> bool:
        """
        Create a CSV containing each variation of a redistribution order with
        the packaged amount per package.

        Parameters:
            order_id            [int]       -   ID of the redistribution on
                                                Plentymarkets

        Returns:
                                [bool]
        """
        (_, assignment, _) =\
            self.__get_internal_data(order_id=order_id)
        if len(assignment.index) == 0:
            logger.error(
                f'No package assignment found within order {order_id}.'
            )
            return False
        dataframe = self._package_list_filler.create_full_view_packagelist()
        if len(dataframe.index) == 0:
            return False
        package_columns = dataframe.columns[1:]
        dataframe.insert(
            loc=1, column='total',
            value=dataframe[package_columns].sum(axis=1)
        )
        file_location = self.output_folder / f'full_view_order_{order_id}.csv'
        dataframe.to_csv(file_location, sep=';', index=False, header=True)
        logger.info(f"File created at {file_location}")
        return True

    def __get_internal_data(
        self, order_id: int, overwrite: bool = False
    ) -> Tuple[dict, pandas.DataFrame, dict]:
        """
        Either read the package assignment from a serialized cache file or
        read it from the REST API.

        Parameters:
            order_id            [int]       -   ID of the redistribution on
                                                Plentymarkets
            overwrite           [bool]      -   Don't use cached data but force
                                                an overwrte of the cache

        Returns:
                                [tuple]     -   Returns the order response,
                                                package assignment for each
                                                SKU and the SKU to Plentymarkets
                                                variation ID mapping
        """
        assignment = pandas.DataFrame()
        order_response = {}
        sku_mapping = {}

        if not overwrite:
            try:
                order_response = self._cache_handler.read_order_response(
                    order_id=order_id
                )
            except FileNotFoundError:
                logger.info(

                    "No cached order response found for"
                    f" redistribution ID: {order_id}"
                    ", try to pull data from REST API"

                )
            try:
                assignment = self._cache_handler.read_order_packages(
                    order_id=order_id
                )
            except FileNotFoundError:
                logger.info(

                    "No cached package assignment data found for"
                    f" redistribution ID: {order_id}"
                    ", try to pull data from REST API"

                )
            try:
                sku_mapping = self._cache_handler.read_sku_mapping(
                    order_id=order_id
                )
            except FileNotFoundError:
                logger.info(
                    "No cached SKU mapping data found for"
                    f" redistribution ID: {order_id}"
                    ", try to pull data from REST API"
                )
            if order_response and len(assignment.index) > 0 and sku_mapping:
                self._package_list_filler.assignment = assignment
                return (order_response, assignment, sku_mapping)
        order_response = self._order_fetcher.pull_order(order_id=order_id)
        if not order_response:
            logger.error(
                f'No redistribution found matching order ID {order_id}.'
            )
            return ({}, pandas.DataFrame(), {})
        if isinstance(order_response, list):
            order_response = order_response[0]
        self._cache_handler.cache_order_response(
            order_id=order_id, order_response=order_response
        )
        sku_mapping = self._database.get_sku_mapping(response=order_response)
        assignment = self._package_list_filler.get_package_assignment(
            order_response=order_response, sku_mapping=sku_mapping
        )
        if len(sku_mapping) > 0:
            self._cache_handler.cache_sku_mapping(
                order_id=order_id, sku_mapping=sku_mapping
            )
        if len(assignment.index) > 0:
            self._cache_handler.cache_order_packages(
                order_id=order_id, dataframe=assignment
            )
        return (order_response, assignment, sku_mapping)
