# Amazon FBA inbound shipment

Tool for the generation of inbound plans to announce a shipment from your warehouse to an Amazon warehouse and for completing the package assignment form utilizing the package assignment from Plentymarkets. This service completes the workflow for redistributions from an internal warehouse to an external warehouse.


### General workflow
1. Generate up-to-date suggestions + sales & stock info using the "List update" function of the control panel
2. Choose the quantity for the desired variations within the spreadsheet
3. Generate a redistribution on Plentymarkets using the "Redistribution" function of the control panel
4. Pick & pack all variations using the Plentymarkets UI.
5. Generate an inbound plan using the "Amazon Inbound" function of the control panel
6. Upload the inbound plan on the Amazon Seller central
7. Create the shipment using the Amazon SellerCentral UI, download the package list as `.tsv` file
8. Fill the package list form using the "Amazon Inbound" function of the control panel
9. Upload the package list form to the Amazon SellerCentral UI, buy & print the shipment labels
10. Book all outgoing transactions within the redistribution order, set status and finish the redistribution

### Configuration

At a minimum this tool requires at least one sender address:
```json
{
    "sender_addresses": [
        {
            "name": "Test Seller",
            "street": "Test street number 15",
            "city": "Test-town",
            "country_code": "US",
            "state": "Texas",
            "postal_code": "12345"
        }
    ]
}
```

But you can add multiple addresses and switch between the addresses via the control panel.
Additionally, you are able to add a default destination country with the option `default_receiver` (The value must be a 2-letter country code in upper case letters, like `DE`, `GB`, `IT`, etc.).

### CLI usage

The service can also be used via the command line interface.

**Generate an inbound plan for a redistribution order on Plentymarkets with ID 12345**  
*Use the first sender address from the configuration by default and ship to the united kingdom, store the result at a custom path*
```bash
python3 -m plenty_rait InboundShipment Inbound --ship_to_country GB --custom_path /tmp/inbound_shipment.csv 12345
```

**Complete the package list assignment form for the Plentymarkets Order with ID 12345**
```bash
python3 -m plenty_rait InboundShipment PackageList --package_list_path /tmp/Package-Information-FBA123XXXXX.tsv --12345
```
