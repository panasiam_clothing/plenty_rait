"""
Import Amazon orders from a seller-central export to a MariaDB database.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import datetime
from decimal import Decimal
import pathlib
import re
import sys
from urllib.parse import urlparse
from loguru import logger
import pandas
import requests
from sqlalchemy.sql import case
from sqlalchemy.orm.session import Session
import tqdm

from plenty_rait.amazon_import.utils import (
    AmazonImportConfigHandler, convert_price
)
from plenty_rait.amazon_import import currency_rate
from plenty_rait.amazon_import.config_schema import (
    amazon_import_configuration_schema
)

from plenty_rait.helper.file_handling import get_encoding
from plenty_rait.helper.database_schema import (
    BaseDatabaseService, AmazonOrdersModel, AmazonOrderItemsModel, StockModel,
    IdentificationModel
)


def check_header(check: str, frame: pandas.DataFrame) -> bool:
    """
    Check that the export file has the correct header.

    Parameters:
        check                   [str]       -   Define the export file type to
                                                verify
        frame                   [DataFrame] -   Dataframe to check

    Return:
                                [bool]      -   boolean verifying if the header
                                                passed the check or not
    """
    if len(frame.index) == 0:
        return False

    columns = {
        'orders': [
            'amazon-order-id', 'order-status', 'purchase-date',
            'fulfillment-channel', 'sales-channel', 'asin',
            'item-price', 'quantity'
        ],
        'stock': [
            'Country', 'Currency code', 'ASIN', 'Total Units', 'Customer Order'
        ]
    }
    required_columns = columns[check]

    return all(column in frame.columns for column in required_columns)


def adjust_column_names(frame: pandas.DataFrame) -> pandas.DataFrame:
    """
    Remove hyphons from the column names as these are difficult to handle
    within named tuples

    Parameters:
        frame       [pandas.Dataframe]  -   Dataframe where columns are to
                                            be adjusted

    Return:
        frame       [pandas.DataFrame]  -   Adjusted dataframe
    """
    frame.columns = frame.columns.str.replace('-', '')
    return frame


class DataCollector:
    @staticmethod
    def read_amazon_order_data(path: str) -> pandas.DataFrame:
        """
        Read out all amazon orders from the amazon export file and return them as
        a DataFrame

        Parameters:
            import_path [str]               -   path to the amazon export file

        Return:
            dataframe          [pandas.DataFrame]  -   Dataframe containing all the
                                                amazon order-data
        """
        encoding = get_encoding(path=path, logger=logger)
        dataframe = pandas.read_csv(path, sep='\t', header=0,
                                    na_values=[], dtype=str, encoding=encoding)
        if not check_header(check='orders', frame=dataframe):
            logger.error(f'Invalid file {path}')
            return pandas.DataFrame()
        dataframe.fillna('')

        dataframe = adjust_column_names(frame=dataframe)

        return dataframe

    @staticmethod
    def read_amazon_stock_data(import_path) -> pandas.DataFrame:
        """
        Read out the stock stored in amazon warehouses for each available
        variation.

        Parameters:
            import_path [str]               -   path to the amazon export file

        Return:
            dataframe          [pandas.DataFrame]  -   Dataframe containing
                                                amazon stock-data
        """
        if re.search('.csv$', import_path):
            encoding = get_encoding(path=import_path, logger=logger)
            dataframe = pandas.read_csv(import_path, sep=',', dtype=str,
                                        encoding=encoding)
        elif re.search('.txt$', import_path):
            encoding = get_encoding(path=import_path, logger=logger)
            dataframe = pandas.read_csv(import_path, sep='\t', dtype=str,
                                        encoding=encoding)
        else:
            logger.error('Amazon stock export expects a [.txt]/[.csv] file '
                         f'(got: {import_path})')
            return pandas.DataFrame()

        if not check_header(check='stock', frame=dataframe):
            logger.error(f'Invalid file {import_path}')
            return pandas.DataFrame()

        dataframe = dataframe.astype(
            {'Total Units': int, 'Customer Order': int}
        )

        dataframe = dataframe.groupby(
            ['Country', 'Currency code', 'ASIN'], as_index=False
        )[['Total Units', 'Customer Order']].sum()
        dataframe['sum'] = dataframe.loc[
            :, ['Total Units', 'Customer Order']
        ].sum(axis=1)
        dataframe = dataframe.groupby(
            ['Currency code', 'ASIN'], as_index=False
        )['sum'].max()

        return dataframe


class DataImport(BaseDatabaseService):
    def __init__(self, origin_map: dict, session: Session = None):
        self.session: Session
        super().__init__(session)
        self.origin_map = origin_map

    def cleanup_variation_ids(self) -> None:
        """
        Search and replace old variation IDs with their new counterparts.

        Search for variation IDs that were removed from the identification
        table (removed on Plentymarkets) and replace them with the current
        matching variation ID for the ASIN.
        """
        variations = dict(
            self.session.query(
                IdentificationModel.asin_all, IdentificationModel.variation_id
            )
        )
        self.session.query(
            AmazonOrderItemsModel
        ).filter(
            AmazonOrderItemsModel.variation_id != 0,
            AmazonOrderItemsModel.variation_id != -1,
            AmazonOrderItemsModel.variation_id.notin_(variations.values())
        ).update(
            {
                'variation_id': case(
                    variations,
                    value=AmazonOrderItemsModel.asin
                )
            }, synchronize_session=False
        )
        self.session.commit()

    def __find_origin(self, row: tuple) -> str:
        """
        Find the correct Plentymarkets ID for the given order referrer.

        The mapping of Plentymarkets ID to Amazon marketplace is done within
        the configuration file.

        Parameters:
            row         [tuple] -   Row from the pandas dataframe

        Return:
                        [str]   -   Plentymarkets order referrer ID
                                    or empty if no match is found.
        """
        if self.origin_map is None:
            logger.error('No origin map found, can not map orders to IDs.')
            sys.exit(1)
        fulfiller = 'FBA' if row.fulfillmentchannel == 'Amazon' else 'FBM'
        try:
            customer_class = 'B2B' if row.isbusinessorder == 'true' else 'B2C'
        except AttributeError:
            customer_class = 'B2C'
        sales_channel = row.saleschannel

        try:
            origin = self.origin_map[fulfiller][sales_channel][customer_class]
        except KeyError:
            return ''

        return origin

    def __search_order_item(self, dataframe) -> pandas.DataFrame:
        """
        Check if the given order_item item is already created within the
        database.

        Parameters:
            dataframe   [DataFrame] -   Dataframe containing amazon order data

        Return:
            dataframe   [DataFrame] -   Dataframe with extra column indicating
                                        the number of order_items found in DB
        """
        order_items = self.session.query(
            AmazonOrderItemsModel.amazon_id, AmazonOrderItemsModel.asin
        ).filter(
            AmazonOrderItemsModel.amazon_id.in_(dataframe['amazonorderid'])
        ).all()
        count_list = []
        for row in dataframe.itertuples():
            count = order_items.count((row.amazonorderid, row.asin))
            if count > 1:
                logger.warning('Order item with Amazon ID: '
                               f'{row.amazonorderid} and ASIN {row.asin} '
                               'exists multiple times within the '
                               'amazon_order_items database.')
            count_list.append(count)
        dataframe['match_item_table'] = count_list

        return dataframe

    def __search_order(self, dataframe) -> pandas.DataFrame:
        """
        Check if the given order is already created within the database.

        Parameters:
            dataframe   [DataFrame] -   Dataframe containing amazon order data

        Return:
            dataframe   [DataFrame] -   Dataframe with extra column indicating
                                        the number of orders found in DB
        """
        old_orders = self.session.query(AmazonOrdersModel).filter(
            AmazonOrdersModel.amazon_id.in_(dataframe['amazonorderid'])
        ).all()
        orders = [x.amazon_id for x in old_orders]
        count_list = []
        for row in dataframe.itertuples():
            count = orders.count(row.amazonorderid)
            if count > 1:
                logger.warning('Order with Amazon ID: {row.amazonorderid} '
                               'exists multiple times within the amazon_orders'
                               ' database.')
            count_list.append(count)
        dataframe['match_order_table'] = count_list

        return dataframe

    def __map_order_identification(self, dataframe) -> dict:
        """
        Map the asins of the amazon order data to the ean and variation_id in
        the identification table

        Parameters:
            dataframe   [DataFrame] -   Dataframe containing amazon order data

        Return:
                        [dict]      -   Dictionary mapping the asin to the
                                        ean and variation_id
        """
        id_map = {}
        for data_row in self.session.query(
            IdentificationModel.variation_id, IdentificationModel.ean,
            IdentificationModel.asin_all
        ).filter(IdentificationModel.asin_all.in_(dataframe['asin'])).all():
            id_map[data_row.asin_all] = {
                'ean': data_row.ean, 'variation_id': data_row.variation_id}

        return id_map

    def __create_order(self, dataframe: pandas.DataFrame) -> list:
        """
        Create a new order entry in the database with no price information.

        The price information is updated by processing the order items of the
        order.

        Parameters:
            row         [tuple]     -   Row from the pandas dataframe
            order_items [list]      -   List of all order_items already found
                                        in DB
            old_orders  [list]      -   List of all orders already found in DB

        Return:
                        [list]      -   List of all order entries added to the
                                        database
        """
        order_list = []
        dataframe = dataframe[dataframe['match_order_table'] == 0]
        dataframe.drop_duplicates(subset=['amazonorderid'], inplace=True)
        dataframe.reset_index(drop=True, inplace=True)
        for row in dataframe.itertuples():
            origin_id = self.__find_origin(row=row)
            if not origin_id:
                origin_id = -1
            try:
                # pytlint: disable=simplifiable-if-expression
                b2b = True if row.isbusinessorder == 'true' else False
            except AttributeError:
                b2b = False
            order = AmazonOrdersModel(
                amazon_id=row.amazonorderid,
                origin_id=origin_id,
                purchase_date=datetime.date.fromisoformat(
                    row.purchasedate[:-15]
                ),
                netto_total=0,
                gross_total=0,
                b2b=b2b
            )

            if not order:
                logger.error(f'Creation of order failed on row:\n{row}'
                             '\nABORT')
                sys.exit(1)
            order_list.append(order)
        return order_list

    @staticmethod
    def __append_order_items(id_map, dataframe, order_list) -> list:
        """
        Create all order items for the given set of orders.
        Calculate the price in Euro & update the price information of the order
        Create a list containing all new orders and order_items.

        Parameters:
            id_map       [dict]      -   Dictionary containing the ean and
                                        variation_id of each asin found in the
                                        identificatio table.
            dataframe   [DataFrame] -   DataFrame containing all the order data
            order_list  [list]      -   List of all new orders without the
                                        connected order items.

        Return:
                        [list]      -   List of all new orders with order_items
        """
        processed_items = []
        for order_index, order in tqdm.tqdm(enumerate(order_list),
                                            total=len(order_list)):
            sub_dataframe = dataframe.loc[
                (dataframe['amazonorderid'] == order.amazon_id)
            ]

            for index, row in sub_dataframe.iterrows():
                processed_items.append(index)
                (gross, netto) = convert_price(total=row['itemprice'],
                                               tax=row['itemtax'],
                                               currency=row['currency'],
                                               date=row['purchasedate'])
                if not gross:
                    # pylint: disable=invalid-string-quote
                    logger.warning("Invalid gross price value "
                                   f"[{row['itemprice']}], default to 0. Amazon"
                                   f" ID: {order.amazon_id}")
                    gross = 0
                order.netto_total += Decimal(netto)
                order.gross_total += Decimal(gross)
                try:
                    order_item = AmazonOrderItemsModel(
                        amazon_id=order.amazon_id,
                        origin_id=order.origin_id,
                        variation_id=id_map[row['asin']]['variation_id'],
                        ean=id_map[row['asin']]['ean'],
                        asin=row['asin'],
                        netto=netto,
                        gross=gross,
                        quantity=row['quantity']
                    )
                except KeyError:
                    # pylint: disable=invalid-string-quote
                    logger.warning(f"ASIN {row['asin']} not found in "
                                   "the Identification Table.")
                    continue
                order_list[order_index].order_items.append(order_item)
        try:
            assert len(processed_items) == len(dataframe.index)
        except AssertionError:
            indeces = [i for i in dataframe.index if i not in processed_items]
            logger.warning('New order items were found for orders which can '
                           'already be found in the database. Please check if '
                           'this is a problem.\n'
                           f'[processed_items({len(processed_items)}) & '
                           f'dataframe length({len(dataframe.index)})]\n'
                           f'Unprocessed rows:\n{dataframe.loc[indeces]}\n')

        return order_list

    def import_amazon_orders(self, dataframe: pandas.DataFrame) -> None:
        """
        Check if the orders of the amazon export file are already found in the
        database and if not create them.

        Parameters:
            dataframe           [DataFrame]     -   DataFrame containing the
                                                    order data to be imported
        """
        dataframe = dataframe[(dataframe['orderstatus'] != 'Cancelled') &
                              (dataframe['itemstatus'] != 'Cancelled')].copy()
        dataframe = self.__search_order_item(dataframe=dataframe)
        dataframe = self.__search_order(dataframe=dataframe)
        dataframe = dataframe[dataframe['match_item_table'] == 0]

        order_list = self.__create_order(dataframe=dataframe)

        id_map = self.__map_order_identification(dataframe=dataframe)
        orders = self.__append_order_items(id_map=id_map, dataframe=dataframe,
                                           order_list=order_list)

        self.session.add_all(orders)
        self.session.commit()

    def import_amazon_stock(
        self, dataframe: pandas.DataFrame, database_column: str, region: str
    ) -> None:
        """
        Import amazon stock data to database

        Parameter:
            dataframe           [DataFrame] -   dataframe containing amazon
                                                stock data
            database_column     [str]       -   database column name in the
                                                database table
            region              [str]       -   region of the amazon stock
        """
        asin_dict = {}
        db_eans = []
        # pylint: disable=pointless-string-statement
        '''
        Multiple countries are potentially grouped under one Amazon
        region (e.g. PAN-EU), these groups currently have the currency
        as a common denominator. This dictionary maps the valid region
        values from the CLI to the currency values. If you add a new
        region to the CLI, you have to extend this mapping.
        '''
        currency_dict = {'EU': 'EUR', 'UK': 'GBP', 'US': 'USD'}
        dataframe = dataframe[
            dataframe['Currency code'] == currency_dict[region]
        ]

        # Sum stock over all asins
        for row in dataframe.itertuples():
            if row.ASIN in asin_dict:
                asin_dict[row.ASIN] =\
                    asin_dict[row.ASIN] + row.sum
            else:
                asin_dict[row.ASIN] = row.sum

        # Map asin to variation_id and ean
        llist = []
        for data_row in self.session.query(
            IdentificationModel.asin_all,
            IdentificationModel.ean,
            IdentificationModel.variation_id
        ).filter(
            IdentificationModel.asin_all.in_(asin_dict.keys())
        ).all():
            llist.append(
                [data_row.variation_id, data_row.ean,
                 asin_dict[data_row.asin_all]])
        db_asin = [
            x for x, in self.session.query(IdentificationModel.asin_all)
        ]
        missing = [asin for asin in asin_dict if asin not in db_asin]
        if len(missing) > 0:
            logger.warning(
                'Identification table is not up-to-date!\nMissing ASINs, '
                'Amazon stock will not be imported because the items have no '
                'identification in Plentymarkets.\n'
                f'{missing}')

        dataframe = pandas.DataFrame(
            llist, columns=['variation_id', 'ean', database_column], dtype=str
        )
        dataframe = dataframe.astype(
            dtype={'variation_id': 'int32', 'ean': 'object',
                   database_column: 'int32'}
        )

        for data_row in self.session.query(StockModel).filter(
            StockModel.ean.notin_(dataframe['ean'])
        ).all():
            setattr(data_row, database_column, 0)

        for data_row in self.session.query(StockModel).filter(
            StockModel.ean.in_(dataframe['ean'])
        ).all():
            data_row.update_by_dataframe(dataframe=dataframe)
            db_eans.append(data_row.ean)

        for row in dataframe[~dataframe['ean'].isin(db_eans)].to_dict(
            orient='records'
        ):
            data_row = StockModel(variation_id=row['variation_id'])
            data_row.update_row(row=row)
            self.session.add(data_row)
        self.session.commit()


class Interface:
    def __init__(self, connection_string: str) -> None:
        self.config_handler = AmazonImportConfigHandler(
            name='amazon_import', config_type='json',
            config_schema=amazon_import_configuration_schema
        )
        self.collector = DataCollector()

        self.importer = DataImport(
            origin_map=self.config_handler.config['origin_map']
        )
        self.importer.create_database_session(
            connection_string=connection_string)

        self.__add_recent_exchange_rate()

    @staticmethod
    def __is_url(url):
        try:
            result = urlparse(url)
            return all([result.scheme, result.netloc])
        except ValueError:
            return False

    def __add_recent_exchange_rate(self) -> None:
        """
        Gather the most recent conversion rates for all configured currencies
        in the 'exchange_rate_currencies' section of the configuration.
        These are used to convert the sales price to euro for orders from
        the current year, as there is no yearly average for the current
        year available.

        WARNING: At the end of each year an average for the last year should
        be entered into the currency_rate module, to ensure properly converting
        prices for older exports.
        """
        current_year = datetime.datetime.now().strftime('%Y')
        base_url: str = self.config_handler.config['exchange_rate_base_url']
        if not self.__is_url(url=base_url):
            raise RuntimeError(
                f'Invalid base URL {base_url} must be a URL to the '
                'exchangerate-api'
            )
        for currency in self.config_handler.config['exchange_rate_currencies']:
            endpoint = f"{base_url}{currency}"
            rate = requests.get(endpoint)
            rate.raise_for_status()
            if not rate:
                logger.warning(f'No data received from: {endpoint}.')
            try:
                currency_rate.currency_rate[current_year][currency] =\
                    rate.json()['rates']['EUR']
            except KeyError as err:
                raise RuntimeError(
                    f'Update the currency conversion table within the currency'
                    '_rate.py file to contain an entry for the year '
                    f'{current_year}.'
                ) from err
            except Exception as err:
                logger.warning(
                    f'No data received from: {endpoint}. (Error: {err})'
                )

    def import_orders_from_export(self, path: pathlib.Path) -> bool:
        export = self.collector.read_amazon_order_data(path=str(path))
        if len(export.index) == 0:
            return False
        self.importer.import_amazon_orders(dataframe=export)
        return True

    def import_stock_from_export(
        self, path: pathlib.Path, region: str
    ) -> bool:
        export = self.collector.read_amazon_stock_data(
            import_path=str(path))
        if len(export.index) == 0:
            return False
        warehouses = self.config_handler.warehouse_handler.get(
            warehouse_type='amazon'
        )
        warehouse = [
            warehouse for warehouse in warehouses
            if warehouse['attributes']['amazon_region'] == region
        ]
        if len(warehouse) < 1:
            logger.error(
                f'No warehouse configured for region {region}, skip stock sync'
            )
            return True
        if len(warehouse) > 1:
            logger.warning(
                f'Multiple configured warehouses match region: {region}. '
                'Default to the first match.'
            )
        database_column = warehouse[0]['database_column_name']
        self.importer.import_amazon_stock(
            dataframe=export, database_column=database_column, region=region
        )
        return True
