# Import Amazon orders (Fulfillment by merchant and amazon) to a MariaDB database

## Installation

Set up the configuration, the folder is automatically created when you run the program the first time.
Look at the [configuration](#config) section of this description for advice.
You can use the `plenty_sync` application for the creation and preparation of the `identification` database table.

Before you can run the program, you will need to install all dependencies to ensure functionality.

```bash
# Clone the the project from GitLab to your computer.
git clone https://gitlab.com/initBasti/plenty_rait.git
# Navigate to the amazon_import directory.
cd plenty_rait/amazon_import
# Install all dependencies
poetry build
tar -xf dist/amazon_import-0.1.1.tar.gz -C ~/
python3 ~/amazon_import-0.1.1/setup.py install
python3 -m plenty_rait AmazonImport -h
```

## Configuration <a name="config">

The application requires information in order to work properly, you will have to provide the host and the target database like this within the `database_config.ini` file in your `.config/plenty_rait_tool/` folder:

```ini
[DATABASE]
host=localhost
database=mydb
```

All other configurations should be placed into the `.config/plenty_rait_tool/amazon_import/config.ini` file.

### Basic configuration

As this tool is created with a close relationship to Plentymarkets, you have to provide a way for ASINs to be mapped to Plentymarkets Variation IDs.
Create a new table in the target database which contains rows, that maps a variation ID to a SKU and to a ASIN (I use an ASIN, that is mapped for all marketplaces).
The table should have the following columns: `variation_id`, `sku`, `ean`, `asin_all`. The `plenty_sync` tool contains an option to create such a table.

### Order-origins configuration

The marketplaces have to be mapped with the origin IDs from Plentymarkets, you can locate those values in your Plentymarkets system under: Setup->Orders->order referrer.
Example:
```json
{
    "origin_map": {
        "FBM": {
            "Amazon.de": {"B2C": 4.01, "B2B": 4.21},
            "Amazon.co.uk": {"B2C": 4.02, "B2B": 4.22},
            "Amazon.com": {"B2C": 4.03},
            "Amazon.fr": {"B2C": 4.04, "B2B": 4.24},
            "Amazon.it": {"B2C": 4.05, "B2B": 4.25},
            "Amazon.es": {"B2C": 4.06, "B2B": 4.26},
            "Amazon.nl": {"B2C": 4.09},
            "Amazon.pl": {"B2C": 4.11},
            "Amazon.se": {"B2C": 4.12}
        },
        "FBA": {
            "Amazon.de": {"B2C": 104.01, "B2B": 104.21},
            "Amazon.co.uk": {"B2C": 104.02, "B2B": 104.22},
            "Amazon.com": {"B2C": 104.03},
            "Amazon.fr": {"B2C": 104.04, "B2B": 104.24},
            "Amazon.it": {"B2C": 104.05, "B2B": 104.25},
            "Amazon.es": {"B2C": 104.06, "B2B": 104.26},
            "Amazon.nl": {"B2C": 104.09},
            "Amazon.pl": {"B2C": 104.11},
            "Amazon.se": {"B2C": 104.12}
        }
    }
}
```

### Currency-conversion configuration

In order to get the most recent currency exchange rate, there is the possibility to add links from the [exchangerate-api](https://exchangerate-api.com).

```json
{
    "exchange_rate_base_url": "https://api.exchangerate-api.com/v4/latest/",
    "exchange_rate_currencies": ["GBP", "USD", "SEK", "PLN"],
}
```

#### WARNING

The calculated Euro prices are not 100% accurate as the rate changes frequently, the values should be an approximate summary and should **never** be used for any official billing, where the amount must be correct.

## Logging

The application logs by default to a standardized location on your platform. On Linux this will usually be: `~/.cache/amazon_import/log`. The file will grow to a maximum of 2 MB, at which point it will start to rotate out old entries.

## Usage

To import amazon orders into the database, go to your [seller central](https://sellercentral.amazon.com):
Download the export, Reports -> Fulfillment by Amazon -> [Sales] All Orders

```bash
python3 -m plenty_rait AmazonImport Order -f amazon_order_export.txt
```

To import amazon FBA inventory into the database, go to your [seller central](https.//sellercentral.amazon.com):
Download the export, Reports -> Fulfillment by Amazon -> [Inventory] Manage FBA Inventory

```bash
# Import all entries related to the EU region from the amazon_stock_export.txt
# file into the database to current_stock table into the column specified in the
# Configuration under Stocks
python3 -m plenty_rait AmazonImport Stock -f amazon_stock_export.txt --region EU
```
