# pylint: disable=invalid-string-quote, line-too-long
amazon_import_configuration_schema = {
    "title": "Amazon import configuration schema",
    "type": "object",
    "required": [
        "exchange_rate_currencies",
        "origin_map"
    ],
    "properties": {
        "exchange_rate_base_url": {
            "type": "string",
            "title": "Exchange rates base url",
            "default": "https://api.exchangerate-api.com/v4/latest/",
            "pattern": "^https://.*$"
        },
        "exchange_rate_currencies": {
            "title": "Exchange_rate_currencies",
            "type": "array",
            "items": {
                "title": "Currencies",
                "description": "3 letter currency codes",
                "type": "string",
                "pattern": "^[A-Z]{3}$"
            }
        },
        "origin_map": {
            "title": "Mapping of Plentymarkets origins",
            "description":
            "Map plentymarket IDs to specific Amazon fulfillment types",
            "type": "object",
            "properties": {
                "FBM": {
                    "title": "Fulfillment by Merchant",
                    "type": "object",
                    "properties": {
                        "Amazon.de": {
                            "title": "Amazon.de",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2C",
                                    "type": "number",
                                    "default": 4.01
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 4.21
                                }
                            }
                        },
                        "Amazon.co.uk": {
                            "title": "Amazon.co.uk",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2C",
                                    "type": "number",
                                    "default": 4.02
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 4.22
                                }
                            }
                        },
                        "Amazon.com": {
                            "title": "Amazon.com",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 4.03
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 4.23
                                }
                            }
                        },
                        "Amazon.fr": {
                            "title": "Amazon.fr",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 4.04
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 4.24
                                }
                            }
                        },
                        "Amazon.it": {
                            "title": "Amazon.it",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 4.05
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 4.25
                                }
                            }
                        },
                        "Amazon.es": {
                            "title": "Amazon.es",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 4.06
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 4.26
                                }
                            }
                        },
                        "Amazon.nl": {
                            "title": "Amazon.nl",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 4.09
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 4.29
                                }
                            }
                        },
                        "Amazon.pl": {
                            "title": "Amazon.pl",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 4.11
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 4.31
                                }
                            }
                        },
                        "Amazon.se": {
                            "title": "Amazon.se",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 4.12
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 4.32
                                }
                            }
                        }
                    }
                },
                "FBA": {
                    "title": "Fulfillment by Amazon",
                    "type": "object",
                    "properties": {
                        "Amazon.de": {
                            "title": "Amazon.de",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2C",
                                    "type": "number",
                                    "default": 104.01
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 104.21
                                }
                            }
                        },
                        "Amazon.co.uk": {
                            "title": "Amazon.co.uk",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2C",
                                    "type": "number",
                                    "default": 104.02
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 104.22
                                }
                            }
                        },
                        "Amazon.com": {
                            "title": "Amazon.com",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 104.03
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 104.23
                                }
                            }
                        },
                        "Amazon.fr": {
                            "title": "Amazon.fr",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 104.04
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 104.24
                                }
                            }
                        },
                        "Amazon.it": {
                            "title": "Amazon.it",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 104.05
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 104.25
                                }
                            }
                        },
                        "Amazon.es": {
                            "title": "Amazon.es",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 104.06
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 104.26
                                }
                            }
                        },
                        "Amazon.nl": {
                            "title": "Amazon.nl",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 104.09
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 104.29
                                }
                            }
                        },
                        "Amazon.pl": {
                            "title": "Amazon.pl",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 104.11
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 104.31
                                }
                            }
                        },
                        "Amazon.se": {
                            "title": "Amazon.se",
                            "type": "object",
                            "properties": {
                                "B2C": {
                                    "title": "B2c",
                                    "type": "number",
                                    "default": 104.12
                                },
                                "B2B": {
                                    "title": "B2b",
                                    "type": "number",
                                    "default": 104.32
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
