"""
Import Amazon orders from a seller-central export to a MariaDB database.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import datetime
from decimal import Decimal, InvalidOperation
import math
import re
from loguru import logger

from plenty_rait.amazon_import.currency_rate import currency_rate
from plenty_rait.helper.config_helper import ConfigHandler


class AmazonImportConfigHandler(ConfigHandler):
    def __init__(self, name: str, config_type: str, config_schema: dict):
        super().__init__(name=name, config_type=config_type,
                         config_schema=config_schema)


def transform_date(amazon_date: str) -> datetime.datetime:
    """Convert a date string from the Amazon export into a datetime object."""
    date: datetime.datetime = None
    if not amazon_date:
        return None

    if len(amazon_date) < 21:
        return None

    date_string = re.sub(r'([-+]\d{2}):(\d{2})$', r'\1\2', amazon_date)
    try:
        date = datetime.datetime.strptime(date_string, '%Y-%m-%dT%H:%M:%S%z')
    except ValueError:
        return None
    return date


def convert_to_euro(price: str, currency: str, con_table: dict) -> str:
    """
    Convert a non-Euro price to a Euro price with help of a conversion table.

    Parameters:
        price           [str]   -   original price from the Amazon export
        currency        [str]   -   currency abbreviation
        con_table       [dict]  -   Predefined conversion table with exchange
                                    rates of multiple currencies.
    Return:
                        [str]   -   Converted price as a string
    """
    if not price or not currency or not con_table:
        return ''
    if currency not in con_table.keys():
        return ''
    conversion = con_table[currency]
    return str(round(float(price) * conversion, 2))


def get_netto(total: str, tax: str) -> str:
    """
    Get the netto portion of an Amazon-export gross price with the tax portion.

    Parameters:
        total           [str]   -   Gross price from the Amazon export
        tax             [str]   -   Tax portion of the gross price

    Return:
                        [str]   -   Netto portion of the gross price
    """
    if not total or not tax:
        return ''
    try:
        if Decimal(total) <= 0 or Decimal(tax) < 0:
            return ''
    except InvalidOperation:
        return ''
    perc = (Decimal(tax) / Decimal(total)) * 100
    if perc < 5 or perc > 30:
        print(f'WARNING: Invalid tax rate: total {total} tax {tax} ({perc} %)')
        return ''
    return str(Decimal(total) - Decimal(tax))


def convert_price(total: str, tax: str, currency: str, date: str) -> tuple:
    """
    Convert the total and tax prices from the Amazon report to netto and
    gross in euro.

    Parameter:
        total           [str]   -   the absolute price from the amazon
                                    export field: item-price
        tax             [str]   -   the tax amount of the absolute price
                                    from the amazon export field:
                                    item-tax
        currency        [str]   -   name of the currency
        date            [str]   -   purchase date in the ISO 8601 format

    Return:
        (gross, netto)  [tuple]
    """
    try:
        currency_rate_for_year = currency_rate[date[0:4]]
    except KeyError:
        logger.error(f'No currency conversion rate for year {date[0:4]}')
        return (-1, -1)

    euro_total = convert_to_euro(price=total, currency=currency,
                                 con_table=currency_rate_for_year)
    if not tax or math.isnan(float(tax)):
        euro_tax = 0
    else:
        euro_tax = convert_to_euro(price=tax, currency=currency,
                                   con_table=currency_rate_for_year)
    netto = get_netto(total=euro_total, tax=euro_tax)
    if not netto:
        netto = 0
    return (euro_total, netto)


def validate_row(row: tuple) -> bool:
    """
    Filter out orders that were cancelled or not coming from amazon.

    Parameter:
        row             [named_tuple]   -   Row from the pandas DataFrame

    Return:
                        [bool]          -   True if none of the conditions
                                            match
    """
    if row.orderstatus == 'Cancelled' or row.saleschannel == 'Non-Amazon':
        return False
    return True
