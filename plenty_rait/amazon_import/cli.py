"""
Import Amazon orders from a seller-central export to a MariaDB database.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse
import pathlib
from loguru import logger


from plenty_rait.amazon_import.service import Interface
from plenty_rait.helper.setup_helper import CommandLineSetup


def setup_order_parser(subparser):
    order_parser = subparser.add_parser(
        'Order', help='Get all orders from the Amazon order file and write '
                      'them into the database')
    order_parser.add_argument('--file', '-f', '--order_export', required=True,
                              help='Export from Amazon containing the orders '
                                   'and order items to be imported.',
                              dest='amazon_import_order_export',
                              type=pathlib.Path)


def setup_stock_parser(subparser):
    stock_parser = subparser.add_parser(
        'Stock', help='Get the stock from the amazon stock file and write it'
                      ' into the database')
    stock_parser.add_argument('--file', '-f', '--stock_export', required=True,
                              help='Export from Amazon containing the '
                                   'stock-data of a Amazon-warehouse',
                              dest='amazon_import_stock_export',
                              type=pathlib.Path)
    stock_parser.add_argument('--amazon_warehouse_region', '--region', '-r',
                              required=True,
                              help='Select the warehouse-region for your '
                                   'amazon_stock_export_file',
                              dest='amazon_import_stock_region',
                              choices=['EU', 'UK', 'US'])


def setup_clean_parser(subparser):
    subparser.add_parser('Clean', help='Cleanup deleted variation IDs')


class AmazonImportCommandLine(CommandLineSetup):
    @staticmethod
    def setup_parser(subparser) -> None:
        parser = subparser.add_parser(
            'AmazonImport',
            help='Update the database with order and stock information from '
            'export files of the Amazon Sellercentral.')
        subparsers = parser.add_subparsers(
            title='Options for amazon_import', dest='amazon_import_command')
        setup_order_parser(subparser=subparsers)
        setup_stock_parser(subparser=subparsers)
        setup_clean_parser(subparser=subparsers)
        parser.add_argument('--create_tables', required=False,
                            help='Create the database tables for amazon orders'
                            ' and amazon order items.',
                            action='store_true', dest='amazon_import_tables')

    @staticmethod
    def validate_parser(args: argparse.Namespace) -> bool:
        if args.amazon_import_command == 'Order':
            if not args.amazon_import_order_export.exists():
                logger.error('No order export file found at '
                             f'{args.amazon_import_order_export}')
                return False
        if args.amazon_import_command == 'Stock':
            if not args.amazon_import_stock_export.exists():
                logger.error('No stock export file found at '
                             f'{args.amazon_import_stock_export}')
                return False
        return True

    @staticmethod
    def handle_command(args: argparse.Namespace,
                       connection_string: str) -> bool:
        interface = Interface(connection_string=connection_string)
        if args.amazon_import_tables:
            tables = ['AmazonOrdersModel', 'AmazonOrderItemsModel',
                      'StockModel']
            interface.importer.create_tables(tables=tables)
            if not args.amazon_import_command:
                return True

        if args.amazon_import_command == 'Clean':
            interface.importer.cleanup_variation_ids()

        if args.amazon_import_command == 'Order':
            interface.import_orders_from_export(
                path=args.amazon_import_order_export)

        if args.amazon_import_command == 'Stock':
            region = args.amazon_import_stock_region
            interface.import_stock_from_export(
                path=args.amazon_import_stock_export, region=region
            )
        return True
