"""
Summarize sales data from multiple database tables into daily and monthly
sales.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from collections import defaultdict
import configparser
import datetime
from decimal import Decimal
from pathlib import Path
import re
import sys
import tempfile
from typing import Dict, Union
from loguru import logger
import pandas
from sqlalchemy import func
from sqlalchemy.orm import Query
from sqlalchemy.orm.session import Session
from sqlalchemy.sql import case
from sqlalchemy.orm.exc import NoResultFound
import tqdm

from plenty_rait.helper.database_schema import (
    BaseDatabaseService, OrdersModel, OrderItemsModel, AmazonOrdersModel,
    AmazonOrderItemsModel, StatisticModel, DailyStatisticModel,
    IdentificationModel
)
from plenty_rait.helper.config_helper import init_config


def get_quantity(dataframe: pandas.DataFrame, variation: int,
                 date: datetime.date, target: str) -> int:
    """
    Get a specific entry from the total quantity dataframe.
    This returns 0 in case of no match, which indicates that there are no
    sales at given date for the variation at any of the origins from current
    custom origin. And -1 on multiple matches, which inidicates an error.
    """
    try:
        if target == 'monthly':
            quantity = dataframe.loc[
                (dataframe['variation_id'] == variation) &
                (dataframe['month'] == date.month) &
                (dataframe['year'] == date.year)
            ]['quantity']
        elif target == 'daily':
            quantity = dataframe.loc[
                (dataframe['variation_id'] == variation) &
                (dataframe['day'] == date.day) &
                (dataframe['month'] == date.month) &
                (dataframe['year'] == date.year)
            ]['quantity']
        if len(quantity.index) > 1:
            return -1
        return quantity.item()
    except ValueError:
        return 0


def check_index(data: dict, check: list) -> str:
    """ Get the index of a tuple within a list of tuples. """
    return str(data['check_list'].index(check))


class DataCollector(BaseDatabaseService):
    """
    Collect the order item entries from the amazon and plentymarkets order
    items.
    """
    def __init__(self, session=None, amazon_origin_prefix='4,104'):
        super().__init__(session)
        self.session: Session
        self.amazon_origin_prefix = amazon_origin_prefix
        self.target = 'monthly'
        self.statistic_model = StatisticModel

    def set_target_daily(self) -> None:
        self.target = 'daily'
        self.statistic_model = DailyStatisticModel

    def set_target_monthly(self) -> None:
        self.target = 'monthly'
        self.statistic_model = StatisticModel

    def __deprecated_ean_check(self, identifications: Dict[int, str]) -> None:
        logger.info("Check for deprecated EAN barcodes...")
        changed_ean_entries = self.session.query(self.statistic_model).filter(
            self.statistic_model.ean.notin_(identifications.values())
        )
        summary = defaultdict(dict)
        for entry in tqdm.tqdm(changed_ean_entries):
            if entry.ean in summary:
                summary[entry.ean]['amount'] += 1
                continue
            variation_id = entry.variation_id if entry.variation_id else -1
            if variation_id == -1:
                logger.warning(f"EAN {entry.ean} without a variation ID!")
            summary[entry.ean] = {'id': variation_id, 'amount': 1}
            if entry.variation_id in identifications:
                summary[entry.ean]['new_ean'] = identifications[entry.variation_id]
            else:
                summary[entry.ean]['new_ean'] = ''

        change_warnings = []
        orphan_warnings = []
        total = {'amount': 0, 'variations': 0, 'orphaned': 0, 'changed': 0}
        for ean, variation in sorted(summary.items(), key=lambda x: x[1]['id']):
            total['variations'] += 1
            total['amount'] += variation['amount']
            if variation['new_ean']:
                total['changed'] += 1
                change_warnings.append(
                    f"EAN change: variation ID {variation['id']}"
                    f" ({ean} -> {variation['new_ean']}) (Amount in "
                    f"{self.target} statistics: {variation['amount']})\n"
                )
            else:
                total['orphaned'] += 1
                orphan_warnings.append(
                    f"Orphaned: variation ID {variation['id']}, EAN "
                    f"{ean} (Amount in {self.target} statistics: "
                    f"{variation['amount']})\n"
                )
        log_path = Path(tempfile.gettempdir()) /\
            f'deprecated_ean_plenty_rait_{self.target}.log'
        with open(log_path, 'w') as log_file:
            for warning in change_warnings:
                log_file.write(warning)
            for warning in orphan_warnings:
                log_file.write(warning)

        logger.info(
            f"*EAN problems found*:\nAt {total['variations']} variations\n"
            f"Total amount of ignored statistic entries: {total['amount']}\n"
            f"EAN barcodes changed: {total['changed']}\nStatistic entries "
            f"orphaned: {total['orphaned']}\nDetails written to {log_path}\n\n"
            "NOTE:\nTo connect the statistics of the old barcode with the "
            "statistics of the new one, adjust the barcode of the statistics"
            " in the database manually.\n"
        )

    def cleanup_statistics(self, print_summary: bool, ean_check: bool = False) -> None:
        """
        Replace deprecated variation IDs within monthly or daily statistics.

        The variation ID of a product can change, for example when a product
        was deleted and recreated. In such a case, the variation IDs of the
        statistic entries point to an invalid variation ID.
        The clean up of the database, has to be performed in a specific order:
        - Replace the variation ID on the deprecated identification table entry
        - Clean up the tables for the Plentymarkets and Amazon order items
        - Clean up the statistic entries

        This routine will try to replace deprecated variation IDs for
        daily/monthly/monthly total/monthly predicted entries.
        Before writing the changes to the database, a list of pending
        changes can be viewed and the changes can be declined by setting
        @print_summary to True.

        Parameters:
            print_summary[bool] -   Ask user to approve any pending changes
            ean_check   [bool]  -   Search through the database for statistic
                                    entries with deprecated EAN barcodes.
                                    This case will not be handled automatically,
                                    as it is unclear to the application wether the
                                    user would like to connect old statistics to
                                    a new EAN barcode or not.
        """

        variations = {
            x.variation_id: x.ean for x in
            self.session.query(
                IdentificationModel.variation_id, IdentificationModel.ean
            )
        }

        if ean_check:
            self.__deprecated_ean_check(identifications=variations)
        stats_entries = self.session.query(self.statistic_model).filter(
            self.statistic_model.variation_id.notin_(variations.keys()))

        if stats_entries.count() == 0:
            logger.info(f'No clean up required for {self.target} statistics.')
            return
        else:
            logger.info(
                f'Clean up required for {stats_entries.count()} entries on '
                f'{self.target} statistics.'
            )

        summary = []
        ean_dict = {}
        variation_dict = {}

        deleted_variations = []
        for entry in stats_entries:
            if entry.ean in deleted_variations:
                continue
            ean_dict[str(entry.variation_id)] = entry.ean
            old_var_id = entry.variation_id
            try:
                new_var_id = self.session.query(
                        IdentificationModel
                    ).filter(IdentificationModel.ean == entry.ean).one()
            except NoResultFound:
                deleted_variations.append(entry.ean)
                continue
            new_var_id = new_var_id.variation_id
            variation_dict[old_var_id] = new_var_id
            summary.append({
                'id': entry.id,
                'old': old_var_id,
                'new': new_var_id
            })

        if not variation_dict:
            return
        self.session.query(self.statistic_model).filter(
            self.statistic_model.variation_id.in_(ean_dict.keys())
        ).update(
            {
                'variation_id':
                case(variation_dict, value=self.statistic_model.variation_id)
            }, synchronize_session=False
        )

        if not print_summary:
            self.session.commit()
            return

        logger.info('Summary of pending changes')
        # pylint: disable=invalid-string-quote
        for entry in summary:
            logger.info(f"Database table: {self.statistic_model} id: "
                        f"{entry['id']} ({entry['old']} -> {entry['new']})")

        accept = ''
        while accept not in ('n', 'y'):
            accept = input(f'Write {len(summary)} changes to DB?(y/n)').lower()
            if accept == 'n':
                self.session.rollback()
                return
        self.session.commit()

    def get_latest_statistic_date(
        self, mode: str
    ) -> Union[datetime.date, None]:
        """
        Search for the statistics entry with the highest date.

        Parameters:
            mode                [str]       -   Either 'montly', 'daily' or
                                                'both', dictates which tables
                                                to lookup

        Returns:
                                [date/None] -   Return None if no entry is
                                                found for the given mode
        """
        daily_date = None
        monthly_date = None
        if mode in ['monthly', 'both']:
            monthly_date = self.session.query(
                func.max(StatisticModel.date_value)
            ).filter(
                StatisticModel.predicted.is_(False)
            ).one()[0]
        if mode in ['daily', 'both']:
            daily_date = self.session.query(
                func.max(DailyStatisticModel.date_value)
            ).one()[0]
        if all(
            isinstance(x, datetime.date) for x in [daily_date, monthly_date]
        ):
            return max(daily_date, monthly_date)
        return daily_date if daily_date else monthly_date

    def __get_order_item_combinations(self, model, order_ids: list,
                                      variations: list) -> dict:
        """
        Create a mapping and count the occurences for each order item
        combination.

        Parameters:
            model               [schema]    -   Schema of the given order
                                                item table
            order_ids          [list]       -   List of specific marketplace
                                                order IDs
            variations          [list]      -   List of Plentymarkets
                                                variation IDs
        Return:
                                [dict]      -   @order_dict with additional
                                                entries for each new
                                                combination
        """
        if model == OrderItemsModel:
            order_id_field = 'external_id'
        elif model == AmazonOrderItemsModel:
            order_id_field = 'amazon_id'

        query = self.session.query(
            model.id,
            getattr(model, order_id_field).label('market_order_id'),
            model.variation_id
        ).filter(
            getattr(model, order_id_field).in_(order_ids),
            model.variation_id.in_(variations)).all()

        keys = [(f'{x.market_order_id};{x.variation_id}', x.id)
                for x in query]
        dict_keys = [x[0] for x in keys]
        order_dict = {}
        for key, item_id in keys:
            if key in order_dict:
                continue
            order_dict[key] = {'id': item_id, 'amount': dict_keys.count(key)}
        return order_dict

    def __find_duplicate(self, amazon_orders: dict) -> dict:
        """
        Check if an order was created on both the orders as well as the
        amazon orders table.

        Parameter:
            amazon_orders       [dict]  :   Dictionary of the Amazon order
                                            numbers and Plentymarkets
                                            variation IDs from order
                                            items pulled from
                                            Plentymarkets.

        Return:
                                [dict]  :   IDs of matching order items from
                                            both the `order_items` and the
                                            `amazon_order_items` tables, mapped
                                            to a union of the amazon order
                                            number and the variation ID.
        """
        # pylint: disable=invalid-string-quote
        external_id = [x['external_id'] for x in amazon_orders]
        variation_id = [x['variation_id'] for x in amazon_orders]

        amazon_order_dict = self.__get_order_item_combinations(
            model=AmazonOrderItemsModel, order_ids=external_id,
            variations=variation_id)
        order_dict = self.__get_order_item_combinations(
            model=OrderItemsModel, order_ids=external_id,
            variations=variation_id)

        duplicates = {}
        for row in amazon_orders:
            key = f"{row['external_id']};{row['variation_id']}"
            try:
                order_item = order_dict[key]
                amazon_item = amazon_order_dict[key]
            except KeyError:
                duplicates[key] = {'order_id': -1, 'amazon_id': -1}
                continue
            if order_item['amount'] > 1 or amazon_item['amount'] > 1:
                logger.warning("Duplicate order items within the same table "
                               f"for ID: {row['external_id']} , variation: "
                               f"{row['variation_id']} \namazon_items: "
                               f"{amazon_item['amount']}\norder_items: "
                               f"{order_item['amount']}")

            if order_item['amount'] == 0 or amazon_item['amount'] == 0:
                duplicates[key] = {'order_id': -1, 'amazon_id': -1}
                continue

            duplicates[key] = {'order_id': order_item['id'],
                               'amazon_id': amazon_item['id']}
        return duplicates

    def __is_amazon_order(self, origin_id: float) -> bool:
        """
        Check if the given Plentymarkets origin ID is associated with Amazon.

        Parameter:
            origin_id           [float]     -   Origin ID from Plentymarkets
                                                representing a marketplace

        Return:
                                [bool]      -   True if it is a amazon origin
                                                False if not
        """
        return int(origin_id) in [
            int(x) for x in self.amazon_origin_prefix.split(',')
        ]

    def __create_lookup_dictionary(self, stat_entries: list) -> dict:
        """
        Create a lookup dictionary for all existing statistic entries within a
        given date range for update and comparision.

        Parameters:
            stat_entries   [list]          -    List of existing statistic
                                                entries

        Return:
                            [dict]          -   Dictionary containing the
                                                statistic entries and a list of
                                                checks used for matching
                                                entries
        """
        check_dict = defaultdict(lambda: defaultdict(lambda: defaultdict(list)))
        for entry in stat_entries:
            check = [entry.variation_id, entry.origin_id]
            if self.target == 'daily':
                check.insert(2, entry.day)
            current_dict = check_dict[entry.year][entry.month]


            if check in current_dict['check_list']:
                logger.error(
                    f'Multiple {self.target} statistic entries found for '
                    f'variation ID: {entry.variation_id}, origin ID: '
                    f'{entry.origin_id}, date: {entry.date_obj}')
                sys.exit(1)

            current_dict['check_list'].append(check)
            current_dict[check_index(current_dict, check)] = entry

        return check_dict

    def __get_total_quantity(self, origin_list: list, start, end) -> int:
        """
        Collect the total sum over the given list of origins for every
        variation within the specified date range.

        Parameters:
            origin_list     [list]      -   List of strings containing the
                                            IDs of the origins, which are
                                            combined into one custom origin
            start           [date]      -   Start date of the date range
            end             [date]      -   End date of the date range

        Return:
                            [DataFrame] -   Data collection of the accumulated
                                            origin quantity sums
        """
        if self.target == 'monthly':
            query = self.session.query(
                StatisticModel.variation_id,
                StatisticModel.month, StatisticModel.year,
                func.sum(StatisticModel.quantity).label('quantity'),
                StatisticModel.ean
            ).filter(
                StatisticModel.origin_id.in_(origin_list),
                StatisticModel.date_value <= end,
                StatisticModel.date_value >= start,
                StatisticModel.predicted.is_(False)
            ).group_by(StatisticModel.variation_id,
                       StatisticModel.month,
                       StatisticModel.year)
        elif self.target == 'daily':
            query = self.session.query(
                DailyStatisticModel.variation_id, DailyStatisticModel.day,
                DailyStatisticModel.month, DailyStatisticModel.year,
                func.sum(DailyStatisticModel.quantity).label('quantity'),
                DailyStatisticModel.ean
            ).filter(
                DailyStatisticModel.origin_id.in_(origin_list),
                DailyStatisticModel.date_value <= end,
                DailyStatisticModel.date_value >= start
            ).group_by(DailyStatisticModel.variation_id,
                       DailyStatisticModel.year,
                       DailyStatisticModel.month,
                       DailyStatisticModel.day)
        dataframe = pandas.read_sql(query.statement, self.session.bind)
        dataframe = dataframe.astype(dtype={'quantity': int})
        return dataframe

    def __get_daily_statistics_entries_within_date_range(
            self, start: datetime.datetime, end: datetime.datetime,
            origin_list: list) -> Query:
        items = self.session.query(
            DailyStatisticModel
        ).filter(
            DailyStatisticModel.date_value <= end,
            DailyStatisticModel.date_value >= start,
            DailyStatisticModel.origin_id.in_(origin_list)
        )
        return items

    def __get_statistics_entries_within_date_range(
            self, start: datetime.datetime, end: datetime.datetime,
            origin_list: list) -> Query:

        items = self.session.query(
            StatisticModel
        ).filter(
            StatisticModel.predicted.is_(False),
            StatisticModel.origin_id.in_(origin_list),
            StatisticModel.date_value <= end,
            StatisticModel.date_value >= start
        )
        return items

    def __create_stat_entry_from_order(self, order_item, order_date):
        order_month = order_date.strftime('%m')
        order_year = order_date.strftime('%Y')
        if self.target == 'monthly':
            return self.__create_stat_entry(
                variation_id=order_item.variation_id,
                ean=order_item.ean,
                origin_id=order_item.origin_id,
                month=order_month, year=order_year,
                quantity=order_item.quantity
            )
        order_day = order_date.strftime('%d')
        return self.__create_stat_entry(
            variation_id=order_item.variation_id,
            ean=order_item.ean,
            origin_id=order_item.origin_id,
            day=order_day, month=order_month, year=order_year,
            quantity=order_item.quantity
        )

    def __create_stat_entry(self, variation_id: int, origin_id: Decimal,
                            month: int, year: int, quantity: int, ean: str,
                            day: int = 1):
        if self.target == 'monthly':
            return StatisticModel(
                variation_id=variation_id,
                ean=ean,
                origin_id=origin_id,
                month=month,
                year=year,
                quantity=quantity,
                predicted=False
            )
        return DailyStatisticModel(
            variation_id=variation_id,
            ean=ean,
            origin_id=origin_id,
            day=day,
            month=month,
            year=year,
            quantity=quantity
        )

    def __get_order_items(self, table: str,
                          start_date: datetime, end_date: datetime) -> Query:
        if table == 'orders':
            order_table = OrdersModel
            item_table = OrderItemsModel
            date_field = 'creation_date'
        elif table == 'amazon_orders':
            order_table = AmazonOrdersModel
            item_table = AmazonOrderItemsModel
            date_field = 'purchase_date'

        if self.target == 'daily':
            stats_field = 'daily_statistic_entry'
        elif self.target == 'monthly':
            stats_field = 'statistic_entry'

        return self.session.query(item_table).join(order_table).filter(
            getattr(order_table, date_field) >= start_date,
            getattr(order_table, date_field) <= end_date,
            getattr(item_table, stats_field) == -1,
            item_table.variation_id != -1,
            item_table.origin_id != -1
        )

    def __synchronize_statistic_assignment(self, entries: list,
                                           target: str) -> None:
        """
        Adjust the statistic entry assignment for Amazon orders in the
        amazon_order_item table for items that overlap in both tables.

        This step makes sure that we do not look at the same order item
        twice.

        Parameters:
            entries             [list]      -   List of order item and
                                                statistic entry combinations
                                                which have been created or
                                                adjusted in the current
                                                session.
        """
        amazon_orders = [
            {
                'variation_id': x['item'].variation_id,
                'external_id': x['item'].external_id,
                'id': x['stat'].id
            }
            for x in entries if self.__is_amazon_order(
                origin_id=x['item'].origin_id)
        ]
        amazon_order_dict = {}
        amazon_id_list = []
        duplicates = self.__find_duplicate(amazon_orders=amazon_orders)
        for entry in amazon_orders:
            # pylint: disable=invalid-string-quote
            key = f"{entry['external_id']};{entry['variation_id']}"
            duplicate = duplicates[key]
            order_id = duplicate['order_id']
            amazon_id = duplicate['amazon_id']
            if order_id != -1 and amazon_id != -1:
                amazon_order_dict[str(amazon_id)] = entry['id']
                amazon_id_list.append(amazon_id)

        if amazon_id_list:
            self.session.query(AmazonOrderItemsModel).filter(
                AmazonOrderItemsModel.id.in_(amazon_id_list)
            ).update(
                {target: case(amazon_order_dict,
                              value=AmazonOrderItemsModel.id)},
                synchronize_session=False
            )

    def __add_orders_from_table(self, table: str,
                                start_date: datetime.datetime,
                                end_date: datetime.datetime) -> None:
        if table not in ['orders', 'amazon_orders']:
            logger.error('Invalid table name, valid names: '
                         '[\'orders\', \'amazon_orders\']')
            return

        if self.target == 'daily':
            stat_entry_field = 'daily_statistic_entry'
        if self.target == 'monthly':
            stat_entry_field = 'statistic_entry'

        order_items = self.__get_order_items(table=table,
                                             start_date=start_date,
                                             end_date=end_date)
        query = self.session.query(self.statistic_model).filter(
            self.statistic_model.date_value <= end_date,
            self.statistic_model.date_value >= start_date,
            self.statistic_model.origin_id >= 0
        )

        logger.info(f'Generate {self.target} statistics over '
                    f'{order_items.count()} rows on table {table}')
        stat_dict = self.__create_lookup_dictionary(stat_entries=query.all())

        new_entries = []
        old_entries = []
        for order_item in tqdm.tqdm(order_items.all()):
            if table == 'orders':
                order_date = order_item.order.creation_date
            elif table == 'amazon_orders':
                order_date = order_item.order.purchase_date

            if isinstance(order_date, datetime.datetime):
                order_date = order_date.date()

            check = [order_item.variation_id, order_item.origin_id]
            if self.target == 'daily':
                check.insert(2, order_date.day)

            current_check = stat_dict[order_date.year][order_date.month]
            if check in current_check['check_list']:
                current_stats_entry = current_check[
                    check_index(current_check, check)]
            else:
                current_stats_entry = None

            if not current_stats_entry:
                statistic_entry = self.__create_stat_entry_from_order(
                    order_item=order_item, order_date=order_date)
                new_entries.append({'stat': statistic_entry,
                                    'item': order_item})
                current_check['check_list'].append(check)
                current_check[check_index(current_check, check)] =\
                    statistic_entry
            else:
                if (self.target == 'daily' or
                        (self.target == 'monthly' and
                         current_stats_entry.predicted is False)):
                    current_stats_entry.quantity += order_item.quantity
                else:
                    current_stats_entry.quantity = order_item.quantity
                    current_stats_entry.predicted = False
                old_entries.append({'stat': current_stats_entry,
                                    'item': order_item})

        # Push to database transaction buffer in order to get an ID
        self.session.add_all(x['stat'] for x in new_entries)
        self.session.flush()

        entries = old_entries + new_entries
        for entry in entries:
            setattr(entry['item'], stat_entry_field, entry['stat'].id)

        if table != 'amazon_orders':
            self.__synchronize_statistic_assignment(entries=entries,
                                                    target=stat_entry_field)

        # Call commit last to persist all changes from DB transaction buffer
        self.session.commit()

    def fetch_statistic_data(self, start_date: datetime.datetime,
                             end_date: datetime.datetime) -> None:
        """
        Get a set of unique orders, that were created during the given
        date-range.

        The statistic table contains three columns, that are used to filter:
            - month
            - year
            - origin_id

        Parameter:
            start_date          [datetime]      -    Start of the date range
            end_date            [datetime]      -    End of the date range
        """
        self.__add_orders_from_table(table='orders', start_date=start_date,
                                     end_date=end_date)
        self.__add_orders_from_table(table='amazon_orders',
                                     start_date=start_date, end_date=end_date)

    def parse_origins(self, custom_origin_config: dict) -> None:
        """
        Convert the origin search patterns from the configuration into lists
        of origins found in the database that match the pattern.

        Parameter:
            custom_origin_config[dict]          -   INI configuration section

        Returns:
                                [dict]          -   custom origin ID mapped to
                                                    all matching origin IDs
                                                    from the database
        """
        if custom_origin_config:
            origin_list = [
                str(origin) for origin, in self.session.query(
                    self.statistic_model.origin_id
                ).filter(self.statistic_model.origin_id >= 0).distinct().all()
            ]
        else:
            return {}

        custom_origins = {}
        for key, value in custom_origin_config.items():
            tmp = []
            except_list = []
            origin_string_list = value.split(',')
            for origin in origin_string_list:
                if origin == '*':
                    tmp = origin_list
                    break
                if re.search(r'\.\*', origin):
                    search = origin.replace('*', '')
                    for entry in origin_list:
                        if re.search(f'^{search}', entry):
                            tmp.append(entry)
                elif re.search(r'\~', origin):
                    search = origin.replace('~', '') + '.'
                    for entry in origin_list:
                        if not re.search(f'^{search}', entry):
                            tmp.append(entry)
                elif re.search(r'!', origin):
                    except_list.append(origin.replace('!', ''))
                else:
                    # Always use two decimal places to align with the database
                    # representation.
                    q_str = str(Decimal(origin).quantize(Decimal('0.01')))
                    if q_str in origin_list:
                        tmp.append(q_str)
                    else:
                        raise RuntimeError(
                            'Invalid origin found in custom origin '
                            f'configuration: {q_str}. Adjust the configuration'
                            f' for {key}.'
                        )
            for exception in except_list:
                try:
                    tmp.pop(tmp.index(exception))
                except ValueError:
                    continue
            custom_origins[key] = list(sorted(set(tmp)))
        return custom_origins

    def get_custom_origin_statistics(
        self, start_date: datetime.datetime, end_date: datetime.datetime,
        custom_origin_config: dict
    ) -> None:
        """
        Sum up all entries for a given date-unit (month-year, day-month-year)
        and variation.

        The origin_id for all origins is '-1'.

        Parameter:
            start_date          [datetime]      -   Start of the date range
            end_date            [datetime]      -   End of the date range
            custom_origin_config[dict]          -   INI configuration section
        """
        custom_origins = self.parse_origins(
            custom_origin_config=custom_origin_config)
        if self.target == 'monthly':
            model = StatisticModel
        else:
            model = DailyStatisticModel

        existing_custom_origin_entries = self.session.query(model).filter(
                model.date_value <= end_date,
                model.date_value >= start_date,
                model.origin_id < 0)

        merged_entries = []
        existing = self.__create_lookup_dictionary(
            stat_entries=existing_custom_origin_entries.all())
        for origin_id, origin_list in custom_origins.items():
            origin_list = [Decimal(x) for x in origin_list]
            if self.target == 'monthly':
                query = self.__get_statistics_entries_within_date_range(
                    start=start_date,
                    end=end_date, origin_list=origin_list)
            elif self.target == 'daily':
                query = self.__get_daily_statistics_entries_within_date_range(
                    start=start_date, end=end_date, origin_list=origin_list)

            logger.info(f'Calculating {self.target} custom origin entries for '
                        f'origin {origin_id} over {query.count()} rows.')

            total_df = self.__get_total_quantity(origin_list=origin_list,
                                                 start=start_date,
                                                 end=end_date)

            new_entries = []
            for entry in tqdm.tqdm(query.all()):
                origin_id = Decimal(str(origin_id)).quantize(Decimal('1.00'))
                check = [entry.variation_id, origin_id]
                if self.target == 'daily':
                    check.insert(2, entry.day)
                current_check = existing[entry.year][entry.month]
                if check in current_check['local']:
                    continue

                total_qty = get_quantity(dataframe=total_df,
                                         variation=entry.variation_id,
                                         date=entry.date_obj,
                                         target=self.target)
                if total_qty < 0:
                    logger.warning('No valid sum for variation '
                                   f'{entry.variation_id} @ {entry.date_obj}')
                    continue
                if total_qty == 0:
                    # silently skip empty sums because no search hit in the
                    # statistics implicitly means that there were no sales
                    continue

                if check in current_check['check_list']:
                    match = current_check[check_index(current_check, check)]
                    match.quantity = total_qty
                    match.predicted = False
                    current_check['local'].append(check)
                    continue

                if self.target == 'monthly':
                    new_entry = self.__create_stat_entry(
                        variation_id=entry.variation_id,
                        origin_id=origin_id, month=entry.month,
                        year=entry.year, quantity=total_qty, ean=entry.ean)
                if self.target == 'daily':
                    new_entry = self.__create_stat_entry(
                        variation_id=entry.variation_id,
                        origin_id=origin_id, day=entry.day, month=entry.month,
                        year=entry.year, quantity=total_qty, ean=entry.ean)
                new_entries.append(new_entry)
                current_check['local'].append(check)
            merged_entries += new_entries

        self.session.add_all(merged_entries)
        self.session.commit()


class Interface:
    def __init__(self, connection_string: str, mode: str) -> None:
        self.mode = mode
        config_path = init_config(name='sales_statistic')
        self.config = configparser.ConfigParser()
        self.config.read(config_path)

        try:
            origin_prefix = self.config['GENERAL']['amazon_origin_prefix']
            self.data_collector = DataCollector(
                amazon_origin_prefix=origin_prefix)
        except KeyError:
            logger.warning('No Amazon origin prefix set in the configuration '
                           'default to 4.x')
            self.data_collector = DataCollector()
        self.data_collector.create_database_session(
            connection_string=connection_string)

    def cleanup(
        self, print_summary: bool = False, ean_check: bool = False
    ) -> bool:
        if self.mode in ['monthly', 'both']:
            self.data_collector.set_target_monthly()
            self.data_collector.cleanup_statistics(
                print_summary=print_summary, ean_check=ean_check
            )
        if self.mode in ['daily', 'both']:
            self.data_collector.set_target_daily()
            self.data_collector.cleanup_statistics(
                print_summary=print_summary, ean_check=ean_check
            )
        return True

    def process_statistics(self, date_range: tuple) -> bool:
        # Monthly statistics do not have a day field, therefore for
        # comparisions the day is assumed to be 1.
        try:
            custom_origin_config = dict(self.config['CustomOrigins'])
        except KeyError:
            custom_origin_config = {}
        if self.mode in ['monthly', 'both']:
            self.data_collector.set_target_monthly()
            month_start = date_range[0].replace(day=1)
            self.data_collector.fetch_statistic_data(
                start_date=month_start, end_date=date_range[1])
            self.data_collector.get_custom_origin_statistics(
                start_date=month_start, end_date=date_range[1],
                custom_origin_config=custom_origin_config
            )
        if self.mode in ['daily', 'both']:
            self.data_collector.set_target_daily()
            self.data_collector.fetch_statistic_data(
                start_date=date_range[0], end_date=date_range[1])
            self.data_collector.get_custom_origin_statistics(
                start_date=date_range[0], end_date=date_range[1],
                custom_origin_config=custom_origin_config
            )
        return True

    def get_latest_statistic_date(self) -> Union[datetime.date, None]:
        return self.data_collector.get_latest_statistic_date(mode=self.mode)
