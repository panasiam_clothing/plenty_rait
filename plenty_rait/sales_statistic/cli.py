"""
Summarize sales data from multiple database tables into daily and monthly
sales.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import datetime
import argparse
from loguru import logger

from plenty_rait.sales_statistic.service import Interface
from plenty_rait.helper.setup_helper import CommandLineSetup


class SalesStatisticCommandLine(CommandLineSetup):
    @staticmethod
    def setup_parser(subparser) -> argparse.ArgumentParser:
        parser = subparser.add_parser('SalesStatistic')
        parser.add_argument(
            '--start', '-s', '--from', '-f', required=False,
            help='Pick the start date of the range to be pulled.[YYYY-MM-DD]',
            dest='sales_statistic_start')
        parser.add_argument(
            '--end', '-e', '--to', '-t', required=False,
            help='Pick the end date of the range to be pulled.[YYYY-MM-DD]',
            dest='sales_statistic_end')
        # pylint: disable=invalid-string-quote
        parser.add_argument(
            '--create_tables', '-c', required=False,
            help="Create the 'statistics' and 'daily_statistics' tables, when "
            "they don't already exist.", action='store_true',
            dest='sales_statistic_tables')
        parser.add_argument(
            '--with_daily_statistic', '--with_daily', '--daily',
            required=False, action='store_true', dest='sales_statistic_daily',
            help='Fill the daily_statistics table')
        parser.add_argument(
            '--with_monthly_statistic', '--with_monthly', '--monthly',
            required=False, action='store_true',
            dest='sales_statistic_monthly',
            help='Fill the statistics table with monthly entries')
        parser.add_argument(
            '--cleanup', '--clean', '--cleanup_statistics', required=False,
            help='Find and replace deprecated variation IDs',
            action='store_true', dest='sales_statistic_cleanup')

    @staticmethod
    def validate_parser(args: argparse.Namespace) -> bool:
        if not (args.sales_statistic_monthly or args.sales_statistic_daily):
            logger.error('Either `daily` and/or `monthly` must be specified.')
            return False
        if args.sales_statistic_cleanup:
            return True
        if not args.sales_statistic_start or not args.sales_statistic_end:
            return False
        arguments = vars(args)
        for key, value in arguments.items():
            if key not in ['sales_statistic_start', 'sales_statistic_end']:
                continue
            try:
                arguments[key] = datetime.date.fromisoformat(value)
            except ValueError:
                logger.error(f'{key} date invalid, required: YYYY-MM-DD')
                return False

        return True

    @staticmethod
    def handle_command(args: argparse.Namespace,
                       connection_string: str) -> bool:
        mode_map = {
            (True, False): 'monthly',
            (False, True): 'daily',
            (True, True): 'both'
        }
        mode = mode_map[
            (args.sales_statistic_monthly, args.sales_statistic_daily)
        ]
        interface = Interface(connection_string=connection_string, mode=mode)
        if args.sales_statistic_tables:
            tables = ['StatisticModel', 'DailyStatisticModel']
            interface.data_collector.create_tables(tables=tables)
        if args.sales_statistic_cleanup:
            return interface.cleanup(print_summary=True)

        return interface.process_statistics(
            date_range=(args.sales_statistic_start, args.sales_statistic_end))
