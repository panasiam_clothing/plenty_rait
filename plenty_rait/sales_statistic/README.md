# Sales statistic

Sort raw data from a MariaDB database into monthly sales tables with different criteria.

## Configuration

### Mapping default prefixes for Amazon FBM & Amazon FBA orders

Specific the order origin ID prefix for Amazon FBM & FBA on your Plentymarkets system (eg. AmazonFBA Germany 104.01, prefix: 104) within the configuration under {HOME}/.config/plenty_rait_tool/sales_statistic/config.ini.

```ini
[GENERAL]
amazon_origin_prefix=4,104
```

### Custom origin-combinations

You can also add custom origin IDs for example to store the combined statistics of all Amazon related origins.
Place the custom origin ID to origin search-pattern mappings under the "CustomOrigin" section.

The term origin refers to a source for orders (Webshop, Amazon, Ebay,
etc.), which are defined on Plentymarkets
(Setup->Orders->Order-origins).
This application **does not** check for conflicts in between custom
origin IDs, once you've set up a custom origin ID, you are responsible
for making sure that all entries related to it, refer to the same origin
combination. For example if you set up '-2' to describe the sum of
quantites for all origins starting with 2 (`-2=2.*`) and you change it
afterwards to `-2=2.*,3.*`, then all entries before the modification
will still contain the old rule.

Here a examples for search patterns with a valid syntax:

```ini
[CustomOrigins]
# All origins starting with 104
-1=104.*
# All origins not starting with 104
-2=~104
# All origins
-3=*
# Origin 104.01 and 104.02
-4=104.01,104.02
# All Amazon related origins
-5=4.*,104.*
# All origins starting with 104 except 104.01
-1=104.*,!104.01
```

## Logging

The application logs by default to a standardized location on your platform. On Linux this will usually be: `~/.cache/sales_statistic/log`. The file will grow to a maximum of 2 MB, at which point it will start to rotate out old entries.

## Usage

**Calculate monthly statistics for a given date range**
```bash
python3 -m plenty_rait SalesStatistic --start 2021-06-03 --end 2021-08-08 --monthly
```

**Calculate monthly & daily statistics for a given date range**
```bash
python3 -m plenty_rait SalesStatistic --start 2021-06-03 --end 2021-08-08 --monthly --daily
```

**Clean up daily statistic entries (search and replace deprecated variation IDs)**
```bash
python3 -m plenty_rait SalesStatistic --cleanup --daily
```
