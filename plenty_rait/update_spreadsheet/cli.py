"""
Sub application of the Plenty RAIT tool with the purpose of writing
identification, stock and prediciton data to a `xlsx/xlsm` file.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import argparse

from plenty_rait.update_spreadsheet.service import Interface


from plenty_rait.helper.setup_helper import CommandLineSetup


def setup_update_parser(subparser) -> None:
    update_parser = subparser.add_parser(
        'Update',
        help='Update a specific sheet or a selection of sheets with data from '
        'the database')
    update_parser.add_argument(
        '--all', '-a', required=False,
        help='Execute all available update commands',
        dest='update_all', action='store_true'
    )
    update_parser.add_argument(
        '--suggestion_sheet_update', '--suggestion', '-s', required=False,
        help='Update the sku, ean, asin, name and variation id within the '
        'suggestion sheets with the data from the database.',
        dest='update_suggestion', action='store_true'
    )
    update_parser.add_argument(
        '--predictions_update', '--predictions', '-p', required=False,
        help='Update the predictions for orders and inventory reorders with '
        'the data from the database.', dest='update_predictions',
        action='store_true'
    )
    update_parser.add_argument(
        '--incoming_update', '--incoming', '-i', required=False,
        help='Update the list of pending incoming variations',
        dest='update_incoming', action='store_true'
    )



def setup_clear_parser(subparser) -> None:
    clear_parser = subparser.add_parser(
        'Clear',
        help='Clear the user input of a specifc sheet'
    )
    clear_parser.add_argument(
        '--target_sheet', '--sheet', '-s', required=True, dest='clear_sheet',
        help='The name of the sheet where a column should be cleared'
    )
    clear_parser.add_argument(
        '--target_column', '--column', '-c', required=True,
        dest='clear_column',
        help='The name of the column within the target sheet which should be '
        'cleared'
    )


class UpdateSpreadsheetCommandLine(CommandLineSetup):
    @staticmethod
    def setup_parser(subparser) -> None:
        parser = subparser.add_parser(
            'UpdateSpreadsheet',
            help='Update the spreadsheet with data from the database'
        )
        subparsers = parser.add_subparsers(
            title='Option for update_spreadsheet',
            help='Choose wheter to update a selection of sheets or to clear '
            'user input.', dest='update_spreadsheet_command'
        )
        setup_update_parser(subparser=subparsers)
        setup_clear_parser(subparser=subparsers)

    @staticmethod
    def handle_command(args: argparse.Namespace,
                       connection_string: str) -> None:
        interface = Interface(connection_string=connection_string)
        if args.update_spreadsheet_command == 'Update':
            if args.update_suggestion:
                if not interface.update(target=['user_interaction_sheets']):
                    return False
            if args.update_predictions:
                if not interface.update(target=['prediction_sheets']):
                    return False
            if args.update_incoming:
                if not interface.update(target=['incoming_sheets']):
                    return False
        elif args.update_spreadsheet_command == 'Clear':
            interface.update_service.clear_user_input(
                sheet_name=args.clear_sheet, column_name=args.clear_column)
        return True
