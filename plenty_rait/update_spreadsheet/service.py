"""
Sub application of the Plenty RAIT tool with the purpose of writing
identification, stock and prediciton data to a `xlsx/xlsm` file.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import datetime
from string import ascii_uppercase
import dateutil.relativedelta
import re
from loguru import logger
import numpy as np
import pandas
from openpyxl import load_workbook
from openpyxl.worksheet.worksheet import Worksheet
from openpyxl.utils.dataframe import dataframe_to_rows
from openpyxl.formula import Tokenizer
from openpyxl.formula.translate import Translator
from openpyxl.styles import Border, Font, PatternFill, Side, Color, Alignment
from openpyxl.styles.protection import Protection
from openpyxl.formatting.rule import ColorScale, FormatObject, Rule, CellIsRule
from openpyxl.formatting.formatting import ConditionalFormattingList
from sqlalchemy import and_, or_, func

from plenty_rait.update_spreadsheet.utils import UpdateSpreadsheetConfigHandler
from plenty_rait.update_spreadsheet.config_schema import (
    update_spreadsheet_configuration_schema
)
from plenty_rait.helper.database_schema import (
    BaseDatabaseService, IdentificationModel, StatisticModel,
    VariationInformationModel, IncomingVariationModel
)
from plenty_rait.helper.file_handling import write_workbook_with_retry


LIGHT_GREY = 'CCCCCC'  # Not required status background
PINK = 'FF8080'  # Identification header background color
YELLOW = 'E5FF80'  # Sales header background color
ORANGE = 'EBA202'  # semi important status background color
RED = '8E4338'  # Required suggestion/status background color
LIGHT_GREEN = 'BAE6CB'  # Not required suggestion background color
GREEN = '388E43'  # Formula header background
GREY_GREEN = '818E86'  # Not required suggestion font color
PURPLE = 'E580FF'  # User input header background


def remove_deprecated_rows(source_df, sheet_df) -> pandas.DataFrame:
    """
    Remove variations from the Excel sheet (dataframe) that were deleted
    from the database.
    """
    test = np.where(
        ~sheet_df['variation_id'].isin(source_df['variation_id']), 1, 0)
    sheet_df['drop'] = test
    drop_indeces = sheet_df[sheet_df['drop'] == 1].index
    sheet_df = sheet_df.drop(drop_indeces)
    sheet_df = sheet_df.drop(['drop'], axis=1)

    return sheet_df


def update_old_identification_values(source_df, sheet_df) -> pandas.DataFrame:
    """
    Update identification values within the Excel sheet dataframe.
    """
    columns = ['ean', 'asin', 'name', 'sku']
    valid_columns = [x for x in columns if x in
                     list(source_df.columns) and x in list(sheet_df.columns)]

    new_df = pandas.merge(sheet_df, source_df, on='variation_id', how='inner',
                          suffixes=('_sheet', '_db'))
    for column in valid_columns:
        sheet_df[column] = new_df[str(f'{column}_db')]

    return sheet_df


def remove_duplicate_rows(source_df, sheet_df) -> pandas.DataFrame:
    """
    Remove rows from the source dataframe (export from database)
    that are already present in the sheet dataframe.
    """
    sheet_variations = sheet_df['variation_id'].values.tolist()
    source_df['duplicate'] = np.where(
        source_df['variation_id'].isin(sheet_variations), 1, 0)
    source_df = source_df[source_df['duplicate'] == 0]
    source_df = source_df.drop(['duplicate'], axis=1)
    source_df = source_df.reset_index(drop=True)

    return source_df


def insert_new_rows(source_df, sheet_df) -> pandas.DataFrame:
    """
    Combine two dataframes and sort them in ascending order.
    """
    for row in source_df.itertuples():
        sheet_df = sheet_df.append({
            'variation_id': row.variation_id,
            'ean': row.ean,
            'asin': row.asin,
            'name': row.name,
            'sku': row.sku
        }, ignore_index=True)
    sheet_df = sheet_df.fillna('')
    sheet_df = sheet_df.sort_values(by='variation_id').reset_index(drop=True)

    return sheet_df


def get_formulas(sheet) -> dict:
    """
    Get the formulas to be used for all rows from the first row and map
    them to the column names.
    """
    # Get the first two rows (including the header row)
    rows = list(sheet.rows)[:2]
    formulas = {
        header_cell.value: first_row_cell.value
        for header_cell, first_row_cell in zip(rows[0], rows[1])
        if first_row_cell.data_type == 'f'  # Search for formula fields
    }
    # Make sure that the row index is always 2, as we iterate from the first
    # content row onwards. This fixes issues where the row index in the first
    # row is incorrect.
    for key, formula in formulas.items():
        for part in Tokenizer(formula).items:
            if (
                part.type == 'OPERAND' and
                re.match(r'^\$*([A-Z][A-Z]*)\d+', part.value)
            ):
                new_value = re.sub(r'\d+', '2', part.value)
                if new_value.find(':'):
                    values = [
                        zip(
                            part.value.replace('$', '').split(':'),
                            new_value.replace('$', '').split(':')
                        )
                    ]
                else:
                    values = [
                        zip(
                            part.value.replace('$', ''),
                            new_value.replace('$', '')
                        )
                    ]
                for replace_match in values:
                    for current, replacement in replace_match:
                        sub = re.sub(current, replacement, formula)
                        formula = sub
                formulas[key] = formula

    return formulas


def apply_formulas(sheet_df, formulas: dict) -> pandas.DataFrame:
    """
    Apply the different formulas to every row with according reference
    updates.
    """
    for i in sheet_df.index:
        for key in formulas:
            if i != 0:
                formulas[key] = Translator(
                    formulas[key], origin='B2').translate_formula('B3')
            sheet_df.loc[i, key] = formulas[key]

    return sheet_df


def get_prediction_min_max_date(source_df) -> tuple:
    min_year = source_df['year'].min()
    min_month = source_df[source_df['year'] == min_year]['month'].min()
    oldest_entry = datetime.datetime(min_year, min_month, 1)
    max_year = source_df['year'].max()
    max_month = source_df[source_df['year'] == max_year]['month'].max()
    newest_entry = datetime.datetime(max_year, max_month, 1)
    return (oldest_entry, newest_entry)


def daterange_generator(start_date, end_date):
    month_diff = (end_date.year - start_date.year) * 12 +\
        (end_date.month - start_date.month)
    for increment in range(int(month_diff)):
        yield start_date + dateutil.relativedelta.relativedelta(
            months=+increment)


def get_multi_level_columns_by_date_range(
        date_range: tuple) -> pandas.MultiIndex:
    years = []
    months = []

    for date in daterange_generator(start_date=date_range[0],
                                    end_date=date_range[1]):
        years.append(date.year)
        months.append(date.month)
    return pandas.MultiIndex.from_arrays([years, months],
                                         names=('year', 'month'))


def get_date_labels_from_date_range(date_range: tuple) -> list:
    result_dates = []
    for date in daterange_generator(start_date=date_range[0],
                                    end_date=date_range[1]):
        result_dates.append(str(f'{date.month}-{date.year}'))
    return result_dates


def get_excel_column_by_index(index: int):
    if index <= 0:
        return ''
    return get_excel_column_by_index(index // 26) + chr(index % 26 + ord('A'))


def remove_special_chars(name: str) -> str:
    special_chars = ['?', '!', ':', '.', ',', '/', '\\',
                     'ä', 'ö', 'ü', 'Ä', 'Ü', 'Ö', 'ß']
    name = ''.join([c for c in name if c not in special_chars])
    name = name.replace(' ', '_').lower()
    return name


class UpdateService(BaseDatabaseService):
    def __init__(self, config, session=None):
        super().__init__(session)
        self.config = config
        if session:
            self.session = session

    @staticmethod
    def __style_sheet_header(sheet: Worksheet, start_letter: str,
                             end_letter: str, areas: dict) -> None:
        thin_border = Side(border_style='thin', color='000000')

        letters = [c for c in ascii_uppercase
                   if start_letter.upper() <= c <= end_letter.upper()]
        sheet.row_dimensions[1].height = 83  # to allow longer text to wrap
        for index, data in enumerate(
            [(sheet.column_dimensions[x], sheet[f'{x}1'])
             for x in letters]
        ):
            col_dim = data[0]
            cell = data[1]
            # Separate header from content
            cell.border = Border(bottom=thin_border)
            # Make sure that the column header text wraps
            cell.alignment = Alignment(wrap_text=True, horizontal='center')
            # Set up a common font for the header row
            cell.font = Font(name='Calibri', bold=True, size=12)
            # Adjust the size of the columns
            if index == 0:
                col_dim.width = 5  # Sorting number column
            elif index == 1:
                col_dim.width = 45  # Name column
            elif index in areas['status']:
                col_dim.width = 9  # Status column

            if index == 0:
                # Sorting number column (dark green)
                cell.fill = PatternFill('solid', fgColor=GREEN)
            elif index in areas['identifier']:
                # Identifier columns (pink)
                cell.fill = PatternFill('solid', fgColor=PINK)
            elif index in areas['formula']:
                # Formula columns (dark green)
                cell.fill = PatternFill('solid', fgColor=GREEN)
            elif index in areas['sales']:
                # Sales columns (yellow)
                cell.fill = PatternFill('solid', fgColor=YELLOW)
            elif index in areas['user_input']:
                # User input columns (purple)
                cell.fill = PatternFill('solid', fgColor=PURPLE)

    @staticmethod
    def __setup_conditional_formats(sheet: Worksheet,
                                    status_column_letter: str,
                                    suggestion_column_letter: str):
        # Delete all existing conditional formats
        sheet.conditional_formatting = ConditionalFormattingList()
        # Column L contains 1,2 or 3 as status values of a formula apply
        # colors depending on the result
        # This column can be used to sort the list accordingly
        color_scale = ColorScale(
            cfvo=[
                FormatObject(type='num', val=1),
                FormatObject(type='num', val=2),
                FormatObject(type='num', val=3)
            ],
            color=[
                Color(LIGHT_GREY),  # light grey (no action needed)
                Color(ORANGE),  # orange (action required insufficient stock)
                Color(RED)
            ]
        )
        color_scale_rule = Rule(type='colorScale', colorScale=color_scale)
        sheet.conditional_formatting.add(
            f'{status_column_letter}1:{status_column_letter}{sheet.max_row}',
            color_scale_rule
        )

        # Column N contains the suggestion and should be highlighted when
        # the suggestion is greater than 0
        sheet.conditional_formatting.add(
            f'{suggestion_column_letter}2:{suggestion_column_letter}'
            f'{sheet.max_row}',
            # Green background and bold text
            CellIsRule(operator='greaterThan', formula=['0'],
                       fill=PatternFill(bgColor=RED),
                       font=Font(size=10, bold=True, color='000000'))
        )
        # Further highlight rows with a suggestion greater than 0 by
        # deemphasizing rows that are <= 0
        sheet.conditional_formatting.add(
            f'{suggestion_column_letter}2:{suggestion_column_letter}'
            f'{sheet.max_row}',
            # Make the entries less readable
            CellIsRule(operator='lessThanOrEqual', formula=['0'],
                       fill=PatternFill(bgColor=LIGHT_GREEN),
                       font=Font(size=10, bold=False, color=GREY_GREEN))
        )

    @staticmethod
    def __setup_conditional_formats_reorder_sheet(
        sheet: Worksheet, remaining_month_column_letter: str,
        remaining_month_column_letter_after: str
    ):
        """
        Set up conditional formatting for two columns that represent
        """
        for letter in [
            remaining_month_column_letter, remaining_month_column_letter_after
        ]:
            sheet.conditional_formatting.add(
                f'{letter}2:{letter}'
                f'{sheet.max_row}',
                CellIsRule(
                    operator='equal', formula=['0'],
                    fill=PatternFill(bgColor=RED),
                    font=Font(size=10, bold=True, color='FFFFFF')
                )
            )
            sheet.conditional_formatting.add(
                f'{letter}2:{letter}'
                f'{sheet.max_row}',
                CellIsRule(
                    operator='between', formula=['0', '4'],
                    fill=PatternFill(bgColor=RED),
                    font=Font(size=10, bold=True, color='E5E6CC')
                )
            )
            sheet.conditional_formatting.add(
                f'{letter}2:{letter}'
                f'{sheet.max_row}',
                CellIsRule(
                    operator='between', formula=['3', '6'],
                    fill=PatternFill(bgColor=ORANGE),
                    font=Font(size=10, bold=False, color='000000')
                )
            )
            sheet.conditional_formatting.add(
                f'{letter}2:{letter}'
                f'{sheet.max_row}',
                CellIsRule(
                    operator='greaterThan', formula=['6'],
                    fill=PatternFill(bgColor=LIGHT_GREEN),
                    font=Font(size=10, bold=False, color=GREY_GREEN)
                )
            )
            sheet.conditional_formatting.add(
                f'{letter}2:{letter}'
                f'{sheet.max_row}',
                CellIsRule(
                    operator='equal', formula=['\"∞\"'],
                    fill=PatternFill(bgColor=LIGHT_GREEN),
                    font=Font(size=10, bold=False, color=GREY_GREEN)
                )
            )

    @staticmethod
    def __separate_row_style(sheet: Worksheet, max_col: int, min_row: int = 2):
        thick_border = Side(border_style='double', color='000000')
        i = 0
        for row in sheet.iter_rows(min_row=min_row, max_col=max_col,
                                   max_row=sheet.max_row):
            j = 0
            for cell in row:
                if j > 1:
                    cell.alignment = Alignment(horizontal='center')
                if i % 2 == 0:
                    if j in [9, 10]:
                        # sales columns
                        cell.fill = PatternFill('solid', fgColor='CDF6CD')
                    else:
                        cell.fill = PatternFill('solid', fgColor='F6F6F6')
                else:
                    if j in [9, 10]:
                        # sales columns
                        cell.fill = PatternFill('solid', fgColor='E4F6E4')
                    else:
                        cell.fill = PatternFill('solid', fgColor='fbfbfb')
                if cell.col_idx == max_col:
                    cell.border = Border(right=thick_border)
                j += 1
            i += 1

        # Freeze the header row and the first two columns (sorting number and
        # name)
        sheet.freeze_panes = 'C2'

    def __style_sheet(self, sheet, letter_range: tuple,
                      status_col: str, suggestion_col: str,
                      max_col: int, areas: dict) -> None:
        """
        Apply a fixed style to the sheet.

        Format the column header, apply conditional formats to the suggestion
        and status column, and differentiate the read-only rows from each
        other by color.
        """
        self.__style_sheet_header(sheet, start_letter=letter_range[0],
                                  end_letter=letter_range[1], areas=areas)
        self.__setup_conditional_formats(
            sheet, status_column_letter=status_col,
            suggestion_column_letter=suggestion_col)
        self.__separate_row_style(sheet=sheet, max_col=max_col)

    @staticmethod
    def __unprotect_user_input_columns(sheet: Worksheet, min_row: int,
                                       area: list) -> None:
        """
        All cells of the suggestion and reorder sheets are protected by default
        , unprotect the cells for user input.

        Parameters:
            min_row
        """
        for row in sheet.iter_rows(min_row=min_row, min_col=area[0] + 1,
                                   max_col=area[-1] + 1,
                                   max_row=sheet.max_row):
            for cell in row:
                cell.protection = Protection(locked=False)

    @staticmethod
    def __get_sheet_as_dataframe(sheet: Worksheet) -> pandas.DataFrame:
        data = sheet.values
        cols = next(data)
        data = list(data)
        sheet_df = pandas.DataFrame(data, columns=cols)
        if len(sheet_df.index) == 0:
            logger.error('Empty Excel sheet.')
            return pandas.DataFrame()
        return sheet_df

    @staticmethod
    def __overwrite_sheet(sheet: Worksheet,
                          dataframe: pandas.DataFrame) -> Worksheet:
        sheet.delete_rows(0, sheet.max_row)
        for row in dataframe_to_rows(dataframe, index=False, header=True):
            sheet.append(row)
        return sheet

    def __get_user_interaction_sheet_names(self) -> list:
        sheets = [('reorder', self.config['reorder_sheet']['name'])]
        sheets += [
            ('redistribution', sheet['name'])
            for sheet in self.config['redistribution_sheets']
        ]
        return sheets

    def update_sheets(self):
        """
        Insert up-to-date identification data and apply the fixed style to
        reorder and redistribution sheet.
        """
        workbook = load_workbook(self.config['file'])
        sheets = self.__get_user_interaction_sheet_names()
        for sheet_type, sheet_name in sheets:
            logger.info(f'Updating sheet {sheet_name} ...')
            sheet = workbook[sheet_name]
            sheet.protection.sheet = False
            sheet_df = self.__fetch_identification_data_from_database(
                sheet=sheet)
            if sheet_type == 'reorder':
                sheet_df = self.__insert_reorder_information(
                    sheet_df=sheet_df,
                    sheet=workbook[self.config['reorder_sheet']['name']])
                if len(sheet_df.index) == 0:
                    logger.info(f"Update of 'reorder' sheet [{sheet_name}] failed.")
                    continue

            sheet = self.__overwrite_sheet(sheet=sheet, dataframe=sheet_df)
            areas = {
                'reorder': {
                    'identifier': [1, 2, 3, 4, 5],
                    'formula': [6, 7, 8, 11, 12, 13, 14, 15, 16, 17, 18],
                    'sales': [9, 10], 'user_input': [19, 20], 'status': [11]
                },
                'redistribution': {
                    'identifier': [1, 2, 3, 4, 5],
                    'formula': [6, 7, 8, 11, 12, 13],
                    'sales': [9, 10], 'user_input': [14, 15], 'status': [11]
                }
            }
            if sheet_type == 'reorder':
                self.__style_sheet(sheet=sheet, letter_range=('A', 'U'),
                                   status_col='L', suggestion_col='R',
                                   max_col=19, areas=areas[sheet_type])
                self.__setup_conditional_formats_reorder_sheet(
                    sheet=sheet, remaining_month_column_letter='Q',
                    remaining_month_column_letter_after='S'
                )
            elif sheet_type == 'redistribution':
                self.__style_sheet(sheet=sheet, letter_range=('A', 'P'),
                                   status_col='L', suggestion_col='N',
                                   max_col=14, areas=areas[sheet_type])
            sheet.protection.sheet = True
            self.__unprotect_user_input_columns(
                sheet=sheet, min_row=2,
                area=areas[sheet_type]['user_input'])

        write_workbook_with_retry(workbook=workbook, path=self.config['file'])

    def __insert_reorder_information(self, sheet_df: pandas.DataFrame,
                                     sheet: Worksheet) -> pandas.DataFrame:
        """
        Insert additional information for reorders into the sheet.

        The producer column is used to split the variations into a separate
        orders for each producer.
        The delivery time column determines the time range it takes to produce
        and ship the product, it is set within plentymarkets for each producer.
        The creation date acts as a hint for the user to determine how accurate
        the suggestion is.
        """
        sug_config = self.config['reorder_sheet']
        producer_col = sug_config['producer_column']
        delivery_col = sug_config['delivery_time_column']
        creation_col = sug_config['creation_date_column']
        columns = [producer_col, delivery_col, creation_col]
        if any(x not in sheet_df.columns for x in columns):
            missing = [x not in sheet_df.columns for x in columns]
            logger.error(
                f'Missing columns {missing} within sheet {sheet.title}.\t'
                'Abort update of additional information in the reorder sheet.'
            )
            return pandas.DataFrame()
        info_df = pandas.DataFrame.from_dict({
            info.variation_id: {
                producer_col: info.producer.producer_name,
                delivery_col: round(info.producer.delivery_time / 30.41),
                creation_col: info.creation_date
            } if info.producer else {
                producer_col: None,
                delivery_col: 6,
                creation_col: info.creation_date
            } for info in self.session.query(
                VariationInformationModel
            ).filter(
                VariationInformationModel.variation_id.in_(
                    sheet_df['variation_id'].to_list()
                )
            )
        }, orient='index')
        # The assign method on the index of the dataframe
        sheet_df.index = sheet_df['variation_id']
        if len(info_df.index) != len(sheet_df):
            # pylint: disable=invalid-string-quote
            logger.error("The 'variation_information' table is not up-to-date")
            return pandas.DataFrame()
        sheet_df = sheet_df.assign(
            **{column: info_df[column] for column in columns}
        )
        sheet_df.sort_values(
            by=[producer_col, 'sku'], inplace=True, ignore_index=True
        )
        formulas = get_formulas(sheet=sheet)
        sheet_df = apply_formulas(sheet_df=sheet_df, formulas=formulas)
        sheet_df['#'] = sheet_df.index
        return sheet_df

    def __fetch_identification_data_from_database(self,
                                                  sheet) -> pandas.DataFrame:
        """
        Update the identification data for each variation within every
        user interaction sheet.
        """
        inventory = self.session.query(IdentificationModel)
        ident_db = pandas.read_sql(inventory.statement, self.session.bind)
        if len(ident_db.index) == 0:
            logger.error('No inventory data located in database.')
            return pandas.DataFrame()

        ident_db = ident_db.rename(columns={'asin_all': 'asin'})
        ident_db = ident_db[ident_db['ean'] != '0']

        sheet_df = self.__get_sheet_as_dataframe(sheet=sheet)
        sheet_df.dropna(subset=['variation_id'], inplace=True)
        formulas = get_formulas(sheet=sheet)
        sheet_df = remove_deprecated_rows(source_df=ident_db,
                                          sheet_df=sheet_df)
        sheet_df = update_old_identification_values(source_df=ident_db,
                                                    sheet_df=sheet_df)
        ident_dedupe_db = remove_duplicate_rows(source_df=ident_db,
                                                sheet_df=sheet_df)
        complete_sheet_df = insert_new_rows(source_df=ident_dedupe_db,
                                            sheet_df=sheet_df)
        complete_sheet_df.sort_values(
            by='sku', inplace=True, ignore_index=True
        )
        complete_sheet_df = apply_formulas(sheet_df=complete_sheet_df,
                                           formulas=formulas)

        # Renew the sorting number, to allow the user to always return to
        # the initial order of variations
        complete_sheet_df['#'] = complete_sheet_df.index
        return complete_sheet_df

    # pylint: disable=invalid-string-quote
    @staticmethod
    def __sufficient_predictions_check(oldest_entry, newest_entry,
                                       sheet_requirement):
        month_diff = (newest_entry.year - oldest_entry.year) * 12 +\
            newest_entry.month - oldest_entry.month
        try:
            if month_diff < int(sheet_requirement):
                logger.warning(
                    f'There are predictions for the next {month_diff} months,'
                    f' but the configuration requires {sheet_requirement}')
        except ValueError:
            logger.error("Invalid 'prediction_interval' value"
                         f" {sheet_requirement}")

    @staticmethod
    def __style_prediciton_sheet(sheet):
        row = tuple(sheet.rows)[1]
        sheet['A2'].value = 'variation_id'
        for cell in row[1:]:
            if cell.value is not None:
                cell.fill = PatternFill('solid', fgColor='00FF0000')
        sheet.delete_rows(0, 1)

    def fetch_predictions_from_database(self):
        now = datetime.datetime.now()

        workbook = load_workbook(self.config['file'])

        for sheet in self.config['prediction_sheets']:
            future_predictions = and_(
                StatisticModel.origin_id == sheet['origin_id'],
                StatisticModel.predicted == 1,
                StatisticModel.quantity > 0,
                or_(
                    and_(
                        StatisticModel.year == now.year,
                        StatisticModel.month >= now.month
                    ),
                    StatisticModel.year > now.year
                )
            )
            predictions = self.session.query(StatisticModel).filter(
                future_predictions
            )
            db_df = pandas.read_sql(predictions.statement, self.session.bind)
            if len(db_df.index) == 0:
                logger.error('No inventory data located in database.')
                return

            (min_date, max_date) = get_prediction_min_max_date(
                source_df=db_df)
            self.__sufficient_predictions_check(
                oldest_entry=min_date, newest_entry=max_date,
                sheet_requirement=sheet['prediction_interval'])
            columns = get_multi_level_columns_by_date_range(
                date_range=(min_date, max_date))
            labels = get_date_labels_from_date_range(
                date_range=(min_date, max_date))

            unique_variations = self.session.query(
                func.distinct(StatisticModel.variation_id)
            ).filter(future_predictions)
            unique_variations = [x[0] for x in unique_variations.all()]

            sheet_df = pandas.DataFrame(columns=columns,
                                        index=unique_variations)

            for row in db_df.itertuples():
                sheet_df.loc[row.variation_id, (row.year, row.month)] =\
                    row.quantity
            sheet_df.fillna(0, inplace=True)
            sheet_df['variation_id'] = sheet_df.index
            variation_col = sheet_df.pop('variation_id')
            sheet_df.insert(0, 'variation_id', variation_col)
            sheet_df.reset_index(drop=True, inplace=True)

            sheet = workbook[sheet['name']]
            sheet = self.__overwrite_sheet(sheet=sheet, dataframe=sheet_df)

            for cell, new_value in zip(tuple(sheet.rows)[1][1:], labels):
                cell.value = new_value
            self.__style_prediciton_sheet(sheet=sheet)

            # Setup a named range with the same name as the sheet
            named_range_name = remove_special_chars(name=sheet['name'])
            named_range_range = str(
                f'$A$1:$A${sheet.max_row}')
            try:
                if workbook.get_named_range(name=named_range_name):
                    del workbook.defined_names[named_range_name]
            except KeyError:
                logger.info(f'Creating named range: {named_range_name}')
            workbook.create_named_range(named_range_name, sheet,
                                        named_range_range)

            write_workbook_with_retry(
                workbook=workbook, path=self.config['file']
            )

    def update_incoming_sheet(self) -> None:
        workbook = load_workbook(self.config['file'])
        sheet_name = self.config['reorder_sheet']['incoming_variations_sheet']
        sheet = workbook[sheet_name]

        inventory = self.session.query(
            IdentificationModel.variation_id, IdentificationModel.ean
        ).filter(
            IdentificationModel.ean != '0'
        )
        ident_db = pandas.read_sql(inventory.statement, self.session.bind)
        if len(ident_db.index) == 0:
            logger.error('No inventory data located in database.')
            return
        ident_db['incoming_sum'] = 0
        unfinished_incoming_variations = self.session.query(
            IncomingVariationModel
        ).filter(
            IncomingVariationModel.receive_date.is_(None)
        )
        incoming = pandas.read_sql(unfinished_incoming_variations.statement,
                                   self.session.bind)
        if len(incoming.index) == 0:
            logger.info('No incoming variations located in database.')
            ident_db['incoming_sum'] = 0
            sheet = self.__overwrite_sheet(sheet=sheet, dataframe=ident_db)
            write_workbook_with_retry(
                workbook=workbook, path=self.config['file']
            )
            return

        def reorder_by_orders(dataframe: pandas.DataFrame) -> pandas.DataFrame:
            new_df = pandas.DataFrame()
            for group in dataframe.groupby('order_id'):
                temp_df = pandas.DataFrame.from_dict(
                    {
                        group[0]: dict(zip(group[1]['variation_id'].to_list(),
                                           group[1]['quantity'].to_list()))
                    }
                )
                if len(new_df.index) == 0:
                    new_df = temp_df
                    continue
                new_df = new_df.merge(temp_df, how='outer', left_index=True,
                                      right_index=True)
            return new_df

        incoming = reorder_by_orders(dataframe=incoming)
        incoming.fillna(0, inplace=True)
        sheet_df = ident_db.merge(incoming, how='outer',
                                  left_on='variation_id', right_index=True)
        sheet_df.fillna(0, inplace=True)
        formula_dict = {
            'incoming_sum':
            f'=SUM(D2:{get_excel_column_by_index(2 + len(incoming.columns))}2)'
        }
        apply_formulas(sheet_df, formula_dict)
        sheet = self.__overwrite_sheet(sheet=sheet, dataframe=sheet_df)
        write_workbook_with_retry(workbook=workbook, path=self.config['file'])

    def clear_user_input(self, sheet_name: str, column_name: str) -> None:
        workbook = load_workbook(self.config['file'])
        valid_sheets = [
            sheet[1] for sheet in self.__get_user_interaction_sheet_names()
        ]
        if sheet_name not in valid_sheets:
            logger.error(f'Invalid sheet name {sheet_name} must be one of '
                         f'{valid_sheets}')
            return

        sheet = workbook[sheet_name]
        header = [cell.value for cell in next(sheet.rows)]
        try:
            column_index = header.index(column_name)
        except ValueError:
            logger.error(f'Column {column_name} not found within the sheet '
                         f'{sheet_name}')
            return
        for row in sheet.iter_rows(min_row=2, min_col=column_index + 1,
                                   max_col=column_index + 1):
            row[0].value = None
        write_workbook_with_retry(workbook=workbook, path=self.config['file'])


class Interface:
    def __init__(self, connection_string: str) -> None:
        config_handler = UpdateSpreadsheetConfigHandler(
            name='update_spreadsheet', config_type='json',
            config_schema=update_spreadsheet_configuration_schema
        )

        self.update_service = UpdateService(config=config_handler.config)
        self.update_service.create_database_session(
            connection_string= connection_string)

    def update(self, target: list) -> bool:
        if 'user_interaction_sheets' in target:
            self.update_service.update_sheets()
        if 'prediction_sheets' in target:
            self.update_service.fetch_predictions_from_database()
        if 'incoming_sheets' in target:
            self.update_service.update_incoming_sheet()
        return True
