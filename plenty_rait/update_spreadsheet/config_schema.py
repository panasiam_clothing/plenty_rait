# pylint: disable=invalid-string-quote
update_spreadsheet_configuration_schema = {
    "title": "Update spreadsheet configuration schema",
    "type": "object",
    "required": [
        "file",
        "reorder_sheet",
        "redistribution_sheets",
        "prediction_sheets",
        "stock_sheet",
        "sales_sheets"
    ],
    "properties": {
        "file": {
            "title": "Spreadsheet file",
            "description": "Full path to the spreadsheet file",
            "type": "string",
            "pattern": "^.*$",
            "example": "/path/to/the/file.xlsx"
        },
        "reorder_sheet": {
            "title": "Reorder sheet",
            "type": "object",
            "required": [
                "name",
                "origin_id",
                "quantity_column",
                "incoming_variations_sheet",
                "producer_column",
                "delivery_time_column",
                "creation_date_column"
            ],
            "properties": {
                "name": {
                    "title": "Name of the sheet",
                    "type": "string",
                    "pattern": "^.*$"
                },
                "origin_id": {
                    "title": "Custom origin_id from the sales_statistics "
                             "table",
                    "type": "number",
                    "example": -1
                },
                "identifier_column": {
                    "title": "Name of the Identifier column",
                    "description": "The name of the column where the "
                                   "identifier is placed",
                    "default": "ean",
                    "type": "string",
                    "pattern": "^.*$",
                    "example": "ean"
                },
                "identifier_type": {
                    "title": "Type of the identifier",
                    "type": "string",
                    "default": "ean",
                    "enum": ["variation_id", "ean", "sku", "asin"]
                },
                "quantity_column": {
                    "title": "Name of the quantity column",
                    "description": "Column within the sheet where the amount "
                                   "for the reorder is specified",
                    "type": "string",
                    "pattern": "^.*$"
                },
                "incoming_variations_sheet": {
                    "title": "Sheet for pending incoming variations",
                    "type": "string",
                    "pattern": "^.*$"
                },
                "producer_column": {
                    "title": "Producer column name",
                    "description": "Store the name of the producer for each"
                                   " variation",
                    "type": "string"
                },
                "delivery_time_column": {
                    "title": "Delivery time column name",
                    "description": "Store the time it takes for a order to be"
                                   " delivered",
                    "type": "string"
                },
                "creation_date_column": {
                    "title": "Creation date column name",
                    "description": "Store the date when the variation was"
                                   " created",
                    "type": "string"
                }
            }
        },
        "redistribution_sheets": {
            "title": "Redistribution_sheets",
            "type": "array",
            "items": {
                "title": "Items",
                "type": "object",
                "required": [
                    "name",
                    "origin_id",
                    "quantity_column",
                    "source_warehouse_id",
                    "target_warehouse_id"
                ],
                "properties": {
                    "name": {
                        "title": "Name of the sheet",
                        "type": "string",
                        "pattern": "^.*$"
                    },
                    "origin_id": {
                        "title": "Custom origin_id from the sales_statistics "
                                 "table",
                        "type": "number",
                        "example": -1
                    },
                    "identifier_column": {
                        "title": "Name of the Identifier column",
                        "description": "The name of the column where the "
                                       "identifier is placed",
                        "default": "ean",
                        "type": "string",
                        "pattern": "^.*$",
                        "example": "ean"
                    },
                    "identifier_type": {
                        "title": "Type of the identifier",
                        "type": "string",
                        "default": "ean",
                        "enum": ["variation_id", "ean", "sku", "asin"]
                    },
                    "quantity_column": {
                        "title": "Name of the quantity column",
                        "description": "Column within the sheet where the "
                                       "amount for the redistribution is "
                                       "specified",
                        "type": "string",
                        "pattern": "^.*$"
                    },
                    "source_warehouse_id": {
                        "title": "Plentymarkets ID of the source warehouse",
                        "type": "integer"
                    },
                    "target_warehouse_id": {
                        "title": "Plentymarkets ID of the target warehouse",
                        "type": "integer"
                    },
                }
            }

        },
        "prediction_sheets": {
            "title": "Prediction_sheets",
            "type": "array",
            "default": [],
            "items": {
                "title": "Items",
                "type": "object",
                "required": [
                    "name",
                    "origin_id",
                    "prediction_interval"
                ],
                "properties": {
                    "name": {
                        "title": "Name of the sheet",
                        "type": "string",
                        "pattern": "^.*$"
                    },
                    "origin_id": {
                        "title": "Custom origin_id from the sales_statistics "
                                 "table",
                        "type": "number",
                        "example": -1
                    },
                    "identifier_column": {
                        "title": "Name of the Identifier column",
                        "description": "The name of the column where the "
                                       "identifier is placed",
                        "default": "ean",
                        "type": "string",
                        "pattern": "^.*$",
                        "example": "ean"
                    },
                    "identifier_type": {
                        "title": "Type of the identifier",
                        "type": "string",
                        "default": "ean",
                        "enum": ["variation_id", "ean", "sku", "asin"]
                    },
                    "prediction_interval": {
                        "title": "Prediction_interval",
                        "type": "integer",
                        "default": 0
                    }
                }
            }

        },
        "stock_sheet": {
            "title": "Stock_sheet",
            "type": "object",
            "required": [
                "name"
            ],
            "properties": {
                "name": {
                    "title": "Name of the sheet",
                    "type": "string",
                    "pattern": "^.*$"
                },
                "identifier_column": {
                    "title": "Name of the Identifier column",
                    "description": "The name of the column where the "
                                   "identifier is placed",
                    "default": "ean",
                    "type": "string",
                    "pattern": "^.*$",
                    "example": "ean"
                },
                "identifier_type": {
                    "title": "Type of the identifier",
                    "type": "string",
                    "default": "ean",
                    "enum": ["variation_id", "ean", "sku", "asin"]
                },
            }
        },
        "sales_sheets": {
            "title": "Sales_sheets",
            "type": "array",
            "default": [],
            "items": {
                "title": "Items",
                "type": "object",
                "required": [
                    "name",
                    "days_range"
                ],
                "properties": {
                    "name": {
                        "title": "Name of the sheet",
                        "type": "string",
                        "pattern": "^.*$"
                    },
                    "days_range": {
                        "title": "Days_range",
                        "type": "integer"
                    }
                }
            }

        }
    }
}
