# Update spreadsheet tool

This tool is part of the Plentymarkets Reorder And Inventory Transfer tool, it is used to write the identification and prediciton data from the database into an excel or libreoffice sheet.

## Configuration

In the configuration file you declare the purpose of each sheet. The Plenty RAIT project provides an excel sheet with order suggestions for different origins, each suggestion sheet is connected to a prediction sheet that contains the predicted sales for the given origin. Let's say you want to have a sheet with suggestions for only origin *4.01*, in that case you need one entry within the `suggestion_sheets` section and one entry within the `prediction_sheets` section:
```json
'suggestion_sheets': [
    {
        'name': 'Sample name',
        'origin_id': 4.01,
        'identifier_column': 'ean',
        'identifier_type': 'ean',
        'quantity_column': 'quantity'
    }
],
'prediction_sheets': [
    {
        'name': 'Sample predictions',
        'origin_id': 4.01,
        'identifier_column': 'variation_id',
        'identifier_type': 'variation_id',
        'prediction_interval': 22
    }
]
```

### Explanation

* `name`: Must match the name of the sheet
* `origin_id`: Plentymarkets origin ID of orders to consider for this sheet (Amazon, Ebay, webshop, custom origins ...)
* `identifier_column`: This field enables other applications to automatically find the identifier column of the sheet
* `identifier_type`: Describes the kind of identifier used in @`identifier_column`, valid values are: [*asin*, *variation_id*, *ean*, *sku*]
* `quantity_column`: Enable other applications to automatically locate the column where the user provides the finished redistribution/order quantities
* `prediction_interval`: Describes how many months of predictions are used for the sheet


Full example:
```json
{
    'file': /path/to/orderlist.xlsx,
    'suggestion_sheets': [
        {
            'name': 'Sample name',
            'origin_id': 4.01,
            'identifier_column': 'ean',
            'identifier_type': 'ean',
            'quantity_column': 'quantity'
        }
    ],
    'prediction_sheets': [
        {
            'name': 'Sample predictions',
            'origin_id': 4.01,
            'identifier_column': 'variation_id',
            'identifier_type': 'variation_id',
            'prediction_interval': 22
        }
    ],
    "stock_sheet": {
        "name": "Stock",
        "identifier_column": "asin",
        "identifier_type": "asin"
    },
    "sales_sheets": [
        {
            "name": "VK30",
            "days_range": 30
        },
        {
            "name": "VK90",
            "days_range": 90
        },
        {
            "name": "VK365",
            "days_range": 365
        }
    ]
}
```

## Logging

The application logs by default to a standardized location on your platform. On Linux this will usually be: `~/.cache/update_spreadsheet/log`. The file will grow to a maximum of 2 MB, at which point it will start to rotate out old entries.

## Usage

When the tool is properly configured using it simply means:

```bash
# Update the identification data (ean, sku) and synchronize the variations
# on the sheet with the variations of the database
python3 -m plenty_rait UpdateSpreadsheet Update --suggestion
# Update the prediction data for the configured amount of months from the database
python3 -m plenty_rait UpdateSpreadsheet Update --predictions
# Clear a specifc column from a sheet of the configured Excel file
python3 -m plenty_rait UpdateSpreadsheet Clear --sheet sample_sheet --column sample_column
```
