"""
Sub application of the Plenty RAIT tool with the purpose of writing
identification, stock and prediciton data to a `xlsx/xlsm` file.

Copyright (C) 2022  Sebastian Fricke, Panasiam

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from plenty_rait.helper.config_helper import (
    ConfigHandler, WarehouseConfigHandler
)


class InsufficientPredictionSheetsError(Exception):
    def __init__(self, missing: list):
        self.missing = missing
        super().__init__()

    def __str__(self):
        return str('Missing prediction sheets for the origin IDs: '
                   f'{self.missing}')


class InvalidWarehouseMapping(Exception):
    def __init__(self, invalid: list):
        self.invalid = invalid
        super().__init__()

    def __str__(self):
        return str('Incorrect warehouse IDs given for the following sheets: '
                   f'{self.invalid}')



class UpdateSpreadsheetConfigHandler(ConfigHandler):
    def __init__(self, name: str, config_type: str, config_schema: dict):
        self.warehouse_handler: WarehouseConfigHandler
        super().__init__(name=name, config_type=config_type,
                         config_schema=config_schema)

    def _validate_configuration(self):
        required_origin_ids = [
            sheet['origin_id']
            for sheet in self.config['redistribution_sheets']
        ]
        required_origin_ids.append(
            self.config['reorder_sheet']['origin_id']
        )
        predicted_origin_ids = [
            sheet['origin_id']
            for sheet in self.config['prediction_sheets']
        ]
        missing_ids = [
            x for x in predicted_origin_ids if x not in required_origin_ids
        ]
        if missing_ids:
            raise InsufficientPredictionSheetsError(missing=missing_ids)

        available_warehouse_ids = self.warehouse_handler.get(
            key='plenty_warehouse_id'
        )
        invalid_warehouse_ids = [
            f"Sheet: {sheet['name']} source: {sheet['source_warehouse_id']}, "
            f"target: {sheet['target_warehouse_id']}"
            for sheet in self.config['redistribution_sheets']
            if sheet['source_warehouse_id'] not in available_warehouse_ids
            or sheet['target_warehouse_id'] not in available_warehouse_ids
        ]
        if invalid_warehouse_ids:
            raise InvalidWarehouseMapping(invalid=invalid_warehouse_ids)
